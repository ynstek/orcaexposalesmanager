//
//  Item.m
//  SalesManager
//
//  Created by Cihan TASKIN on 29/05/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "Item.h"


@implementation Item

@dynamic brandName;
@dynamic countryName;
@dynamic discounted;
@dynamic itemCategoryCode;
@dynamic itemNo;
@dynamic name;
@dynamic packSize;
@dynamic photoData;
@dynamic productGroupCode;
@dynamic quantity;
@dynamic salesUnit;
@dynamic searchDescription;
@dynamic stockQty;
@dynamic unitPrice;
@dynamic unitSize;
@dynamic vat;
@dynamic kgInBarcode;

@end
