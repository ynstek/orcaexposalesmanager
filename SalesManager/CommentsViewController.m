//
//  CommentsViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 06/01/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "CommentsViewController.h"
#import "AppDelegate.h"

@interface CommentsViewController ()

@end

@implementation CommentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    self.commentTextView.text = gf.selectedOrder.comment;
}

-(void)viewDidDisappear:(BOOL)animated{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    SalesHeader *salesHeader = [gf getOrderByDocNo:gf.selectedOrder.documentNo];
    salesHeader.comment = self.commentTextView.text;
    gf.selectedOrder.comment = self.commentTextView.text;
    [gf saveCurrentChanges];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"dismissCommentsPopover" object:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)codButtonPressed:(id)sender {
    [self setConstantText:@"[COD]"];
}

- (IBAction)podButtonPressed:(id)sender {
    [self setConstantText:@"[POD]"];
}

- (IBAction)nopymButtonPressed:(id)sender {
    [self setConstantText:@"[NO/PYM]"];
}

- (IBAction)chqButtonPressed:(id)sender {
    [self setConstantText:@"[CHQ]"];
}

- (IBAction)clearButtonPressed:(id)sender {
    self.commentTextView.text = @"";
}

-(void)setConstantText : (NSString *)text {
    if ([self.commentTextView.text isEqualToString:@""]){
        self.commentTextView.text = text;
    }
    else{
        self.commentTextView.text = [NSString stringWithFormat:@"%@\n%@",self.commentTextView.text,text];
    }
}
@end
