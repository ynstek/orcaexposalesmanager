//
//  MenuOptionsViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 15/02/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "MenuOptionsViewController.h"
#import "AppDelegate.h"
#import "CategoryTableViewController.h"
#import "UIViewController+MaryPopin.h"
#import "DatePickerViewController.h"

@interface MenuOptionsViewController ()

@end

@implementation MenuOptionsViewController
#define debug 1

-(void)setSwitchControls : (BOOL)switch1 : (BOOL)switch2 : (BOOL)switch3 : (BOOL)switch4{
    [self.allItemsSwitch setOn:switch1];
    [self.inactiveItemsSwitch setOn:switch2];
    [self.lastSaleItemsSwitch setOn:switch3];
    [self.unSoldItemsSwitch setOn:switch4];
    
}

-(void)refreshButtonTitles{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    if (gf.showOnlyNewItems == YES){
        [self.onlyNewItemsButton setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    }
    else{
        [self.onlyNewItemsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    NSArray *arrayForCheck = [gf getFilteredCategories:@"Country" :@""];
    if ([arrayForCheck count] > 0){
        [self.countriesButton setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    }
    else{
        [self.countriesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    arrayForCheck = [gf getFilteredCategories:@"ItemCategory" :@""];
    if ([arrayForCheck count] > 0){
        [self.categoriesButton setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    }
    else{
        [self.categoriesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    
    arrayForCheck = [gf getFilteredCategories:@"ProductGroup" :@""];
    if ([arrayForCheck count] > 0){
        [self.subCategoriesButton setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    }
    else{
        [self.subCategoriesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[self.indicator1 setHidden:YES];
    [self.orderView setHidden:NO];
    [self.filterView setHidden:YES];
    
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.isSearchItemsNeeded = NO;
    
    [self refreshButtonTitles];
    switch(gf.itemArrayFilter) {
        case 1:
            [self setSwitchControls:YES :NO :NO :NO];
            break;
        case 2:
            [self setSwitchControls:NO :YES :NO :NO];
            break;
        case 3:
            [self setSwitchControls:NO :NO :YES :NO];
            break;
        case 4:
            [self setSwitchControls:NO :NO :NO :YES];
            break;
        default:
            [self setSwitchControls:YES :NO :NO :NO];
            break;
    }
    
    if (((self.inactiveItemsSwitch.isOn == NO) && (self.lastSaleItemsSwitch.isOn == NO) && (self.unSoldItemsSwitch.isOn == NO)) ||
        ((self.inactiveItemsSwitch.isOn == YES) && (self.lastSaleItemsSwitch.isOn == YES) && (self.unSoldItemsSwitch.isOn == YES))) {
        [self.allItemsSwitch setOn:YES];
        [self.inactiveItemsSwitch setOn:NO];
        [self.lastSaleItemsSwitch setOn:NO];
        [self.unSoldItemsSwitch setOn:NO];
    }
    
    
    self.orderNoLabel.text = gf.selectedOrder.documentNo;
    self.customerNoLabel.text = gf.selectedOrder.sellToNo;
    self.customerNameLabel.text = gf.selectedCustomer.name;
    self.customerPostCodeLabel.text = gf.selectedCustomer.postCode;
    self.customerBalanceLabel.text = [NSString stringWithFormat:@"£%@",gf.selectedCustomer.balance.stringValue];
    self.customerUnclearedLabel.text = [NSString stringWithFormat:@"£%@",gf.selectedCustomer.unclearedFunds.stringValue];
    self.orderTotalLabel.text = [gf getOrderTotalAmountInclVAT:self.orderNoLabel.text];
    
    NSDateFormatter * formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd/MM/yy"];
    NSString * dateString = [formatter stringFromDate: gf.selectedOrder.requestedDeliveryDate];
    self.requestedDeliveryDateTextField.text = dateString;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissDatePickerPopover) name:@"dismissDatePickerPopover" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissCategoriesPopover) name:@"dismissCategoriesPopover" object:nil];
}

- (void)dismissDatePickerPopover {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    NSDateFormatter * formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd/MM/yy"];
    NSString * dateString = [formatter stringFromDate: gf.selectedOrder.requestedDeliveryDate];
    
    self.requestedDeliveryDateTextField.text = dateString;
}

-(void)viewDidAppear:(BOOL)animated{
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"dismissMenuOptionsPopover" object:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dismissDatePickerPopover" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dismissCategoriesPopover" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)orderButtonPressed:(id)sender {
    [self.orderView setHidden:NO];
    [self.filterView setHidden:YES];
}

- (IBAction)filterButtonPressed:(id)sender {
    [self.orderView setHidden:YES];
    [self.filterView setHidden:NO];
}

- (IBAction)resetFilterButtonPressed:(id)sender {
    
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.isSearchItemsNeeded = YES;
    gf.itemArrayFilter = 1;
    [gf clearCategorySelections:@""];
    [self setSwitchControls:YES :NO :NO :NO];
    gf.showOnlyNewItems = NO;
    [gf populateSearchDescriptions:@"Item"];
    
    [self refreshButtonTitles];
}
- (IBAction)newItemsButtonPressed:(id)sender {
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.isSearchItemsNeeded = YES;
    if (gf.showOnlyNewItems == YES){
        gf.showOnlyNewItems = NO;
    }
    else{
        gf.showOnlyNewItems = YES;
    }
    
    [NSThread detachNewThreadSelector:@selector(threadSetHiddenIndicator:) toTarget:self withObject:self.indicator1];
    [NSThread detachNewThreadSelector:@selector(threadStartAnimating:) toTarget:self withObject:self.indicator1];

    [gf populateSearchDescriptions:@"Item"];
    [self refreshButtonTitles];
    [self.indicator1 stopAnimating];
    
}

- (IBAction)categoriesButtonPressed:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    [gf clearCategorySelections : @"ProductGroup"];
    gf.isSearchItemsNeeded = YES;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CategoryTableViewController *categoryFilter =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"CategoryTableViewController"];
    categoryFilter.categoryName = @"ItemCategory";
    categoryFilter.relatedValueCode = @"";
    self.popover = [[UIPopoverController alloc]initWithContentViewController:categoryFilter];
    self.popover.delegate = self;
    [self.popover presentPopoverFromRect:self.categoriesButton.bounds inView:self.categoriesButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (IBAction)subCategoriesButtonPressed:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    NSArray *ItemCatArray = [gf getFilteredCategories:@"ItemCategory" : @""];
    
    if ([ItemCatArray count] == 1){
        gf.isSearchItemsNeeded = YES;
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        CategoryTableViewController *categoryFilter =
            [mainStoryboard instantiateViewControllerWithIdentifier:@"CategoryTableViewController"];
        categoryFilter.categoryName = @"ProductGroup";
        Categories *itemCat = ItemCatArray[0];
        categoryFilter.relatedValueCode = itemCat.categoryValueCode;
        self.popover = [[UIPopoverController alloc]initWithContentViewController:categoryFilter];
        self.popover.delegate = self;
        [self.popover presentPopoverFromRect:self.subCategoriesButton.bounds inView:self.subCategoriesButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning!" message:@"You need to select only one Category before selecting Subcategories!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}

- (IBAction)countriesButtonPressed:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.isSearchItemsNeeded = YES;
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];
    CategoryTableViewController *categoryFilter =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"CategoryTableViewController"];
    categoryFilter.categoryName = @"Country";
    categoryFilter.relatedValueCode = @"";
    self.popover = [[UIPopoverController alloc]initWithContentViewController:categoryFilter];
    self.popover.delegate = self;
    [self.popover presentPopoverFromRect:self.countriesButton.bounds inView:self.countriesButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (IBAction)allItemsSwitchChanged:(id)sender {
    if (self.allItemsSwitch.isOn == NO){
        [self.allItemsSwitch setOn:YES];
    }
    else{
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.itemArrayFilter = 1;
        [self setSwitchControls:YES :NO :NO :NO];
        [gf populateSearchDescriptions:@"Item"];
    }
}
- (void) threadStartAnimating : (UIActivityIndicatorView*)currIndicator {
    if (currIndicator == self.indicator1){
        [self.indicator1 startAnimating];
    }
    else if (currIndicator == self.indicator2){
        [self.indicator2 startAnimating];
    }
    else if (currIndicator == self.indicator3){
        [self.indicator3 startAnimating];
    }
}
- (void) threadSetHiddenIndicator : (UIActivityIndicatorView*)currIndicator {
    if (currIndicator == self.indicator1){
        [self.indicator1 setHidden:NO];
    }
    else if (currIndicator == self.indicator2){
        [self.indicator2 setHidden:NO];
    }
    else if (currIndicator == self.indicator3){
        [self.indicator3 setHidden:NO];
    }
}
-(void)dismissCategoriesPopover{
    
    [NSThread detachNewThreadSelector:@selector(threadSetHiddenIndicator:) toTarget:self withObject:self.indicator1];
    [NSThread detachNewThreadSelector:@selector(threadStartAnimating:) toTarget:self withObject:self.indicator1];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    [gf populateSearchDescriptions:@"Item"];
    [self refreshButtonTitles];
    [self.indicator1 stopAnimating];
}
- (IBAction)inactiveItemsSwitchChanged:(id)sender {
    [NSThread detachNewThreadSelector:@selector(threadSetHiddenIndicator:) toTarget:self withObject:self.indicator1];
    [NSThread detachNewThreadSelector:@selector(threadStartAnimating:) toTarget:self withObject:self.indicator1];
    
    if (self.inactiveItemsSwitch.isOn == NO){
        [self.inactiveItemsSwitch setOn:YES];
    }
    else{
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.itemArrayFilter = 2;
        self.indicator1.hidden = NO;
        [self setSwitchControls:NO :YES :NO :NO];
        [gf populateSearchDescriptions:@"Item"];
    }
    [self.indicator1 stopAnimating];
}

- (IBAction)lastSaleItemsSwitchChanged:(id)sender {
    [NSThread detachNewThreadSelector:@selector(threadSetHiddenIndicator:) toTarget:self withObject:self.indicator2];
    [NSThread detachNewThreadSelector:@selector(threadStartAnimating:) toTarget:self withObject:self.indicator2];
    if (self.lastSaleItemsSwitch.isOn == NO){
        [self.lastSaleItemsSwitch setOn:YES];
    }
    else{
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.itemArrayFilter = 3;
        [self setSwitchControls:NO :NO :YES :NO];
        [gf populateSearchDescriptions:@"Item"];
    }
    [self.indicator2 stopAnimating];
}

- (IBAction)unsoldItemsSwitchChanged:(id)sender {
    [NSThread detachNewThreadSelector:@selector(threadSetHiddenIndicator:) toTarget:self withObject:self.indicator1];
    [NSThread detachNewThreadSelector:@selector(threadStartAnimating:) toTarget:self withObject:self.indicator1];
    if (self.unSoldItemsSwitch.isOn == NO){
        [self.unSoldItemsSwitch setOn:YES];
    }
    else{
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.itemArrayFilter = 4;
        [self setSwitchControls:NO :NO :NO :YES];
        [gf populateSearchDescriptions:@"Item"];
    }
    [self.indicator1 stopAnimating];
}

- (IBAction)datePickerImagePressed:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];
    DatePickerViewController *datePickerView =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"DatePickerViewController"];
    
    datePickerView.parentName = @"Catalog";
    datePickerView.titleText = @"Delivery Date";
    self.popover = [[UIPopoverController alloc]initWithContentViewController:datePickerView];
    self.popover.delegate = self;
    
    self.popover.popoverContentSize = CGSizeMake(150, 220);
    [self.popover presentPopoverFromRect:self.requestedDeliveryDateTextField.bounds inView:self.requestedDeliveryDateTextField permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

@end
