//
//  SalesLineFunctions.m
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "SalesLineFunctions.h"
#import "AppDelegate.h"

@interface SalesLineFunctions() <MSFilter>

@property (nonatomic, strong) MSTable *salesLTable;
@property (nonatomic) NSInteger busyCount;

@end

@implementation SalesLineFunctions
@synthesize salesLRecords;

- (void)emptyCoreDataTable:(NSString*)tableName : (NSManagedObject*) table{
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:tableName];
    
    NSArray *fetchedObjects =
    [cdh.context executeFetchRequest:request error:nil];
    
    for(table in fetchedObjects){
        [cdh.context deleteObject:table];
    }
}

- (void)loadDataRecursiveForQuery:(MSQuery *)query withCompletion:(CompletionBlock)completion
{
    query.includeTotalCount = YES;
    query.fetchLimit = 1000; //note: you can adjust this to whatever amount is optimum
    query.fetchOffset = self.salesLResults.count;
    
    [query readWithCompletion:^(NSArray *items,NSInteger totalCount,NSError *error){
        if(!error) {
            //add the items to our local copy
            [self.salesLResults addObjectsFromArray:items];
            
            if (totalCount > [self.salesLResults count]) {
                [self loadDataRecursiveForQuery:query withCompletion:completion];
            } else {
                completion();
            }
        }
        else{
            [self logErrorIfNotNil:error];
            completion();
        }
    }];
}


-(void)SendLines : (NSString*)docNo completion:(CompletionWithIndexBlock)completion{
    self.salesLRecords = [[NSMutableArray alloc] init];
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@ && lastInvoice == %@",docNo,[NSNumber numberWithBool:NO]];// do not need to filter marktodelete here as we filter them from header.
    
    [request setPredicate:filter];
    NSArray *fetchedObjects =
    [cdh.context executeFetchRequest:request error:nil];
    
    if ([fetchedObjects count] > 0){
        for (SalesLine *salesL in fetchedObjects)
        {
            NSDictionary *salesLDic;
            if (salesL.itemNo != nil){
                salesLDic = @{ @"documentno" : salesL.documentNo, @"itemname" : salesL.itemName,
                                         @"itemno" : salesL.itemNo,@"itemunit" : salesL.itemUnit,
                                         @"lineamount" : salesL.lineAmount,@"itemunitprice" : salesL.itemUnitPrice,
                                         @"lineno" : salesL.lineNo,@"orderdate" : salesL.orderDate,
                                         @"quantity" : salesL.quantity, @"lastinvoice": salesL.lastInvoice, @"salespersoncode":cdh.activeUser.salesPersonCode};
                
            }
            else{
                salesLDic = @{ @"documentno" : @"", @"itemname" : @"",
                               @"itemno" : @"",@"itemunit" : @"",
                               @"lineamount" : @"",@"itemunitprice" : @"",
                               @"lineno" : @"",@"orderdate" : @"",
                               @"quantity" : @"", @"lastinvoice": @"", @"salespersoncode":cdh.activeUser.salesPersonCode};
            }
            [self addRecord:salesLDic:salesLRecords completion:^(NSUInteger index){
                salesL.markToDelete = [NSNumber numberWithBool:YES];
                if ([fetchedObjects count] == index + 1){
                    completion(index);
                }
            }];
        }
    }
    else{
        completion(0);
    }
    
}

-(void)deleteLines : (NSString*) docNo completion:(CompletionBlock)completion {
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"documentNo == %@",docNo];
    
    [self.salesLTable readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         salesLRecords = [results mutableCopy];
         self.salesLResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:self.salesLTable predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.salesLResults){
                 [self.salesLTable delete:recDic completion:^(id itemId, NSError *error) {
                     if (error){
                         [self logErrorIfNotNil:error];
                     }
                 }];
             }
             if (!error){
                 completion();
             }
         }];
     }];
}

-(void)addRecord:(NSDictionary *)salesLDic : (NSArray*) items completion:(CompletionWithIndexBlock)completion
{
    [self.salesLTable insert:salesLDic completion:^(NSDictionary *result, NSError *error)
     {
         NSUInteger index = [items count];
         
         if (!error){
         [(NSMutableArray *)items insertObject:result atIndex:index];
         }
         else{
             [self logErrorIfNotNil:error];
         }
         // Let the caller know that we finished
         completion(index);
     }];
}

-(void)insertCoreDataTable : (MSTable*) table{
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    cdh.activeUser.isReadyToUse = [NSNumber numberWithBool:NO];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"salespersoncode == %@ && lastinvoice == %@",cdh.activeUser.salesPersonCode,[NSNumber numberWithBool:YES]];
    [table readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.salesLRecords = [results mutableCopy];
         self.salesLResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:table predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.salesLResults){
                     SalesLine *salesLine = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                     salesLine.documentNo = [recDic objectForKey:@"documentno"];
                     salesLine.itemName = [recDic objectForKey:@"itemname"];
                     salesLine.itemNo = [recDic objectForKey:@"itemno"];
                     salesLine.itemUnit = [recDic objectForKey:@"itemunit"];
                     salesLine.lineAmount = [recDic objectForKey:@"lineamount"];
                     salesLine.itemUnitPrice = [recDic objectForKey:@"itemunitprice"];
                     salesLine.lineNo = [recDic objectForKey:@"lineno"];
                     salesLine.orderDate = [recDic objectForKey:@"orderdate"];
                     salesLine.quantity = [recDic objectForKey:@"quantity"];
                     salesLine.lastInvoice = [recDic objectForKey:@"lastinvoice"];
                     salesLine.salesPersonCode = [recDic objectForKey:@"salespersoncode"];
             }
             [cdh saveContext];
             NSLog(@"Last Invoice Lines saved!");
         }];
     }];
}
- (void)logErrorIfNotNil:(NSError *) error
{
    if (error)
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.errorMessage = error.localizedDescription;
        gf.progress = 1.0f;
        NSLog(@"ERROR %@", error);
    }
}

- (void)busy:(BOOL)busy
{
    // assumes always executes on UI thread
    if (busy)
    {
        if (self.busyCount == 0 && self.busyUpdate != nil)
        {
            self.busyUpdate(YES);
        }
        self.busyCount++;
    }
    else
    {
        if (self.busyCount == 1 && self.busyUpdate != nil)
        {
            self.busyUpdate(FALSE);
        }
        self.busyCount--;
    }
}
#pragma mark * MSFilter methods

+ (SalesLineFunctions *)defaultService
{
    static SalesLineFunctions* service;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[SalesLineFunctions alloc] init];
    });
    
    return service;
}

-(SalesLineFunctions *)init
{
    self = [super init];
    if (self)
    {
        // Initialize the Mobile Service client with your URL and key
        MSClient *client = [MSClient clientWithApplicationURLString:@"https://expoipadapp.azure-mobile.net/"
                                                     applicationKey:@"MgayYYCxnOsHFtZJhTbtGPmFeeTdDU98"];
        
        // Add a Mobile Service filter to enable the busy indicator
        self.client = [client clientWithFilter:self];
        self.salesLTable = [_client tableWithName:@"SalesLine"];
        self.salesLRecords = [[NSMutableArray alloc] init];
        self.busyCount = 0;
    }
    return self;
}

- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response
{
    // A wrapped response block that decrements the busy counter
    MSFilterResponseBlock wrappedResponse = ^(NSHTTPURLResponse *innerResponse, NSData *data, NSError *error)
    {
        [self busy:NO];
        response(innerResponse, data, error);
    };
    
    // Increment the busy counter before sending the request
    [self busy:YES];
    next(request, wrappedResponse);
}
@end
