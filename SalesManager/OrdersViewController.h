//
//  OrdersViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 09/03/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesHeaderFunctions.h"

@interface OrdersViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property NSMutableArray *salesOrderArray;
@property (strong, nonatomic) SalesHeaderFunctions *salesHF;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)sendButtonPressed:(id)sender;
- (IBAction)selectAllButtonPressed:(id)sender;
@end
