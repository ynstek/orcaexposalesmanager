//
//  CustomerViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 14/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "CustomerViewController.h"
#import "Customer.h"
#import "AppDelegate.h"
#import "PaymentViewController.h"

@interface CustomerViewController()
@property NSMutableArray *balanceArray,*orderArray;
@end

@implementation CustomerViewController

-(void)viewWillAppear:(BOOL)animated{
    [self refreshView];
}
-(void)refreshView{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    Customer *customer = gf.selectedCustomer;
    self.noTextField.text = customer.customerNo;
    self.nameTextField.text = customer.name;
    self.addressTextField.text = customer.address;
    self.address2TextField.text = customer.address2;
    self.phoneTextField.text = customer.phone;
    self.postCodeTextField.text = customer.postCode;
    self.cityTextField.text = customer.city;
    self.balanceTextField.text = customer.balance.stringValue;
    self.balanceOnDeviceTextField.text = [gf getCustomerBalanceOnDevice:customer];
    self.unclearedFundsTextField.text = customer.unclearedFunds.stringValue;
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request2 = [NSFetchRequest fetchRequestWithEntityName:@"BalanceHistory"];
    NSPredicate *filter2 = [NSPredicate predicateWithFormat:@"customerNo == %@ and printed = %@",gf.selectedCustomer.customerNo,[NSNumber numberWithBool:NO]];
    request2.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"paymentOnDevice" ascending:NO],[NSSortDescriptor sortDescriptorWithKey:@"documentDate" ascending:NO],nil];
    [request2 setPredicate:filter2];
    self.balanceArray = [[cdh.context executeFetchRequest:request2 error:nil] mutableCopy];
    
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesHeader"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"sellToNo == %@ && markToDelete == %@ && lastInvoice == %@",gf.selectedCustomer.customerNo,[NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO]];
    [request setPredicate:filter];
    request.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"documentNo" ascending:YES],nil];
    self.orderArray = [[cdh.context executeFetchRequest:request error:nil] mutableCopy];
    [self.balanceHistoryTableView reloadData];
    [self.orderListTableView reloadData];
    gf.selectedOrder = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"ipadmini_customerview_bg.jpg"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissPaymentPopover) name:@"ViewControllerShouldReloadNotification" object:nil];
    
    self.balanceHistoryTableView.delegate = self;
    self.balanceHistoryTableView.dataSource = self;
    
    self.orderListTableView.delegate = self;
    self.orderListTableView.dataSource = self;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.balanceHistoryTableView){
        return [self.balanceArray count];
    }
    else{
        return [self.orderArray count];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *localDateString;
    if (tableView == self.balanceHistoryTableView){
        static NSString *cellIdentifier = @"balanceCell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        BalanceHistory *balanceHistory = [self.balanceArray objectAtIndex:indexPath.row];
        
        if ([balanceHistory.paymentOnDevice isEqualToNumber:[NSNumber numberWithBool:YES]]){
            cell.contentView.backgroundColor = [UIColor colorWithRed:130/255.0f green:140/255.0f blue:185/255.0f alpha:1.0f];
        }
        else if ([balanceHistory.documentType isEqualToString:@"Invoice"]){
            
            cell.contentView.backgroundColor = [UIColor colorWithRed:30/255.0f green:140/255.0f blue:50/255.0f alpha:1.0f];
        }
        else if ([balanceHistory.documentType isEqualToString:@"Payment"]){
            cell.contentView.backgroundColor = [UIColor colorWithRed:50/255.0f green:90/255.0f blue:230/255.0f alpha:1.0f];
        }
        else if ([balanceHistory.documentType isEqualToString:@"Credit_Memo"]){
            cell.contentView.backgroundColor = [UIColor colorWithRed:200/255.0f green:30/255.0f blue:30/255.0f alpha:1.0f];
        }
        localDateString = [dateFormatter stringFromDate:balanceHistory.documentDate];
        
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - Amount : £%@",balanceHistory.documentNo,balanceHistory.documentAmount];
        cell.textLabel.text = [NSString stringWithFormat:@"%@ | %@ %@",localDateString,balanceHistory.documentType,balanceHistory.paymentType];
    }
    else{
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        static NSString *cellIdentifier = @"orderCell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        cell.contentView.backgroundColor = [UIColor colorWithRed:26/255.0f green:104/255.0f blue:164/255.0f alpha:1.0f];
        SalesHeader *salesHeader = [self.orderArray objectAtIndex:indexPath.row];
        localDateString = [dateFormatter stringFromDate:salesHeader.orderDate];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@ | %@",salesHeader.documentNo,localDateString];
        
        cell.detailTextLabel.text = [NSString stringWithFormat:@"£%@",[gf getOrderTotalAmountInclVAT:salesHeader.documentNo]];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.balanceHistoryTableView){
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        CGRect rect=CGRectMake(cell.bounds.origin.x+600, cell.bounds.origin.y+10, 50, 30);
        
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.selectedPayment = [self.balanceArray objectAtIndex:indexPath.row];
        if ([gf.selectedPayment.paymentOnDevice isEqualToNumber:[NSNumber numberWithBool:YES]]){
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PaymentViewController *payment = [mainStoryboard instantiateViewControllerWithIdentifier:@"PaymentViewController"];
            self.popover = [[UIPopoverController alloc]initWithContentViewController:payment];
            self.popover.delegate = self;
            [self.popover presentPopoverFromRect:rect inView:cell permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    }
}
-(void)openPaymentView{
    
}
- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];

    if (tableView == self.orderListTableView){
        
        if (editingStyle == UITableViewCellEditingStyleDelete) {
                SalesHeader *salesHeader = [self.orderArray objectAtIndex:indexPath.row];
                [gf deleteOrder:salesHeader];
                [self.orderArray removeObjectAtIndex:indexPath.row];
                [self.orderListTableView reloadData];
        }
    }
    else{
        BalanceHistory *balanceHistory = [self.balanceArray objectAtIndex:indexPath.row];
        if ([balanceHistory.paymentOnDevice isEqualToNumber:[NSNumber numberWithBool:YES]]){
            if (editingStyle == UITableViewCellEditingStyleDelete) {
                [gf deleteBalanceHistory:balanceHistory];
                [self.balanceArray removeObjectAtIndex:indexPath.row];
                [self.balanceHistoryTableView reloadData];
            }
        }
    }
    
    self.balanceOnDeviceTextField.text = [gf getCustomerBalanceOnDevice:gf.selectedCustomer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"CatalogView"])
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.selectedOrder = [self.orderArray objectAtIndex:[self.orderListTableView indexPathForSelectedRow].row];
    }
    else {
        NSLog(@"Undefined Segue Attempted!");
    }
}
- (void)dismissPaymentPopover {
    [self refreshView];
}

@end
