//
//  SalesPriceFunctions.m
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "SalesPriceFunctions.h"
#import "AppDelegate.h"

@interface SalesPriceFunctions() <MSFilter>

@property (nonatomic, strong) MSTable *salesPTable;
@property (nonatomic) NSInteger busyCount;

@end

@implementation SalesPriceFunctions

- (void)emptyCoreDataTable:(NSString*)tableName : (NSManagedObject*) table{
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:tableName];
    
    NSArray *fetchedObjects =
    [cdh.context executeFetchRequest:request error:nil];
    
    for(table in fetchedObjects){
        [cdh.context deleteObject:table];
    }
}

- (void)loadDataRecursiveForQuery:(MSQuery *)query withCompletion:(CompletionBlock)completion
{
    query.includeTotalCount = YES;
    query.fetchLimit = 1000; //note: you can adjust this to whatever amount is optimum
    query.fetchOffset = self.salesPResults.count;
    
    [query readWithCompletion:^(NSArray *items,NSInteger totalCount,NSError *error){
        if(!error) {
            //add the items to our local copy
            [self.salesPResults addObjectsFromArray:items];
            
            if (totalCount > [self.salesPResults count]) {
                [self loadDataRecursiveForQuery:query withCompletion:completion];
            } else {
                completion();
            }
        }
        else{
            [self logErrorIfNotNil:error];
            completion();
        }
    }];
}

-(void)updateSalesPrices : (NSDictionary*) recDic{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesPrice"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"itemNo == %@ and itemUnit == %@ and salesCode == %@",[recDic objectForKey:@"itemno"],[recDic objectForKey:@"itemunit"],[recDic objectForKey:@"salescode"]];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    SalesPrice *salesPrice;
    if ([fetchedObjects count] == 0){
        salesPrice = [NSEntityDescription insertNewObjectForEntityForName:@"SalesPrice" inManagedObjectContext:cdh.context];
    }
    else{
        salesPrice = fetchedObjects[0];
    }
    
    salesPrice.itemNo = [recDic objectForKey:@"itemno"];
    salesPrice.itemUnit = [recDic objectForKey:@"itemunit"];
    salesPrice.minQty = [recDic objectForKey:@"minqty"];
    salesPrice.unitPrice = [recDic objectForKey:@"unitprice"];
    salesPrice.salesCode = [recDic objectForKey:@"salescode"];
}

-(void)updateCoreDataTable : (MSTable*) table : (NSString*)itemNo{
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"itemNo == %@",itemNo];
    [table readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         
         self.salesPRecords = [results mutableCopy];
         
         self.salesPResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:table predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.salesPResults){
                 if ([table.name isEqualToString:@"SalesPrice"]){
                     [self updateSalesPrices:recDic];
                 }
             }
             [cdh saveContext];
         }];
     }];
}

-(void)insertCoreDataTable : (MSTable*) table{
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    cdh.activeUser.isReadyToUse = [NSNumber numberWithBool:NO];
    //NSPredicate * predicate = [NSPredicate predicateWithFormat:@"name != %@",@""];
    [table readWithPredicate:nil completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.salesPRecords = [results mutableCopy];
         self.salesPResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:table predicate:nil];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.salesPResults){
                 if ([table.name isEqualToString:@"SalesPrice"]){
                     if ([recDic objectForKey:@"minqty"] != nil){
                         SalesPrice *salesPrice = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                         salesPrice.itemNo = [recDic objectForKey:@"itemno"];
                         salesPrice.itemUnit = [recDic objectForKey:@"itemunit"];
                         salesPrice.minQty = [recDic objectForKey:@"minqty"];
                         salesPrice.unitPrice = [recDic objectForKey:@"unitprice"];
                         salesPrice.salesCode = [recDic objectForKey:@"salescode"];
                     }
                 }
             }
             [cdh saveContext];
             NSLog(@"Sales Prices saved!");
         }];
     }];
}
- (void)logErrorIfNotNil:(NSError *) error
{
    if (error)
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.errorMessage = error.localizedDescription;
        gf.progress = 1.0f;
        NSLog(@"ERROR %@", error);
    }
}

- (void)busy:(BOOL)busy
{
    // assumes always executes on UI thread
    if (busy)
    {
        if (self.busyCount == 0 && self.busyUpdate != nil)
        {
            self.busyUpdate(YES);
        }
        self.busyCount++;
    }
    else
    {
        if (self.busyCount == 1 && self.busyUpdate != nil)
        {
            self.busyUpdate(FALSE);
        }
        self.busyCount--;
    }
}
#pragma mark * MSFilter methods

+ (SalesPriceFunctions *)defaultService
{
    static SalesPriceFunctions* service;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[SalesPriceFunctions alloc] init];
    });
    
    return service;
}

-(SalesPriceFunctions *)init
{
    self = [super init];
    if (self)
    {
        // Initialize the Mobile Service client with your URL and key
        MSClient *client = [MSClient clientWithApplicationURLString:@"https://expoipadapp.azure-mobile.net/"
                                                     applicationKey:@"MgayYYCxnOsHFtZJhTbtGPmFeeTdDU98"];
        
        // Add a Mobile Service filter to enable the busy indicator
        self.client = [client clientWithFilter:self];
        self.salesPTable = [_client tableWithName:@"SalesPrice"];
        self.salesPRecords = [[NSMutableArray alloc] init];
        self.busyCount = 0;
    }
    return self;
}

- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response
{
    // A wrapped response block that decrements the busy counter
    MSFilterResponseBlock wrappedResponse = ^(NSHTTPURLResponse *innerResponse, NSData *data, NSError *error)
    {
        [self busy:NO];
        response(innerResponse, data, error);
    };
    
    // Increment the busy counter before sending the request
    [self busy:YES];
    next(request, wrappedResponse);
}
@end
