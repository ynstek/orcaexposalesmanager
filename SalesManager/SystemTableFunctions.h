//
//  SystemTableFunctions.h
//  SalesManager
//
//  Created by Cihan TASKIN on 11/04/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WindowsAzureMobileServices/WindowsAzureMobileServices.h>
#import "User.h"

#pragma mark * Block Definitions

typedef void (^CompletionBlock) ();
typedef void (^CompletionWithIndexBlock) (NSUInteger index);
typedef void (^BusyUpdateBlock) (BOOL busy);

@interface SystemTableFunctions : NSObject

@property (nonatomic, strong)   NSArray *systemRecords;
@property (nonatomic, copy)     BusyUpdateBlock busyUpdate;
@property (nonatomic, strong)   MSClient *client;
@property (nonatomic, strong) NSMutableArray *systemResults;

-(void)checkIfAzureBusy:(NSString*)fake withCompletion:(CompletionBlock)completion;

+ (SystemTableFunctions *)defaultService;

- (void)refreshDataOnSuccess:(CompletionBlock)completion;

- (void)completeSystem:(NSDictionary *)system
          completion:(CompletionWithIndexBlock)completion;

- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response;

@end
