//
//  Barcode.h
//  
//
//  Created by Cihan TASKIN on 04/06/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Barcode : NSManagedObject

@property (nonatomic, retain) NSString * itemNo;
@property (nonatomic, retain) NSString * unit;
@property (nonatomic, retain) NSString * barcode;

@end
