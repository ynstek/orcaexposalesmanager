//
//  ChangePasswordViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 29/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "AppDelegate.h"
@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)updateButtonPressed:(id)sender {
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    if ([self.password1.text isEqualToString:self.password2.text] && ![self.password1.text isEqualToString:@"firsttime"]){
        UserFunctions *userF = [(AppDelegate *)[[UIApplication sharedApplication] delegate] userF];
        [userF updateUserOnCloud : self.password1.text];
        if ([cdh.activeUser.role isEqualToString:@"Warehouse"])
            [self performSegueWithIdentifier:@"ChangePasswordForWarehouse" sender:self];
        else
            [self performSegueWithIdentifier:@"ChangePassword" sender:self];
    }
}
@end
