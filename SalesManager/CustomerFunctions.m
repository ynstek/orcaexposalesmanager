//
//  CustomerFunctions.m
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "CustomerFunctions.h"
#import "AppDelegate.h"

@interface CustomerFunctions() <MSFilter>

@property (nonatomic, strong) MSTable *customerTable,*balanceTable;
@property (nonatomic) NSInteger busyCount;

@end

@implementation CustomerFunctions
@synthesize customerRecords;

- (void)emptyCoreDataTable:(NSString*)tableName : (NSManagedObject*) table{
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:tableName];
    
    NSArray *fetchedObjects =
    [cdh.context executeFetchRequest:request error:nil];
    
    for(table in fetchedObjects){
        [cdh.context deleteObject:table];
    }
}

- (void)loadDataRecursiveForQuery:(MSQuery *)query withCompletion:(CompletionBlock)completion
{
    query.includeTotalCount = YES;
    query.fetchLimit = 1000; //note: you can adjust this to whatever amount is optimum
    query.fetchOffset = self.customerResults.count;
    
    [query readWithCompletion:^(NSArray *items,NSInteger totalCount,NSError *error){
        if(!error) {
            //add the items to our local copy
            [self.customerResults addObjectsFromArray:items];
            
            NSLog([NSString stringWithFormat:@"%lu", (unsigned long)[self.customerResults count]]);
            if (totalCount > [self.customerResults count]) {
                [self loadDataRecursiveForQuery:query withCompletion:completion];
            } else {
                completion();
            }
        }
        else{
            [self logErrorIfNotNil:error];
            completion();
        }
    }];
}
-(void)updateCustomer : (NSDictionary*) recDic{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Customer"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"customerNo == %@",[recDic objectForKey:@"customerno"]];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    Customer *customer;
    if ([fetchedObjects count] == 0){
        customer = [NSEntityDescription insertNewObjectForEntityForName:@"Customer" inManagedObjectContext:cdh.context];
    }
    else{
        customer = fetchedObjects[0];
    }
    
    customer.address = [recDic objectForKey:@"address"];
    customer.address2 = [recDic objectForKey:@"address2"];
    customer.balance = [recDic objectForKey:@"balance"];
    customer.block = [recDic objectForKey:@"block"];
    customer.city = [recDic objectForKey:@"city"];
    customer.customerNo = [recDic objectForKey:@"customerno"];
    customer.name = [recDic objectForKey:@"name"];
    customer.phone = [recDic objectForKey:@"phone"];
    customer.postCode = [recDic objectForKey:@"postcode"];
    customer.salesPersonCode = [recDic objectForKey:@"salespersoncode"];
    customer.priceGroup = [recDic objectForKey:@"pricegroup"];
}

-(void)updateCoreDataTable : (NSString*)customerNo  withCompletion:(CompletionBlock)completion {
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"customerNo == %@",customerNo];
    [self.customerTable readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.customerRecords = [results mutableCopy];
         self.customerResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:self.customerTable predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.customerResults){
                 [self updateCustomer:recDic];
                 BalanceHistoryFunctions *balanceF = [BalanceHistoryFunctions new];
                 [balanceF updateCoreDataTable:self.balanceTable:customerNo];
             }
         }];
     }];
    completion();
    
}

-(void)insertCoreDataTable : (MSTable*) table{
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    cdh.activeUser.isReadyToUse = [NSNumber numberWithBool:NO];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"salespersoncode == %@",cdh.activeUser.salesPersonCode];
    [table readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.customerRecords = [results mutableCopy];
         self.customerResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:table predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.customerResults){
                 if ([table.name isEqualToString:@"Customer"]){
                     Customer *customer = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                     customer.address = [recDic objectForKey:@"address"];
                     customer.address2 = [recDic objectForKey:@"address2"];
                     customer.balance = [recDic objectForKey:@"balance"];
                     customer.block = [recDic objectForKey:@"block"];
                     customer.city = [recDic objectForKey:@"city"];
                     customer.customerNo = [recDic objectForKey:@"customerno"];
                     customer.name = [recDic objectForKey:@"name"];
                     customer.phone = [recDic objectForKey:@"phone"];
                     customer.postCode = [recDic objectForKey:@"postcode"];
                     customer.salesPersonCode = [recDic objectForKey:@"salespersoncode"];
                     customer.priceGroup = [recDic objectForKey:@"pricegroup"];
                     customer.unclearedFunds = [recDic objectForKey:@"unclearedfunds"];
                 }
             }
             [cdh saveContext];
             NSLog(@"Customers saved!");
         }];
     }];
}

#pragma mark * MSFilter methods

- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response
{
    // A wrapped response block that decrements the busy counter
    MSFilterResponseBlock wrappedResponse = ^(NSHTTPURLResponse *innerResponse, NSData *data, NSError *error)
    {
        [self busy:NO];
        response(innerResponse, data, error);
    };
    
    // Increment the busy counter before sending the request
    [self busy:YES];
    next(request, wrappedResponse);
}

+ (CustomerFunctions *)defaultService
{
    static CustomerFunctions* service;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[CustomerFunctions alloc] init];
    });
    
    return service;
}

-(CustomerFunctions *)init
{
    self = [super init];
    
    if (self)
    {
        // Initialize the Mobile Service client with your URL and key
        MSClient *client = [MSClient clientWithApplicationURLString:@"https://expoipadapp.azure-mobile.net/"
                                                     applicationKey:@"MgayYYCxnOsHFtZJhTbtGPmFeeTdDU98"];
        
        // Add a Mobile Service filter to enable the busy indicator
        self.client = [client clientWithFilter:self];
        self.customerTable = [_client tableWithName:@"Customer"];
        self.balanceTable = [_client tableWithName:@"BalanceHistory"];
        self.customerRecords = [[NSMutableArray alloc] init];
        self.busyCount = 0;
    }
    return self;
}

- (void)busy:(BOOL)busy
{
    // assumes always executes on UI thread
    if (busy)
    {
        if (self.busyCount == 0 && self.busyUpdate != nil)
        {
            self.busyUpdate(YES);
        }
        self.busyCount++;
    }
    else
    {
        if (self.busyCount == 1 && self.busyUpdate != nil)
        {
            self.busyUpdate(FALSE);
        }
        self.busyCount--;
    }
}

- (void)logErrorIfNotNil:(NSError *) error
{
    if (error)
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.errorMessage = error.localizedDescription;
        gf.progress = 1.0f;
        NSLog(@"ERROR %@", error);
    }
}
@end
