//
//  CustomerFunctions.h
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WindowsAzureMobileServices/WindowsAzureMobileServices.h>
#import "Customer.h"

#pragma mark * Block Definitions

typedef void (^CompletionBlock) ();
typedef void (^CompletionWithIndexBlock) (NSUInteger index);
typedef void (^BusyUpdateBlock) (BOOL busy);

@interface CustomerFunctions : NSObject

@property (nonatomic, strong)   NSArray *customerRecords;
@property (nonatomic, copy)     BusyUpdateBlock busyUpdate;
@property (nonatomic, strong)   MSClient *client;
@property (nonatomic, strong) NSMutableArray *customerResults;


-(void)insertCoreDataTable : (MSTable*) table;
-(void)updateCoreDataTable : (NSString*)customerNo  withCompletion:(CompletionBlock)completion;

+ (CustomerFunctions *)defaultService;

- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response;

@end
