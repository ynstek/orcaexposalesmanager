//
//  SyncFunctions.h
//  SalesManager
//
//  Created by Cihan TASKIN on 02/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WindowsAzureMobileServices/WindowsAzureMobileServices.h>
#import "AppDelegate.h"

#pragma mark * Block Definitions

typedef void (^CompletionBlock) ();
typedef void (^CompletionWithIndexBlock) (NSUInteger index);
typedef void (^BusyUpdateBlock) (BOOL busy);

@interface SyncFunctions : NSObject

@property (nonatomic, strong)   NSArray *balanceHistoryRecords,*categoriesRecords, *customerRecords, *devicesRecords, *itemRecords, *itemUnitsRecords, *lastSaleRecords, *noSeriesRecords,
                                        *salesHeaderRecords, *salesLineRecords, *salesPriceRecords, *userRecords, *changeLogRecords;
@property (nonatomic, copy)     BusyUpdateBlock busyUpdate;
@property (nonatomic) NSInteger busyCount;
@property (nonatomic, strong)   MSClient *client;
@property (nonatomic, strong) NSMutableArray *results;
+ (SyncFunctions *)defaultService;


- (void)addRecord:(NSDictionary *)item :(MSTable*)table :(NSArray*) items completion:(CompletionWithIndexBlock)completion;
-(void)checkAzure;
- (void)backUpMasterTables;
- (void)updateCoreDataTables;
- (void)doNewInstallation;
- (void)getChangeOnly;

- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response;

@end
