//
//  CoreDataHelper.h
//  SalesManager
//
//  Created by Cihan TASKIN on 12/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "User.h"

@interface CoreDataHelper : NSObject

@property (nonatomic, readonly) NSManagedObjectContext       *context;
@property (strong, nonatomic) User *activeUser;
@property (nonatomic, readonly) NSManagedObjectContext       *importContext;
@property (nonatomic, readonly) NSManagedObjectModel         *model;
@property (nonatomic, readonly) NSPersistentStoreCoordinator *coordinator;
@property (nonatomic, readonly) NSPersistentStore            *store;
- (void)setupCoreData;
- (void)saveContext;

@end
