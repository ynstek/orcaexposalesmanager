//
//  Categories.m
//  
//
//  Created by Cihan TASKIN on 27/05/2015.
//
//

#import "Categories.h"


@implementation Categories

@dynamic categoryName;
@dynamic categoryValue;
@dynamic isFiltered;
@dynamic relatedValueCode;
@dynamic categoryValueCode;

@end
