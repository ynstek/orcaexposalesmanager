//
//  SalesHeader.h
//  SalesManager
//
//  Created by Cihan TASKIN on 23/01/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SalesHeader : NSManagedObject

@property (nonatomic, retain) NSString * comment;
@property (nonatomic, retain) NSString * documentNo;
@property (nonatomic, retain) NSNumber * markToDelete; // This is used for sent orders.
@property (nonatomic, retain) NSDate * orderDate;
@property (nonatomic, retain) NSDate * requestedDeliveryDate;
@property (nonatomic, retain) NSString * salesPersonCode;
@property (nonatomic, retain) NSString * sellToNo;
@property (nonatomic, retain) NSNumber * markToSend; //this is used for parking orders in NAV for Warehouse module
@property (nonatomic, retain) NSNumber * lastInvoice;

@end
