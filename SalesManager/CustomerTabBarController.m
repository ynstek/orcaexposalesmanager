//
//  CustomerTabBarController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 14/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "CustomerTabBarController.h"
#import "AppDelegate.h"
#import "SalesHeader.h"
#import "NoSeries.h"
#import "PaymentViewController.h"
#import "CustomerViewController.h"
#import "MiniPrinterFunctions.h"
#import "GridViewViewController.h"

@implementation CustomerTabBarController
#define debug 1

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissPaymentPopover) name:@"dismissPaymentPopover" object:nil];
    
    
    //to create a space between bar item buttons
    //START
    self.fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    self.fixedItem.width = 40.0f;
    //END
    
    self.doneButton = [[UIBarButtonItem alloc] initWithTitle:@"New Order" style:UIBarButtonItemStyleDone target:self action:@selector(createNewOrder)];
    self.doneButton2 = [[UIBarButtonItem alloc] initWithTitle:@"New Payment" style:UIBarButtonItemStyleDone target:self action:@selector(openPaymentView)];
    self.printLastInvoiceButton = [[UIBarButtonItem alloc] initWithTitle:@"Print" style:UIBarButtonItemStyleDone target:self action:@selector(printLastInvoice)];
    self.miniStatButton = [[UIBarButtonItem alloc] initWithTitle:@"Mini Statement" style:UIBarButtonItemStyleDone target:self action:@selector(printMiniStatement)];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:self.doneButton,self.fixedItem, self.doneButton2,self.fixedItem,self.miniStatButton, nil];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{

    if ([item.title isEqualToString:@"Customer Details"]){//to create a space between bar item buttons
        //self.doneButton = [[UIBarButtonItem alloc] initWithTitle:@"New Order" style:UIBarButtonItemStyleDone target:self action:@selector(createNewOrder)];
        //self.doneButton2 = [[UIBarButtonItem alloc] initWithTitle:@"New Payment" style:UIBarButtonItemStyleDone target:self action:@selector(openPaymentView)];
        self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:self.doneButton,self.fixedItem, self.doneButton2,self.fixedItem,self.miniStatButton, nil];
    }
    else{
        self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects: self.printLastInvoiceButton,nil];
    }
    
}

- (void)dismissPaymentPopover {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ViewControllerShouldReloadNotification" object:self];
}

-(void)printMiniStatement{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirmation"
                                                    message:@"Do you want to print Mini Statement?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
}

-(void)printLastInvoice{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirmation"
                                                    message:@"Do you want to print Last Invoice?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex) {
        case 0: //"No" pressed
            break;
        case 1: //"Yes" pressed
            if (blocking) {
                return;
            }
            blocking = YES;
            NSString *portName = [AppDelegate getPortName];
            NSString *portSettings = [AppDelegate getPortSettings];
            if ([self.tabBar.selectedItem.title isEqualToString:@"Customer Details"]){
                [MiniPrinterFunctions printMiniStatementWithPortName:portName portSettings:portSettings widthInch:0];
            }
            else{
                [MiniPrinterFunctions PrintSampleReceiptWithPortname:portName portSettings:portSettings widthInch:0:YES];
            }
            blocking = NO;
            break;
    }
}

-(void)openPaymentView{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.selectedPayment = nil;
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];
    PaymentViewController *payment = [mainStoryboard instantiateViewControllerWithIdentifier:@"PaymentViewController"];
    
    self.popover = [[UIPopoverController alloc]initWithContentViewController:payment];
    self.popover.delegate = self;
    self.popover.popoverContentSize = CGSizeMake(300, 300);
    [self.popover presentPopoverFromBarButtonItem:self.doneButton2
                         permittedArrowDirections:UIPopoverArrowDirectionUp
                                         animated:YES];
}

-(void)createNewOrder{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    gf.isAutoAdded = NO;
    
    SalesHeader *newSalesHeader =
    [NSEntityDescription insertNewObjectForEntityForName:@"SalesHeader" inManagedObjectContext:cdh.context];
    
    NSError *error = nil;
    if (![cdh.context
          obtainPermanentIDsForObjects:[NSArray arrayWithObject:newSalesHeader]
          error:&error]) {
        NSLog(@"Couldn't obtain a permanent ID for object %@", error);
    }
    
    
    NoSeries *noSeries = gf.getNewOrderNo;
    
    newSalesHeader.documentNo = [NSMutableString stringWithFormat:@"%@-%@%@",noSeries.userNoSeriesCode,noSeries.seriesCode,noSeries.seriesNo];
    newSalesHeader.sellToNo = gf.selectedCustomer.customerNo;
    newSalesHeader.salesPersonCode = cdh.activeUser.salesPersonCode;
    newSalesHeader.orderDate = gf.getTodayDate;
    newSalesHeader.requestedDeliveryDate = newSalesHeader.orderDate;
    gf.selectedOrder = newSalesHeader;
    
    [cdh saveContext];
    
    /*
    CatalogViewController *catalog = [self.storyboard instantiateViewControllerWithIdentifier:@"CatalogViewController"];
    [self.navigationController pushViewController:catalog animated:YES];
    */
     GridViewViewController *catalog = [self.storyboard instantiateViewControllerWithIdentifier:@"GridViewViewController"];
    [self.navigationController pushViewController:catalog animated:YES];
    
}
@end
