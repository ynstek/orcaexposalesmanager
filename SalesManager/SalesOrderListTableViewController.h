//
//  SalesOrderListTableViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 29/05/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesOrderListTableViewController : UITableViewController

@property (strong, nonatomic) UIBarButtonItem *refreshButton,*backButton;

@end
