//
//  ItemUnitsFunctions.m
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "ItemUnitsFunctions.h"
#import "AppDelegate.h"

@interface ItemUnitsFunctions() <MSFilter>

@property (nonatomic, strong) MSTable *itemUnitsTable;
@property (nonatomic) NSInteger busyCount;

@end

@implementation ItemUnitsFunctions
@synthesize itemUnitsRecords;

- (void)emptyCoreDataTable:(NSString*)tableName : (NSManagedObject*) table{
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:tableName];
    
    NSArray *fetchedObjects =
    [cdh.context executeFetchRequest:request error:nil];
    
    for(table in fetchedObjects){
        [cdh.context deleteObject:table];
    }
}

- (void)loadDataRecursiveForQuery:(MSQuery *)query withCompletion:(CompletionBlock)completion
{
    query.includeTotalCount = YES;
    query.fetchLimit = 1000; //note: you can adjust this to whatever amount is optimum
    query.fetchOffset = self.itemUnitsResults.count;
    
    [query readWithCompletion:^(NSArray *items,NSInteger totalCount,NSError *error){
        if(!error) {
            //add the items to our local copy
            [self.itemUnitsResults addObjectsFromArray:items];
            
            if (totalCount > [self.itemUnitsResults count]) {
                [self loadDataRecursiveForQuery:query withCompletion:completion];
            } else {
                completion();
            }
        }
        else{
            [self logErrorIfNotNil:error];
            completion();
        }
    }];
}
-(void)updateItemUnits : (NSDictionary*) recDic{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"ItemUnits"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"itemNo == %@ and unit == %@",[recDic objectForKey:@"itemno"], [recDic objectForKey:@"unit"]];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    ItemUnits *itemUnit;
    if ([fetchedObjects count] == 0){
        if ([recDic objectForKey:@"packsize"] != nil){
            itemUnit = [NSEntityDescription insertNewObjectForEntityForName:@"ItemUnits" inManagedObjectContext:cdh.context];
        }
    }
    else{
        itemUnit = fetchedObjects[0];
    }
    
    itemUnit.barcode = [recDic objectForKey:@"barcode"];
    itemUnit.itemNo = [recDic objectForKey:@"itemno"];
    itemUnit.unit = [recDic objectForKey:@"unit"];
    itemUnit.qtyPerUnit = [recDic objectForKey:@"qtyperunit"];
}

-(void)updateCoreDataTable : (MSTable*) table : (NSString*)itemNo{
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"itemNo == %@",itemNo];
    [table readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         
         self.itemUnitsRecords = [results mutableCopy];
         
         self.itemUnitsResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:table predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.itemUnitsResults){
                 if ([table.name isEqualToString:@"ItemUnits"]){
                     if ([recDic objectForKey:@"barcode"] != nil){
                         [self updateItemUnits:recDic];
                     }
                 }
             }
             [cdh saveContext];
         }];
     }];
}

-(void)insertCoreDataTable : (MSTable*) table{
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    cdh.activeUser.isReadyToUse = [NSNumber numberWithBool:NO];
    //NSPredicate * predicate = [NSPredicate predicateWithFormat:@"itemNo == %@",itemNo];
    [table readWithPredicate:nil completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         
        self.itemUnitsRecords = [results mutableCopy];
         
         self.itemUnitsResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:table predicate:nil];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.itemUnitsResults){
                 if ([table.name isEqualToString:@"ItemUnits"]){
                     if ([recDic objectForKey:@"barcode"] != nil){
                         ItemUnits *itemUnit = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                         itemUnit.barcode = [recDic objectForKey:@"barcode"];
                         itemUnit.itemNo = [recDic objectForKey:@"itemno"];
                         itemUnit.unit = [recDic objectForKey:@"unit"];
                         itemUnit.qtyPerUnit = [recDic objectForKey:@"qtyperunit"];
                     }
                 }
             }
             [cdh saveContext];
             NSLog(@"Item Units saved!");
         }];
     }];
}

#pragma mark * MSFilter methods

- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response
{
    // A wrapped response block that decrements the busy counter
    MSFilterResponseBlock wrappedResponse = ^(NSHTTPURLResponse *innerResponse, NSData *data, NSError *error)
    {
        [self busy:NO];
        response(innerResponse, data, error);
    };
    
    // Increment the busy counter before sending the request
    [self busy:YES];
    next(request, wrappedResponse);
}

+ (ItemUnitsFunctions *)defaultService
{
    static ItemUnitsFunctions* service;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[ItemUnitsFunctions alloc] init];
    });
    
    return service;
}

-(ItemUnitsFunctions *)init
{
    self = [super init];
    
    if (self)
    {
        // Initialize the Mobile Service client with your URL and key
        MSClient *client = [MSClient clientWithApplicationURLString:@"https://expoipadapp.azure-mobile.net/"
                                                     applicationKey:@"MgayYYCxnOsHFtZJhTbtGPmFeeTdDU98"];
        
        // Add a Mobile Service filter to enable the busy indicator
        self.client = [client clientWithFilter:self];
        self.itemUnitsTable = [_client tableWithName:@"ItemUnits"];
        self.itemUnitsRecords = [[NSMutableArray alloc] init];
        self.busyCount = 0;
    }
    return self;
}

- (void)busy:(BOOL)busy
{
    // assumes always executes on UI thread
    if (busy)
    {
        if (self.busyCount == 0 && self.busyUpdate != nil)
        {
            self.busyUpdate(YES);
        }
        self.busyCount++;
    }
    else
    {
        if (self.busyCount == 1 && self.busyUpdate != nil)
        {
            self.busyUpdate(FALSE);
        }
        self.busyCount--;
    }
}

- (void)logErrorIfNotNil:(NSError *) error
{
    if (error)
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.errorMessage = error.localizedDescription;
        gf.progress = 1.0f;
        NSLog(@"ERROR %@", error);
    }
}

@end
