//
//  AppDelegate.m
//  SalesManager
//
//  Created by Cihan TASKIN on 12/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "AppDelegate.h"
#import "User.h"
#import "ItemUnits.h"
#import "GridViewViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
#define debug 1


-(void)loading {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    //GenericFunctions *gf = [self gf];
    //[gf populateSearchDescriptions : @"Item"];
    //[gf populateSearchDescriptions : @"Customer"];
}

- (GenericFunctions*)gf {
    
    if (!_genericFunctions) {
        static dispatch_once_t predicate;
        dispatch_once(&predicate, ^{
            _genericFunctions = [GenericFunctions new];
        });
    }
    return _genericFunctions;
}
- (CustomerFunctions*)customerF {
    
    if (!_customerFunctions) {
        static dispatch_once_t predicate;
        dispatch_once(&predicate, ^{
            _customerFunctions = [CustomerFunctions new];
        });
    }
    return _customerFunctions;
}

- (ItemFunctions*)itemF {
    
    if (!_itemFunctions) {
        static dispatch_once_t predicate;
        dispatch_once(&predicate, ^{
            _itemFunctions = [ItemFunctions new];
        });
    }
    return _itemFunctions;
}

- (BalanceHistoryFunctions*)balanceF {
    
    if (!_balanceHistoryFunctions) {
        static dispatch_once_t predicate;
        dispatch_once(&predicate, ^{
            _balanceHistoryFunctions = [BalanceHistoryFunctions new];
        });
    }
    return _balanceHistoryFunctions;
}
- (SalesPriceFunctions*)salesPF {
    
    if (!_salesPriceFunctions) {
        static dispatch_once_t predicate;
        dispatch_once(&predicate, ^{
            _salesPriceFunctions = [SalesPriceFunctions new];
        });
    }
    return _salesPriceFunctions;
}
- (UserFunctions*)userF {
    
    if (!_userFunctions) {
        static dispatch_once_t predicate;
        dispatch_once(&predicate, ^{
            _userFunctions = [UserFunctions new];
        });
    }
    return _userFunctions;
}
- (SystemTableFunctions*)sysTableF {
    
    if (!_systemTableFunctions) {
        static dispatch_once_t predicate;
        dispatch_once(&predicate, ^{
            _systemTableFunctions = [SystemTableFunctions new];
        });
    }
    return _systemTableFunctions;
}
- (NoSeriesFunctions*)noSerF {
    
    if (!_noSeriesFunctions) {
        static dispatch_once_t predicate;
        dispatch_once(&predicate, ^{
            _noSeriesFunctions = [NoSeriesFunctions new];
        });
    }
    return _noSeriesFunctions;
}
- (CatgoryFunctions*)catF {
    
    if (!_catgoryFunctions) {
        static dispatch_once_t predicate;
        dispatch_once(&predicate, ^{
            _catgoryFunctions = [CatgoryFunctions new];
        });
    }
    return _catgoryFunctions;
}
- (LastSalesFunctions*)lastSaleF {
    
    if (!_lastSalesFunctions) {
        static dispatch_once_t predicate;
        dispatch_once(&predicate, ^{
            _lastSalesFunctions = [LastSalesFunctions new];
        });
    }
    return _lastSalesFunctions;
}
- (SalesHeaderFunctions*)salesHF {
    
    if (!_salesHeaderFunctions) {
        static dispatch_once_t predicate;
        dispatch_once(&predicate, ^{
            _salesHeaderFunctions = [SalesHeaderFunctions new];
        });
    }
    return _salesHeaderFunctions;
}
- (SalesLineFunctions*)salesLF {
    
    if (!_salesLineFunctions) {
        static dispatch_once_t predicate;
        dispatch_once(&predicate, ^{
            _salesLineFunctions = [SalesLineFunctions new];
        });
    }
    return _salesLineFunctions;
}
- (ItemUnitsFunctions*)itemUnitsF {
    
    if (!_itemUnitsFunctions) {
        static dispatch_once_t predicate;
        dispatch_once(&predicate, ^{
            _itemUnitsFunctions = [ItemUnitsFunctions new];
        });
    }
    return _itemUnitsFunctions;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    if (debug==1) {
    NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [[Captuvo sharedCaptuvoDeviceDebug] addCaptuvoDelegate:self];
    [[Captuvo sharedCaptuvoDevice]enableCaptuvoDebug:YES];
    
    self.client = [MSClient clientWithApplicationURLString:@"https://expoipadapp.azure-mobile.net/"
                                            applicationKey:@"MgayYYCxnOsHFtZJhTbtGPmFeeTdDU98"];
    return YES;
}

-(void)captuvoConnected{
    
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:nil
                                                   message:@"----------Captuvo Connected"
                                                  delegate:self
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil];
    //[alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    
    [(GridViewViewController*)self.viewController1 viewDidLoad] ;
    
}
-(void)captuvoDisconnected{
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:nil
                                                   message:@"***********Captuvo Disconnected"
                                                  delegate:nil
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil];
    //[alert show];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [[Captuvo sharedCaptuvoDevice]stopDecoderHardware];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[self cdh] saveContext];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [self cdh];
    [self adminProcess];
    [self loading];
    
    [[Captuvo sharedCaptuvoDevice] addCaptuvoDelegate:self];
    [[Captuvo sharedCaptuvoDevice] startDecoderHardware];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    
    [[self cdh] saveContext];
}
#pragma mark - Core Data stack
- (CoreDataHelper*)cdh {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    if (!_coreDataHelper) {
        static dispatch_once_t predicate;
        dispatch_once(&predicate, ^{
            _coreDataHelper = [CoreDataHelper new];
        });
        [_coreDataHelper setupCoreData];
    }
    return _coreDataHelper;
}

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "Orca-Business-Solutions.Orca_Mobile_Sales" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


- (void)adminProcess {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"name == %@",@"orca"];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    User *user;
    if ([fetchedObjects count] == 0){
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:cdh.context];
        user.name = @"orca";
        user.password = @"orca";
        user.salesPersonCode = @"";
        user.userNoSeriesCode = @"";
        user.role = @"";
        [cdh saveContext];
    }
    
//    request = [NSFetchRequest fetchRequestWithEntityName:@"ItemUnits"];
//    filter = [NSPredicate predicateWithFormat:@"itemNo == %@ && unit == %@",@"PENPIC05",@"BOX"];
//    [request setPredicate:filter];
//    fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
//    if ([fetchedObjects count] > 0){
//        ItemUnits *test = fetchedObjects[0];
//        [cdh.context deleteObject:test];
//        [cdh saveContext];
//    }
    
}

-(void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler{
    
    self.backgroundTransferCompletionHandler = completionHandler;
    
}

//Printing

+ (NSString*)getPortName
{
    return @"BT:PRNT Star";//portName;
}

+ (NSString *)getPortSettings
{
    return @"mini";//portSettings;
}
@end
