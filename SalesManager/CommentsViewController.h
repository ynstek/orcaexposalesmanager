//
//  CommentsViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 06/01/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentsViewController : UIViewController
- (IBAction)codButtonPressed:(id)sender;
- (IBAction)podButtonPressed:(id)sender;
- (IBAction)nopymButtonPressed:(id)sender;
- (IBAction)chqButtonPressed:(id)sender;
- (IBAction)clearButtonPressed:(id)sender;

@property (strong, nonatomic) IBOutlet UITextView *commentTextView;
@end
