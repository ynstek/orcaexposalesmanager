//
//  LastSales.h
//  SalesManager
//
//  Created by Cihan TASKIN on 08/04/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface LastSales : NSManagedObject

@property (nonatomic, retain) NSString * customerNo;
@property (nonatomic, retain) NSString * itemNo;
@property (nonatomic, retain) NSDate * postingDate;
@property (nonatomic, retain) NSDecimalNumber * salesPrice;
@property (nonatomic, retain) NSNumber * salesQty;
@property (nonatomic, retain) NSString * salesUnit;
@property (nonatomic, retain) NSString * salesPersonCode;

@end
