//
//  LoginViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 14/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *activeField;
@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
- (IBAction)loginButtonPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *versionLabel;

@end
