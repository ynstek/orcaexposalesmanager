//
//  JSONHelper.h
//  Orca Mobile Sales
//
//  Created by Cihan TASKIN on 19/09/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NAVResult.h"

@interface JSONHelper : NSObject<UIActionSheetDelegate>
+(NSDictionary *)syncJSONDataFromURL:(NSString *)urlString;
+(NAVResult *)loadJSONDataFromURL:(NSString *)urlString;
+(NAVResult*)postJSONDataToURL:(NSString *)urlString JSONdata:(NSString*)JSONdata;
+(void)showError:(NSString *)messageTitle : (NSString*)messageContent;
@end
