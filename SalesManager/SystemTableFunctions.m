//
//  SystemTableFunctions.m
//  SalesManager
//
//  Created by Cihan TASKIN on 11/04/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "SystemTableFunctions.h"
#import "AppDelegate.h"

@interface SystemTableFunctions() <MSFilter>

@property (nonatomic, strong) MSTable *systemTable;
@property (nonatomic) NSInteger busyCount;

@end

@implementation SystemTableFunctions
@synthesize systemRecords;

- (void)loadDataRecursiveForQuery:(MSQuery *)query withCompletion:(CompletionBlock)completion
{
    query.includeTotalCount = YES;
    query.fetchLimit = 1000; //note: you can adjust this to whatever amount is optimum
    query.fetchOffset = self.systemResults.count;
    
    [query readWithCompletion:^(NSArray *items,NSInteger totalCount,NSError *error){
        if(!error) {
            //add the items to our local copy
            [self.systemResults addObjectsFromArray:items];
            
            if (totalCount > [self.systemResults count]) {
                [self loadDataRecursiveForQuery:query withCompletion:completion];
            } else {
                completion();
            }
        }
        else{
            [self logErrorIfNotNil:error];
            completion();
        }
    }];
}

-(void)checkIfAzureBusy:(NSString*)fake withCompletion:(CompletionBlock)completion{
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"busy == %@",[NSNumber numberWithBool:NO]];
    [self.systemTable readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.systemRecords = [results mutableCopy];
         self.systemResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:self.systemTable predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             if ([results count] > 0) {
                 gf.isServerBusy = NO;
                 completion();
             }
             else{
                 gf.isServerBusy = YES;
                 completion();
             }
         }];
     }];
}
#pragma mark * MSFilter methods

- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response
{
    // A wrapped response block that decrements the busy counter
    MSFilterResponseBlock wrappedResponse = ^(NSHTTPURLResponse *innerResponse, NSData *data, NSError *error)
    {
        [self busy:NO];
        response(innerResponse, data, error);
    };
    
    // Increment the busy counter before sending the request
    [self busy:YES];
    next(request, wrappedResponse);
}

+ (SystemTableFunctions *)defaultService
{
    static SystemTableFunctions* service;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[SystemTableFunctions alloc] init];
    });
    
    return service;
}

-(SystemTableFunctions *)init
{
    self = [super init];
    
    if (self)
    {
        // Initialize the Mobile Service client with your URL and key
        MSClient *client = [MSClient clientWithApplicationURLString:@"https://expoipadapp.azure-mobile.net/"
                                                     applicationKey:@"MgayYYCxnOsHFtZJhTbtGPmFeeTdDU98"];
        
        // Add a Mobile Service filter to enable the busy indicator
        self.client = [client clientWithFilter:self];
        self.systemTable = [_client tableWithName:@"SystemTable"];
        self.systemRecords = [[NSMutableArray alloc] init];
        self.busyCount = 0;
    }
    return self;
}

- (void) refreshDataOnSuccess:(CompletionBlock)completion{
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@""];
    
    [self.systemTable readWithPredicate:nil completion:^(NSArray *results, NSInteger totalCount, NSError *error) {
        [self logErrorIfNotNil:error];
        systemRecords = [results mutableCopy];
        
        completion();
    }];
}

-(void) completeSystem:(NSDictionary *)system completion:(CompletionWithIndexBlock)completion{
    NSMutableArray *mutableSystems = (NSMutableArray*) systemRecords;
    NSMutableDictionary *mutable = [system mutableCopy];
    [mutable setObject:@"YES" forKey:@"complete"];
    
    NSUInteger index = [systemRecords indexOfObjectIdenticalTo:system];
    [mutableSystems replaceObjectAtIndex:index withObject:mutable];
    
    [self.systemTable update:mutable completion:^(NSDictionary *system, NSError *error) {
        [self logErrorIfNotNil:error];
        NSUInteger index = [systemRecords indexOfObjectIdenticalTo:mutable];
        [mutableSystems removeObjectAtIndex:index];
        
        completion(index);
    }];
}

- (void)busy:(BOOL)busy
{
    // assumes always executes on UI thread
    if (busy)
    {
        if (self.busyCount == 0 && self.busyUpdate != nil)
        {
            self.busyUpdate(YES);
        }
        self.busyCount++;
    }
    else
    {
        if (self.busyCount == 1 && self.busyUpdate != nil)
        {
            self.busyUpdate(FALSE);
        }
        self.busyCount--;
    }
}

- (void)logErrorIfNotNil:(NSError *) error
{
    if (error)
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.errorMessage = error.localizedDescription;
        gf.progress = 1.0f;
        NSLog(@"ERROR %@", error);
    }
}


@end
