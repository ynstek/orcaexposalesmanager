//
//  SalesPriceFunctions.h
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WindowsAzureMobileServices/WindowsAzureMobileServices.h>
#import "SalesPrice.h"

#pragma mark * Block Definitions

typedef void (^CompletionBlock) ();
typedef void (^CompletionWithIndexBlock) (NSUInteger index);
typedef void (^BusyUpdateBlock) (BOOL busy);

@interface SalesPriceFunctions : NSObject

@property (nonatomic, strong)   NSArray *salesPRecords;
@property (nonatomic, copy)     BusyUpdateBlock busyUpdate;
@property (nonatomic, strong)   MSClient *client;
@property (nonatomic, strong) NSMutableArray *salesPResults;

-(void)insertCoreDataTable : (MSTable*) table;
-(void)updateCoreDataTable : (MSTable*) table : (NSString*)itemNo;

+ (SalesPriceFunctions *)defaultService;

- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response;

@end
