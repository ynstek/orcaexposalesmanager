//
//  KeyboardViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 03/03/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "KeyboardViewController.h"
#import "AppDelegate.h"

@interface KeyboardViewController ()

@end

@implementation KeyboardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    self.keyboardValueLabel.text = gf.keyboardValue;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidDisappear:(BOOL)animated{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.keyboardValue = self.keyboardValueLabel.text;
    [[NSNotificationCenter defaultCenter]postNotificationName:@"dismissCustomKeyboard" object:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)setKeyboardText : (NSString*)buttonValue{
    
    if ([buttonValue isEqualToString:@""]){
        self.keyboardValueLabel.text = @"0";
    }
    else if ([self.keyboardValueLabel.text isEqualToString:@"0"]){
        self.keyboardValueLabel.text = buttonValue;
    }
    else{
        if ([buttonValue isEqualToString:@"."]){
            if ([self.keyboardValueLabel.text rangeOfString:@"."].location == NSNotFound) {
                self.keyboardValueLabel.text = [self.keyboardValueLabel.text stringByAppendingString:buttonValue];
            }
        }
        else{
            self.keyboardValueLabel.text = [self.keyboardValueLabel.text stringByAppendingString:buttonValue];
        }
    }
}

- (IBAction)dotButtonPressed:(id)sender {
    [self setKeyboardText:@"."];
}

- (IBAction)clearButtonPressed:(id)sender {
    [self setKeyboardText:@""];
}

- (IBAction)numberZeroButtonPressed:(id)sender {
    [self setKeyboardText:@"0"];
}

- (IBAction)numberOneButtonPressed:(id)sender {
    [self setKeyboardText:@"1"];
}

- (IBAction)numberTwoButtonPressed:(id)sender {
    [self setKeyboardText:@"2"];
}

- (IBAction)numberThreeButtonPressed:(id)sender {
    [self setKeyboardText:@"3"];
}

- (IBAction)numberFourButtonPressed:(id)sender {
    [self setKeyboardText:@"4"];
}

- (IBAction)numberFiveButtonPressed:(id)sender {
    [self setKeyboardText:@"5"];
}

- (IBAction)numberSixButtonPressed:(id)sender {
    [self setKeyboardText:@"6"];
}

- (IBAction)numberSevenButtonPressed:(id)sender {
    [self setKeyboardText:@"7"];
}

- (IBAction)numberEightButtonPressed:(id)sender {
    [self setKeyboardText:@"8"];
}

- (IBAction)numberNineButtonPressed:(id)sender {
    [self setKeyboardText:@"9"];
}
@end
