//
//  User.h
//  SalesManager
//
//  Created by Cihan TASKIN on 28/05/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface User : NSManagedObject

@property (nonatomic, retain) NSNumber * currentUser;
@property (nonatomic, retain) NSNumber * isReadyToUse;
@property (nonatomic, retain) NSDate * lastUpdatedDate;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * salesPersonCode;
@property (nonatomic, retain) NSNumber * toBeSynced;
@property (nonatomic, retain) NSString * userNoSeriesCode;
@property (nonatomic, retain) NSString * role;

@end
