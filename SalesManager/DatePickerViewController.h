//
//  DatePickerViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 29/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DatePickerViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) NSString *parentName,*titleText;
@property bool isHistoryEnabled;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@end
