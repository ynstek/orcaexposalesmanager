//
//  PrintOptionsViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 06/02/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "PrintOptionsViewController.h"
#import "MiniPrinterFunctions.h"
#import "AppDelegate.h"

@interface PrintOptionsViewController ()

@end

@implementation PrintOptionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.showPriceSwitch setOn:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)printButtonPressed:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirmation"
                                                    message:@"Do you want to print?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex) {
        case 0: //"No" pressed
            break;
        case 1: //"Yes" pressed
            if (blocking) {
                    return;
                }
                blocking = YES;
                NSString *portName = [AppDelegate getPortName];
                NSString *portSettings = [AppDelegate getPortSettings];
            [MiniPrinterFunctions PrintSampleReceiptWithPortname:portName portSettings:portSettings widthInch:0:self.showPriceSwitch.isOn];
                blocking = NO;
            break;
    }
}
@end
