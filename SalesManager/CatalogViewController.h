//
//  CatalogViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 29/05/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatalogViewController : UIViewController<UITableViewDataSource,UITextFieldDelegate,UITableViewDelegate,UISearchBarDelegate,UIPopoverControllerDelegate,UIGestureRecognizerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *salesLineTableView;
@property (strong, nonatomic) IBOutlet UITableView *uomTableView;
@property (strong, nonatomic) NSString *prevQuantity,*prevPrice,*barcodeUnit;
@property (strong, nonatomic) UITableViewCell *selectedCell;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *linesButton,*sendOrderButton,*backButton, *parkOrderButton;
@property BOOL isFiltered;
@property (strong, nonatomic) UITextField *activeTextField;
@property (strong,nonatomic) UIPopoverController *popover;
@property (strong, nonatomic) IBOutlet UISearchBar *searchFilter1;
@property (strong, nonatomic) IBOutlet UISearchBar *searchFilter2;
@property (strong, nonatomic) IBOutlet UILabel *customerNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *postCodeLabel;
@property (strong, nonatomic) IBOutlet UILabel *balanceLabel;
@property (strong, nonatomic) IBOutlet UILabel *orderTotalAmountLabel;
@property (strong,nonatomic) NSMutableArray *filteredSearchResults;
@property (strong, nonatomic) IBOutlet UITextField *barcodeTextField;

- (IBAction)increaseButtonPressed:(id)sender;
- (IBAction)decreaseButtonPressed:(id)sender;
- (IBAction)unitSelectionChanged:(id)sender;
- (IBAction)qtyTextFieldEditingDidBegin:(id)sender;
- (IBAction)qtyTextFieldEditingDidEnd:(id)sender;
- (IBAction)unitPriceTextFieldEditingDidBegin:(id)sender;
- (IBAction)unitPriceTextFieldEditingDidEnd:(id)sender;


@end
