//
//  SalesLineViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 09/03/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "SalesLineViewController.h"
#import "AppDelegate.h"

@interface SalesLineViewController ()
@property NSMutableArray *salesLineArray,*searchResult;

@end

@implementation SalesLineViewController
#define debug 1

- (void)hideKeyboardWhenBackgroundIsTapped {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    UITapGestureRecognizer *tgr =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(hideKeyboard)];
    [tgr setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tgr];
}
- (void)hideKeyboard {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [self.view endEditing:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    self.orderNo = gf.selectedOrder.documentNo;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self refreshView];
    
    [self hideKeyboardWhenBackgroundIsTapped];
}

-(void)refreshView{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
    //NSLog(self.orderNo);
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@",self.orderNo];
    [request setPredicate:filter];
    
    request.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"lineNo" ascending:NO],nil];
    
    self.salesLineArray = [[cdh.context executeFetchRequest:request error:nil] mutableCopy];
    [self.tableView reloadData];
    
    self.navigationItem.title = [NSString stringWithFormat:@"£%@",[gf getOrderTotalAmountInclVAT:gf.selectedOrder.documentNo]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if (searchText.length == 0)
    {
        self.isFiltered = NO;
    }
    else{
        
        self.isFiltered = YES;
        
        self.searchResult = [[NSMutableArray alloc] init];
        
        CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
        NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@",self.orderNo];
        [request setPredicate:filter];
        self.salesLineArray = [[cdh.context executeFetchRequest:request error:nil] mutableCopy];
        
        for (SalesLine *salesLine in self.salesLineArray){
            NSRange itemNameRange = [salesLine.itemName rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (itemNameRange.location != NSNotFound){
                [self.searchResult addObject:salesLine];
            }
        }
    }
    [self.tableView reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.isFiltered == NO)
    {
        return [self.salesLineArray count];
    }
    else{
        return [self.searchResult count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                    forIndexPath:indexPath];
    
    UILabel *itemNameLabel = (UILabel*)[cell viewWithTag:101];
    UILabel *itemUnitSizeLabel = (UILabel*)[cell viewWithTag:102];
    UILabel *itemPackSizeLabel = (UILabel*)[cell viewWithTag:103];
    UILabel *itemVatPercentageLabel = (UILabel*)[cell viewWithTag:104];
    UILabel *freeTextLabel = (UILabel*)[cell viewWithTag:105];
    UILabel *itemQuantityLabel = (UILabel*)[cell viewWithTag:106];
    UILabel *itemUnitLabel = (UILabel*)[cell viewWithTag:107];
    UILabel *lineAmountLabel = (UILabel*)[cell viewWithTag:108];
    
    UILabel *lineNoLabel = (UILabel *)[cell viewWithTag:201];
    UILabel *itemNoLabel = (UILabel *)[cell viewWithTag:202];
    
    UITextField *itemUnitPriceTextField = (UITextField*)[cell viewWithTag:301];
    
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    SalesLine *salesLine;
    if (self.isFiltered == NO){
        salesLine = [self.salesLineArray objectAtIndex:indexPath.row];
    }
    else{
        salesLine = [self.searchResult objectAtIndex:indexPath.row];
    }
    
    //NSLog(salesLine.itemNo);
    Item *item = [gf getItem:salesLine.itemNo];
    
    lineNoLabel.text = salesLine.lineNo.stringValue;
    itemNoLabel.text = salesLine.itemNo;
    itemUnitLabel.text = salesLine.itemUnit;
    [self setUnitColor:itemUnitLabel];
    //NSLog(salesLine.itemName);
    itemNameLabel.text = salesLine.itemName;
    itemUnitSizeLabel.text = [NSString stringWithFormat:@"Unit Size:%@",item.unitSize];
    itemPackSizeLabel.text = [NSString stringWithFormat:@"Pack Size:%@",item.packSize];
    itemVatPercentageLabel.text = [NSString stringWithFormat:@"VAT%%%@",item.vat.stringValue];
    itemQuantityLabel.text = salesLine.quantity.stringValue;
    itemUnitPriceTextField.text = salesLine.itemUnitPrice.stringValue;
    lineAmountLabel.text = [NSString stringWithFormat:@"£%@",salesLine.lineAmount.stringValue];
    
    if ([salesLine.freeItem isEqualToNumber:[NSNumber numberWithBool:YES
                                             ]]){
        [freeTextLabel setHidden:NO];
        [itemUnitPriceTextField setEnabled:NO];
    }
    else{
        [freeTextLabel setHidden:YES];
        [itemUnitPriceTextField setEnabled:YES];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        SalesLine *deleteTarget;
        if (self.isFiltered == NO){
            deleteTarget = [self.salesLineArray objectAtIndex:indexPath.row];
            [self.salesLineArray removeObject:[self.salesLineArray objectAtIndex:indexPath.row]];
            [gf deleteSalesLine:deleteTarget];
        }
        
        [self.tableView reloadData];
        self.navigationItem.title = [NSString stringWithFormat:@"£%@",[gf getOrderTotalAmountInclVAT:gf.selectedOrder.documentNo]];
    }
}

- (IBAction)itemUnitPriceTextFieldEditingDidBegin:(id)sender {
    UICollectionViewCell* cell = (UICollectionViewCell *)[[sender superview] superview];
    UITextField *itemUnitPriceTextField = (UITextField *)[cell viewWithTag:301];
    self.prevPrice = itemUnitPriceTextField.text;
    itemUnitPriceTextField.text = @"";
}

- (IBAction)itemUnitPriceTextFieldEditingDidEnd:(id)sender {
    
    UICollectionViewCell* cell = (UICollectionViewCell *)[[sender superview] superview];
    UITextField *itemUnitPriceTextField = (UITextField *)[cell viewWithTag:301];
    
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    if ([gf validateNumTextField:itemUnitPriceTextField.text] == YES){
        if (![self.prevPrice isEqualToString:itemUnitPriceTextField.text]){
            [self updateSalesLine:cell :nil :itemUnitPriceTextField.text];
        }
    }
    else{
        itemUnitPriceTextField.text = self.prevPrice;
    }
    
}

- (IBAction)increaseButtonPressed:(id)sender {
    UICollectionViewCell* cell = (UICollectionViewCell *)[[sender superview] superview];
    UILabel *itemQuantityLabel = (UILabel*)[cell viewWithTag:106];
    float test = [itemQuantityLabel.text floatValue];
    test +=1;
    [self updateSalesLine:cell :[[NSNumber numberWithFloat:test] stringValue]:nil];
}

- (IBAction)decreaseButtonPressed:(id)sender {
    UICollectionViewCell* cell = (UICollectionViewCell *)[[sender superview] superview];
    UILabel *itemQuantityLabel = (UILabel*)[cell viewWithTag:106];
    float test = [itemQuantityLabel.text floatValue];
    test -=1;
    [self updateSalesLine:cell :[[NSNumber numberWithFloat:test] stringValue]:nil];
}

-(void)setUnitColor : (UILabel*) itemUnitLabel{
    if ([itemUnitLabel.text isEqualToString:@"BOX"]){
        itemUnitLabel.backgroundColor = [UIColor blueColor];
    }
    else if ([itemUnitLabel.text isEqualToString:@"EACH"]) {
        itemUnitLabel.backgroundColor = [UIColor redColor];
    }
    else{
        itemUnitLabel.backgroundColor = [UIColor blackColor];
    }
}

-(void)updateSalesLine:(UICollectionViewCell*)cell : (NSString*)newQty : (NSString*)newPrice{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    UILabel *lineNoLabel = (UILabel *)[cell viewWithTag:201];
    UILabel *itemNoLabel = (UILabel *)[cell viewWithTag:202];
    UILabel *itemNameLabel = (UILabel *)[cell viewWithTag:101];
    UILabel *freeTextLabel = (UILabel*)[cell viewWithTag:105];
    UILabel *itemQuantityLabel = (UILabel*)[cell viewWithTag:106];
    UILabel *itemUnitLabel = (UILabel*)[cell viewWithTag:107];
    UITextField * itemUnitPriceTextField = (UITextField*)[cell viewWithTag:301];
    
    UILabel *lineAmountLabel = (UILabel*)[cell viewWithTag:108];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    SalesLine *currentSalesLine;
    if (freeTextLabel.hidden == YES){
        currentSalesLine = [gf getSalesLine:self.orderNo :lineNoLabel.text : YES];
    }
    else{
        currentSalesLine = [gf getFreeSalesLine:lineNoLabel.text : NO];
    }
    
    itemUnitLabel.text = currentSalesLine.itemUnit;
    
    if (newPrice == nil){
        
        itemQuantityLabel.text = newQty;
        float test = [itemQuantityLabel.text floatValue];
        if (test <= 0){
            [gf deleteSalesLine:currentSalesLine];
            [self refreshView];
            //itemQuantityLabel.text = @"0";
            //lineNoLabel.text = @"0";
            //itemUnitPrice.TextField.text = [gf getBestPrice:itemNoLabel.text :selectedUnit :itemQuantityLabel.text];
        }
        else{
            if ([currentSalesLine.manualDiscount isEqualToNumber:[NSNumber numberWithBool:NO]]){
                NSString *bestPrice = [gf getBestPrice:itemNoLabel.text :itemUnitLabel.text : itemQuantityLabel.text];
                if (bestPrice != nil){
                    itemUnitPriceTextField.text = bestPrice;
                    currentSalesLine.autoDiscount = [NSNumber numberWithBool:YES];
                }
            }
            
            
            currentSalesLine.itemNo = itemNoLabel.text;
            currentSalesLine.itemName = itemNameLabel.text;
            currentSalesLine.itemUnit = itemUnitLabel.text;
            
            currentSalesLine.itemUnitPrice = [NSDecimalNumber decimalNumberWithString:itemUnitPriceTextField.text];
            currentSalesLine.quantity = [NSNumber numberWithFloat:itemQuantityLabel.text.floatValue];
            currentSalesLine.orderDate = gf.selectedOrder.orderDate;
            
            NSDecimal unitPriceTextInDecimal = [[NSNumber numberWithFloat:[itemUnitPriceTextField.text floatValue]] decimalValue];
            NSDecimalNumber * unitPriceDecimalInDecimalNumber = [NSDecimalNumber decimalNumberWithDecimal:unitPriceTextInDecimal];
            
            NSDecimal qtyTextInDecimal = [[NSNumber numberWithFloat:[itemQuantityLabel.text floatValue]] decimalValue];
            NSDecimalNumber * qtyDecimalInDecimalNumber = [NSDecimalNumber decimalNumberWithDecimal:qtyTextInDecimal];
            
            currentSalesLine.lineAmount = [unitPriceDecimalInDecimalNumber decimalNumberByMultiplyingBy:qtyDecimalInDecimalNumber];
            lineNoLabel.text = currentSalesLine.lineNo.stringValue;
        }
    }
    else{
        currentSalesLine.itemNo = itemNoLabel.text;
        currentSalesLine.itemName = itemNameLabel.text;
        currentSalesLine.itemUnit = itemUnitLabel.text;
        currentSalesLine.quantity = [NSNumber numberWithFloat:itemQuantityLabel.text.floatValue];
        currentSalesLine.itemUnitPrice = [NSDecimalNumber decimalNumberWithString:newPrice];
        currentSalesLine.orderDate = gf.selectedOrder.orderDate;
        currentSalesLine.manualDiscount = [NSNumber numberWithBool:YES];
        currentSalesLine.autoDiscount = [NSNumber numberWithBool:NO];
        
        NSDecimal qtyTextInDecimal = [[NSNumber numberWithFloat:[itemQuantityLabel.text floatValue]] decimalValue];
        NSDecimalNumber * qtyDecimalInDecimalNumber = [NSDecimalNumber decimalNumberWithDecimal:qtyTextInDecimal];
        
        currentSalesLine.lineAmount = [currentSalesLine.itemUnitPrice decimalNumberByMultiplyingBy:qtyDecimalInDecimalNumber];
        lineNoLabel.text = currentSalesLine.lineNo.stringValue;
    }
    
    if (currentSalesLine.itemNo != nil){
        NSDecimalNumber *listAmount = [NSDecimalNumber decimalNumberWithString:[gf getListPrice:currentSalesLine.itemNo :currentSalesLine.itemUnit]];
        listAmount = [listAmount decimalNumberByMultiplyingBy:currentSalesLine.quantity];
        if ([currentSalesLine.lineAmount compare:listAmount] == NSOrderedAscending){
            currentSalesLine.discountAmount = [listAmount decimalNumberBySubtracting:currentSalesLine.lineAmount];
            //[self showCrossedListPrice:currentSalesLine.discountAmount.stringValue];
        }
    }
    lineAmountLabel.text = [NSString stringWithFormat:@"£%@",currentSalesLine.lineAmount.stringValue];
    
    self.navigationItem.title = [NSString stringWithFormat:@"£%@",[gf getOrderTotalAmountInclVAT:gf.selectedOrder.documentNo]];
    [gf saveCurrentChanges];
}


@end
