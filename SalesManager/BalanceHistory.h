//
//  BalanceHistory.h
//  
//
//  Created by Cihan TASKIN on 27/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BalanceHistory : NSManagedObject

@property (nonatomic, retain) NSString * chequeNo;
@property (nonatomic, retain) NSString * customerNo;
@property (nonatomic, retain) NSDecimalNumber * documentAmount;
@property (nonatomic, retain) NSDate * documentDate;
@property (nonatomic, retain) NSString * documentNo;
@property (nonatomic, retain) NSString * documentType;
@property (nonatomic, retain) NSDate * dueDate;
@property (nonatomic, retain) NSNumber * entryNo;
@property (nonatomic, retain) NSNumber * paymentOnDevice;
@property (nonatomic, retain) NSString * paymentType;
@property (nonatomic, retain) NSNumber * printed;
@property (nonatomic, retain) NSDecimalNumber * remainingAmount;
@property (nonatomic, retain) NSString * salesPersonCode;

@end
