//
//  OrdersTableViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 10/01/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "CoreDataTableViewController.h"
#import "SalesHeaderFunctions.h"

@interface OrdersTableViewController : CoreDataTableViewController

@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneButton,*doneButton2;

@property (strong, nonatomic) SalesHeaderFunctions *salesHF;

@end
