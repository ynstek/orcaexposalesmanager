//
//  UserFunctions.m
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "UserFunctions.h"
#import "AppDelegate.h"

@interface UserFunctions() <MSFilter>

@property (nonatomic, strong) MSTable *userTable;
@property (nonatomic) NSInteger busyCount;

@end

@implementation UserFunctions
@synthesize userRecords;

- (void)emptyCoreDataTable:(NSString*)tableName : (NSManagedObject*) table{
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:tableName];
    
    NSArray *fetchedObjects =
    [cdh.context executeFetchRequest:request error:nil];
    
    for(table in fetchedObjects){
        [cdh.context deleteObject:table];
    }
}

- (void)loadDataRecursiveForQuery:(MSQuery *)query withCompletion:(CompletionBlock)completion
{
    query.includeTotalCount = YES;
    query.fetchLimit = 1000; //note: you can adjust this to whatever amount is optimum
    query.fetchOffset = self.userResults.count;
    
    [query readWithCompletion:^(NSArray *items,NSInteger totalCount,NSError *error){
        if(!error) {
            //add the items to our local copy
            [self.userResults addObjectsFromArray:items];
            
            if (totalCount > [self.userResults count]) {
                [self loadDataRecursiveForQuery:query withCompletion:completion];
            } else {
                completion();
            }
        }
        else{
            [self logErrorIfNotNil:error];
            completion();
        }
    }];
}

-(void)checkCurrentUserIfStillActiveOnAzure:(NSString*)fake withCompletion:(CompletionBlock)completion{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"name == %@",cdh.activeUser.name];
    [self.userTable readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.userRecords = [results mutableCopy];
         self.userResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:self.userTable predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             if ([results count] > 0) {
                 gf.isUserEnabled = YES;
                 completion();
             }
             else{
                 gf.isUserEnabled = NO;
                 completion();
             }
         }];
     }];
}

-(void)setLastUpdatedDate : (NSDate*)systemDate{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"name == %@",cdh.activeUser.name];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        User *user = fetchedObjects[0];
        user.lastUpdatedDate = systemDate;
    }
    
}

-(void)setActiveUser{
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    NSArray *fetchedObjects =
    [cdh.context executeFetchRequest:request error:nil];
    
    if ([fetchedObjects count] > 0){
        for (User *user in fetchedObjects){
            if ([user.name isEqualToString:cdh.activeUser.name]){
                user.currentUser = [NSNumber numberWithBool:YES];
            }
            else{
                user.currentUser = [NSNumber numberWithBool:NO];
            }
        }
    }
    [cdh saveContext];
}

- (void)updateTableRow:(NSDictionary *)item withTableName:(NSString *)tableName {
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"name == %@",cdh.activeUser.name];
    [self.userTable readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.userRecords = [results mutableCopy];
         self.userResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:self.userTable predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.userResults){
                 NSDictionary *params = @{ @"table" : tableName };
                 
                 NSDictionary *userRec = @{@"id":[recDic objectForKey:@"id"], @"name" : cdh.activeUser.name, @"password" : cdh.activeUser.password, @"salespersoncode" : cdh.activeUser.salesPersonCode, @"usernoseriescode" : cdh.activeUser.userNoSeriesCode};
                 
                 [self.userTable update:userRec parameters:params completion:^(NSDictionary *result, NSError *error) {
                     if (error != nil){
                         cdh.activeUser.toBeSynced = [NSNumber numberWithBool:YES];
                     }
                     [self logErrorIfNotNil:error];
                     
                 }];
             }
         }];
     }];
}

- (void)updateUserOnCloud : (NSString*)newPass{
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    if (newPass != nil){
        cdh.activeUser.password = newPass;
    }
    [self updateTableRow:nil withTableName:@"User"];
    [cdh saveContext];
}

- (void)emptyCoreDataTable {
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"name != %@",@"orca"];
    [request setPredicate:filter];
    
    NSArray *fetchedObjects =
    [cdh.context executeFetchRequest:request error:nil];
    
    for(User *table in fetchedObjects){
        [cdh.context deleteObject:table];
    }
}

-(void)updateUserCoreData : (NSDictionary*) recDic{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"name == %@",[recDic objectForKey:@"name"]];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    User *user;
    if ([fetchedObjects count] == 0){
        user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:cdh.context];
    }
    else{
        user = fetchedObjects[0];
    }
    
    user.name = [recDic objectForKey:@"name"];
    user.password = [recDic objectForKey:@"password"];
    user.salesPersonCode = [recDic objectForKey:@"salespersoncode"];
    user.userNoSeriesCode = [recDic objectForKey:@"usernoseriescode"];
    
}

-(void)updateCoreDataTable : (NSString*)salesPersonCode{
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"salesPersonCode == %@",salesPersonCode];
    [self.userTable readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.userRecords = [results mutableCopy];
         self.userResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:self.userTable predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.userResults){
                 [self updateUserCoreData:recDic];
             }
             [cdh saveContext];
         }];
     }];
    
}

-(void)insertCoreDataTable : (MSTable*) table{
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    cdh.activeUser.isReadyToUse = [NSNumber numberWithBool:NO];
    //NSPredicate * predicate = [NSPredicate predicateWithFormat:@"name != %@",@""];
    [table readWithPredicate:nil completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.userRecords = [results mutableCopy];
         self.userResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:table predicate:nil];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.userResults){
                 User *user = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                 user.name = [recDic objectForKey:@"name"];
                 user.password = [recDic objectForKey:@"password"];
                 user.salesPersonCode = [recDic objectForKey:@"salespersoncode"];
                 user.userNoSeriesCode = [recDic objectForKey:@"usernoseriescode"];
                 user.role = [recDic objectForKey:@"role"];
                 user.lastUpdatedDate = [NSDate date];
             }
             gf.isSyncCompleted = YES;
             [cdh saveContext];
             NSLog(@"Users saved!");
         }];
     }];
}

#pragma mark * MSFilter methods

- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response
{
    // A wrapped response block that decrements the busy counter
    MSFilterResponseBlock wrappedResponse = ^(NSHTTPURLResponse *innerResponse, NSData *data, NSError *error)
    {
        [self busy:NO];
        response(innerResponse, data, error);
    };
    
    // Increment the busy counter before sending the request
    [self busy:YES];
    next(request, wrappedResponse);
}

+ (UserFunctions *)defaultService
{
    static UserFunctions* service;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[UserFunctions alloc] init];
    });
    
    return service;
}

-(UserFunctions *)init
{
    self = [super init];
    
    if (self)
    {
        // Initialize the Mobile Service client with your URL and key
        MSClient *client = [MSClient clientWithApplicationURLString:@"https://expoipadapp.azure-mobile.net/"
                                                     applicationKey:@"MgayYYCxnOsHFtZJhTbtGPmFeeTdDU98"];
        
        // Add a Mobile Service filter to enable the busy indicator
        self.client = [client clientWithFilter:self];
        self.userTable = [_client tableWithName:@"User"];
        self.userRecords = [[NSMutableArray alloc] init];
        self.busyCount = 0;
    }
    return self;
}

- (void) refreshDataOnSuccess:(CompletionBlock)completion{
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@""];
    
    [self.userTable readWithPredicate:nil completion:^(NSArray *results, NSInteger totalCount, NSError *error) {
        [self logErrorIfNotNil:error];
        userRecords = [results mutableCopy];
        
        completion();
    }];
}

-(void) addUser:(NSDictionary *)user completion:(CompletionWithIndexBlock)completion{
    [self.userTable insert:user completion:^(NSDictionary *result, NSError *error) {
        [self logErrorIfNotNil:error];
        
        NSUInteger index = [userRecords count];
        [(NSMutableArray*)userRecords insertObject:result atIndex:index];
        completion(index);
    }];
}

-(void) completeUser:(NSDictionary *)user completion:(CompletionWithIndexBlock)completion{
    NSMutableArray *mutableUsers = (NSMutableArray*) userRecords;
    NSMutableDictionary *mutable = [user mutableCopy];
    [mutable setObject:@"YES" forKey:@"complete"];
    
    NSUInteger index = [userRecords indexOfObjectIdenticalTo:user];
    [mutableUsers replaceObjectAtIndex:index withObject:mutable];
    
    [self.userTable update:mutable completion:^(NSDictionary *user, NSError *error) {
        [self logErrorIfNotNil:error];
        NSUInteger index = [userRecords indexOfObjectIdenticalTo:mutable];
        [mutableUsers removeObjectAtIndex:index];
        
        completion(index);
    }];
}

- (void)busy:(BOOL)busy
{
    // assumes always executes on UI thread
    if (busy)
    {
        if (self.busyCount == 0 && self.busyUpdate != nil)
        {
            self.busyUpdate(YES);
        }
        self.busyCount++;
    }
    else
    {
        if (self.busyCount == 1 && self.busyUpdate != nil)
        {
            self.busyUpdate(FALSE);
        }
        self.busyCount--;
    }
}

- (void)logErrorIfNotNil:(NSError *) error
{
    if (error)
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.errorMessage = error.localizedDescription;
        gf.progress = 1.0f;
        NSLog(@"ERROR %@", error);
    }
}

@end
