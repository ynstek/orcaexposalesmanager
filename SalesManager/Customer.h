//
//  Customer.h
//  SalesManager
//
//  Created by Cihan TASKIN on 03/02/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Customer : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * address2;
@property (nonatomic, retain) NSDecimalNumber * balance;
@property (nonatomic, retain) NSNumber * block;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * customerNo;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * postCode;
@property (nonatomic, retain) NSString * priceGroup;
@property (nonatomic, retain) NSString * salesPersonCode;
@property (nonatomic, retain) NSDecimalNumber * unclearedFunds;

@end
