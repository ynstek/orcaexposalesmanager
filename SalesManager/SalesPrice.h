//
//  SalesPrice.h
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SalesPrice : NSManagedObject

@property (nonatomic, retain) NSString * itemNo;
@property (nonatomic, retain) NSString * itemUnit;
@property (nonatomic, retain) NSNumber * minQty;
@property (nonatomic, retain) NSDecimalNumber * unitPrice;
@property (nonatomic, retain) NSString * salesCode;

@end
