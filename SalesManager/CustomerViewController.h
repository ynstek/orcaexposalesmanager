//
//  CustomerViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 14/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SyncFunctions.h"

@interface CustomerViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIPopoverControllerDelegate>
@property (strong, nonatomic) IBOutlet UITextField *noTextField;
@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *addressTextField;
@property (strong, nonatomic) IBOutlet UITextField *address2TextField;
@property (strong, nonatomic) IBOutlet UITextField *phoneTextField;
@property (strong, nonatomic) IBOutlet UITextField *postCodeTextField;
@property (strong, nonatomic) IBOutlet UITextField *cityTextField;
@property (strong, nonatomic) IBOutlet UITextField *balanceTextField;
@property (strong, nonatomic) IBOutlet UITextField *balanceOnDeviceTextField;

@property (strong, nonatomic) IBOutlet UITableView *balanceHistoryTableView;
@property (strong, nonatomic) IBOutlet UITableView *orderListTableView;
@property (strong, nonatomic) IBOutlet UITextField *unclearedFundsTextField;


@property (strong,nonatomic) UIPopoverController *popover;

@end
