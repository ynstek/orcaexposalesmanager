//
//  SentOrdersViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 09/03/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "SentOrdersViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import <unistd.h>
#import "MiniPrinterFunctions.h"
#import "DatePickerViewController.h"


@interface SentOrdersViewController () <MBProgressHUDDelegate> {
    MBProgressHUD *HUD;
    long long expectedLength;
    long long currentLength;
}

@end

@implementation SentOrdersViewController
NSDate *fromDate,*toDate;
bool isPrinting;

#define debug 1

-(void)refreshView{
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesHeader"];
    
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    [gf clearSelectedOrders];
    
    NSDateFormatter * formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd-MM-yy"];
    
    fromDate = [formatter dateFromString:self.fromDateTextField.text];
    toDate = [gf dateForEndOfDay:[formatter dateFromString:self.toDateTextField.text]];
    
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"salesPersonCode == %@ && markToDelete == %@ && lastInvoice == %@ && orderDate >= %@ && orderDate <= %@",cdh.activeUser.salesPersonCode,[NSNumber numberWithBool:YES],[NSNumber numberWithBool:NO],fromDate,toDate];
    
    [request setPredicate:filter];
    
    request.sortDescriptors =
    [NSArray arrayWithObjects:
     [NSSortDescriptor sortDescriptorWithKey:@"documentNo" ascending:YES],nil];
    
    self.salesOrderArray = [[cdh.context executeFetchRequest:request error:nil] mutableCopy];
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    NSDateFormatter * formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd/MM/yy"];
    NSString * dateString = [formatter stringFromDate: [gf getTodayDate]];
    
    self.fromDateTextField.text = dateString;
    self.toDateTextField.text = dateString;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissDatePickerPopover) name:@"dismissDatePickerPopover" object:nil];
    
    [self refreshView];
    
    gf.progress = 0.0f;
    
    self.salesHF = [SalesHeaderFunctions defaultService];
    [self followBusyStatus];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.salesOrderArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    static NSString *cellIdentifier = @"Order Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    SalesHeader *salesHeader = [self.salesOrderArray objectAtIndex:indexPath.row];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *localDateString = [dateFormatter stringFromDate:salesHeader.orderDate];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@ - %@",salesHeader.documentNo,[gf getCustomerName:salesHeader.sellToNo],localDateString];
    
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"£ %@",[gf getOrderTotalAmountInclVAT:salesHeader.documentNo]];
    
    
    if ([salesHeader.markToSend isEqualToNumber:[NSNumber numberWithBool:YES]]){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    SalesHeader *salesHeader = [self.salesOrderArray objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark){
        salesHeader.markToSend = [NSNumber numberWithBool:NO];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else{
        salesHeader.markToSend = [NSNumber numberWithBool:YES];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    [cdh saveContext];
}

- (void)sendOrders{
    isPrinting = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirmation"
                                                    message:@"Are you sure to re-send order(s)?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    switch(buttonIndex) {
        case 0: //"No" pressed
            //do something?
            break;
        case 1: //"Yes" pressed
            if (!isPrinting){
                gf.errorMessage = @"";
                HUD = nil;
                [self.salesHF SendHeaders:YES];
            }
            else{
                if (blocking) {
                    return;
                }
                blocking = YES;
                NSString *portName = [AppDelegate getPortName];
                NSString *portSettings = [AppDelegate getPortSettings];
                [MiniPrinterFunctions printOrderListWithPortname:portName portSettings:portSettings widthInch:0:fromDate:toDate];
                blocking = NO;
            }
            break;
    }
}
- (void)followBusyStatus{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    self.salesHF.busyUpdate = ^(BOOL busy)
    {
        if (busy)
        {
            gf.progress = 0.0f;
            if (HUD == nil){
                HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                [self.navigationController.view addSubview:HUD];
                HUD.dimBackground = YES;
                HUD.delegate = self;
                HUD.labelText = @"Re-Sending Order(s)";
                HUD.detailsLabelText = @"Please Wait";
                [HUD showWhileExecuting:@selector(myProgressTask) onTarget:self withObject:nil animated:YES];
            }
        } else
        {
        }
    };
}
- (void)myProgressTask {
    // This just increases the progress indicator in a loop
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    while (gf.progress < 1.0f) {
    }
    if ([gf.errorMessage isEqualToString:@""]){
        gf.errorMessage = @"Order(s) are sent sucessfully!";
        [self refreshView];
    }
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Re-Sending Orders" message:gf.errorMessage delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)sendButtonPressed:(id)sender {
    [self sendOrders];
}
- (IBAction)showButtonPressed:(id)sender {
    [self refreshView];
    
}
- (IBAction)printButtonPressed:(id)sender {
    
    isPrinting = YES;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirmation"
                                                    message:@"Do you want to print Order Report?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
}

- (IBAction)fromDatePickerImagePressed:(id)sender {
    self.datePickerCaller = @"fromDate";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];
    DatePickerViewController *datePickerView =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"DatePickerViewController"];
    
    datePickerView.parentName = @"OrderList";
    datePickerView.titleText = @"From Date";
    datePickerView.isHistoryEnabled = YES;
    self.popover = [[UIPopoverController alloc]initWithContentViewController:datePickerView];
    self.popover.delegate = self;
    self.popover.popoverContentSize = CGSizeMake(150, 220);
    [self.popover presentPopoverFromRect:self.fromDateTextField.bounds inView:self.fromDateTextField permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (IBAction)toDatePickerImagePressed:(id)sender {
    self.datePickerCaller = @"toDate";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];
    DatePickerViewController *datePickerView =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"DatePickerViewController"];
    
    datePickerView.parentName = @"OrderList";
    datePickerView.titleText = @"To Date";
    datePickerView.isHistoryEnabled = YES;
    
    self.popover = [[UIPopoverController alloc]initWithContentViewController:datePickerView];
    self.popover.delegate = self;
    self.popover.popoverContentSize = CGSizeMake(150, 220);
    [self.popover presentPopoverFromRect:self.fromDateTextField.bounds inView:self.fromDateTextField permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)dismissDatePickerPopover {
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    if ([self.datePickerCaller isEqualToString:@"fromDate"]){
        self.fromDateTextField.text = gf.selectedDate;
    }
    else{
        self.toDateTextField.text = gf.selectedDate;
    }
}

@end
