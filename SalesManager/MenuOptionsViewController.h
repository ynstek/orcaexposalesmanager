//
//  MenuOptionsViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 15/02/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuOptionsViewController : UIViewController<UIPopoverControllerDelegate>

//Order Details START
@property (strong, nonatomic) IBOutlet UIView *orderView;
@property (strong, nonatomic) IBOutlet UILabel *orderNoLabel;
@property (strong, nonatomic) IBOutlet UILabel *customerNoLabel;
@property (strong, nonatomic) IBOutlet UILabel *customerNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *customerPostCodeLabel;
@property (strong, nonatomic) IBOutlet UILabel *customerBalanceLabel;
@property (strong, nonatomic) IBOutlet UILabel *customerUnclearedLabel;
@property (strong, nonatomic) IBOutlet UILabel *orderTotalLabel;
@property (strong, nonatomic) IBOutlet UITextField *requestedDeliveryDateTextField;
- (IBAction)datePickerImagePressed:(id)sender;

- (IBAction)orderButtonPressed:(id)sender;

//Order Details END

//Filter View START
@property (strong, nonatomic) IBOutlet UIView *filterView;
@property (strong,nonatomic) UIPopoverController *popover;
@property (strong, nonatomic) IBOutlet UIButton *countriesButton;
@property (strong, nonatomic) IBOutlet UISwitch *allItemsSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *inactiveItemsSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *lastSaleItemsSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *unSoldItemsSwitch;
@property (strong, nonatomic) IBOutlet UIButton *onlyNewItemsButton;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator1;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator2;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicator3;
@property (strong, nonatomic) IBOutlet UIButton *categoriesButton;
@property (strong, nonatomic) IBOutlet UIButton *subCategoriesButton;

- (IBAction)filterButtonPressed:(id)sender;
- (IBAction)resetFilterButtonPressed:(id)sender;
- (IBAction)countriesButtonPressed:(id)sender;
- (IBAction)allItemsSwitchChanged:(id)sender;
- (IBAction)inactiveItemsSwitchChanged:(id)sender;
- (IBAction)lastSaleItemsSwitchChanged:(id)sender;
- (IBAction)unsoldItemsSwitchChanged:(id)sender;
- (IBAction)categoriesButtonPressed:(id)sender;
- (IBAction)subCategoriesButtonPressed:(id)sender;
//Filter View END

@end
