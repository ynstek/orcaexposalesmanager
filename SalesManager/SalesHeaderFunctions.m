//
//  SalesHeaderFunctions.m
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "SalesHeaderFunctions.h"
#import "AppDelegate.h"

@interface SalesHeaderFunctions() <MSFilter>

@property (nonatomic, strong) MSTable *salesHTable;
@property (nonatomic) NSInteger busyCount;

@end

@implementation SalesHeaderFunctions
#define debug 1

@synthesize salesHRecords;

- (void)emptyCoreDataTable:(NSString*)tableName : (NSManagedObject*) table{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:tableName];
    
    NSArray *fetchedObjects =
    [cdh.context executeFetchRequest:request error:nil];
    
    for(table in fetchedObjects){
        [cdh.context deleteObject:table];
    }
}

- (void)loadDataRecursiveForQuery:(MSQuery *)query withCompletion:(CompletionBlock)completion
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    query.includeTotalCount = YES;
    query.fetchLimit = 1000; //note: you can adjust this to whatever amount is optimum
    query.fetchOffset = self.salesHResults.count;
    
    [query readWithCompletion:^(NSArray *items,NSInteger totalCount,NSError *error){
        if(!error) {
            //add the items to our local copy
            [self.salesHResults addObjectsFromArray:items];
            
            if (totalCount > [self.salesHResults count]) {
                [self loadDataRecursiveForQuery:query withCompletion:completion];
                completion();
            } else {
                completion();
            }
        }
        else{
            [self logErrorIfNotNil:error];
            completion();
        }
    }];
}

- (void) updateSalesHeader : (NSString*)docNo completion:(CompletionBlock)completion {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"documentNo == %@",docNo];
    
    [self.salesHTable readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         salesHRecords = [results mutableCopy];
         self.salesHResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:self.salesHTable predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.salesHResults){
                 NSDictionary *params = @{ @"table" : self.salesHTable.name};
                 
                 NSDictionary *salesHDic = @{ @"documentno" : [recDic objectForKey:@"documentno"], @"salespersoncode" : [recDic objectForKey:@"salespersoncode"],
                                              @"selltono" : [recDic objectForKey:@"selltono"], @"requesteddeliverydate" : [recDic objectForKey:@"requesteddeliverydate"],
                                              @"orderdate" : [recDic objectForKey:@"orderdate"], @"readytosync" : [NSNumber numberWithBool:YES],
                                              @"comment" : [recDic objectForKey:@"comment"],@"id":[recDic objectForKey:@"id"]};
                 
                 [self.salesHTable update:salesHDic parameters:params completion:^(NSDictionary *result, NSError *error) {
                     [self logErrorIfNotNil:error];
                     completion();
                 }];
             }
         }];
     }];
}

-(void)deleteOrderFromCloud : (NSString*) docNo completion:(CompletionBlock)completion {
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"documentNo == %@",docNo];
    
    [self.salesHTable readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         salesHRecords = [results mutableCopy];
         self.salesHResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:self.salesHTable predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.salesHResults){
                 [self.salesHTable delete:recDic completion:^(id itemId, NSError *error) {
                     if (error){
                         [self logErrorIfNotNil:error];
                     }
                 }];
             }
             if (!error){
                 [self logErrorIfNotNil:error];
                 completion();
             }
         }];
     }];
}

-(void)SendHeaders : (BOOL)isResend{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    //SalesHeaders
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesHeader"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"salesPersonCode == %@ && markToDelete == %@ && markToSend == %@ && lastInvoice == %@",cdh.activeUser.salesPersonCode, [NSNumber numberWithBool:isResend], [NSNumber numberWithBool:YES],[NSNumber numberWithBool:NO]];
    
    [request setPredicate:filter];
    NSArray *fetchedObjects =
    [cdh.context executeFetchRequest:request error:nil];
    int counter = 0;
    if ([fetchedObjects count] > 0){
        for (SalesHeader *salesH in fetchedObjects)
        {
            counter += 1;
            if ([gf checkIfLineExistsInOrder:salesH.documentNo]){
                if (salesH.comment == nil){
                    salesH.comment = @"";
                }
            
                NSDictionary *salesHDic = @{ @"documentno" : salesH.documentNo, @"salespersoncode" : salesH.salesPersonCode,
                                             @"selltono" : salesH.sellToNo, @"requesteddeliverydate" : salesH.requestedDeliveryDate,
                                             @"orderdate" : salesH.orderDate, @"readytosync" : [NSNumber numberWithBool:NO], @"comment" : salesH.comment,
                                             @"lastinvoice" : salesH.lastInvoice};
            
                [self deleteOrderFromCloud:salesH.documentNo completion:^{
                    [self addRecord:salesHDic:salesHRecords completion:^(NSUInteger index){
                        SalesLineFunctions *salesLineF = [SalesLineFunctions defaultService];
                        [salesLineF SendLines:salesH.documentNo completion:^(NSUInteger index) {
                            [self updateSalesHeader:salesH.documentNo completion:^{
                                NoSeriesFunctions *noSerF = [NoSeriesFunctions defaultService];
                                [noSerF updateNoSerOnCloud : @"" completion:^{
                                    salesH.markToDelete = [NSNumber numberWithBool:YES];
                                    salesH.markToSend = [NSNumber numberWithBool:NO];
                                    [cdh saveContext];//order is fully sent to Azure successfully!
                                    if ([fetchedObjects count] == counter){
                                        gf.progress = 1.0f;
                                    }
                                }];
                            }];
                        }];
                    }];
                }];
            }
        }
    }
    
}

-(void)addRecord:(NSDictionary *)salesHDic : (NSArray*) items completion:(CompletionWithIndexBlock)completion
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [self.salesHTable insert:salesHDic completion:^(NSDictionary *result, NSError *error)
     {
         NSUInteger index = [items count];
         if (!error){
         [(NSMutableArray *)items insertObject:result atIndex:index];
         }
         else{
             [self logErrorIfNotNil:error];
         }
         
             
             
         // Let the caller know that we finished
         completion(index);
     }];
}
-(void)insertCoreDataTable : (MSTable*) table{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    
    cdh.activeUser.isReadyToUse = [NSNumber numberWithBool:NO];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"salespersoncode == %@ && lastinvoice == %@",cdh.activeUser.salesPersonCode,[NSNumber numberWithBool:YES]];
    [table readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.salesHRecords = [results mutableCopy];
         self.salesHResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:table predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.salesHResults){
                 if ([[recDic objectForKey:@"lastinvoice"] isEqualToNumber:[NSNumber numberWithBool:YES]]){
                     SalesHeader *salesHeader = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                     salesHeader.documentNo = [recDic objectForKey:@"documentno"];
                     salesHeader.comment = [recDic objectForKey:@"note"];
                     salesHeader.salesPersonCode = [recDic objectForKey:@"salespersoncode"];
                     salesHeader.sellToNo = [recDic objectForKey:@"selltono"];
                     salesHeader.requestedDeliveryDate = [recDic objectForKey:@"requesteddeliverydate"];
                     salesHeader.orderDate = [recDic objectForKey:@"orderdate"];
                     salesHeader.lastInvoice = [recDic objectForKey:@"lastinvoice"];
                 }
             }
             [cdh saveContext];
             NSLog(@"Last Invoice Headers saved!");
         }];
     }];
}

- (void)logErrorIfNotNil:(NSError *) error
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    if (error)
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.errorMessage = error.localizedDescription;
        gf.progress = 1.0f;
        NSLog(@"ERROR %@", error);
    }
}

- (void)busy:(BOOL)busy
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    // assumes always executes on UI thread
    if (busy)
    {
        if (self.busyCount == 0 && self.busyUpdate != nil)
        {
            self.busyUpdate(YES);
        }
        self.busyCount++;
    }
    else
    {
        if (self.busyCount == 1 && self.busyUpdate != nil)
        {
            self.busyUpdate(FALSE);
        }
        self.busyCount--;
    }
}
#pragma mark * MSFilter methods

+ (SalesHeaderFunctions *)defaultService
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    static SalesHeaderFunctions* service;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[SalesHeaderFunctions alloc] init];
    });
    
    return service;
}

-(SalesHeaderFunctions *)init
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    self = [super init];
    if (self)
    {
        // Initialize the Mobile Service client with your URL and key
        MSClient *client = [MSClient clientWithApplicationURLString:@"https://expoipadapp.azure-mobile.net/" applicationKey:@"MgayYYCxnOsHFtZJhTbtGPmFeeTdDU98"];
        
        // Add a Mobile Service filter to enable the busy indicator
        self.client = [client clientWithFilter:self];
        self.salesHTable = [_client tableWithName:@"SalesHeader"];
        self.salesHRecords = [[NSMutableArray alloc] init];
        self.busyCount = 0;
    }
    return self;
}

- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    // A wrapped response block that decrements the busy counter
    MSFilterResponseBlock wrappedResponse = ^(NSHTTPURLResponse *innerResponse, NSData *data, NSError *error)
    {
        [self busy:NO];
        response(innerResponse, data, error);
    };
    
    // Increment the busy counter before sending the request
    [self busy:YES];
    next(request, wrappedResponse);
}
@end
