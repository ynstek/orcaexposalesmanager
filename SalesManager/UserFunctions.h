//
//  UserFunctions.h
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WindowsAzureMobileServices/WindowsAzureMobileServices.h>
#import "User.h"

#pragma mark * Block Definitions

typedef void (^CompletionBlock) ();
typedef void (^CompletionWithIndexBlock) (NSUInteger index);
typedef void (^BusyUpdateBlock) (BOOL busy);

@interface UserFunctions : NSObject

@property (nonatomic, strong)   NSArray *userRecords;
@property (nonatomic, copy)     BusyUpdateBlock busyUpdate;
@property (nonatomic, strong)   MSClient *client;
@property (nonatomic, strong) NSMutableArray *userResults;

- (void)insertCoreDataTable : (MSTable*) table;
- (void)updateUserOnCloud : (NSString*)newPass;
- (void)emptyCoreDataTable;
- (void)setActiveUser;
- (void)checkCurrentUserIfStillActiveOnAzure:(NSString*)fake withCompletion:(CompletionBlock)completion;
- (void)updateCoreDataTable : (NSString*)username;
- (void)setLastUpdatedDate : (NSDate*)systemDate;


+ (UserFunctions *)defaultService;

- (void)refreshDataOnSuccess:(CompletionBlock)completion;

- (void)addUser:(NSDictionary *)user
     completion:(CompletionWithIndexBlock)completion;

- (void)completeUser:(NSDictionary *)user
          completion:(CompletionWithIndexBlock)completion;

- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response;
@end
