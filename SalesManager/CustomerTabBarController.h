//
//  CustomerTabBarController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 14/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomerTabBarController : UITabBarController<UITabBarControllerDelegate,UIPopoverControllerDelegate>{
    BOOL blocking;
}

@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneButton,*doneButton2,*fixedItem,*printLastInvoiceButton,*miniStatButton;
@property (strong,nonatomic) UIPopoverController *popover;
@end
