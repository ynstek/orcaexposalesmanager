//
//  NAVResult.m
//  Orca Mobile Sales
//
//  Created by Cihan TASKIN on 11/10/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "NAVResult.h"

@implementation NAVResult

@synthesize WasSuccessful;
@synthesize Exception;
@synthesize PassValueIfNeeded;

@end
