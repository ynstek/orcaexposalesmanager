//
//  Categories.h
//  
//
//  Created by Cihan TASKIN on 27/05/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Categories : NSManagedObject

@property (nonatomic, retain) NSString * categoryName;
@property (nonatomic, retain) NSString * categoryValue;
@property (nonatomic, retain) NSNumber * isFiltered;
@property (nonatomic, retain) NSString * relatedValueCode;
@property (nonatomic, retain) NSString * categoryValueCode;

@end
