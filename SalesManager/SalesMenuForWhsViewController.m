//
//  SalesMenuForWhsViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 04/06/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "SalesMenuForWhsViewController.h"
#import "AppDelegate.h"

@interface SalesMenuForWhsViewController ()

@end

@implementation SalesMenuForWhsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.selectedOrderList = @"";
    gf.itemArrayFilter = 1;
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"warehouseSalesMenu.jpg"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)ordersInDevicePressed:(id)sender {
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.selectedOrderList = @"ordersInDevice";
}

- (IBAction)parkedOrdersPressed:(id)sender {
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.selectedOrderList = @"parkedOrders";
}

- (IBAction)sentOrdersPressed:(id)sender {
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.selectedOrderList = @"sentOrders";
}
@end
