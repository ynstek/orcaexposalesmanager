//
//  CoreDataCollectionViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 15/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataHelper.h"

@interface CoreDataCollectionViewController : UICollectionViewController<NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) NSFetchedResultsController *frc;
- (void)performFetch;
@end
