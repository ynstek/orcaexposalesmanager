//
//  BalanceHistory.m
//  
//
//  Created by Cihan TASKIN on 27/05/2015.
//
//

#import "BalanceHistory.h"


@implementation BalanceHistory

@dynamic chequeNo;
@dynamic customerNo;
@dynamic documentAmount;
@dynamic documentDate;
@dynamic documentNo;
@dynamic documentType;
@dynamic dueDate;
@dynamic entryNo;
@dynamic paymentOnDevice;
@dynamic paymentType;
@dynamic printed;
@dynamic remainingAmount;
@dynamic salesPersonCode;

@end
