//
//  SyncViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SyncFunctions.h"

@interface SyncViewController : UIViewController<NSURLSessionDelegate>

@property (strong, nonatomic) SyncFunctions *sync;
- (IBAction)newInstallation:(id)sender;
- (IBAction)getAllImagesButtonPressed:(id)sender;

@end
