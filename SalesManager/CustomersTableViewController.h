//
//  CustomersTableViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 14/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "CoreDataTableViewController.h"

@interface CustomersTableViewController : CoreDataTableViewController<UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property BOOL isFiltered;

@end
