//
//  CatalogViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 29/05/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "CatalogViewController.h"
#import "AppDelegate.h"
#import "KeyboardViewController.h"
#import "SalesLineViewController.h"

@interface CatalogViewController ()

@property NSString *prevItemNo,*prevUnit;
@property float changedQty;
@property BOOL isNewSelectedItem;
@end

@implementation CatalogViewController

#define debug 1
- (void)hideKeyboardWhenBackgroundIsTapped {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    UITapGestureRecognizer *tgr =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(hideKeyboard)];
    [tgr setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tgr];
}
- (void)hideKeyboard {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [self.view endEditing:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [self refreshView];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //to create a space between bar item buttons
    //START
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 40.0f;
    //END
    self.linesButton = [[UIBarButtonItem alloc] initWithTitle:@"Lines" style:UIBarButtonItemStyleDone target:self action:@selector(showLines)];
    self.sendOrderButton = [[UIBarButtonItem alloc] initWithTitle:@"Send" style:UIBarButtonItemStyleDone target:self action:@selector(sendOrder)];
    self.parkOrderButton = [[UIBarButtonItem alloc] initWithTitle:@"Park" style:UIBarButtonItemStyleDone target:self action:@selector(parkOrder)];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:self.linesButton, fixedItem, self.sendOrderButton, fixedItem, self.parkOrderButton,  nil];
    
    self.backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(backButtonPressed)];
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:self.backButton, nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissCustomKeyboard) name:@"dismissCustomKeyboard" object:nil];
    [self hideKeyboardWhenBackgroundIsTapped];

}
-(void)refreshView{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    self.customerNameLabel.text = gf.selectedCustomer.name;
    self.postCodeLabel.text = gf.selectedCustomer.postCode;
    self.balanceLabel.text = gf.selectedCustomer.balance.stringValue;
    self.orderTotalAmountLabel.text = [gf getOrderTotalAmountInclVAT:gf.selectedOrder.documentNo];
    [self refreshUOMsOnly];
    [self.salesLineTableView reloadData];
}
-(void)refreshUOMsOnly{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    [gf getUOMList];
    [self.uomTableView reloadData];
}
-(void)backButtonPressed{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:3] animated:YES];

}
-(void)sendOrder{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    [gf sendNAVOrder : [NSNumber numberWithBool:NO]];
    [self backButtonPressed];
}
-(void)parkOrder{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    [gf sendNAVOrder : [NSNumber numberWithBool:YES]];
    [self backButtonPressed];
}
-(void)showLines{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    SalesLineViewController *salesLineView = [self.storyboard instantiateViewControllerWithIdentifier:@"SalesLineViewController"];
    [self.navigationController pushViewController:salesLineView animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    if (tableView == self.salesLineTableView){
        if (self.isFiltered == NO)
        {
            return [gf.items count];
        }
        else{
            return [gf.searchItemResults count];
        }
    }
    else{
        return [gf.uoms count];
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    UITableViewCell *cell;
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    if (tableView == self.salesLineTableView){
        cell = [self.salesLineTableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    Item *item;
    
    UILabel *lineNoLabel = (UILabel *)[cell viewWithTag:201];
    UILabel *itemNoLabel = (UILabel *)[cell viewWithTag:202];
    UILabel *itemNameLabel = (UILabel *)[cell viewWithTag:101];
    UILabel *itemPackAndUnitSizeLabel = (UILabel *)[cell viewWithTag:102];
    UITextField *itemUnitPriceTextField = (UITextField *)[cell viewWithTag:301];
    UITextField *qtyTextField = (UITextField *)[cell viewWithTag:302];
    UISegmentedControl *unitSelection = (UISegmentedControl*)[cell viewWithTag:401];
    
    if (self.isFiltered == NO){
        item = (Item*)[gf.items objectAtIndex:indexPath.row];
    }
    else{
        item = (Item*)[gf.searchItemResults objectAtIndex:indexPath.row];
    }
    
    
    while(unitSelection.numberOfSegments > 0) {
        [unitSelection removeSegmentAtIndex:0 animated:NO];
    }
    
    NSArray *itemUnits = [gf getItemUnits:item.itemNo];
    int counter = 0;
    for (ItemUnits *uom in itemUnits){
        [unitSelection insertSegmentWithTitle:uom.unit
                                      atIndex:counter
                                     animated:false];
        if (self.barcodeUnit != nil){
            if ([uom.unit isEqualToString:self.barcodeUnit]){
                unitSelection.selectedSegmentIndex = counter;
                self.barcodeUnit = nil;
                //NSLog(uom.unit);
            }
        }
        else if ([uom.unit isEqualToString:item.salesUnit]){
            unitSelection.selectedSegmentIndex = counter;
            //NSLog(uom.unit);
        }
        counter += 1;
    }
    
    NSString *selectedUnit = [unitSelection titleForSegmentAtIndex:unitSelection.selectedSegmentIndex];
    
    [self setUnitColor:unitSelection : selectedUnit];
    
    itemNoLabel.text = item.itemNo;
    lineNoLabel.text = [gf getCurrentLineNo:gf.selectedOrder.documentNo :itemNoLabel.text :[unitSelection titleForSegmentAtIndex:unitSelection.selectedSegmentIndex]].stringValue;
    
    SalesLine *currSalesLine = [gf getSalesLine:gf.selectedOrder.documentNo :lineNoLabel.text : NO];
    if (currSalesLine == nil){
        qtyTextField.text = @"0";
        lineNoLabel.text = @"0";
        itemUnitPriceTextField.text = [gf getBestPrice:item.itemNo :selectedUnit :qtyTextField.text];
    }
    else{
        qtyTextField.text = currSalesLine.quantity.stringValue;
        lineNoLabel.text = currSalesLine.lineNo.stringValue;
        itemUnitPriceTextField.text = currSalesLine.itemUnitPrice.stringValue;
    }
        
        itemNameLabel.text = item.name;
        itemPackAndUnitSizeLabel.text = [NSString stringWithFormat:@"%@ X %@",item.packSize,item.unitSize];
        
    if ((self.isFiltered == YES) && (![self.barcodeTextField.text isEqualToString:@""])){
        self.selectedCell = cell;
        float test = [qtyTextField.text floatValue];
        
        if (gf.selectedItemDetails != nil){
            Item *item = [gf getItem:gf.selectedItemDetails.itemNo];
            if (item.kgInBarcode == [NSNumber numberWithInt:1]){
                //test +=100;
                test += [self getKgQtyFromBarcode:self.barcodeTextField.text :test].floatValue;
            }
            else{
                test +=1;
            }
        }
        [self updateSalesLine:cell :[[NSNumber numberWithFloat:test] stringValue]:nil];
        self.barcodeTextField.text = @"";
    }
    
    
    }
    else{
        cell = [self.uomTableView dequeueReusableCellWithIdentifier:@"uomCell" forIndexPath:indexPath];
        UnitsOfMeasure *uom = (UnitsOfMeasure*)[gf.uoms objectAtIndex:indexPath.row];
        cell.textLabel.text = uom.code;
        cell.detailTextLabel.text = uom.qtyOnOrder.stringValue;
    }
    return cell;
}
-(NSString*)getKgQtyFromBarcode : (NSString*)barcodeText : (float)lineQty{
    NSString *kgValueInStr = [barcodeText substringWithRange:NSMakeRange([barcodeText length] - 6, 5)];
    NSMutableString *mu = [NSMutableString stringWithString:kgValueInStr];
    [mu insertString:@"." atIndex:2];
    return mu;
}
- (IBAction)unitSelectionChanged:(id)sender {
    [self setSelectedItem:(UITableViewCell *)[[sender superview] superview]:NO];
    [self changeUnit:NO];
}
-(void)changeUnit : (BOOL)setLastUnit{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    UILabel *lineNoLabel = (UILabel *)[self.selectedCell viewWithTag:201];
    UILabel *itemNoLabel = (UILabel *)[self.selectedCell viewWithTag:202];
    
    UITextField *itemUnitPriceTextField = (UITextField *)[self.selectedCell viewWithTag:301];
    UITextField *qtyTextField = (UITextField *)[self.selectedCell viewWithTag:302];
    
    UISegmentedControl *unitSelection = (UISegmentedControl*)[self.selectedCell viewWithTag:401];
    
    
    NSString *selectedUnit = [unitSelection titleForSegmentAtIndex:unitSelection.selectedSegmentIndex];
    [self setUnitColor:unitSelection : selectedUnit];
    
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    SalesLine *currentSalesLine = [gf getSalesLineByUnit:gf.selectedOrder.documentNo :itemNoLabel.text : selectedUnit];
    if (currentSalesLine == nil){
        lineNoLabel.text = @"0";
        qtyTextField.text = @"0";
        itemUnitPriceTextField.text = [gf getBestPrice:itemNoLabel.text :selectedUnit : @"0"];
    }
    else{
        lineNoLabel.text = currentSalesLine.lineNo.stringValue;
        qtyTextField.text = currentSalesLine.quantity.stringValue;
        itemUnitPriceTextField.text = currentSalesLine.itemUnitPrice.stringValue;
        
        NSDecimalNumber *listAmount = [NSDecimalNumber decimalNumberWithString:[gf getListPrice:currentSalesLine.itemNo :currentSalesLine.itemUnit]];
        listAmount = [listAmount decimalNumberByMultiplyingBy:currentSalesLine.quantity];
        if ([currentSalesLine.lineAmount compare:listAmount] == NSOrderedAscending){
            currentSalesLine.discountAmount = [listAmount decimalNumberBySubtracting:currentSalesLine.lineAmount];
        }
    }
}
-(void)setUnitColor : (UISegmentedControl*) unitSelection : (NSString*)selectedUnit{
    if ([selectedUnit isEqualToString:@"BOX"]){
        unitSelection.tintColor = [UIColor blueColor];
    }
    else if ([selectedUnit isEqualToString:@"EACH"]) {
        unitSelection.tintColor = [UIColor redColor];
    }
    else{
        unitSelection.tintColor = [UIColor blackColor];
    }
}
-(void)setSelectedItem : (UITableViewCell*)selectedCell : (BOOL)updateLastItem{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    
    self.selectedCell = selectedCell;
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    if (gf.selectedItemDetails == nil){
        gf.selectedItemDetails = [[SelectedItemDetails alloc] init];
    }
    
    
    UILabel *itemNoLabel = (UILabel *)[selectedCell viewWithTag:202];
    UILabel *itemNameLabel = (UILabel *)[selectedCell viewWithTag:101];
    UISegmentedControl *unitSelection = (UISegmentedControl*)[selectedCell viewWithTag:401];
    
    
    Item *item = [gf getItem:itemNoLabel.text];
    gf.selectedItemDetails.itemName = itemNameLabel.text;
    gf.selectedItemDetails.itemNo = itemNoLabel.text;
    gf.selectedItemDetails.itemPackSize = item.packSize;
    gf.selectedItemDetails.itemUnitSize = item.unitSize;
    gf.selectedItemDetails.itemSalesUnit = [unitSelection titleForSegmentAtIndex:unitSelection.selectedSegmentIndex];
    gf.selectedItemDetails.itemUnitPrice = [gf getListPrice:item.itemNo :gf.selectedItemDetails.itemSalesUnit];
    gf.selectedItemDetails.itemVat = item.vat.stringValue;
    
    if ([self.prevItemNo isEqualToString:gf.selectedItemDetails.itemNo] && [self.prevUnit isEqualToString:gf.selectedItemDetails.itemSalesUnit]){
        self.isNewSelectedItem = NO;
    }
    else{
        self.isNewSelectedItem = YES;
    }
    
    if (updateLastItem == YES) {
        self.prevItemNo = gf.selectedItemDetails.itemNo;
        self.prevUnit = gf.selectedItemDetails.itemSalesUnit;
    }
}
- (IBAction)qtyTextFieldEditingDidBegin:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [self setSelectedItem:(UITableViewCell *)[[sender superview] superview]:NO];
    UITextField *qtyTextField = (UITextField *)[self.selectedCell viewWithTag:302];
    self.activeTextField = qtyTextField;
    
    self.prevQuantity = qtyTextField.text;
    qtyTextField.text = @"";
    
    
    //cihan
    [qtyTextField resignFirstResponder];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];
    KeyboardViewController *keyboard =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"KeyboardViewController"];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.keyboardValue = self.prevQuantity;
    self.popover = [[UIPopoverController alloc]initWithContentViewController:keyboard];
    self.popover.delegate = self;
    self.popover.popoverContentSize = CGSizeMake(229, 366);
    [self.popover presentPopoverFromRect:qtyTextField.bounds inView:qtyTextField permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}
-(IBAction)qtyTextFieldEditingDidEnd:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [self setSelectedItem:(UITableViewCell *)[[sender superview] superview]:NO];
    UITextField *qtyTextField = (UITextField *)[self.selectedCell viewWithTag:302];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    if ([gf validateNumTextField:qtyTextField.text] == YES){
        if (![self.prevQuantity isEqualToString:qtyTextField.text]){
            self.changedQty = qtyTextField.text.floatValue;
            [self updateSalesLine:self.selectedCell :[[NSNumber numberWithFloat:self.changedQty] stringValue]:nil];
        }
    }
    else{
        qtyTextField.text = self.prevQuantity;
    }
}

-(void)dismissCustomKeyboard{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    if (self.activeTextField.tag == 302){
        UITextField *qtyTextField = (UITextField *)[self.selectedCell viewWithTag:302];
        qtyTextField.text = gf.keyboardValue;
        gf.keyboardValue = @"";
        [qtyTextField becomeFirstResponder];
        [qtyTextField resignFirstResponder];
    }
    else{
        UITextField *itemUnitPriceTextField = (UITextField *)[self.selectedCell viewWithTag:301];
        itemUnitPriceTextField.text = gf.keyboardValue;
        gf.keyboardValue = @"";
        [itemUnitPriceTextField becomeFirstResponder];
        [itemUnitPriceTextField resignFirstResponder];
    }
    self.activeTextField = nil;
}

- (IBAction)unitPriceTextFieldEditingDidBegin:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [self setSelectedItem:(UITableViewCell *)[[sender superview] superview]:NO];
    UITextField *itemUnitPriceTextField = (UITextField *)[self.selectedCell viewWithTag:301];
    self.prevPrice = itemUnitPriceTextField.text;
    itemUnitPriceTextField.text = @"";
    
    
    [itemUnitPriceTextField resignFirstResponder];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];
    KeyboardViewController *keyboard =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"KeyboardViewController"];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.keyboardValue = self.prevPrice;
    self.popover = [[UIPopoverController alloc]initWithContentViewController:keyboard];
    self.popover.delegate = self;
    self.popover.popoverContentSize = CGSizeMake(229, 366);
    [self.popover presentPopoverFromRect:itemUnitPriceTextField.bounds inView:itemUnitPriceTextField permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (IBAction)unitPriceTextFieldEditingDidEnd:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    UITableViewCell* cell = (UITableViewCell *)[[sender superview] superview];
    UITextField *itemUnitPriceTextField = (UITextField *)[cell viewWithTag:301];
    
    
    if ([gf validateNumTextField:itemUnitPriceTextField.text] == YES){
        if (![self.prevPrice isEqualToString:itemUnitPriceTextField.text]){
            [self updateSalesLine:cell :nil :itemUnitPriceTextField.text];
        }
    }
    else{
        itemUnitPriceTextField.text = self.prevPrice;
    }
}

- (IBAction)increaseButtonPressed:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [self setSelectedItem:(UITableViewCell *)[[sender superview] superview]:YES];
    UITextField *qtyTextField = (UITextField *)[self.selectedCell viewWithTag:302];
    
    self.changedQty = [qtyTextField.text floatValue];
    self.changedQty +=1;
    [self updateSalesLine:self.selectedCell :[[NSNumber numberWithFloat:self.changedQty] stringValue]:nil];
}

- (IBAction)decreaseButtonPressed:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [self setSelectedItem:(UITableViewCell *)[[sender superview] superview]:YES];
    UITextField *qtyTextField = (UITextField *)[self.selectedCell viewWithTag:302];
    
    self.changedQty = [qtyTextField.text floatValue];
    self.changedQty -=1;
    [self updateSalesLine:self.selectedCell :[[NSNumber numberWithFloat:self.changedQty] stringValue]:nil];
}


-(void)updateSalesLine:(UITableViewCell*)cell : (NSString*)newQty : (NSString*)newPrice{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    UILabel *lineNoLabel = (UILabel *)[cell viewWithTag:201];
    UILabel *itemNoLabel = (UILabel *)[cell viewWithTag:202];
    UILabel *itemNameLabel = (UILabel *)[cell viewWithTag:101];
    UITextField *itemUnitPriceTextField = (UITextField *)[cell viewWithTag:301];
    UITextField *qtyTextField = (UITextField *)[cell viewWithTag:302];
    UISegmentedControl *unitSelection = (UISegmentedControl*)[cell viewWithTag:401];
    NSString *selectedUnit = [unitSelection titleForSegmentAtIndex:unitSelection.selectedSegmentIndex];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    SalesLine *currentSalesLine = [gf getSalesLine:gf.selectedOrder.documentNo :lineNoLabel.text : YES];
    
    if (newPrice == nil){
        
        qtyTextField.text = newQty;
        float test = [qtyTextField.text floatValue];
        if (test <= 0){
            [gf deleteSalesLine:currentSalesLine];
            qtyTextField.text = @"0";
            lineNoLabel.text = @"0";
            itemUnitPriceTextField.text = [gf getBestPrice:itemNoLabel.text :selectedUnit :qtyTextField.text];
        }
        else{
            if ([currentSalesLine.manualDiscount isEqualToNumber:[NSNumber numberWithBool:NO]]){
                NSString *bestPrice = [gf getBestPrice:itemNoLabel.text :selectedUnit : qtyTextField.text];
                if (bestPrice != nil){
                    itemUnitPriceTextField.text = bestPrice;
                    currentSalesLine.autoDiscount = [NSNumber numberWithBool:YES];
                }
            }
            
            
            currentSalesLine.itemNo = itemNoLabel.text;
            currentSalesLine.itemName = itemNameLabel.text;
            currentSalesLine.itemUnit = selectedUnit;
            currentSalesLine.itemUnitPrice = [NSDecimalNumber decimalNumberWithString:itemUnitPriceTextField.text];
            currentSalesLine.quantity = [NSNumber numberWithFloat:qtyTextField.text.floatValue];
            currentSalesLine.orderDate = gf.selectedOrder.orderDate;
            
            NSDecimal unitPriceTextInDecimal = [[NSNumber numberWithFloat:[itemUnitPriceTextField.text floatValue]] decimalValue];
            NSDecimalNumber * unitPriceDecimalInDecimalNumber = [NSDecimalNumber decimalNumberWithDecimal:unitPriceTextInDecimal];
            
            NSDecimal qtyTextInDecimal = [[NSNumber numberWithFloat:[qtyTextField.text floatValue]] decimalValue];
            NSDecimalNumber * qtyDecimalInDecimalNumber = [NSDecimalNumber decimalNumberWithDecimal:qtyTextInDecimal];
            
            currentSalesLine.lineAmount = [unitPriceDecimalInDecimalNumber decimalNumberByMultiplyingBy:qtyDecimalInDecimalNumber];
            lineNoLabel.text = currentSalesLine.lineNo.stringValue;
        }
    }
    else{
        currentSalesLine.itemNo = itemNoLabel.text;
        currentSalesLine.itemName = itemNameLabel.text;
        currentSalesLine.itemUnit = selectedUnit;
        if (![currentSalesLine.quantity isEqualToNumber:[NSNumber numberWithInt:0]]){
            currentSalesLine.quantity = [NSDecimalNumber decimalNumberWithString:qtyTextField.text];
        }
        else{
            qtyTextField.text = [NSNumber numberWithFloat:1.0f].stringValue;
            currentSalesLine.quantity = [NSDecimalNumber decimalNumberWithString:@"1"];
        }
        currentSalesLine.itemUnitPrice = [NSDecimalNumber decimalNumberWithString:newPrice];
        currentSalesLine.orderDate = gf.selectedOrder.orderDate;
        currentSalesLine.manualDiscount = [NSNumber numberWithBool:YES];
        currentSalesLine.autoDiscount = [NSNumber numberWithBool:NO];
        
        NSDecimal unitPriceTextInDecimal = [[NSNumber numberWithFloat:[itemUnitPriceTextField.text floatValue]] decimalValue];
        NSDecimalNumber * unitPriceDecimalInDecimalNumber = [NSDecimalNumber decimalNumberWithDecimal:unitPriceTextInDecimal];
        
        NSDecimal qtyTextInDecimal = [[NSNumber numberWithFloat:[qtyTextField.text floatValue]] decimalValue];
        NSDecimalNumber * qtyDecimalInDecimalNumber = [NSDecimalNumber decimalNumberWithDecimal:qtyTextInDecimal];
        
        currentSalesLine.lineAmount = [unitPriceDecimalInDecimalNumber decimalNumberByMultiplyingBy:qtyDecimalInDecimalNumber];
        lineNoLabel.text = currentSalesLine.lineNo.stringValue;
    }
    
    
    if (newPrice == nil){
        [self setSelectedItem:cell:YES];
    }
    else{
        [self setSelectedItem:cell:NO];
    }
    
    if (currentSalesLine.itemNo != nil){
        NSDecimalNumber *listAmount = [NSDecimalNumber decimalNumberWithString:[gf getListPrice:currentSalesLine.itemNo :currentSalesLine.itemUnit]];
        listAmount = [listAmount decimalNumberByMultiplyingBy:currentSalesLine.quantity];
        if ([currentSalesLine.lineAmount compare:listAmount] == NSOrderedAscending){
            currentSalesLine.discountAmount = [listAmount decimalNumberBySubtracting:currentSalesLine.lineAmount];
        }
    }
    
    [gf saveCurrentChanges];
    
    self.orderTotalAmountLabel.text = [gf getOrderTotalAmountInclVAT:gf.selectedOrder.documentNo];
    [self refreshUOMsOnly];
    //self.isNewSelectedItem = NO;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.selectedItemDetails = nil;
    [gf setSelectedItemByBarcodeNEW:textField.text];
    if (gf.selectedItemDetails == nil){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No Item with this barcode " message:self.barcodeTextField.text delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        self.barcodeTextField.text = @"";
    }
    self.searchFilter1.text = gf.selectedItemDetails.itemName;
    self.searchFilter2.text = @"";
    [self startSearchItems];
    return YES;
}

-(void)searchItemDescriptions : (NSString*)searchText{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    NSRange itemRange;
    if (gf.searchItemResults.count == 0){
        for (Item *item in gf.items){
            itemRange = [item.name rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if (itemRange.location != NSNotFound){
                [gf.searchItemResults addObject:item];
            }
            else{
                itemRange = [item.unitSize rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if (itemRange.location != NSNotFound){
                    [gf.searchItemResults addObject:item];
                }
            }
        }
    }
    else{
        self.filteredSearchResults = [gf.searchItemResults copy];
        for (Item *item in self.filteredSearchResults){
            itemRange = [item.name rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if (itemRange.location == NSNotFound){
                itemRange = [item.unitSize rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if (itemRange.location == NSNotFound){
                    [gf.searchItemResults removeObject:item];
                }
            }
        }
    }
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if (searchText.length == 0)
    {
        [self startSearchItems];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self startSearchItems];
}
-(void)startSearchItems{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    self.isFiltered = YES;
    gf.searchItemResults = [[NSMutableArray alloc] init];
    
    if (![self.searchFilter1.text isEqualToString:@""]){
        [self searchItemDescriptions:self.searchFilter1.text];
    }
    if (![self.searchFilter2.text isEqualToString:@""]){
        [self searchItemDescriptions:self.searchFilter2.text];
    }
    
    if ([self checkIfNoFilter]){
        self.isFiltered = NO;
    }
    
    //[self.salesLineTableView reloadData];
    [self refreshView];
}
-(BOOL)checkIfNoFilter{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    if ((![self.searchFilter1.text isEqualToString:@""]) || (![self.searchFilter2.text isEqualToString:@""])){
        return NO;
    }
    else{
        return YES;
    }
}

@end
