//
//  PrintOptionsViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 06/02/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrintOptionsViewController : UIViewController <UIPopoverControllerDelegate>{
    BOOL blocking;
}

- (IBAction)printButtonPressed:(id)sender;
@property (strong,nonatomic) UIPopoverController *popover;
@property (strong, nonatomic) IBOutlet UISwitch *showPriceSwitch;

@end
