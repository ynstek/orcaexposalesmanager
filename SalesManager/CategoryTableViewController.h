//
//  CategoryTableViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 25/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "CoreDataTableViewController.h"

@interface CategoryTableViewController : CoreDataTableViewController
@property (strong, nonatomic) NSString *categoryName,*relatedValueCode;
@end
