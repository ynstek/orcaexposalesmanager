//
//  UnitsOfMeasure.h
//  
//
//  Created by Cihan TASKIN on 04/06/2015.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface UnitsOfMeasure : NSManagedObject

@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDecimalNumber * qtyOnOrder;

@end
