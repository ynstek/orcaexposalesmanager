//
//  ItemUnits.m
//  SalesManager
//
//  Created by Cihan TASKIN on 24/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "ItemUnits.h"


@implementation ItemUnits

@dynamic itemNo;
@dynamic unit;
@dynamic qtyPerUnit;
@dynamic barcode;

@end
