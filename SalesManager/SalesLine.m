//
//  SalesLine.m
//  SalesManager
//
//  Created by Cihan TASKIN on 17/02/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "SalesLine.h"


@implementation SalesLine

@dynamic autoDiscount;
@dynamic discountAmount;
@dynamic documentNo;
@dynamic itemName;
@dynamic itemNo;
@dynamic itemUnit;
@dynamic itemUnitPrice;
@dynamic lastInvoice;
@dynamic lineAmount;
@dynamic lineNo;
@dynamic manualDiscount;
@dynamic markToDelete;
@dynamic orderDate;
@dynamic quantity;
@dynamic salesPersonCode;
@dynamic freeItem;

@end
