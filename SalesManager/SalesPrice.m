//
//  SalesPrice.m
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "SalesPrice.h"


@implementation SalesPrice

@dynamic itemNo;
@dynamic itemUnit;
@dynamic minQty;
@dynamic unitPrice;
@dynamic salesCode;

@end
