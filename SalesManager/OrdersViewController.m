//
//  OrdersViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 09/03/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "OrdersViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import <unistd.h>

@interface OrdersViewController () <MBProgressHUDDelegate> {
    MBProgressHUD *HUD;
    long long expectedLength;
    long long currentLength;
}

@end

@implementation OrdersViewController
#define debug 1

-(void)refreshView{
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    [gf clearSelectedOrders];
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesHeader"];
    
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"salesPersonCode == %@ && markToDelete == %@ && lastInvoice == %@",cdh.activeUser.salesPersonCode,[NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO]];
    [request setPredicate:filter];
    
    request.sortDescriptors =
    [NSArray arrayWithObjects:
     [NSSortDescriptor sortDescriptorWithKey:@"documentNo" ascending:YES],nil];
    
    self.salesOrderArray = [[cdh.context executeFetchRequest:request error:nil] mutableCopy];
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self refreshView];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.progress = 0.0f;
    
    self.salesHF = [SalesHeaderFunctions defaultService];
    [self followBusyStatus];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.salesOrderArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    static NSString *cellIdentifier = @"Order Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    SalesHeader *salesHeader = [self.salesOrderArray objectAtIndex:indexPath.row];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *localDateString = [dateFormatter stringFromDate:salesHeader.orderDate];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@ - %@",salesHeader.documentNo,[gf getCustomerName:salesHeader.sellToNo],localDateString];
    
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"£ %@",[gf getOrderTotalAmountInclVAT:salesHeader.documentNo]];
    
    
    if ([salesHeader.markToSend isEqualToNumber:[NSNumber numberWithBool:YES]]){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    SalesHeader *salesHeader = [self.salesOrderArray objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark){
        salesHeader.markToSend = [NSNumber numberWithBool:NO];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else{
        salesHeader.markToSend = [NSNumber numberWithBool:YES];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    [cdh saveContext];
}

- (void)sendOrders{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirmation"
                                                    message:@"Are you sure to send order(s)?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    switch(buttonIndex) {
        case 0: //"No" pressed
            //do something?
            break;
        case 1: //"Yes" pressed
            gf.errorMessage = @"";
            HUD = nil;
            [self.salesHF SendHeaders:NO];
            break;
    }
}
- (void)followBusyStatus{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    self.salesHF.busyUpdate = ^(BOOL busy)
    {
        if (busy)
        {
            gf.progress = 0.0f;
            if (HUD == nil){
                HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                [self.navigationController.view addSubview:HUD];
                HUD.dimBackground = YES;
                HUD.delegate = self;
                HUD.labelText = @"Sending Order(s)";
                HUD.detailsLabelText = @"Please Wait";
                [HUD showWhileExecuting:@selector(myProgressTask) onTarget:self withObject:nil animated:YES];
            }
        } else
        {
        }
    };
}
- (void)myProgressTask {
    // This just increases the progress indicator in a loop
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    while (gf.progress < 1.0f) {
    }
    if ([gf.errorMessage isEqualToString:@""]){
        gf.errorMessage = @"Order(s) are sent sucessfully!";
        [self refreshView];
    }
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sending Orders" message:gf.errorMessage delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
}

- (void)selectAllOrders{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request2 =
    [NSFetchRequest fetchRequestWithEntityName:@"SalesHeader"];
    
    NSPredicate *filter2 = [NSPredicate predicateWithFormat:@"salesPersonCode == %@ && markToDelete == %@",cdh.activeUser.salesPersonCode,[NSNumber numberWithBool:NO]];
    [request2 setPredicate:filter2];
    
    NSArray *fetchedObjects2 =[cdh.context executeFetchRequest:request2 error:nil];
    
    
    
    NSFetchRequest *request =
    [NSFetchRequest fetchRequestWithEntityName:@"SalesHeader"];
    
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"salesPersonCode == %@ && markToDelete == %@ && markToSend == %@",cdh.activeUser.salesPersonCode,[NSNumber numberWithBool:NO],[NSNumber numberWithBool:YES]];
    [request setPredicate:filter];
    
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    
    if ([fetchedObjects count] == [fetchedObjects2 count]){
        for (SalesHeader *salesH in fetchedObjects2){
            salesH.markToSend = [NSNumber numberWithBool:NO];
        }
    }
    else{
        for (SalesHeader *salesH in fetchedObjects2){
            salesH.markToSend = [NSNumber numberWithBool:YES];
        }
    }
    [cdh saveContext];
    [self refreshView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)sendButtonPressed:(id)sender {
    [self sendOrders];
}

- (IBAction)selectAllButtonPressed:(id)sender {
    [self selectAllOrders];
}
@end
