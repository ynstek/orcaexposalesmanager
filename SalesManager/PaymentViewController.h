//
//  PaymentViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 04/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentViewController : UIViewController <UIPrintInteractionControllerDelegate,UIPopoverControllerDelegate>
{

BOOL blocking;
    
}

@property (strong, nonatomic) IBOutlet UISegmentedControl *paymentTypeSelection;
@property (strong, nonatomic) IBOutlet UITextField *dueDateTextField;
@property (strong, nonatomic) IBOutlet UITextField *chequeNoTextField;
@property (strong, nonatomic) IBOutlet UITextField *paymentAmountTextField;
@property (strong,nonatomic) UIPopoverController *popover;
- (IBAction)saveButtonPressed:(id)sender;
- (IBAction)datePickerImagePressed:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *paymentDateTextField;
- (IBAction)paymentTypeSelectionChanged:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *dueDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *chequeNoLabel;

@end
