//
//  ChangePasswordViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 29/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *password1;
@property (strong, nonatomic) IBOutlet UITextField *password2;

- (IBAction)updateButtonPressed:(id)sender;
@end
