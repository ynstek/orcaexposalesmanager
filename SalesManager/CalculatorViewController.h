//
//  CalculatorViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 28/05/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorViewController : UIViewController

@property (strong, nonatomic) NSString *nextOperation,*resultText,*firstNum,*secondNum;
@property BOOL *newNum;
@property (strong, nonatomic) IBOutlet UILabel *keyboardValueLabel;
- (IBAction)dotButtonPressed:(id)sender;
- (IBAction)clearButtonPressed:(id)sender;
- (IBAction)numberZeroButtonPressed:(id)sender;
- (IBAction)numberOneButtonPressed:(id)sender;
- (IBAction)numberTwoButtonPressed:(id)sender;
- (IBAction)numberThreeButtonPressed:(id)sender;
- (IBAction)numberFourButtonPressed:(id)sender;
- (IBAction)numberFiveButtonPressed:(id)sender;
- (IBAction)numberSixButtonPressed:(id)sender;
- (IBAction)numberSevenButtonPressed:(id)sender;
- (IBAction)numberEightButtonPressed:(id)sender;
- (IBAction)numberNineButtonPressed:(id)sender;
- (IBAction)divideButtonPressed:(id)sender;
- (IBAction)multiplyButtonPressed:(id)sender;
- (IBAction)subtractButtonPressed:(id)sender;
- (IBAction)plusButtonPressed:(id)sender;
- (IBAction)equalsButtonPressed:(id)sender;

@end
