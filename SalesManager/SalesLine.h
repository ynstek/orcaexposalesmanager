//
//  SalesLine.h
//  SalesManager
//
//  Created by Cihan TASKIN on 17/02/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SalesLine : NSManagedObject

@property (nonatomic, retain) NSNumber * autoDiscount;
@property (nonatomic, retain) NSDecimalNumber * discountAmount;
@property (nonatomic, retain) NSString * documentNo;
@property (nonatomic, retain) NSString * itemName;
@property (nonatomic, retain) NSString * itemNo;
@property (nonatomic, retain) NSString * itemUnit;
@property (nonatomic, retain) NSDecimalNumber * itemUnitPrice;
@property (nonatomic, retain) NSNumber * lastInvoice;
@property (nonatomic, retain) NSDecimalNumber * lineAmount;
@property (nonatomic, retain) NSNumber * lineNo;
@property (nonatomic, retain) NSNumber * manualDiscount;
@property (nonatomic, retain) NSNumber * markToDelete;
@property (nonatomic, retain) NSDate * orderDate;
@property (nonatomic, retain) NSDecimalNumber * quantity;
@property (nonatomic, retain) NSString * salesPersonCode;
@property (nonatomic, retain) NSNumber * freeItem;

@end
