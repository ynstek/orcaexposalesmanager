//
//  BalanceHistoryFunctions.m
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "BalanceHistoryFunctions.h"
#import "AppDelegate.h"

@interface BalanceHistoryFunctions() <MSFilter>

@property (nonatomic, strong) MSTable *balanceTable;
@property (nonatomic) NSInteger busyCount;

@end

@implementation BalanceHistoryFunctions
@synthesize balanceRecords;

- (void)emptyCoreDataTable:(NSString*)tableName : (NSManagedObject*) table{
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:tableName];
    
    NSArray *fetchedObjects =
    [cdh.context executeFetchRequest:request error:nil];
    
    for(table in fetchedObjects){
        [cdh.context deleteObject:table];
    }
}

- (void)loadDataRecursiveForQuery:(MSQuery *)query withCompletion:(CompletionBlock)completion
{
    query.includeTotalCount = YES;
    query.fetchLimit = 1000; //note: you can adjust this to whatever amount is optimum
    query.fetchOffset = self.balanceResults.count;
    
    [query readWithCompletion:^(NSArray *items,NSInteger totalCount,NSError *error){
        if(!error) {
            //add the items to our local copy
            [self.balanceResults addObjectsFromArray:items];
            
            if (totalCount > [self.balanceResults count]) {
                [self loadDataRecursiveForQuery:query withCompletion:completion];
            } else {
                completion();
            }
        }
        else{
            [self logErrorIfNotNil:error];
            completion();
        }
    }];
}
-(void)updateBalance : (NSDictionary*) recDic{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"BalanceHistory"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"customerNo == %@ and documentNo == %@",[recDic objectForKey:@"customerno"],[recDic objectForKey:@"documentno"]];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    BalanceHistory *balance;
    if ([fetchedObjects count] == 0){
        balance = [NSEntityDescription insertNewObjectForEntityForName:@"BalanceHistory" inManagedObjectContext:cdh.context];
    }
    else{
        balance = fetchedObjects[0];
    }
    balance.customerNo = [recDic objectForKey:@"customerno"];
    balance.documentAmount = [recDic objectForKey:@"documentamount"];
    balance.remainingAmount = [recDic objectForKey:@"remainingamount"];
    balance.documentDate = [recDic objectForKey:@"documentdate"];
    balance.documentType = [recDic objectForKey:@"documenttype"];
    balance.paymentType = [recDic objectForKey:@"paymenttype"];
}

-(void)updateCoreDataTable : (MSTable*) table : (NSString*)customerNo{
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"customerNo == %@",customerNo];
    [table readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.balanceRecords = [results mutableCopy];
         self.balanceResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:table predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.balanceResults){
                 [self updateBalance:recDic];
             }
             [cdh saveContext];
         }];
     }];
    
}

-(void)insertCoreDataTable : (MSTable*) table{
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    cdh.activeUser.isReadyToUse = [NSNumber numberWithBool:NO];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"salespersonecode == %@",cdh.activeUser.salesPersonCode];
    [table readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.balanceRecords = [results mutableCopy];
         self.balanceResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:table predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.balanceResults){
                 if ([table.name isEqualToString:@"BalanceHistory"]){
                     BalanceHistory *balance = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                     balance.customerNo = [recDic objectForKey:@"customerno"];
                     balance.documentNo = [recDic objectForKey:@"documentno"];
                     balance.documentAmount = [recDic objectForKey:@"documentamount"];
                     balance.remainingAmount = [recDic objectForKey:@"remainingamount"];
                     balance.documentDate = [recDic objectForKey:@"documentdate"];
                     balance.documentType = [recDic objectForKey:@"documenttype"];
                     balance.paymentType = [recDic objectForKey:@"paymenttype"];
                     balance.printed = [NSNumber numberWithBool:NO];
                 }
             }
             [cdh saveContext];
             NSLog(@"Balance Histories saved!");
         }];
     }];
}
#pragma mark * MSFilter methods

- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response
{
    // A wrapped response block that decrements the busy counter
    MSFilterResponseBlock wrappedResponse = ^(NSHTTPURLResponse *innerResponse, NSData *data, NSError *error)
    {
        [self busy:NO];
        response(innerResponse, data, error);
    };
    
    // Increment the busy counter before sending the request
    [self busy:YES];
    next(request, wrappedResponse);
}

+ (BalanceHistoryFunctions *)defaultService
{
    static BalanceHistoryFunctions* service;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[BalanceHistoryFunctions alloc] init];
    });
    return service;
}

-(BalanceHistoryFunctions *)init
{
    self = [super init];
    
    if (self)
    {
        // Initialize the Mobile Service client with your URL and key
        MSClient *client = [MSClient clientWithApplicationURLString:@"https://expoipadapp.azure-mobile.net/"
                                                     applicationKey:@"MgayYYCxnOsHFtZJhTbtGPmFeeTdDU98"];
        
        // Add a Mobile Service filter to enable the busy indicator
        self.client = [client clientWithFilter:self];
        self.balanceTable = [_client tableWithName:@"BalanceHistory"];
        self.balanceRecords = [[NSMutableArray alloc] init];
        self.busyCount = 0;
    }
    return self;
}

- (void)busy:(BOOL)busy
{
    // assumes always executes on UI thread
    if (busy)
    {
        if (self.busyCount == 0 && self.busyUpdate != nil)
        {
            self.busyUpdate(YES);
        }
        self.busyCount++;
    }
    else
    {
        if (self.busyCount == 1 && self.busyUpdate != nil)
        {
            self.busyUpdate(FALSE);
        }
        self.busyCount--;
    }
}

- (void)logErrorIfNotNil:(NSError *) error
{
    if (error)
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.errorMessage = error.localizedDescription;
        gf.progress = 1.0f;
        NSLog(@"ERROR %@", error);
    }
}

@end
