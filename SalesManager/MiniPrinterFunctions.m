//
//  MiniPrinterFunctions.m
//  IOS_SDK
//
//  Created by Tzvi on 8/2/11.
//  Copyright 2011 - 2013 STAR MICRONICS CO., LTD. All rights reserved.
//

#import "MiniPrinterFunctions.h"
//#import "StarBitmap.h"
#import <sys/time.h>
#import "SalesHeader.h"
#import "SalesLine.h"
#import "Item.h"
#import "AppDelegate.h"


@implementation MiniPrinterFunctions

+ (void)sendCommand:(NSData *)commands portName:(NSString *)portName portSettings:(NSString *)portSettings timeoutMillis:(u_int32_t)timeoutMillis
{
    unsigned char *commandsToSendToPrinter = (unsigned char*)malloc(commands.length);
    [commands getBytes:commandsToSendToPrinter];
    int commandSize = (int)[commands length];
    
    SMPort *starPort = nil;
    @try
    {
        starPort = [SMPort getPort:portName :portSettings :timeoutMillis];
        if (starPort == nil)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fail to Open Port"
                                                            message:@""
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            //[alert release];
            return;
        }
        
        struct timeval endTime;
        gettimeofday(&endTime, NULL);
        endTime.tv_sec += 60;
        
        StarPrinterStatus_2 status;
        [starPort beginCheckedBlock:&status :2];
        
        if (status.offline == SM_TRUE)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Printer is offline"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            //[alert release];
            return;
        }
        
        int totalAmountWritten = 0;
        while (totalAmountWritten < commandSize)
        {
            int remaining = commandSize - totalAmountWritten;
            
            int amountWritten = [starPort writePort:commandsToSendToPrinter :totalAmountWritten :remaining];
            totalAmountWritten += amountWritten;
            
            struct timeval now;
            gettimeofday(&now, NULL);
            if (now.tv_sec > endTime.tv_sec)
            {
                break;
            }
        }
        
        //SM-T300(Wi-Fi): To use endCheckedBlock method, require F/W 2.4 or later.
        starPort.endCheckedBlockTimeoutMillis = 40000;
        [starPort endCheckedBlock:&status :2];
        if (status.offline == SM_TRUE)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"An error has occurred during printing."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            //[alert release];
            return;
        }
        
        if (totalAmountWritten < commandSize)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Printer Error"
                                                            message:@"Write port timed out"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            //[alert release];
        }
    }
    @catch (PortException *exception)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Printer Error"
                                                        message:@"Write port timed out"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        //[alert release];
    }
    @finally
    {
        [SMPort releasePort:starPort];
        free(commandsToSendToPrinter);
    }
}

/**
 * This function prints raw text to the portable printer.  It show how the text can be formated.  For example changing its size.
 * portName - Port name to use for communication. This should be "TCP:<IP Address>" or "BT:PRNT Star".
 * portSettings - Should be mini, the port settings mini is used for portable printers
 * underline - boolean variable that Tells the printer if should underline the text
 * emphasized - boolean variable that tells the printer if it should emphasize the printed text.  This is sort of like bold but not as dark, but darker then regular characters.
 * upsideDown - boolean variable that tells the printer if the text should be printed upside-down
 * invertColor - boolean variable that tells the printer if it should invert the text its printing.  All White space will become black and the characters will be left white
 * heightExpansion - This integer tells the printer what multiple the character height should be, this should be from 0 to 7 representing multiples from 1 to 8
 * widthExpansion - This integer tell the printer what multiple the character width should be, this should be from 0 to 7 representing multiples from 1 to 8
 * eftMargin - The left margin for text on the portable printer.  This number should be be from 0 to 65536 but it should never get that high or the text can be pushed off the page.
 * alignment - The alignment of the text. The printers support left, right, and center justification
 * textToPrint - The text to send to the printer
 * textToPrintSize - The amount of text to send to the printer.  This should be the size of the preceding parameter
 */
+ (void)PrintText:(NSString*)portName PortSettings:(NSString*)portSettings Underline:(bool)underline Emphasized:(bool)emphasized Upsideddown:(bool)upsideddown InvertColor:(bool)invertColor HeightExpansion:(unsigned char)heightExpansion WidthExpansion:(unsigned char)widthExpansion LeftMargin:(int)leftMargin Alignment:(Alignment)alignment TextToPrint:(unsigned char*)textToPrint TextToPrintSize:(unsigned int)textToPrintSize;
{
    NSMutableData *commands = [[NSMutableData alloc] init];
    
	unsigned char initial[] = {0x1b, 0x40};
	[commands appendBytes:initial length:2];
    
    unsigned char underlineCommand[] = {0x1b, 0x2d, 0x00};
    if (underline)
    {
        underlineCommand[2] = 49;
    }
    else
    {
        underlineCommand[2] = 48;
    }
    [commands appendBytes:underlineCommand length:3];
    
    unsigned char emphasizedCommand[] = {0x1b, 0x45, 0x00};
    if (emphasized)
    {
        emphasizedCommand[2] = 1;
    }
    else
    {
        emphasizedCommand[2] = 0;
    }
    [commands appendBytes:emphasizedCommand length:3];
    
    unsigned char upsidedownCommand[] = {0x1b, 0x7b, 0x00};
    if (upsideddown)
    {
        upsidedownCommand[2] = 1;
    }
    else
    {
        upsidedownCommand[2] = 0;
    }
    [commands appendBytes:upsidedownCommand length:3];
    
    unsigned char invertColorCommand[] = {0x1d, 0x42, 0x00};
    if (invertColor)
    {
        invertColorCommand[2] = 1;
    }
    else
    {
        invertColorCommand[2] = 0;
    }
    [commands appendBytes:invertColorCommand length:3];
    
    unsigned char characterSizeCommand[] = {0x1d, 0x21, 0x00};
    characterSizeCommand[2] = heightExpansion | (widthExpansion << 4);
    [commands appendBytes:characterSizeCommand length:3];
    
    unsigned char leftMarginCommand[] = {0x1d, 0x4c, 0x00, 0x00};
    leftMarginCommand[2] = leftMargin % 256;
    leftMarginCommand[3] = leftMargin / 256;
    [commands appendBytes:leftMarginCommand length:4];
    
    unsigned char justificationCommand[] = {0x1b, 0x61, 0x00};
    switch (alignment)
    {
        case Left:
            justificationCommand[2] = 48;
            break;
        case Center:
            justificationCommand[2] = 49;
            break;
        case Right:
            justificationCommand[2] = 50;
            break;
    }
    [commands appendBytes:justificationCommand length:3];
    
    [commands appendBytes:textToPrint length:textToPrintSize];
    
    unsigned char LF = 10;
    [commands appendBytes:&LF length:1];
    [commands appendBytes:&LF length:1];
    
    [self sendCommand:commands portName:portName portSettings:portSettings timeoutMillis:10000];
    
    //[commands release];
}

/**
 * This function print the sample receipt
 * portName - Port name to use for communication
 * portSettings - The port settings to use
 * widthInch - printable width (2/3/4 [inch])
 */
+ (void)PrintSampleReceiptWithPortname:(NSString *)portName portSettings:(NSString *)portSettings widthInch:(int)printableWidth : (BOOL)showPrice
{
    NSData *commands = nil;
    commands = [self create2InchReceipt:showPrice];
    
    [self sendCommand:commands portName:portName portSettings:portSettings timeoutMillis:10000];
    
}
+ (void)printCollectionReportWithPortname:(NSString *)portName portSettings:(NSString *)portSettings widthInch:(int)printableWidth : (NSDate*)fromDateString : (NSDate*)toDateString
{
    NSData *commands = nil;
    commands = [self printCollectionReport : (NSDate*)fromDateString : (NSDate*)toDateString];
    
    [self sendCommand:commands portName:portName portSettings:portSettings timeoutMillis:10000];
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    [cdh saveContext];
}
+ (void)printOrderListWithPortname:(NSString *)portName portSettings:(NSString *)portSettings widthInch:(int)printableWidth : (NSDate*)fromDateString : (NSDate*)toDateString
{
    NSData *commands = nil;
    commands = [self printOrderListReport : (NSDate*)fromDateString : (NSDate*)toDateString];
    
    [self sendCommand:commands portName:portName portSettings:portSettings timeoutMillis:10000];
    
}
+ (void)PrintSavedPaymentWithPortName:(NSString *)portName portSettings:(NSString *)portSettings widthInch:(int)printableWidth
{
    NSData *commands = nil;
    commands = [self PrintSavedPayment];
    
    [self sendCommand:commands portName:portName portSettings:portSettings timeoutMillis:10000];
    
}
+ (void)printMiniStatementWithPortName:(NSString *)portName portSettings:(NSString *)portSettings widthInch:(int)printableWidth
{
    NSData *commands = nil;
    commands = [self printMiniStatementReport];
    
    [self sendCommand:commands portName:portName portSettings:portSettings timeoutMillis:10000];
    
}
+ (NSData *)PrintSavedPayment
{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    NSMutableData *commands = [NSMutableData new];
    
    [commands appendBytes:"\x1d\x57\x80\x31"
                   length:sizeof("\x1d\x57\x80\x31") - 1];    // Page Area Setting     <GS> <W> nL nH  (nL = 128, nH = 1)
    
    [commands appendBytes:"\x1b\x61\x01"
                   length:sizeof("\x1b\x61\x01") - 1];    // Center Justification  <ESC> a n       (0 Left, 1 Center, 2 Right)
    
    
    
    if (gf.selectedPayment != nil){
        
        commands = [self createHeader:@"Collection Receipt" : NO];
        
        [commands appendBytes:"\x1b\x61\x01"
                       length:sizeof("\x1b\x61\x01") - 1];    // Center Alignment
        commands = [self addLineSeperator:commands];
        [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
        [commands appendData:[[NSString stringWithFormat:@"\nCollection Type : %@\n",gf.selectedPayment.paymentType] dataUsingEncoding:NSASCIIStringEncoding]];
        if ([gf.selectedPayment.paymentType isEqualToString:@"Cash"]){
            commands = [self addLineSeperator:commands];
            [commands appendBytes:"\x1d\x21\x11" length:sizeof("\x1d\x21\x11") - 1];    // Width and Height Character Expansion  <GS>  !  n
            [commands appendBytes:"\x1b\x61\x01" length:sizeof("\x1b\x61\x01") - 1];    // Center Alignment
            [commands appendData:[[NSString stringWithFormat:@"\nAmount GBP : %.2f\n",[gf.selectedPayment.documentAmount doubleValue]] dataUsingEncoding:NSASCIIStringEncoding]];
        }
        else{
            [commands appendData:[[NSString stringWithFormat:@"Due Date : %@\n",[dateFormatter stringFromDate:gf.selectedPayment.dueDate]] dataUsingEncoding:NSASCIIStringEncoding]];
            [commands appendData:[[NSString stringWithFormat:@"Cheque No : %@\n",gf.selectedPayment.chequeNo] dataUsingEncoding:NSASCIIStringEncoding]];
            commands = [self addLineSeperator:commands];
            [commands appendBytes:"\x1d\x21\x11" length:sizeof("\x1d\x21\x11") - 1];    // Width and Height Character Expansion  <GS>  !  n
            [commands appendBytes:"\x1b\x61\x01" length:sizeof("\x1b\x61\x01") - 1];    // Center Alignment
            [commands appendData:[[NSString stringWithFormat:@"\nAmount GBP : %.2f\n",[gf.selectedPayment.documentAmount doubleValue]] dataUsingEncoding:NSASCIIStringEncoding]];
        }
        commands = [self createFooter:commands :YES:NO];
    }
    
    return commands;
}

+(NSMutableData*)createSignSection : (NSMutableData*) cmd {
    
    
    NSString *lineSeperator = @"\n------------------------------------------------\n";
    
    
    
    
    [cmd appendData:[lineSeperator dataUsingEncoding:NSASCIIStringEncoding]];
    
    [cmd appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
    [cmd appendData:[@"| SIGNATURE :" dataUsingEncoding:NSASCIIStringEncoding]];
    [cmd appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
    [cmd appendData:[@"|\n" dataUsingEncoding:NSASCIIStringEncoding]];
    
    [cmd appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
    [cmd appendData:[@"|" dataUsingEncoding:NSASCIIStringEncoding]];
    [cmd appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
    [cmd appendData:[@"|\n" dataUsingEncoding:NSASCIIStringEncoding]];
    
    [cmd appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
    [cmd appendData:[@"|" dataUsingEncoding:NSASCIIStringEncoding]];
    [cmd appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
    [cmd appendData:[@"|\n" dataUsingEncoding:NSASCIIStringEncoding]];
    
    [cmd appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
    [cmd appendData:[@"|" dataUsingEncoding:NSASCIIStringEncoding]];
    [cmd appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
    [cmd appendData:[@"|" dataUsingEncoding:NSASCIIStringEncoding]];
    [cmd appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
    
    [cmd appendData:[lineSeperator dataUsingEncoding:NSASCIIStringEncoding]];
    
    
    return cmd;
}

+(NSMutableData*)addLineSeperator : (NSMutableData*)cmd{
    
    NSString *lineSeperator = @"\n================================================\n";
    [cmd appendData:[lineSeperator dataUsingEncoding:NSASCIIStringEncoding]];
    return cmd;
}
+(NSMutableData*)createFooter : (NSMutableData*) cmd : (BOOL)isSignRequired : (BOOL)isOrder{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    [cmd appendBytes:"\x1d\x21\x00" length:sizeof("\x1d\x21\x00") - 1];    // Cancel Expansion - Reference Star Portable Printer Programming Manual
    cmd = [self addLineSeperator:cmd];
    
    if (isSignRequired){
        cmd = [self createSignSection:cmd];
    }
    
    [cmd appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
    [cmd appendData:[[NSString stringWithFormat:@"\nUser : %@\n",cdh.activeUser.name] dataUsingEncoding:NSASCIIStringEncoding]];
    
    if (isOrder){
        if (gf.selectedOrder.comment != nil){
            [cmd appendData:[[NSString stringWithFormat:@"Note : %@\n",gf.selectedOrder.comment] dataUsingEncoding:NSASCIIStringEncoding]];
        }
        [cmd appendBytes:"\x1b\x61\x01" length:sizeof("\x1b\x61\x01") - 1];    // Center Alignment
        [cmd appendData:[@"\nThank you for shopping\n" dataUsingEncoding:NSASCIIStringEncoding]];
    }
    [cmd appendData:[@"\n\n\n\n\n\n\n" dataUsingEncoding:NSASCIIStringEncoding]];
    
    return cmd;
}


+(NSMutableData*)createHeader2 : (NSMutableData*) cmd : (BOOL)isOrder{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    if (gf.selectedCustomer != nil){
        NSString *header6,*header7,*header8,*header9,*header10;

        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        NSString *orderDateString;
    
        header6 = [NSString stringWithFormat:@"\nCustomer  : %@\n",gf.selectedCustomer.name];
        header7 = [NSString stringWithFormat:@"Code      : %@\n",gf.selectedCustomer.customerNo];
        if (isOrder){
            orderDateString = [dateFormatter stringFromDate:gf.selectedOrder.orderDate];
            header8 = [NSString stringWithFormat:@"Order No  : %@\n",gf.selectedOrder.documentNo];
            header9 = [NSString stringWithFormat:@"Date      : %@\n",orderDateString];
            header10 = [NSString stringWithFormat:@"Post Code : %@\n",gf.selectedCustomer.postCode];
        }
        else{
            orderDateString = [dateFormatter stringFromDate:gf.selectedPayment.documentDate];
            header8 = @"";
            if (orderDateString != NULL){
                header9 = [NSString stringWithFormat:@"Date      : %@\n",orderDateString];
            }
            header10 = @"";
        }
    
    
        [cmd appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
        [cmd appendData:[header6 dataUsingEncoding:NSASCIIStringEncoding]];
        [cmd appendData:[header7 dataUsingEncoding:NSASCIIStringEncoding]];
        [cmd appendData:[header8 dataUsingEncoding:NSASCIIStringEncoding]];
        [cmd appendData:[header9 dataUsingEncoding:NSASCIIStringEncoding]];
        [cmd appendData:[header10 dataUsingEncoding:NSASCIIStringEncoding]];
    }
    return cmd;
}

+(NSMutableData*)createHeader : (NSString*)title : (BOOL)isOrder{
    NSString *header1,*header2,*header3,*header4,*header5;
    header1 = @"Expo Foods Limited\n";
    header2 = @"9, Morson Road\n";
    header3 = @"Enfield, London EN3 4NQ\n";
    header4 = @"Tel:02085314264 Fax:02085318129\n";
    header5 = [NSString stringWithFormat:@"\n%@\n",title];
    NSMutableData *commands = [NSMutableData new];
    [commands appendBytes:"\x1d\x57\x80\x31"
                   length:sizeof("\x1d\x57\x80\x31") - 1];    // Page Area Setting     <GS> <W> nL nH  (nL = 128, nH = 1)
    [commands appendBytes:"\x1b\x61\x01"
                   length:sizeof("\x1b\x61\x01") - 1];    // Center Justification  <ESC> a n       (0 Left, 1 Center, 2 Right)
    [commands appendData:[header1 dataUsingEncoding:NSASCIIStringEncoding]];
    [commands appendData:[header2 dataUsingEncoding:NSASCIIStringEncoding]];
    [commands appendData:[header3 dataUsingEncoding:NSASCIIStringEncoding]];
    [commands appendData:[header4 dataUsingEncoding:NSASCIIStringEncoding]];
    commands = [self addLineSeperator:commands];
    [commands appendData:[header5 dataUsingEncoding:NSASCIIStringEncoding]];
    commands = [self addLineSeperator:commands];
    commands = [self createHeader2:commands:isOrder];
    
    return commands;
}

+ (NSData *)create2InchReceipt : (BOOL)showPrice
{
    NSString *line1,*TotalInclVatText,*lineAmountText;
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];

    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@",gf.selectedOrder.documentNo];
    [request setPredicate:filter];
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    NSMutableData *commands;
    if ([fetchedObjects count] > 0){
        
        
        if ([gf.selectedOrder.lastInvoice isEqualToNumber:[NSNumber numberWithBool:YES]]){
            commands = [self createHeader : @"LAST INOVICE":YES];
        }
        else
        {
            commands = [self createHeader : @"ORDER RECEIPT":YES];
        }
        
        [commands appendBytes:"\x1b\x61\x01" length:sizeof("\x1b\x61\x01") - 1];    // Center Alignment
        commands = [self addLineSeperator:commands];
        [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
        [commands appendData:[@"Description" dataUsingEncoding:NSASCIIStringEncoding]];
        
        if (showPrice == YES){
            [commands appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
            [commands appendData:[@"Amount" dataUsingEncoding:NSASCIIStringEncoding]];
            [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
        }
        
        //[commands appendData:[lineSeperator dataUsingEncoding:NSASCIIStringEncoding]];
        commands = [self addLineSeperator:commands];
        
        //[commands appendBytes:"\x1b\x45\x01" length:sizeof("\x1b\x45\x01") - 1];    // Set Emphasized Printing ON
        //[commands appendBytes:"\x1b\x45\x00" length:sizeof("\x1b\x45\x00") - 1];    // Set Emphasized Printing OFF (same command as on)
        
        for (SalesLine *salesLine in fetchedObjects){
            [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
            line1 = [NSString stringWithFormat:@"%@", salesLine.itemName];
            [commands appendData:[line1 dataUsingEncoding:NSASCIIStringEncoding]];
            [commands appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
            if (showPrice == YES){
                lineAmountText = [NSString stringWithFormat:@" - %@ %@ %.2f",salesLine.itemUnit,[salesLine.quantity stringValue],[salesLine.lineAmount doubleValue]];
            }
            else{
                lineAmountText = [NSString stringWithFormat:@" - %@ %@",salesLine.itemUnit,[salesLine.quantity stringValue]];
            }
            [commands appendData:[lineAmountText dataUsingEncoding:NSASCIIStringEncoding]];
        }
        
        if ([gf.selectedOrder.lastInvoice isEqualToNumber:[NSNumber numberWithBool:NO]]){
            [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
            commands = [self addLineSeperator:commands];
            TotalInclVatText = [NSString stringWithFormat:@"%.2f\n",[[gf getOrderTotalAmountInclVAT:gf.selectedOrder.documentNo] doubleValue]];
            
            //Net Total
            [commands appendData:[@"Net Total" dataUsingEncoding:NSASCIIStringEncoding]];
            [commands appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
            [commands appendData:[[gf getOrderTotalAmount:gf.selectedOrder.documentNo] dataUsingEncoding:NSASCIIStringEncoding]];
            [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
            
            //Vat Total
            [commands appendData:[@"VAT Total" dataUsingEncoding:NSASCIIStringEncoding]];
            [commands appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
            [commands appendData:[[gf getOrderTotalVATAmount:gf.selectedOrder.documentNo] dataUsingEncoding:NSASCIIStringEncoding]];
            [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
            
            //Total incl. VAT
            [commands appendData:[@"Total GBP" dataUsingEncoding:NSASCIIStringEncoding]];
            [commands appendBytes:"\x1d\x21\x11" length:sizeof("\x1d\x21\x11") - 1];    // Width and Height Character Expansion  <GS>  !  n
            [commands appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
            [commands appendData:[TotalInclVatText dataUsingEncoding:NSASCIIStringEncoding]];
            [commands appendBytes:"\x1d\x21\x00" length:sizeof("\x1d\x21\x00") - 1];    // Cancel Expansion - Reference Star Portable Printer Programming Manual
            [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
            //Discount Total
            if (![[gf getOrderTotalDiscountAmount:gf.selectedOrder.documentNo] isEqualToString:@"0.00"]){
                commands = [self addLineSeperator:commands];
                [commands appendData:[@"Your Discount Total" dataUsingEncoding:NSASCIIStringEncoding]];
                [commands appendBytes:"\x1d\x21\x11" length:sizeof("\x1d\x21\x11") - 1];    // Width and Height Character Expansion  <GS>  !  n
                [commands appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
                [commands appendData:[[gf getOrderTotalDiscountAmount:gf.selectedOrder.documentNo] dataUsingEncoding:NSASCIIStringEncoding]];
            }
        }
        
        [self createFooter:commands :NO :YES];
    }
    
    return commands;
}
+ (NSData *)printCollectionReport : (NSDate*)fromDate : (NSDate*)toDate
{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    NSDateFormatter * formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    NSString *reportDateString = [formatter stringFromDate:[NSDate date]];
    NSString *fromDateString = [formatter stringFromDate:fromDate];
    NSString *toDateString = [formatter stringFromDate:toDate];
    
    
    NSString *line1,*lineAmountText;
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
   
    NSMutableData *commands = [NSMutableData new];
    
    [commands appendBytes:"\x1d\x57\x80\x31"
                   length:sizeof("\x1d\x57\x80\x31") - 1];    // Page Area Setting     <GS> <W> nL nH  (nL = 128, nH = 1)
    
    [commands appendBytes:"\x1b\x61\x01"
                   length:sizeof("\x1b\x61\x01") - 1];    // Center Justification  <ESC> a n       (0 Left, 1 Center, 2 Right)
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"BalanceHistory"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"salesPersonCode == %@ && documentDate >= %@ && documentDate <= %@ && paymentOnDevice == %@",cdh.activeUser.salesPersonCode, fromDate,toDate,[NSNumber numberWithBool:YES]];
    [request setPredicate:filter];
    request.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"customerNo"
                                                                                      ascending:YES],nil];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    
    if ([fetchedObjects count] > 0){
        
        commands = [self createHeader:@"Collection Report" :NO];
        
        [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
        [commands appendData:[[NSString stringWithFormat:@"Report Date : %@\n",reportDateString] dataUsingEncoding:NSASCIIStringEncoding]];
        [commands appendData:[[NSString stringWithFormat:@"From : %@ to %@",fromDateString,toDateString] dataUsingEncoding:NSASCIIStringEncoding]];
        
        commands = [self addLineSeperator:commands];
        
        [commands appendData:[@"Customer Name" dataUsingEncoding:NSASCIIStringEncoding]];
        [commands appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
        [commands appendData:[@"Type   Total" dataUsingEncoding:NSASCIIStringEncoding]];
        commands = [self addLineSeperator:commands];
        
        NSDecimalNumberHandler *roundUp = [NSDecimalNumberHandler
                                           decimalNumberHandlerWithRoundingMode:NSRoundUp
                                           scale:2
                                           raiseOnExactness:NO
                                           raiseOnOverflow:NO
                                           raiseOnUnderflow:NO
                                           raiseOnDivideByZero:YES];
        NSDecimalNumber *totalChequeAmount = [NSDecimalNumber numberWithInt:0];
        NSDecimalNumber *totalCashAmount = [NSDecimalNumber numberWithInt:0];
        for (BalanceHistory *payment in fetchedObjects){
            [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
            line1 = [NSString stringWithFormat:@"%@", [gf getCustomerName:payment.customerNo]];
            [commands appendData:[line1 dataUsingEncoding:NSASCIIStringEncoding]];
            [commands appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
            lineAmountText = [NSString stringWithFormat:@"%@ - %.2f\n",payment.paymentType,[payment.documentAmount doubleValue]];
            [commands appendData:[lineAmountText dataUsingEncoding:NSASCIIStringEncoding]];
            [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
            [commands appendData:[[NSString stringWithFormat:@"Code : %@\n",payment.customerNo] dataUsingEncoding:NSASCIIStringEncoding]];
            if ([payment.paymentType isEqualToString:@"Cash"]){
                totalCashAmount = [totalCashAmount decimalNumberByAdding:payment.documentAmount];
            }
            else{
                totalChequeAmount = [totalChequeAmount decimalNumberByAdding:payment.documentAmount];
            }
            payment.printed = [NSNumber numberWithBool:YES];
        }
        
        [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
        commands = [self addLineSeperator:commands];
        
        //Cash Total
        [commands appendData:[@"Cash Total GBP" dataUsingEncoding:NSASCIIStringEncoding]];
        [commands appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
        [commands appendData:[[NSString stringWithFormat:@"%.2f",[totalCashAmount doubleValue]] dataUsingEncoding:NSASCIIStringEncoding]];
        [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
            
        //Cheque Total
        [commands appendData:[@"Cheque Total GBP" dataUsingEncoding:NSASCIIStringEncoding]];
        [commands appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
        [commands appendData:[[NSString stringWithFormat:@"%.2f",[totalChequeAmount doubleValue]] dataUsingEncoding:NSASCIIStringEncoding]];
        
        [self createFooter:commands :YES :NO];
        
    }
    
    return commands;
}

+ (NSData *)printOrderListReport : (NSDate*)fromDate : (NSDate*)toDate
{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    NSDateFormatter * formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    NSString *reportDateString = [formatter stringFromDate:[NSDate date]];
    NSString *fromDateString = [formatter stringFromDate:fromDate];
    NSString *toDateString = [formatter stringFromDate:toDate];
    
    
    NSString *line1,*lineAmountText;
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSMutableData *commands = [NSMutableData new];
    
    [commands appendBytes:"\x1d\x57\x80\x31"
                   length:sizeof("\x1d\x57\x80\x31") - 1];    // Page Area Setting     <GS> <W> nL nH  (nL = 128, nH = 1)
    
    [commands appendBytes:"\x1b\x61\x01"
                   length:sizeof("\x1b\x61\x01") - 1];    // Center Justification  <ESC> a n       (0 Left, 1 Center, 2 Right)
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesHeader"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"salesPersonCode == %@ && markToDelete == %@ && lastInvoice == %@ && orderDate >= %@ && orderDate <= %@",cdh.activeUser.salesPersonCode,[NSNumber numberWithBool:YES],[NSNumber numberWithBool:NO], fromDate,toDate];
    [request setPredicate:filter];
    
    request.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"documentNo" ascending:YES],nil];
    
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    
    if ([fetchedObjects count] > 0){
        
        commands = [self createHeader:@"Order Report" :NO];
        
        [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
        [commands appendData:[[NSString stringWithFormat:@"Report Date : %@\n",reportDateString] dataUsingEncoding:NSASCIIStringEncoding]];
        [commands appendData:[[NSString stringWithFormat:@"From : %@ to %@",fromDateString,toDateString] dataUsingEncoding:NSASCIIStringEncoding]];
        
        commands = [self addLineSeperator:commands];
        
        [commands appendData:[@"Customer Name" dataUsingEncoding:NSASCIIStringEncoding]];
        [commands appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
        [commands appendData:[@"Total" dataUsingEncoding:NSASCIIStringEncoding]];
        commands = [self addLineSeperator:commands];
        
        NSDecimalNumberHandler *roundUp = [NSDecimalNumberHandler
                                           decimalNumberHandlerWithRoundingMode:NSRoundUp
                                           scale:2
                                           raiseOnExactness:NO
                                           raiseOnOverflow:NO
                                           raiseOnUnderflow:NO
                                           raiseOnDivideByZero:YES];
        
        NSDecimalNumber *totalAmount = [NSDecimalNumber numberWithInt:0];
        for (SalesHeader *salesH in fetchedObjects){
            [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
            line1 = [NSString stringWithFormat:@"%@", [gf getCustomerName:salesH.sellToNo]];
            [commands appendData:[line1 dataUsingEncoding:NSASCIIStringEncoding]];
            [commands appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
            lineAmountText = [NSString stringWithFormat:@"%.2f\n",[[gf getOrderTotalAmountInclVAT:salesH.documentNo] doubleValue]];
            [commands appendData:[lineAmountText dataUsingEncoding:NSASCIIStringEncoding]];
            [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
            [commands appendData:[[NSString stringWithFormat:@"Code : %@\n",salesH.sellToNo] dataUsingEncoding:NSASCIIStringEncoding]];
            
            totalAmount = [totalAmount decimalNumberByAdding:[NSDecimalNumber decimalNumberWithString:[gf getOrderTotalAmountInclVAT:salesH.documentNo]]];
                    
        }
        
        [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
        commands = [self addLineSeperator:commands];
        
        //Total
        [commands appendData:[@"Total GBP" dataUsingEncoding:NSASCIIStringEncoding]];
        [commands appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
        [commands appendData:[[NSString stringWithFormat:@"%.2f",[totalAmount doubleValue]] dataUsingEncoding:NSASCIIStringEncoding]];
        [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
        
        [self createFooter:commands :YES :NO];
        
    }
    
    return commands;
}

+ (NSData *)printMiniStatementReport
{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    
    NSString *line1,*lineAmountText;
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    
    NSMutableData *commands = [NSMutableData new];
    
    [commands appendBytes:"\x1d\x57\x80\x31"
                   length:sizeof("\x1d\x57\x80\x31") - 1];    // Page Area Setting     <GS> <W> nL nH  (nL = 128, nH = 1)
    
    [commands appendBytes:"\x1b\x61\x01"
                   length:sizeof("\x1b\x61\x01") - 1];    // Center Justification  <ESC> a n       (0 Left, 1 Center, 2 Right)
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"BalanceHistory"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"customerNo == %@ and paymentOnDevice == %@",gf.selectedCustomer.customerNo,[NSNumber numberWithBool:NO]];
    [request setPredicate:filter];
    request.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"documentDate" ascending:NO],nil];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    
    if ([fetchedObjects count] > 0){
        
        commands = [self createHeader:@"MINI STATEMENT" :NO];
        commands = [self addLineSeperator:commands];
        [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
        [commands appendData:[@"   Date     Document No." dataUsingEncoding:NSASCIIStringEncoding]];
        [commands appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
        [commands appendData:[@"Type       Total" dataUsingEncoding:NSASCIIStringEncoding]];
        commands = [self addLineSeperator:commands];
        
        for (BalanceHistory *balance in fetchedObjects){
            [commands appendBytes:"\x1b\x61\x00" length:sizeof("\x1b\x61\x00") - 1];    // Left Alignment
            line1 = [NSString stringWithFormat:@"%@  %@", [dateFormatter stringFromDate:balance.documentDate],balance.documentNo];
            [commands appendData:[line1 dataUsingEncoding:NSASCIIStringEncoding]];
            [commands appendBytes:"\x1b\x61\x02" length:sizeof("\x1b\x61\x02") - 1];    // Right Alignment
            lineAmountText = [NSString stringWithFormat:@"%@    %.2f\n",balance.documentType,[balance.documentAmount doubleValue]];
            [commands appendData:[lineAmountText dataUsingEncoding:NSASCIIStringEncoding]];
        }
        
        commands = [self addLineSeperator:commands];
        [commands appendBytes:"\x1b\x61\x01" length:sizeof("\x1b\x61\x01") - 1];    // Center Alignment
        [commands appendBytes:"\x1d\x21\x11" length:sizeof("\x1d\x21\x11") - 1];    // Width and Height Character Expansion  <GS>  !  n
        
        [commands appendData:[[NSString stringWithFormat:@"BALANCE GBP %.2f",[gf.selectedCustomer.balance doubleValue]] dataUsingEncoding:NSASCIIStringEncoding]];
        commands = [self createFooter:commands :NO :NO];
        
    }
    
    return commands;
}
@end
