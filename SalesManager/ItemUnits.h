//
//  ItemUnits.h
//  SalesManager
//
//  Created by Cihan TASKIN on 24/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ItemUnits : NSManagedObject

@property (nonatomic, retain) NSString * itemNo;
@property (nonatomic, retain) NSString * unit;
@property (nonatomic, retain) NSNumber * qtyPerUnit;
@property (nonatomic, retain) NSString * barcode;

@end
