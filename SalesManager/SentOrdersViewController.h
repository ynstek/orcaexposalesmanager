//
//  SentOrdersViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 09/03/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesHeaderFunctions.h"

@interface SentOrdersViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    BOOL blocking;
}

@property (strong, nonatomic) NSString *datePickerCaller;
@property (strong, nonatomic) IBOutlet UITextField *fromDateTextField;
@property (strong, nonatomic) IBOutlet UITextField *toDateTextField;
@property (strong,nonatomic) UIPopoverController *popover;

- (IBAction)showButtonPressed:(id)sender;
- (IBAction)printButtonPressed:(id)sender;
- (IBAction)fromDatePickerImagePressed:(id)sender;
- (IBAction)toDatePickerImagePressed:(id)sender;


@property NSMutableArray *salesOrderArray;
@property (strong, nonatomic) SalesHeaderFunctions *salesHF;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)sendButtonPressed:(id)sender;
@end
