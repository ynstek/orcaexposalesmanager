//
//  GridViewViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 12/02/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "GridViewViewController.h"
#import "AppDelegate.h"
#import "Item.h"
#import "LastSales.h"
#import "CategoryTableViewController.h"
#import "SalesLineViewController.h"
#import "CommentsViewController.h"
#import "MiniPrinterFunctions.h"
#import "PrintOptionsViewController.h"
#import "MenuOptionsViewController.h"
#import "DatePickerViewController.h"
#import "BKTPopinControllerViewController.h"
#import "UIViewController+MaryPopin.h"
#import "BKTPopin2ControllerViewController.h"
#import "KeyboardViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "CalculatorViewController.h"

@interface GridViewViewController ()
@property float cellWidth,cellHeight,collectionWidth,collectionHeight,changedQty;
@property BOOL isNewSelectedItem,isSearchIdOn;
@property NSString *prevItemNo,*prevUnit,*test;
@property BKTPopin2ControllerViewController *popin;
@end

@implementation GridViewViewController

@synthesize decoderData;

#define debug 1

- (void)hideKeyboardWhenBackgroundIsTapped {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    UITapGestureRecognizer *tgr =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(hideKeyboard)];
    [tgr setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tgr];
}
- (void)hideKeyboard {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [self.view endEditing:YES];
}

-(void)viewDidDisappear:(BOOL)animated{
    [[Captuvo sharedCaptuvoDevice]stopDecoderHardware];
}

-(void)viewWillAppear:(BOOL)animated{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    self.isNewSelectedItem = YES;
    [self.collectionView reloadData];
    
    [[Captuvo sharedCaptuvoDevice] addCaptuvoDelegate:self];
    [[Captuvo sharedCaptuvoDevice] startDecoderHardware];
    
}

-(void)decoderDataReceived:(NSString *)data{
    NSLog(@"Decoder Data Received: %@",data);
    
    //[self getEndTime] ;
    
    self.decoderData = data;
    
    
    
    [self SearchItemBarcodes];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    if (gf.selectedItemDetails == nil){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No Item with this barcode " message:self.decoderData delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
    else{
        
        self.test = nil;//calisiyor
        if (self.popin == nil){
            self.popin = [[BKTPopin2ControllerViewController alloc] init];
        }
        [self.popin.presentingPopinViewController dismissCurrentPopinControllerAnimated:YES completion:^{
            self.test = @"";
            [self.popin setPopinTransitionStyle:BKTPopinTransitionStyleSlide];
            [self.popin setPopinTransitionDirection:BKTPopinTransitionDirectionTop];
            [self presentPopinController:self.popin animated:YES completion:^{
            }];
        }];
        if (self.test == nil){
            [self.popin setPopinTransitionStyle:BKTPopinTransitionStyleSlide];
            [self.popin setPopinTransitionDirection:BKTPopinTransitionDirectionTop];
            [self presentPopinController:self.popin animated:YES completion:^{
            }];
        }
        
    }
}

- (void)viewDidLoad {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [super viewDidLoad];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    NSDateFormatter * formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd/MM/yy"];
    NSString * reqDateString = [formatter stringFromDate: gf.selectedOrder.requestedDeliveryDate];
    NSString * orderDateString = [formatter stringFromDate: gf.selectedOrder.orderDate];
    
    if ([reqDateString isEqualToString:orderDateString]){
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle:nil];
        DatePickerViewController *datePickerView =
        [mainStoryboard instantiateViewControllerWithIdentifier:@"DatePickerViewController"];
        
        datePickerView.parentName = @"Catalog";
        datePickerView.titleText = @"Delivery Date";
        self.popover = [[UIPopoverController alloc]initWithContentViewController:datePickerView];
        self.popover.delegate = self;
        self.popover.popoverContentSize = CGSizeMake(150, 220);
        [self.popover presentPopoverFromRect:self.menuOptionsButton.bounds inView:self.menuOptionsButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:NO];
        
    }
    
    
    if (gf.itemArrayFilter != 1){
        gf.itemArrayFilter =1;
        [gf populateSearchDescriptions:@"Item"];
    }
    
    [self prepareCollectionViewForDefaultView];
    
    [self clearSelectedItem];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissCommentsPopover) name:@"dismissCommentsPopover" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissMenuOptionsPopover) name:@"dismissMenuOptionsPopover" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissPrintOptionsPopover) name:@"dismissPrintOptionsPopover" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissItemCardWithBarcode) name:@"dismissItemCardWithBarcode" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissCustomKeyboard) name:@"dismissCustomKeyboard" object:nil];
    
    
    [self.autoAddSwitch setOn:gf.isAutoAdded];
    [self.searchIdOnlySwitch setOn:self.isSearchIdOn];
    
    [gf clearCategorySelections : @""];
    
    //to create a space between bar item buttons
    //START
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 40.0f;
    //END
    
    //self.doneButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"commentImage.jpg"] style:UIBarButtonItemStyleDone target:self action:@selector(showComments)];
    
    self.noteButton = [[UIBarButtonItem alloc] initWithTitle:@"Note" style:UIBarButtonItemStyleDone target:self action:@selector(showComments)];
    self.linesButton = [[UIBarButtonItem alloc] initWithTitle:@"Lines" style:UIBarButtonItemStyleDone target:self action:@selector(showLines)];
    self.printButton = [[UIBarButtonItem alloc] initWithTitle:@"Print" style:UIBarButtonItemStyleDone target:self action:@selector(printOrder)];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:self.linesButton, fixedItem, self.noteButton, fixedItem, self.printButton, nil];
    
    [self setCommentButtonFont];
    [self hideKeyboardWhenBackgroundIsTapped];
    
    //[self decoderDataReceived:@"5997476343029"];
}

- (void)printOrder{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];
    PrintOptionsViewController *printOptions =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"PrintOptionsViewController"];
    self.popover = [[UIPopoverController alloc]initWithContentViewController:printOptions];
    self.popover.delegate = self;
    self.popover.popoverContentSize = CGSizeMake(200, 150);
    [self.popover presentPopoverFromBarButtonItem:self.printButton
                         permittedArrowDirections:UIPopoverArrowDirectionUp
                                         animated:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    switch(buttonIndex) {
        case 0: //"No" pressed
            self.decoderData = @"";
            break;
        case 1: //"Yes" pressed
            [self updateSalesLine:self.selectedCell :[[NSNumber numberWithFloat:self.changedQty] stringValue]:nil];
            break;
    }
    self.selectedCell = nil;
}

- (void)didReceiveMemoryWarning {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setCommentButtonFont{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    if (![gf.selectedOrder.comment isEqualToString:@""] && gf.selectedOrder.comment != nil){
        [self.noteButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                 [UIFont fontWithName:@"Helvetica-Bold" size:18.0], NSFontAttributeName,
                                                 [UIColor redColor], NSForegroundColorAttributeName,
                                                 nil]
                                       forState:UIControlStateNormal];
    }
    else{
        [self.noteButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                 [UIFont fontWithName:@"Helvetica" size:16.0], NSFontAttributeName,
                                                 [UIColor redColor], NSForegroundColorAttributeName,
                                                 nil]
                                       forState:UIControlStateNormal];
    }
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    if (self.isFiltered == NO)
    {
        return [gf.items count];
    }
    else{
        return [gf.searchItemResults count];
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    self.lastIndexPath = indexPath;
    
    UICollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    Item *item;
    UILabel *itemNameLabel = (UILabel *)[cell viewWithTag:101];
    UILabel *barcodeImage = (UILabel *)[cell viewWithTag:105];
    UILabel *itemPackAndUnitSizeLabel = (UILabel *)[cell viewWithTag:106];
    
    UILabel *syncErrorLabel = (UILabel *)[cell viewWithTag:556];
    syncErrorLabel.transform=CGAffineTransformMakeRotation( ( -28 * M_PI ) / 180 );
    UIView *syncErrorView = (UIView *)[cell viewWithTag:555];
    @try{
        [syncErrorView setHidden:YES];
        if (self.isFiltered == NO){
            item = (Item*)[gf.items objectAtIndex:indexPath.row];
        }
        else{
            item = (Item*)[gf.searchItemResults objectAtIndex:indexPath.row];
        }
        
        UILabel *lineNoLabel = (UILabel *)[cell viewWithTag:201];
        UILabel *itemNoLabel = (UILabel *)[cell viewWithTag:202];
        
        UILabel *lastSaleQtyLabel = (UILabel *)[cell viewWithTag:102];
        UILabel *lastSaleUnitLabel = (UILabel *)[cell viewWithTag:103];
        UILabel *lastSalePriceLabel = (UILabel *)[cell viewWithTag:104];
        UILabel *lastSaleDate = (UILabel *)[cell viewWithTag:111];
        UIButton *addSP = (UIButton *)[cell viewWithTag:112];
        
        UIImageView *saleImageView = (UIImageView*)[cell viewWithTag:501];
        UIImageView *itemImageView = (UIImageView*)[cell viewWithTag:502];
        UIImageView *noStockImageView = (UIImageView*)[cell viewWithTag:503];
        UIImageView *crossLineImageView = (UIImageView*)[cell viewWithTag:504];
        UILabel *listPriceLabel = (UILabel *)[cell viewWithTag:110];
        UILabel *decreaseLabel = (UILabel *)[cell viewWithTag:107];
        UIButton *decreaseButton = (UIButton *)[cell viewWithTag:108];
        UIButton *itemCardButton = (UIButton *)[cell viewWithTag:109];
        UISegmentedControl *unitSelection = (UISegmentedControl*)[cell viewWithTag:401];
        UITextField *itemUnitPriceTextField = (UITextField *)[cell viewWithTag:301];
        UITextField *qtyTextField = (UITextField *)[cell viewWithTag:302];
        
        UIButton *increaseButton = (UIButton *)[cell viewWithTag:114];
        
        [[increaseButton layer] setBorderWidth:1.0f];
        [[increaseButton layer] setBorderColor:[UIColor grayColor].CGColor];
        [[decreaseButton layer] setBorderWidth:1.0f];
        [[decreaseButton layer] setBorderColor:[UIColor grayColor].CGColor];
        [[addSP layer] setBorderWidth:1.0f];
        [[addSP layer] setBorderColor:[UIColor grayColor].CGColor];
        
        NSString *filepath=[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpeg",item.itemNo]];
        itemImageView.image=[UIImage imageWithContentsOfFile:filepath];
        
        
        if ([item.discounted isEqualToNumber:[NSNumber numberWithBool:YES]]) {
            [saleImageView setHidden:NO];
        }
        else{
            [saleImageView setHidden:YES];
        }
        if ([item.stockQty floatValue] > [[NSNumber numberWithInt:1] floatValue]){
            [noStockImageView setHidden:YES];
        }
        else{
            [noStockImageView setHidden:NO];
        }
        
        NSString *selectedUnit;
        
        @try{
        while(unitSelection.numberOfSegments > 0) {
            [unitSelection removeSegmentAtIndex:0 animated:NO];
        }
        
            NSLog(item.itemNo);
            NSArray *itemUnits = [gf getItemUnits:item.itemNo];
            int counter = 0;
            for (ItemUnits *uom in itemUnits){
                [unitSelection insertSegmentWithTitle:uom.unit
                                              atIndex:counter
                                             animated:false];
                if (self.barcodeUnit != nil){
                    if ([uom.unit isEqualToString:self.barcodeUnit]){
                        unitSelection.selectedSegmentIndex = counter;
                        self.barcodeUnit = nil;
                        //NSLog(uom.unit);
                    }
                }
                else if ([uom.unit isEqualToString:item.salesUnit]){
                    if (![uom.barcode isEqualToString:@""]){
                        barcodeImage.font = [UIFont fontWithName:@"New" size:40];
                        barcodeImage.text = [NSString stringWithFormat:@"%@",uom.barcode];
                    }
                    else{
                        barcodeImage.text = @"";
                    }
                    unitSelection.selectedSegmentIndex = counter;
                    //NSLog(uom.unit);
                }
                counter += 1;
            }
            selectedUnit = [unitSelection titleForSegmentAtIndex:unitSelection.selectedSegmentIndex];
            
            [self setUnitColor:unitSelection : selectedUnit];
            
        }
        @catch (NSException *ex){
            //[self showErrorMessage:[NSString stringWithFormat:@"Item %@ has a sycn issue",item.name]];
            [syncErrorView setHidden:NO];
        }
        
        itemNoLabel.text = item.itemNo;
        lineNoLabel.text = [gf getCurrentLineNo:gf.selectedOrder.documentNo :itemNoLabel.text :[unitSelection titleForSegmentAtIndex:unitSelection.selectedSegmentIndex]].stringValue;
        
        SalesLine *currSalesLine = [gf getSalesLine:gf.selectedOrder.documentNo :lineNoLabel.text : NO];
        if (currSalesLine == nil){
            qtyTextField.text = @"0";
            lineNoLabel.text = @"0";
            itemUnitPriceTextField.text = [gf getBestPrice:item.itemNo :selectedUnit :qtyTextField.text];
        }
        else{
            qtyTextField.text = currSalesLine.quantity.stringValue;
            lineNoLabel.text = currSalesLine.lineNo.stringValue;
            itemUnitPriceTextField.text = currSalesLine.itemUnitPrice.stringValue;
        }
        
        
        itemNameLabel.text = item.name;
        itemPackAndUnitSizeLabel.text = [NSString stringWithFormat:@"%@ X %@",item.packSize,item.unitSize];
        
        LastSales *lastSales = [gf getLastSales:gf.selectedOrder.sellToNo :itemNoLabel.text];
        lastSaleQtyLabel.text = lastSales.salesQty.stringValue;
        lastSaleUnitLabel.text = lastSales.salesUnit;
        
        NSDateFormatter * formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"dd/MM/yyyy"];
        NSString * dateString = [formatter stringFromDate: lastSales.postingDate];
        lastSaleDate.text = dateString;
        
        if (lastSales == nil){
            [addSP setHidden:YES];
            lastSalePriceLabel.text = @"";
        }
        else{
            [addSP setHidden:NO];
            lastSalePriceLabel.text = [NSString stringWithFormat:@"£%@",lastSales.salesPrice.stringValue];
        }
        
        if ((self.isFiltered == YES) && (![self.decoderData isEqualToString:@""]) && (gf.isAutoAdded == YES)){
            self.selectedCell = cell;
            float test = [qtyTextField.text floatValue];
            if (test == 0){
                test +=1;
                [self updateSalesLine:cell :[[NSNumber numberWithFloat:test] stringValue]:nil];
                self.decoderData = @"";
            }
            else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Item is already added. Qty = %.2f",test]
                                                                message:@"Do you want to add one more?"
                                                               delegate:self
                                                      cancelButtonTitle:@"No"
                                                      otherButtonTitles:@"Yes", nil];
                [alert show];
            }
        }
        
        NSString *listPriceText = [gf getListPrice:itemNoLabel.text :selectedUnit];
        listPriceLabel.text = listPriceText;
        if ([itemUnitPriceTextField.text isEqualToString:listPriceText]){
            [listPriceLabel setHidden:YES];
            [crossLineImageView setHidden:YES];
        }
        else{
            listPriceLabel.text = [NSString stringWithFormat:@"%@",currSalesLine.discountAmount];
            [listPriceLabel setHidden:NO];
            [crossLineImageView setHidden:NO];
        }
        [self setFreeLineQuantity:cell:selectedUnit];
    }
    @catch (NSException *ex){
        [syncErrorView setHidden:NO];
    }
    
    return cell;
    
}

-(void)setFreeLineQuantity : (UICollectionViewCell*)cell : (NSString*)unit{
    
    UILabel *freeQtyLabel = (UILabel *)[cell viewWithTag:116];
    UILabel *itemNoLabel = (UILabel *)[cell viewWithTag:202];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    SalesLine *freeLine = [gf getFreeSalesLineByUnit :itemNoLabel.text : unit];
    
    if (freeLine == nil){
        freeQtyLabel.text = @"Add Free";
    }
    else{
        freeQtyLabel.text = [NSString stringWithFormat:@"Free (%@)",freeLine.quantity.stringValue];
    }
}

- (IBAction)qtyTextFieldEditingDidBegin:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [self setSelectedItem:(UICollectionViewCell *)[[sender superview] superview]:NO];
    UITextField *qtyTextField = (UITextField *)[self.selectedCell viewWithTag:302];
    self.activeTextField = qtyTextField;
    
    self.prevQuantity = qtyTextField.text;
    qtyTextField.text = @"";
    
    
    //cihan
    [qtyTextField resignFirstResponder];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];
    KeyboardViewController *keyboard =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"KeyboardViewController"];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.keyboardValue = self.prevQuantity;
    self.popover = [[UIPopoverController alloc]initWithContentViewController:keyboard];
    self.popover.delegate = self;
    self.popover.popoverContentSize = CGSizeMake(229, 366);
    [self.popover presentPopoverFromRect:qtyTextField.bounds inView:qtyTextField permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

-(IBAction)qtyTextFieldEditingDidEnd:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [self setSelectedItem:(UICollectionViewCell *)[[sender superview] superview]:NO];
    UITextField *qtyTextField = (UITextField *)[self.selectedCell viewWithTag:302];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    if ([gf validateNumTextField:qtyTextField.text] == YES){
        if (![self.prevQuantity isEqualToString:qtyTextField.text]){
            self.changedQty = qtyTextField.text.floatValue;
            [self showConfirmationMessageIfNeeded];
        }
    }
    else{
        qtyTextField.text = self.prevQuantity;
    }
}

-(void)dismissCustomKeyboard{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    if (self.activeTextField.tag == 302){
        UITextField *qtyTextField = (UITextField *)[self.selectedCell viewWithTag:302];
        qtyTextField.text = gf.keyboardValue;
        gf.keyboardValue = @"";
        [qtyTextField becomeFirstResponder];
        [qtyTextField resignFirstResponder];
    }
    else{
        UITextField *itemUnitPriceTextField = (UITextField *)[self.selectedCell viewWithTag:301];
        itemUnitPriceTextField.text = gf.keyboardValue;
        gf.keyboardValue = @"";
        [itemUnitPriceTextField becomeFirstResponder];
        [itemUnitPriceTextField resignFirstResponder];
    }
    self.activeTextField = nil;
}

- (IBAction)unitPriceTextFieldEditingDidBegin:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [self setSelectedItem:(UICollectionViewCell *)[[sender superview] superview]:NO];
    UITextField *itemUnitPriceTextField = (UITextField *)[self.selectedCell viewWithTag:301];
    self.prevPrice = itemUnitPriceTextField.text;
    itemUnitPriceTextField.text = @"";
    
    
    [itemUnitPriceTextField resignFirstResponder];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];
    KeyboardViewController *keyboard =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"KeyboardViewController"];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.keyboardValue = self.prevPrice;
    self.popover = [[UIPopoverController alloc]initWithContentViewController:keyboard];
    self.popover.delegate = self;
    self.popover.popoverContentSize = CGSizeMake(229, 366);
    [self.popover presentPopoverFromRect:itemUnitPriceTextField.bounds inView:itemUnitPriceTextField permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (IBAction)unitPriceTextFieldEditingDidEnd:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    UICollectionViewCell* cell = (UICollectionViewCell *)[[sender superview] superview];
    UITextField *itemUnitPriceTextField = (UITextField *)[cell viewWithTag:301];
    
    
    if ([gf validateNumTextField:itemUnitPriceTextField.text] == YES){
        if (![self.prevPrice isEqualToString:itemUnitPriceTextField.text]){
            [self updateSalesLine:cell :nil :itemUnitPriceTextField.text];
        }
    }
    else{
        itemUnitPriceTextField.text = self.prevPrice;
    }
    
    
}

- (IBAction)autoAddSwitchChanged:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    if ([sender isOn]){
        gf.isAutoAdded = YES;
        //[self decoderDataReceived:@"7622100993872"];
    }
    else
    {
        gf.isAutoAdded = NO;
    }
}

- (IBAction)searchIdSwitchChanged:(id)sender {
    if ([sender isOn]){
        self.isSearchIdOn = YES;
    }
    else{
        self.isSearchIdOn = NO;
    }
}

-(void)updateSalesLine:(UICollectionViewCell*)cell : (NSString*)newQty : (NSString*)newPrice{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    UILabel *lineNoLabel = (UILabel *)[cell viewWithTag:201];
    UILabel *itemNoLabel = (UILabel *)[cell viewWithTag:202];
    UILabel *itemNameLabel = (UILabel *)[cell viewWithTag:101];
    UITextField *itemUnitPriceTextField = (UITextField *)[cell viewWithTag:301];
    UITextField *qtyTextField = (UITextField *)[cell viewWithTag:302];
    UISegmentedControl *unitSelection = (UISegmentedControl*)[cell viewWithTag:401];
    NSString *selectedUnit = [unitSelection titleForSegmentAtIndex:unitSelection.selectedSegmentIndex];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    SalesLine *currentSalesLine;
    @try {
        
        currentSalesLine = [gf getSalesLine:gf.selectedOrder.documentNo :lineNoLabel.text : YES];
        
        if (newPrice == nil){
            
            qtyTextField.text = newQty;
            float test = [qtyTextField.text floatValue];
            if (test <= 0){
                [gf deleteSalesLine:currentSalesLine];
                qtyTextField.text = @"0";
                lineNoLabel.text = @"0";
                itemUnitPriceTextField.text = [gf getBestPrice:itemNoLabel.text :selectedUnit :qtyTextField.text];
            }
            else{
                if ([currentSalesLine.manualDiscount isEqualToNumber:[NSNumber numberWithBool:NO]]){
                    NSString *bestPrice = [gf getBestPrice:itemNoLabel.text :selectedUnit : qtyTextField.text];
                    if (bestPrice != nil){
                        itemUnitPriceTextField.text = bestPrice;
                        currentSalesLine.autoDiscount = [NSNumber numberWithBool:YES];
                    }
                }
                
                
                currentSalesLine.itemNo = itemNoLabel.text;
                currentSalesLine.itemName = itemNameLabel.text;
                currentSalesLine.itemUnit = selectedUnit;
                currentSalesLine.itemUnitPrice = [NSDecimalNumber decimalNumberWithString:itemUnitPriceTextField.text];
                currentSalesLine.quantity = [NSNumber numberWithFloat:qtyTextField.text.floatValue];
                currentSalesLine.orderDate = gf.selectedOrder.orderDate;
                
                NSDecimal unitPriceTextInDecimal = [[NSNumber numberWithFloat:[itemUnitPriceTextField.text floatValue]] decimalValue];
                NSDecimalNumber * unitPriceDecimalInDecimalNumber = [NSDecimalNumber decimalNumberWithDecimal:unitPriceTextInDecimal];
                
                NSDecimal qtyTextInDecimal = [[NSNumber numberWithFloat:[qtyTextField.text floatValue]] decimalValue];
                NSDecimalNumber * qtyDecimalInDecimalNumber = [NSDecimalNumber decimalNumberWithDecimal:qtyTextInDecimal];
                
                currentSalesLine.lineAmount = [unitPriceDecimalInDecimalNumber decimalNumberByMultiplyingBy:qtyDecimalInDecimalNumber];
                lineNoLabel.text = currentSalesLine.lineNo.stringValue;
            }
        }
        else{
            currentSalesLine.itemNo = itemNoLabel.text;
            currentSalesLine.itemName = itemNameLabel.text;
            currentSalesLine.itemUnit = selectedUnit;
            if (![currentSalesLine.quantity isEqualToNumber:[NSNumber numberWithInt:0]]){
                currentSalesLine.quantity = [NSDecimalNumber decimalNumberWithString:qtyTextField.text];
            }
            else{
                qtyTextField.text = [NSNumber numberWithFloat:1.0f].stringValue;
                currentSalesLine.quantity = [NSDecimalNumber decimalNumberWithString:@"1"];
            }
            currentSalesLine.itemUnitPrice = [NSDecimalNumber decimalNumberWithString:newPrice];
            currentSalesLine.orderDate = gf.selectedOrder.orderDate;
            currentSalesLine.manualDiscount = [NSNumber numberWithBool:YES];
            currentSalesLine.autoDiscount = [NSNumber numberWithBool:NO];
            
            NSDecimal unitPriceTextInDecimal = [[NSNumber numberWithFloat:[itemUnitPriceTextField.text floatValue]] decimalValue];
            NSDecimalNumber * unitPriceDecimalInDecimalNumber = [NSDecimalNumber decimalNumberWithDecimal:unitPriceTextInDecimal];
            
            NSDecimal qtyTextInDecimal = [[NSNumber numberWithFloat:[qtyTextField.text floatValue]] decimalValue];
            NSDecimalNumber * qtyDecimalInDecimalNumber = [NSDecimalNumber decimalNumberWithDecimal:qtyTextInDecimal];
            
            currentSalesLine.lineAmount = [unitPriceDecimalInDecimalNumber decimalNumberByMultiplyingBy:qtyDecimalInDecimalNumber];
            lineNoLabel.text = currentSalesLine.lineNo.stringValue;
        }
        
        
        if (newPrice == nil){
            [self setSelectedItem:cell:YES];
        }
        else{
            [self setSelectedItem:cell:NO];
        }
        
        if (currentSalesLine.itemNo != nil){
            NSDecimalNumber *listAmount = [NSDecimalNumber decimalNumberWithString:[gf getListPrice:currentSalesLine.itemNo :currentSalesLine.itemUnit]];
            listAmount = [listAmount decimalNumberByMultiplyingBy:currentSalesLine.quantity];
            if ([currentSalesLine.lineAmount compare:listAmount] == NSOrderedAscending){
                currentSalesLine.discountAmount = [listAmount decimalNumberBySubtracting:currentSalesLine.lineAmount];
                [self showCrossedListPrice:currentSalesLine.discountAmount.stringValue];
            }
        }
        
        [gf saveCurrentChanges];
    } @catch (NSException *ex) {
        [gf deleteSalesLine:currentSalesLine];
        [self showErrorMessage:[NSString stringWithFormat:@"%@. This sales line will be deleted!",ex]];
    }
    //self.isNewSelectedItem = NO;
}

-(void)showCrossedListPrice : (NSString*)discountedAmount{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    UITextField *itemUnitPriceTextField = (UITextField *)[self.selectedCell viewWithTag:301];
    UILabel *itemNoLabel = (UILabel *)[self.selectedCell viewWithTag:202];
    UISegmentedControl *unitSelection = (UISegmentedControl*)[self.selectedCell viewWithTag:401];
    UIImageView *crossLineImageView = (UIImageView*)[self.selectedCell viewWithTag:504];
    UILabel *listPriceLabel = (UILabel *)[self.selectedCell viewWithTag:110];
    NSString *selectedUnit = [unitSelection titleForSegmentAtIndex:unitSelection.selectedSegmentIndex];
    
    
    
    NSString *listPriceText = [gf getListPrice:itemNoLabel.text :selectedUnit];
    
    if ([itemUnitPriceTextField.text isEqualToString:listPriceText]){
        [listPriceLabel setHidden:YES];
        [crossLineImageView setHidden:YES];
    }
    else{
        listPriceLabel.text = [NSString stringWithFormat:@"%@",discountedAmount];
        [listPriceLabel setHidden:NO];
        [crossLineImageView setHidden:NO];
    }
    
}

-(void)showConfirmationMessageIfNeeded{
    UILabel *itemNoLabel = (UILabel *)[self.selectedCell viewWithTag:202];
    UISegmentedControl *unitSelection = (UISegmentedControl*)[self.selectedCell viewWithTag:401];
    NSString *selectedUnit = [unitSelection titleForSegmentAtIndex:unitSelection.selectedSegmentIndex];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    NSNumber *otherQty = [gf getQtyIfItemExistsInOrder:itemNoLabel.text :selectedUnit];
    if ((self.isNewSelectedItem == YES) && (![otherQty isEqualToNumber:[NSNumber numberWithInt:0]])){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Item is already added with a different unit. Qty = %@",otherQty]
                                                        message:@"Do you want to change Quantity?"
                                                       delegate:self
                                              cancelButtonTitle:@"No"
                                              otherButtonTitles:@"Yes", nil];
        [alert show];
        
    }
    else{
        [self updateSalesLine:self.selectedCell :[[NSNumber numberWithFloat:self.changedQty] stringValue]:nil];
    }
}

-(void)showErrorMessage : (NSString*) errMsg{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Catalog Sales Screen Error" message:[NSString stringWithFormat:@"Error : %@",errMsg] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)increaseButtonPressed:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [self setSelectedItem:(UICollectionViewCell *)[[sender superview] superview]:YES];
    UITextField *qtyTextField = (UITextField *)[self.selectedCell viewWithTag:302];
    
    self.changedQty = [qtyTextField.text floatValue];
    self.changedQty +=1;
    
    [self showConfirmationMessageIfNeeded];
}

- (IBAction)decreaseButtonPressed:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [self setSelectedItem:(UICollectionViewCell *)[[sender superview] superview]:YES];
    UITextField *qtyTextField = (UITextField *)[self.selectedCell viewWithTag:302];
    
    self.changedQty = [qtyTextField.text floatValue];
    self.changedQty -=1;
    [self showConfirmationMessageIfNeeded];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    //self.isNewSelectedItem = YES;
    [self setSelectedItem:[collectionView cellForItemAtIndexPath:indexPath]:NO];
}

-(void)setSelectedItem : (UICollectionViewCell*)selectedCell : (BOOL)updateLastItem{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    
    self.selectedCell = selectedCell;
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    if (gf.selectedItemDetails == nil){
        gf.selectedItemDetails = [[SelectedItemDetails alloc] init];
    }
    
    
    UILabel *itemNoLabel = (UILabel *)[selectedCell viewWithTag:202];
    UILabel *itemNameLabel = (UILabel *)[selectedCell viewWithTag:101];
    UISegmentedControl *unitSelection = (UISegmentedControl*)[selectedCell viewWithTag:401];
    
    UIView *syncErrorView = (UIView *)[selectedCell viewWithTag:555];
    
    if (syncErrorView.hidden)
    {
    
    Item *item = [gf getItem:itemNoLabel.text];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      [NSString stringWithString:[NSString stringWithFormat:@"%@.jpeg",item.itemNo]] ];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    
    gf.selectedItemDetails.itemImagePath = path;
    
    gf.selectedItemDetails.itemName = itemNameLabel.text;
    gf.selectedItemDetails.itemNo = itemNoLabel.text;
    gf.selectedItemDetails.itemPackSize = item.packSize;
    gf.selectedItemDetails.itemUnitSize = item.unitSize;
    gf.selectedItemDetails.itemStockQty = item.stockQty.stringValue;
    gf.selectedItemDetails.itemSalesUnit = [unitSelection titleForSegmentAtIndex:unitSelection.selectedSegmentIndex];
    gf.selectedItemDetails.itemUnitPrice = [gf getListPrice:item.itemNo :gf.selectedItemDetails.itemSalesUnit];
    gf.selectedItemDetails.itemBrandName = item.brandName;
    gf.selectedItemDetails.itemCountryName = item.countryName;
    gf.selectedItemDetails.itemSearchDescription = item.searchDescription;
    gf.selectedItemDetails.itemVat = item.vat.stringValue;
    
    LastSales *lastSale = [gf getLastSales:gf.selectedOrder.sellToNo :itemNoLabel.text];
    if (lastSale == nil){
        gf.selectedItemDetails.itemLastSaleDate = @"";
        gf.selectedItemDetails.itemLastSaleQty = @"0";
        gf.selectedItemDetails.itemLastSaleUnit = @"0";
        gf.selectedItemDetails.itemLastSalePrice = @"0.00";
    }
    else{
        NSDateFormatter * formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"dd/MM/yyyy"];
        NSString * dateString = [formatter stringFromDate: lastSale.postingDate];
        
        gf.selectedItemDetails.itemLastSaleDate = dateString;
        gf.selectedItemDetails.itemLastSaleQty = lastSale.salesQty.stringValue;
        gf.selectedItemDetails.itemLastSaleUnit = lastSale.salesUnit;
        gf.selectedItemDetails.itemLastSalePrice = [NSString stringWithFormat:@"%@",lastSale.salesPrice.stringValue];
    }
    
    if ([self.prevItemNo isEqualToString:gf.selectedItemDetails.itemNo] && [self.prevUnit isEqualToString:gf.selectedItemDetails.itemSalesUnit]){
        self.isNewSelectedItem = NO;
    }
    else{
        self.isNewSelectedItem = YES;
    }
    
    if (updateLastItem == YES) {
        self.prevItemNo = gf.selectedItemDetails.itemNo;
        self.prevUnit = gf.selectedItemDetails.itemSalesUnit;
    }
    }
}

-(void)clearSelectedItem{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.selectedItemDetails.itemLastSaleDate = @"";
    gf.selectedItemDetails.itemLastSaleQty = @"0";
    gf.selectedItemDetails.itemLastSaleUnit = @"0";
    gf.selectedItemDetails.itemLastSalePrice = @"0.00";
    
    
    gf.selectedItemDetails.itemImagePath = @"";
    
    gf.selectedItemDetails.itemName = @"";
    gf.selectedItemDetails.itemNo = @"";
    gf.selectedItemDetails.itemPackSize = @"";
    gf.selectedItemDetails.itemUnitSize = @"";
    gf.selectedItemDetails.itemStockQty = @"";
    gf.selectedItemDetails.itemSalesUnit = @"";
    gf.selectedItemDetails.itemUnitPrice = @"";
    gf.selectedItemDetails.itemBrandName = @"";
    gf.selectedItemDetails.itemCountryName = @"";
    gf.selectedItemDetails.itemSearchDescription = @"";
    gf.selectedItemDetails.itemVat = @"";
}
- (IBAction)unitSelectionChanged:(id)sender {
    [self setSelectedItem:(UICollectionViewCell *)[[sender superview] superview]:NO];
    [self changeUnit:NO];
}

-(void)setUnitColor : (UISegmentedControl*) unitSelection : (NSString*)selectedUnit{
    if ([selectedUnit isEqualToString:@"BOX"]){
        unitSelection.tintColor = [UIColor blueColor];
    }
    else if ([selectedUnit isEqualToString:@"EACH"]) {
        unitSelection.tintColor = [UIColor redColor];
    }
    else{
        unitSelection.tintColor = [UIColor blackColor];
    }
}

-(void)changeUnit : (BOOL)setLastUnit{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    
    //self.isNewSelectedItem = YES;
    
    UILabel *lineNoLabel = (UILabel *)[self.selectedCell viewWithTag:201];
    UILabel *itemNoLabel = (UILabel *)[self.selectedCell viewWithTag:202];
    
    //UILabel *itemNameLabel = (UILabel *)[cell viewWithTag:101];
    UITextField *itemUnitPriceTextField = (UITextField *)[self.selectedCell viewWithTag:301];
    UITextField *qtyTextField = (UITextField *)[self.selectedCell viewWithTag:302];
    UISegmentedControl *unitSelection = (UISegmentedControl*)[self.selectedCell viewWithTag:401];
    
    
    UILabel *lastSaleUnitLabel = (UILabel *)[self.selectedCell viewWithTag:103];
    
    if (setLastUnit == YES){
        int counter = 0;
        while (unitSelection.numberOfSegments > counter){
            if ([lastSaleUnitLabel.text isEqualToString:[unitSelection titleForSegmentAtIndex:counter]]){
                unitSelection.selectedSegmentIndex = counter;
                break;
            }
            counter += 1;
        }
    }
    
    NSString *selectedUnit = [unitSelection titleForSegmentAtIndex:unitSelection.selectedSegmentIndex];
    [self setUnitColor:unitSelection : selectedUnit];
    
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    SalesLine *currentSalesLine = [gf getSalesLineByUnit:gf.selectedOrder.documentNo :itemNoLabel.text : selectedUnit];
    if (currentSalesLine == nil){
        lineNoLabel.text = @"0";
        qtyTextField.text = @"0";
        itemUnitPriceTextField.text = [gf getBestPrice:itemNoLabel.text :selectedUnit : @"0"];
        [self showCrossedListPrice:@"0"];
    }
    else{
        lineNoLabel.text = currentSalesLine.lineNo.stringValue;
        qtyTextField.text = currentSalesLine.quantity.stringValue;
        itemUnitPriceTextField.text = currentSalesLine.itemUnitPrice.stringValue;
        
        NSDecimalNumber *listAmount = [NSDecimalNumber decimalNumberWithString:[gf getListPrice:currentSalesLine.itemNo :currentSalesLine.itemUnit]];
        listAmount = [listAmount decimalNumberByMultiplyingBy:currentSalesLine.quantity];
        if ([currentSalesLine.lineAmount compare:listAmount] == NSOrderedAscending){
            currentSalesLine.discountAmount = [listAmount decimalNumberBySubtracting:currentSalesLine.lineAmount];
            [self showCrossedListPrice:currentSalesLine.discountAmount.stringValue];
        }
    }
    
    [self setFreeLineQuantity:self.selectedCell :selectedUnit];
}

- (IBAction)addLastSale:(id)sender {
    
    [self setSelectedItem:(UICollectionViewCell *)[[sender superview] superview]:YES];
    
    UILabel *lastSalePriceLabel = (UILabel *)[self.selectedCell viewWithTag:104];
    if (![lastSalePriceLabel.text isEqualToString:@""]){
        
        UILabel *lineNoLabel = (UILabel *)[self.selectedCell viewWithTag:201];
        UILabel *itemNoLabel = (UILabel *)[self.selectedCell viewWithTag:202];
        
        //UILabel *itemNameLabel = (UILabel *)[cell viewWithTag:101];
        UITextField *itemUnitPriceTextField = (UITextField *)[self.selectedCell viewWithTag:301];
        UITextField *qtyTextField = (UITextField *)[self.selectedCell viewWithTag:302];
        UISegmentedControl *unitSelection = (UISegmentedControl*)[self.selectedCell viewWithTag:401];
        UILabel *lastSalePriceLabel = (UILabel *)[self.selectedCell viewWithTag:104];
        
        
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        
        
        //itemUnitPriceTextField.text = gf.selectedItemDetails.itemUnitPrice;
        
        
        [self changeUnit:YES];
        
        NSString *selectedUnit = [unitSelection titleForSegmentAtIndex:unitSelection.selectedSegmentIndex];
        
        
        SalesLine *currentSalesLine = [gf getSalesLineByUnit:gf.selectedOrder.documentNo :itemNoLabel.text : selectedUnit];
        if (currentSalesLine == nil){
            lineNoLabel.text = @"0";
            qtyTextField.text = @"0";
        }
        else{
            lineNoLabel.text = currentSalesLine.lineNo.stringValue;
            qtyTextField.text = currentSalesLine.quantity.stringValue;
        }
        float test = [qtyTextField.text floatValue];
        test +=1;
        itemUnitPriceTextField.text = [lastSalePriceLabel.text substringFromIndex:1];
        [self updateSalesLine:self.selectedCell :nil:lastSalePriceLabel.text];
        [self updateSalesLine:self.selectedCell :[[NSNumber numberWithFloat:test] stringValue]:nil];
    }
}

- (IBAction)addFreeButtonPressed:(id)sender {
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    SalesLine *currentSalesLine;
    @try {
        
        [self setSelectedItem:(UICollectionViewCell *)[[sender superview] superview]:YES];
        UILabel *itemNameLabel = (UILabel *)[self.selectedCell viewWithTag:101];
        UISegmentedControl *unitSelection = (UISegmentedControl*)[self.selectedCell viewWithTag:401];
        UILabel *listPriceLabel = (UILabel *)[self.selectedCell viewWithTag:110];
        
        NSString *selectedUnit = [unitSelection titleForSegmentAtIndex:unitSelection.selectedSegmentIndex];
        currentSalesLine = [gf getFreeSalesLineByUnit :gf.selectedItemDetails.itemNo : selectedUnit];
        if (currentSalesLine == nil){
            currentSalesLine = [gf getFreeSalesLine:@"0" :YES];
        }
        
        currentSalesLine.itemNo = gf.selectedItemDetails.itemNo;
        currentSalesLine.itemName = itemNameLabel.text;
        currentSalesLine.itemUnit = selectedUnit;
        currentSalesLine.itemUnitPrice = [NSDecimalNumber decimalNumberWithString:@"0"];
        
        float test = currentSalesLine.quantity.floatValue;
        test +=1;
        
        currentSalesLine.manualDiscount = [NSNumber numberWithBool:YES];
        currentSalesLine.quantity = [NSNumber numberWithFloat:test];
        currentSalesLine.discountAmount = [currentSalesLine.quantity decimalNumberByMultiplyingBy:[NSDecimalNumber decimalNumberWithString:listPriceLabel.text]];
        currentSalesLine.orderDate = gf.selectedOrder.orderDate;
        currentSalesLine.lineAmount = [NSNumber numberWithInt:0];
        
        [gf saveCurrentChanges];
        [self setFreeLineQuantity:self.selectedCell:selectedUnit];
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Free Item Added" message:[NSString stringWithFormat:@"Free item quantity is %@",currentSalesLine.quantity.stringValue] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    } @catch (NSException *ex) {
        [gf deleteSalesLine:currentSalesLine];
        [self showErrorMessage:[NSString stringWithFormat:@"%@.This sales line will be deleted!",ex]];
    }
}

-(void)showComments{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];
    CommentsViewController *commentFilter =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"CommentsViewController"];
    self.popover = [[UIPopoverController alloc]initWithContentViewController:commentFilter];
    self.popover.delegate = self;
    self.popover.popoverContentSize = CGSizeMake(400, 300);
    [self.popover presentPopoverFromBarButtonItem:self.noteButton
                         permittedArrowDirections:UIPopoverArrowDirectionUp
                                         animated:YES];
}

- (IBAction)calculatorButtonPressed:(id)sender {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];
    CalculatorViewController *calculator =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"CalculatorViewController"];
    self.popover = [[UIPopoverController alloc]initWithContentViewController:calculator];
    self.popover.delegate = self;
    [self.popover presentPopoverFromRect:self.calculatorButton.bounds inView:self.calculatorButton permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    [self.popover setPopoverContentSize:CGSizeMake(305,443) animated:YES];
}

- (IBAction)testButtonPressed:(id)sender {
    
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];
    MenuOptionsViewController *menuOptions =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"MenuOptionsViewController"];
    self.popover = [[UIPopoverController alloc]initWithContentViewController:menuOptions];
    self.popover.delegate = self;
    [self.popover presentPopoverFromRect:self.menuOptionsButton.bounds inView:self.menuOptionsButton permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    [self.popover setPopoverContentSize:CGSizeMake(500,375) animated:YES];
    
}

- (IBAction)itemCardButtonPressed:(id)sender {
    
    [self setSelectedItem:(UICollectionViewCell *)[[sender superview] superview]:NO];
    
    
    BKTPopinControllerViewController *popin = [[BKTPopinControllerViewController alloc] init];
    //[popin setPopinOptions:BKTPopinDisableAutoDismiss];
    [popin setPopinTransitionStyle:BKTPopinTransitionStyleSpringySlide];
    [popin setPopinTransitionDirection:BKTPopinTransitionDirectionTop];
    [self presentPopinController:popin animated:YES completion:^{
        
    }];
}

-(void)showLines{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    SalesLineViewController *salesLineView = [self.storyboard instantiateViewControllerWithIdentifier:@"SalesLineViewController"];
    [self.navigationController pushViewController:salesLineView animated:YES];
}

-(void)dismissItemCardWithBarcode{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    self.popin = nil;
    [self.collectionView reloadData];
}

-(void)dismissCommentsPopover{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [self setCommentButtonFont];
}

-(void)dismissPrintOptionsPopover{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
}
-(void)dismissMenuOptionsPopover{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    [self.collectionView reloadData];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    [self.collectionView performBatchUpdates:^{
        [self.collectionView.collectionViewLayout invalidateLayout];
        [self.collectionView setCollectionViewLayout:self.collectionView.collectionViewLayout animated:YES];
    } completion:^(BOOL finished) {
        if (gf.isSearchItemsNeeded){
            [self startSearchItems];
        }
        [self.collectionView reloadData];
    }];
}

-(void)startSearchItems{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    self.isFiltered = YES;
    gf.searchItemResults = [[NSMutableArray alloc] init];
    
    if (![self.searchFilter1.text isEqualToString:@""]){
        [self searchItemDescriptions:self.searchFilter1.text];
    }
    if (![self.searchFilter2.text isEqualToString:@""]){
        [self searchItemDescriptions:self.searchFilter2.text];
    }
    
    if ([self checkIfNoFilter]){
        self.isFiltered = NO;
    }
    
    [self.collectionView reloadData];
    
}

-(BOOL)checkIfNoFilter{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    if ((![self.searchFilter1.text isEqualToString:@""]) || (![self.searchFilter2.text isEqualToString:@""])){
        return NO;
    }
    else{
        return YES;
    }
}

-(void)searchItemDescriptions : (NSString*)searchText{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    NSRange itemRange;
    if (gf.searchItemResults.count == 0){
        for (Item *item in gf.items){
            if (self.isSearchIdOn == NO){
                itemRange = [item.name rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if (itemRange.location != NSNotFound){
                    [gf.searchItemResults addObject:item];
                }
                else{
                    itemRange = [item.unitSize rangeOfString:searchText options:NSCaseInsensitiveSearch];
                    if (itemRange.location != NSNotFound){
                        [gf.searchItemResults addObject:item];
                    }
                    //else{
                    //    itemRange = [item.packSize rangeOfString:searchText options:NSCaseInsensitiveSearch];
                    //    if (itemRange.location != NSNotFound){
                    //        [gf.searchItemResults addObject:item];
                    //    }
                    //}
                }
            }
            else{
                itemRange = [item.searchDescription rangeOfString:searchText];
                if ([item.searchDescription isEqualToString:searchText]){
                    [gf.searchItemResults addObject:item];
                }
            }
        }
    }
    else{
        self.filteredSearchResults = [gf.searchItemResults copy];
        for (Item *item in self.filteredSearchResults){
            if (self.isSearchIdOn == NO){
                itemRange = [item.name rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if (itemRange.location == NSNotFound){
                    itemRange = [item.unitSize rangeOfString:searchText options:NSCaseInsensitiveSearch];
                    if (itemRange.location == NSNotFound){
                        //    itemRange = [item.packSize rangeOfString:searchText options:NSCaseInsensitiveSearch];
                        //    if (itemRange.location == NSNotFound){
                        [gf.searchItemResults removeObject:item];
                        //    }
                    }
                }
            }
            else{
                itemRange = [item.searchDescription rangeOfString:searchText options:NSCaseInsensitiveSearch];
                if (itemRange.location == NSNotFound){
                    [gf.searchItemResults removeObject:item];
                }
            }
        }
    }
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if (searchText.length == 0)
    {
        [self startSearchItems];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self startSearchItems];
}

-(void)SearchItemBarcodes{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.selectedItemDetails = nil;
    [gf setSelectedItemByBarcode:self.decoderData];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [UIView beginAnimations:@"collectionView" context:nil];
    self.collectionView.frame = CGRectMake(self.collectionView.frame.origin.x, self.collectionView.frame.origin.y, self.collectionWidth, self.collectionHeight);
    [UIView commitAnimations];
    return CGSizeMake(self.cellWidth, self.cellHeight);
}

- (void)prepareCollectionViewForDefaultView{
    self.cellWidth = 369;
    self.cellHeight = 210;
    self.collectionWidth = 758;
    self.collectionHeight = 890;
    
    UITextField *itemUnitPriceTextField = (UITextField *)[self.selectedCell viewWithTag:301];
    UITextField *qtyTextField = (UITextField *)[self.selectedCell viewWithTag:302];
    UIImageView *itemImageView = (UIImageView*)[self.selectedCell viewWithTag:502];
    
    [UIView beginAnimations:@"collectionView" context:nil];
    
    [self.printButton setEnabled:YES];
    [self.noteButton setEnabled:YES];
    [self.linesButton setEnabled:YES];
    [self.navigationItem setHidesBackButton:NO animated:YES];
    
    UICollectionViewFlowLayout *flow = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
    flow.sectionInset = UIEdgeInsetsMake(0, 10, 0, 0);
    flow.minimumLineSpacing = 10;
    [itemUnitPriceTextField setHidden:NO];
    [qtyTextField setHidden:NO];
    //imageView.frame = CGRectMake(0, 0, 115, 115);
    itemImageView.frame = CGRectMake(0, 0, 115, 115);
    [UIView commitAnimations];
}

@end
