//
//  SyncViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "SyncViewController.h"
#import "MBProgressHUD.h"
#import <unistd.h>

@interface SyncViewController ()<MBProgressHUDDelegate> {
    MBProgressHUD *HUD;
    long long expectedLength;
    long long currentLength;
}


@end

@implementation SyncViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"ipadmini_updatebg.jpg"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.progress = 0.0f;
}

- (void)myProgressTask {
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    // This just increases the progress indicator in a loop
    while (!gf.isSyncCompleted) {
        //progress += 0.01f;
        //HUD.progress = progress;
        //usleep(50000);
    }
    if ([gf.errorMessage isEqualToString:@""]){
        gf.errorMessage = @"Your device is up-to-date now!";
    }
    
    if (![gf.errorMessage isEqualToString:@""]){
        [self showErrorMessage:gf.errorMessage];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showErrorMessage : (NSString*) errMsg{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Sync Result"] message:[NSString stringWithFormat:@"%@",errMsg] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)newInstallation:(id)sender {
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.progress = 0.0f;
    gf.isSyncCompleted = NO;
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    HUD.dimBackground = YES;
    HUD.delegate = self;
    HUD.labelText = @"Updating";
    HUD.detailsLabelText = @"Please Wait";
    [HUD showWhileExecuting:@selector(myProgressTask) onTarget:self withObject:nil animated:YES];
    
    self.sync = [SyncFunctions defaultService];
    //[self followBusyStatus];
    [self.sync checkAzure];
}

- (IBAction)getAllImagesButtonPressed:(id)sender {
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    [gf getAllImages];
}

- (IBAction)getChangesOnlyButtonPressed:(id)sender {
    self.sync = [SyncFunctions defaultService];
    
    [self.sync getChangeOnly];
}
@end
