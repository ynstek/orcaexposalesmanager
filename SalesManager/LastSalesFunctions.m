//
//  LastSalesFunctions.m
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "LastSalesFunctions.h"
#import "AppDelegate.h"

@interface LastSalesFunctions() <MSFilter>

@property (nonatomic, strong) MSTable *lastSaleTable;
@property (nonatomic) NSInteger busyCount;

@end
@implementation LastSalesFunctions
@synthesize lastSaleRecords;

- (void)emptyCoreDataTable:(NSString*)tableName : (NSManagedObject*) table{
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:tableName];
    
    NSArray *fetchedObjects =
    [cdh.context executeFetchRequest:request error:nil];
    
    for(table in fetchedObjects){
        [cdh.context deleteObject:table];
    }
}

- (void)loadDataRecursiveForQuery:(MSQuery *)query withCompletion:(CompletionBlock)completion
{
    query.includeTotalCount = YES;
    query.fetchLimit = 1000; //note: you can adjust this to whatever amount is optimum
    query.fetchOffset = self.lastSaleResults.count;
    
    [query readWithCompletion:^(NSArray *items,NSInteger totalCount,NSError *error){
        if(!error) {
            //add the items to our local copy
            [self.lastSaleResults addObjectsFromArray:items];
            
            NSLog([NSString stringWithFormat:@"%lu", (unsigned long)[self.lastSaleResults count]]);
            if (totalCount > [self.lastSaleResults count]) {
                [self loadDataRecursiveForQuery:query withCompletion:completion];
            } else {
                completion();
            }
        }
        else{
            [self logErrorIfNotNil:error];
            completion();
        }
    }];
}

-(void)updateLastSale : (NSDictionary*) recDic{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"LastSales"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"customerNo == %@ and itemNo == %@ and salesUnit == %@",[recDic objectForKey:@"primarykey"],[recDic objectForKey:@"primarykey2"],[recDic objectForKey:@"primarykey3"]];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    LastSales *lastSale;
    if ([fetchedObjects count] == 0){
        lastSale = [NSEntityDescription insertNewObjectForEntityForName:@"LastSales" inManagedObjectContext:cdh.context];
    }
    else{
        lastSale = fetchedObjects[0];
    }
    
    lastSale.customerNo = [recDic objectForKey:@"customerno"];
    lastSale.itemNo = [recDic objectForKey:@"itemno"];
    lastSale.salesPrice = [recDic objectForKey:@"salesprice"];
    lastSale.salesQty = [recDic objectForKey:@"salesqty"];
    lastSale.salesUnit = [recDic objectForKey:@"salesunit"];
    lastSale.postingDate = [recDic objectForKey:@"postingdate"];
}

-(void)updateCoreDataTable : (NSString*)customerNo : (NSString*)itemNo : (NSString*)itemUnit{
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"customerno == %@ and itemno == %@ and salesunit == %@",customerNo,itemNo,itemUnit];
    [self.lastSaleTable readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.lastSaleRecords = [results mutableCopy];
         self.lastSaleResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:self.lastSaleTable predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.lastSaleResults){
                 [self updateLastSale:recDic];
             }
             [cdh saveContext];
         }];
     }];
    
}
-(void)insertCoreDataTable : (MSTable*) table{
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    cdh.activeUser.isReadyToUse = [NSNumber numberWithBool:NO];
    NSLog(cdh.activeUser.salesPersonCode);
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"salespersonecode == %@",cdh.activeUser.salesPersonCode];
    [table readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.lastSaleRecords = [results mutableCopy];
         self.lastSaleResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:table predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.lastSaleResults){
                 if ([table.name isEqualToString:@"LastSales"]){
                     LastSales *lastSales = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                     lastSales.customerNo = [recDic objectForKey:@"customerno"];
                     lastSales.itemNo = [recDic objectForKey:@"itemno"];
                     lastSales.salesPrice = [recDic objectForKey:@"salesprice"];
                     lastSales.salesQty = [recDic objectForKey:@"salesqty"];
                     lastSales.salesUnit = [recDic objectForKey:@"salesunit"];
                     lastSales.postingDate = [recDic objectForKey:@"postingdate"];
                     lastSales.salesPersonCode = [recDic objectForKey:@"salespersoncode"];
                 }
             }
             [cdh saveContext];
             gf.isSyncCompleted = YES;
             NSLog(@"LastSales saved!");
         }];
     }];
}
- (void)logErrorIfNotNil:(NSError *) error
{
    if (error)
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.errorMessage = error.localizedDescription;
        gf.progress = 1.0f;
        NSLog(@"ERROR %@", error);
    }
}

- (void)busy:(BOOL)busy
{
    // assumes always executes on UI thread
    if (busy)
    {
        if (self.busyCount == 0 && self.busyUpdate != nil)
        {
            self.busyUpdate(YES);
        }
        self.busyCount++;
    }
    else
    {
        if (self.busyCount == 1 && self.busyUpdate != nil)
        {
            self.busyUpdate(FALSE);
        }
        self.busyCount--;
    }
}

#pragma mark * MSFilter methods
+ (LastSalesFunctions *)defaultService
{
    static LastSalesFunctions* service;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[LastSalesFunctions alloc] init];
    });
    return service;
}

-(LastSalesFunctions *)init
{
    self = [super init];
    if (self)
    {
        // Initialize the Mobile Service client with your URL and key
        MSClient *client = [MSClient clientWithApplicationURLString:@"https://expoipadapp.azure-mobile.net/" applicationKey:@"MgayYYCxnOsHFtZJhTbtGPmFeeTdDU98"];
        // Add a Mobile Service filter to enable the busy indicator
        self.client = [client clientWithFilter:self];
        self.lastSaleTable = [_client tableWithName:@"LastSales"];
        self.lastSaleRecords = [[NSMutableArray alloc] init];
        self.busyCount = 0;
    }
    return self;
}

- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response
{
    // A wrapped response block that decrements the busy counter
    MSFilterResponseBlock wrappedResponse = ^(NSHTTPURLResponse *innerResponse, NSData *data, NSError *error)
    {
        [self busy:NO];
        response(innerResponse, data, error);
    };
    
    // Increment the busy counter before sending the request
    [self busy:YES];
    next(request, wrappedResponse);
}
@end
