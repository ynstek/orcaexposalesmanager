//
//  JSONHelper.m
//  Orca Mobile Sales
//
//  Created by Cihan TASKIN on 19/09/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "JSONHelper.h"

@implementation JSONHelper

+(NSDictionary *)syncJSONDataFromURL:(NSString *)urlString
{
    // This function takes the URL of a web service, calls it, and either returns "nil", or a NSDictionary,
    // describing the JSON data that was returned.
    //
    NSError *error;
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // Call the web service, and (if it's successful) store the raw data that it returns
    NSData *data = [ NSURLConnection sendSynchronousRequest:request returningResponse: nil error:&error ];
    if (!data)
    {
        NSLog(@"Download Error: %@", error.localizedDescription);
        return nil;
    }
    
    // Parse the (binary) JSON data from the web service into an NSDictionary object
    id dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];

    
    return dictionary;
}



+(NAVResult *)loadJSONDataFromURL:(NSString *)urlString
{
    NAVResult* result = [[NAVResult alloc] init];
 
    NSError *error;
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // Call the web service, and (if it's successful) store the raw data that it returns
    NSData *data = [ NSURLConnection sendSynchronousRequest:request returningResponse: nil error:&error ];
    if (!data)
    {
        result.WasSuccessful = 0;
        result.Exception = [NSString stringWithFormat:@"Download Error: %@", error.localizedDescription];
        return result;
    }
    
    NSString *resultString = [[NSString alloc] initWithBytes: [data bytes] length:[data length] encoding: NSUTF8StringEncoding];
    
    if (resultString == nil)
    {
        result.WasSuccessful = 0;
        result.Exception = [NSString stringWithFormat:@"An exception occurred: %@", error.localizedDescription];
        return result;
    }
    
    // We need to convert our JSON string into a NSDictionary object
    NSData *data2 = [resultString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data2 options:kNilOptions error:&error];
    if (dictionary == nil) {
        result.WasSuccessful = 0;
        result.Exception = [NSString stringWithFormat:@"JSON Error: %@", error];
        return result;
    }
    
    result.WasSuccessful = [[dictionary valueForKey:@"WasSuccessful"] integerValue];
    result.Exception = [dictionary valueForKey:@"Exception"];
    result.PassValueIfNeeded = [dictionary valueForKey:@"PassValueIfNeeded"];
    return result;
}

+(NAVResult*)postJSONDataToURL:(NSString *)urlString JSONdata:(NSString*)JSONdata
{
    NAVResult* result = [[NAVResult alloc] init];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSData *postData = [JSONdata dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", [postData length]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSError *error;
    NSData *data = [ NSURLConnection sendSynchronousRequest:request returningResponse: nil error:&error ];
    if (!data)
    {
        result.WasSuccessful = 0;
        result.Exception = [NSString stringWithFormat:@"Could not call the web service: %@", urlString];
        return result;
    }
    
    
    // The JSON web call did complete, but perhaps it hit an error (such as a foreign key
    // constraint, if we've accidentally sent an unknown User_ID to the [Survey] INSERT command).
    //
    // The ResultString will return a "WasSuccessful" value, and an Exception value:
    //     { "Exception":"", "WasSuccesful":1 }
    // or this
    //     { "Exception:":"The INSERT statement conflicted with ...", "WasSuccesful":0 }"
    //
    
    NSString *resultString = [[NSString alloc] initWithBytes: [data bytes] length:[data length] encoding: NSUTF8StringEncoding];
    
    if (resultString == nil)
    {
        result.WasSuccessful = 0;
        result.Exception = [NSString stringWithFormat:@"An exception occurred: %@", error.localizedDescription];
        return result;
    }
    
    // We need to convert our JSON string into a NSDictionary object
    NSData *data2 = [resultString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data2 options:kNilOptions error:&error];
    if (dictionary == nil)
    {
        result.WasSuccessful = 0;
        result.Exception = @"Unable to parse the JSON return string.";
        return result;
    }
    
    result.WasSuccessful = [[dictionary valueForKey:@"WasSuccessful"] integerValue];
    result.Exception = [dictionary valueForKey:@"Exception"];
    result.PassValueIfNeeded = [dictionary valueForKey:@"PassValueIfNeeded"];
    
    return result;
}
+(void)showError:(NSString *)messageTitle : (NSString*)messageContent
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:messageTitle
                                  message:messageContent
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        
        [alertView show];
    });
}
@end
