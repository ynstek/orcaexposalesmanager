//
//  LastInvoiceViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 05/03/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "LastInvoiceViewController.h"
#import "AppDelegate.h"

@interface LastInvoiceViewController ()
@property NSMutableArray *lastInvoiceArray,*searchResult;
@end

@implementation LastInvoiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self refreshView];
}
-(void)refreshView{
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
    
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@",[gf getLastInvoiceNo : YES]];
    [request setPredicate:filter];
    
    request.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"lineNo" ascending:YES],nil];
    
    self.lastInvoiceArray = [[cdh.context executeFetchRequest:request error:nil] mutableCopy];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.isFiltered == NO)
    {
        return [self.lastInvoiceArray count];
    }
    else{
        return [self.searchResult count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                    forIndexPath:indexPath];
    
    UILabel *itemNameLabel = (UILabel*)[cell viewWithTag:101];
    UILabel *itemQuantityLabel = (UILabel*)[cell viewWithTag:102];
    UILabel *itemUnitLabel = (UILabel*)[cell viewWithTag:103];
    UILabel *itemUnitPriceLabel = (UILabel*)[cell viewWithTag:104];
    UILabel *lineAmountLabel = (UILabel*)[cell viewWithTag:105];
    SalesLine *salesLine;
    if (self.isFiltered == NO){
        salesLine = [self.lastInvoiceArray objectAtIndex:indexPath.row];
    }
    else{
        salesLine = [self.searchResult objectAtIndex:indexPath.row];
    }
    itemNameLabel.text = salesLine.itemName;
    itemQuantityLabel.text = salesLine.quantity.stringValue;
    
    
    itemUnitLabel.text = salesLine.itemUnit;
    itemUnitPriceLabel.text = [NSString stringWithFormat:@"£ %@",salesLine.itemUnitPrice.stringValue];
    lineAmountLabel.text = [NSString stringWithFormat:@"£ %@",salesLine.lineAmount.stringValue];
    
    return cell;
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if (searchText.length == 0)
    {
        self.isFiltered = NO;
    }
    else{
        
        self.isFiltered = YES;
        
        self.searchResult = [[NSMutableArray alloc] init];
        
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        
        CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
        
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
        
        NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@",[gf getLastInvoiceNo : YES]];
        [request setPredicate:filter];
        self.lastInvoiceArray = [[cdh.context executeFetchRequest:request error:nil] mutableCopy];
        
        
        
        for (SalesLine *salesLine in self.lastInvoiceArray){
            NSRange itemNameRange = [salesLine.itemName rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (itemNameRange.location != NSNotFound){
                [self.searchResult addObject:salesLine];
            }
        }
    }
    [self.tableView reloadData];
}
@end
