//
//  NoSeries.h
//  SalesManager
//
//  Created by Cihan TASKIN on 24/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface NoSeries : NSManagedObject

@property (nonatomic, retain) NSString * seriesCode;
@property (nonatomic, retain) NSNumber * seriesNo;
@property (nonatomic, retain) NSString * userNoSeriesCode;

@end
