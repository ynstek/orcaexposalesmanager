//
//  LastSales.m
//  SalesManager
//
//  Created by Cihan TASKIN on 08/04/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "LastSales.h"


@implementation LastSales

@dynamic customerNo;
@dynamic itemNo;
@dynamic postingDate;
@dynamic salesPrice;
@dynamic salesQty;
@dynamic salesUnit;
@dynamic salesPersonCode;

@end
