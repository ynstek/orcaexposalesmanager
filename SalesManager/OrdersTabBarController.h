//
//  OrdersTabBarController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 09/03/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrdersTabBarController : UITabBarController

@end
