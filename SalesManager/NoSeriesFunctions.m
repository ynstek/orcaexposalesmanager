//
//  NoSeriesFunctions.m
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "NoSeriesFunctions.h"
#import "AppDelegate.h"

@interface NoSeriesFunctions() <MSFilter>

@property (nonatomic, strong) MSTable *noSerTable;
@property (nonatomic) NSInteger busyCount;

@end
@implementation NoSeriesFunctions
@synthesize noSerRecords;

- (void)emptyCoreDataTable:(NSString*)tableName : (NSManagedObject*) table{
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:tableName];
    
    NSArray *fetchedObjects =
    [cdh.context executeFetchRequest:request error:nil];
    
    for(table in fetchedObjects){
        [cdh.context deleteObject:table];
    }
}

- (void)loadDataRecursiveForQuery:(MSQuery *)query withCompletion:(CompletionBlock)completion
{
    query.includeTotalCount = YES;
    query.fetchLimit = 1000; //note: you can adjust this to whatever amount is optimum
    query.fetchOffset = self.noSerResults.count;
    
    [query readWithCompletion:^(NSArray *items,NSInteger totalCount,NSError *error){
        if(!error) {
            //add the items to our local copy
            [self.noSerResults addObjectsFromArray:items];
            
            if (totalCount > [self.noSerResults count]) {
                [self loadDataRecursiveForQuery:query withCompletion:completion];
            } else {
                completion();
            }
        }
        else{
            [self logErrorIfNotNil:error];
            completion();
        }
    }];
}

- (void)updateTableRow : (NoSeries*)currentNoSer completion:(CompletionBlock)completion  {
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"usernoseriescode == %@",currentNoSer.userNoSeriesCode];
    [self.noSerTable readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.noSerRecords = [results mutableCopy];
         self.noSerResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:self.noSerTable predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.noSerResults){
                 NSDictionary *params = @{ @"table" : self.noSerTable.name };
                 
                 NSDictionary *noSerRec = @{@"id":[recDic objectForKey:@"id"], @"seriescode" : currentNoSer.seriesCode, @"seriesno" : currentNoSer.seriesNo, @"usernoseriescode" : currentNoSer.userNoSeriesCode };
                 
                 [self.noSerTable update:noSerRec parameters:params completion:^(NSDictionary *result, NSError *error) {
                     if (error != nil){
                         cdh.activeUser.toBeSynced = [NSNumber numberWithBool:YES];
                     }
                     else{
                         [self logErrorIfNotNil:error];
                     }
                     completion();
                 }];
             }
         }];
     }];
}

- (void)updateNoSerOnCloud : (NSString*)fake completion:(CompletionBlock)completion {
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NoSeries"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"userNoSeriesCode == %@",cdh.activeUser.userNoSeriesCode];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        [self updateTableRow:fetchedObjects[0] completion:^{
            completion();
        }];
    }
}
-(void)insertCoreDataTable : (MSTable*) table{
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    cdh.activeUser.isReadyToUse = [NSNumber numberWithBool:NO];
    //NSPredicate * predicate = [NSPredicate predicateWithFormat:@"name != %@",@""];
    [table readWithPredicate:nil completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.noSerRecords = [results mutableCopy];
         self.noSerResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:table predicate:nil];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.noSerResults){
                 if ([table.name isEqualToString:@"NoSeries"]){
                     if ([recDic objectForKey:@"seriesno"] != nil){
                         NoSeries *noSeries = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                         noSeries.seriesCode = [recDic objectForKey:@"seriescode"];
                         noSeries.seriesNo = [recDic objectForKey:@"seriesno"];
                         noSeries.userNoSeriesCode = [recDic objectForKey:@"usernoseriescode"];
                     }
                 }
             }
             [cdh saveContext];
             NSLog(@"Noseries saved!");
         }];
     }];
}

#pragma mark * MSFilter methods

- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response
{
    // A wrapped response block that decrements the busy counter
    MSFilterResponseBlock wrappedResponse = ^(NSHTTPURLResponse *innerResponse, NSData *data, NSError *error)
    {
        [self busy:NO];
        response(innerResponse, data, error);
    };
    
    // Increment the busy counter before sending the request
    [self busy:YES];
    next(request, wrappedResponse);
}

+ (NoSeriesFunctions *)defaultService
{
    static NoSeriesFunctions* service;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[NoSeriesFunctions alloc] init];
    });
    
    return service;
}

-(NoSeriesFunctions *)init
{
    self = [super init];
    
    if (self)
    {
        // Initialize the Mobile Service client with your URL and key
        MSClient *client = [MSClient clientWithApplicationURLString:@"https://expoipadapp.azure-mobile.net/"
                                                     applicationKey:@"MgayYYCxnOsHFtZJhTbtGPmFeeTdDU98"];
        
        // Add a Mobile Service filter to enable the busy indicator
        self.client = [client clientWithFilter:self];
        self.noSerTable = [_client tableWithName:@"NoSeries"];
        self.noSerRecords = [[NSMutableArray alloc] init];
        self.busyCount = 0;
    }
    return self;
}

- (void)busy:(BOOL)busy
{
    // assumes always executes on UI thread
    if (busy)
    {
        if (self.busyCount == 0 && self.busyUpdate != nil)
        {
            self.busyUpdate(YES);
        }
        self.busyCount++;
    }
    else
    {
        if (self.busyCount == 1 && self.busyUpdate != nil)
        {
            self.busyUpdate(FALSE);
        }
        self.busyCount--;
    }
}

- (void)logErrorIfNotNil:(NSError *) error
{
    if (error)
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.errorMessage = error.localizedDescription;
        gf.progress = 1.0f;
        NSLog(@"ERROR %@", error.localizedDescription);
    }
}

@end
