//
//  User.m
//  SalesManager
//
//  Created by Cihan TASKIN on 28/05/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic currentUser;
@dynamic isReadyToUse;
@dynamic lastUpdatedDate;
@dynamic name;
@dynamic password;
@dynamic salesPersonCode;
@dynamic toBeSynced;
@dynamic userNoSeriesCode;
@dynamic role;

@end
