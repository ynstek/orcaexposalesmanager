//
//  NAVResult.h
//  Orca Mobile Sales
//
//  Created by Cihan TASKIN on 11/10/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NAVResult : NSObject

@property(nonatomic) NSInteger WasSuccessful;
@property(strong, nonatomic) NSString * Exception;
@property(strong, nonatomic) NSString * PassValueIfNeeded;

@end
