//
//  SyncFunctions.m
//  SalesManager
//
//  Created by Cihan TASKIN on 02/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "SyncFunctions.h"
#import "AppDelegate.h"
#import <WindowsAzureMobileServices/WindowsAzureMobileServices.h>


#pragma mark * Private interace


@interface SyncFunctions() <MSFilter>

@property (nonatomic, strong) MSTable *balanceHistoryTable,*categoriesTable,*customerTable,
                                        *devicesTable,*itemTable,*itemUnitsTable,*lastSalesTable,
                                        *noSeriesTable,*salesHeaderTable,*salesLineTable,*salesPriceTable,
                                        *userTable,*changeLogTable;



@property (nonatomic, strong) Customer *passedCustomer;
@property (nonatomic, strong) SalesHeader *passedSalesHeader;
@property (nonatomic, strong) SalesLine *passedSalesLine;
@property (nonatomic, strong) ItemUnits *passedItemUnits;
@property (nonatomic, strong) Categories *passedCategories;
@property (nonatomic, strong) NoSeries *passedNoseries;
@property (nonatomic, strong) LastSales *passedLastSales;
@property (nonatomic, strong) SalesPrice *passedSalesPrice;
@property (nonatomic, strong) User *passedUser;
@property (nonatomic, strong) BalanceHistory *passedBalanceHistory;
@property (nonatomic, strong) Item *passedItem;


@end

@implementation SyncFunctions

+ (SyncFunctions *)defaultService
{
    // Create a singleton instance of QSTodoService
    static SyncFunctions* service;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[SyncFunctions alloc] init];
    });
    
    return service;
}

-(SyncFunctions *)init
{
    self = [super init];
    
    if (self)
    {
        //WE NEED TO CREATE SEPERATE TABLE AND ITEMS VARIABLES FOR EACH TABLE!!!!
        
        // Initialize the Mobile Service client with your URL and key
        MSClient *client = [MSClient clientWithApplicationURLString:@"https://expoipadapp.azure-mobile.net/"
                                                     applicationKey:@"MgayYYCxnOsHFtZJhTbtGPmFeeTdDU98"];
        
        // Add a Mobile Service filter to enable the busy indicator
        self.client = [client clientWithFilter:self];
        
        // Create an MSTable instance to allow us to work with the TodoItem table
        self.balanceHistoryTable = [_client tableWithName:@"BalanceHistory"];
        self.categoriesTable = [_client tableWithName:@"Categories"];
        self.customerTable = [_client tableWithName:@"Customer"];
        self.devicesTable = [_client tableWithName:@"Devices"];
        self.itemTable = [_client tableWithName:@"Item"];
        self.itemUnitsTable = [_client tableWithName:@"ItemUnits"];
        self.lastSalesTable = [_client tableWithName:@"LastSales"];
        self.noSeriesTable = [_client tableWithName:@"NoSeries"];
        self.salesHeaderTable = [_client tableWithName:@"SalesHeader"];
        self.salesLineTable = [_client tableWithName:@"SalesLine"];
        self.salesPriceTable = [_client tableWithName:@"SalesPrice"];
        self.userTable = [_client tableWithName:@"User"];
        self.changeLogTable = [_client tableWithName:@"ChangeLog"];
        self.balanceHistoryRecords = [[NSMutableArray alloc] init];
        
        self.busyCount = 0;
    }
    
    return self;
}


- (void)emptyCoreDataTable:(NSString*)tableName : (NSManagedObject*) table{
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:tableName];
    
    if ([tableName isEqualToString:@"SalesHeader"] || [tableName isEqualToString:@"SalesLine"]){
        
        NSPredicate *filter = [NSPredicate predicateWithFormat:@"lastInvoice == %@",[NSNumber numberWithBool:YES]];
        [request setPredicate:filter];
    }
    else if ([tableName isEqualToString:@"BalanceHistory"]){
        NSPredicate *filter = [NSPredicate predicateWithFormat:@"paymentOnDevice == %@",[NSNumber numberWithBool:NO]];
        [request setPredicate:filter];
    }
    NSArray *fetchedObjects =
    [cdh.context executeFetchRequest:request error:nil];
    
    for(table in fetchedObjects){
        [cdh.context deleteObject:table];
    }
}
- (void)loadDataRecursiveForQuery:(MSQuery *)query withCompletion:(CompletionBlock)completion
{
    query.includeTotalCount = YES;
    query.fetchLimit = 1000; //note: you can adjust this to whatever amount is optimum
    query.fetchOffset = self.results.count;
    
    [query readWithCompletion:^(NSArray *items,NSInteger totalCount,NSError *error){
        if(!error) {
            //add the items to our local copy
            [self.results addObjectsFromArray:items];
            
            if (totalCount > [self.results count]) {
                [self loadDataRecursiveForQuery:query withCompletion:completion];
            } else {
                completion();
            }
        }
    }];
}

-(void)insertCoreDataTable : (MSTable*) table{
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    //NSPredicate * predicate = [NSPredicate predicateWithFormat:@"name != %@",@""];
    [table readWithPredicate:nil completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         
         if ([table.name isEqualToString:@"Customer"]){
             self.customerRecords = [results mutableCopy];
         }
         else if ([table.name isEqualToString:@"BalanceHistory"]){
             self.balanceHistoryRecords = [results mutableCopy];
         }
         else if ([table.name isEqualToString:@"Categories"]){
             self.categoriesRecords = [results mutableCopy];
         }
         else if ([table.name isEqualToString:@"Item"]){
             self.itemRecords = [results mutableCopy];
         }
         else if ([table.name isEqualToString:@"ItemUnits"]){
             self.itemUnitsRecords = [results mutableCopy];
         }
         else if ([table.name isEqualToString:@"LastSales"]){
             self.lastSaleRecords = [results mutableCopy];
         }
         else if ([table.name isEqualToString:@"NoSeries"]){
             self.noSeriesRecords = [results mutableCopy];
         }
         else if ([table.name isEqualToString:@"SalesHeader"]){
             self.salesHeaderRecords = [results mutableCopy];
         }
         else if ([table.name isEqualToString:@"SalesLine"]){
             self.salesLineRecords = [results mutableCopy];
         }
         else if ([table.name isEqualToString:@"SalesPrice"]){
             self.salesPriceRecords = [results mutableCopy];
         }
         else if ([table.name isEqualToString:@"User"]){
             self.userRecords = [results mutableCopy];
         }
         
         self.results = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:table predicate:nil];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.results){
                 if ([table.name isEqualToString:@"Customer"]){
                     if ([recDic objectForKey:@"phone"] != nil){
                         Customer *customer = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                         customer.address = [recDic objectForKey:@"address"];
                         customer.address2 = [recDic objectForKey:@"address2"];
                         customer.balance = [recDic objectForKey:@"balance"];
                         customer.block = [recDic objectForKey:@"block"];
                         customer.city = [recDic objectForKey:@"city"];
                         customer.customerNo = [recDic objectForKey:@"customerno"];
                         customer.name = [recDic objectForKey:@"name"];
                         customer.phone = [recDic objectForKey:@"phone"];
                         customer.postCode = [recDic objectForKey:@"postcode"];
                         customer.salesPersonCode = [recDic objectForKey:@"salespersoncode"];
                     }
                 }
                 else if ([table.name isEqualToString:@"BalanceHistory"]){
                     if ([recDic objectForKey:@"paymenttype"] != nil){
                         BalanceHistory *balance = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                         balance.customerNo = [recDic objectForKey:@"customerno"];
                         balance.documentAmount = [recDic objectForKey:@"documentamount"];
                         balance.documentDate = [recDic objectForKey:@"documentdate"];
                         balance.documentNo = [recDic objectForKey:@"documentno"];
                         balance.documentType = [recDic objectForKey:@"documenttype"];
                         balance.paymentType = [recDic objectForKey:@"paymenttype"];
                     }
                 }
                 else if ([table.name isEqualToString:@"Categories"]){
                     if ([recDic objectForKey:@"categoryvaluecode"] != nil){
                         Categories *categories = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                         categories.categoryName = [recDic objectForKey:@"categoryname"];
                         categories.categoryValue = [recDic objectForKey:@"categoryvalue"];
                     }
                 }
                 else if ([table.name isEqualToString:@"Item"]){
                     if ([recDic objectForKey:@"packsize"] != nil){
                         Item *item = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                         item.brandName = [recDic objectForKey:@"brandname"];
                         item.countryName = [recDic objectForKey:@"countryname"];
                         item.discounted = [recDic objectForKey:@"discounted"];
                         item.itemNo = [recDic objectForKey:@"itemno"];
                         item.name = [recDic objectForKey:@"name"];
                         item.packSize = [recDic objectForKey:@"packsize"];
                         item.salesUnit = [recDic objectForKey:@"salesunit"];
                         item.searchDescription = [recDic objectForKey:@"searchdescription"];
                         item.stockQty = [recDic objectForKey:@"stockqty"];
                         item.unitPrice = [recDic objectForKey:@"unitprice"];
                         item.unitSize = [recDic objectForKey:@"unitsize"];
                         item.vat = [recDic objectForKey:@"vat"];
                     }
                 }
                 else if ([table.name isEqualToString:@"ItemUnits"]){
                     if ([recDic objectForKey:@"barcode"] != nil){
                         ItemUnits *itemUnit = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                         itemUnit.barcode = [recDic objectForKey:@"barcode"];
                         itemUnit.itemNo = [recDic objectForKey:@"itemno"];
                         itemUnit.unit = [recDic objectForKey:@"unit"];
                         itemUnit.qtyPerUnit = [recDic objectForKey:@"qtyperunit"];
                     }
                 }
                 else if ([table.name isEqualToString:@"LastSales"]){
                     if ([recDic objectForKey:@"salesqty"] != nil){
                         LastSales *lastSales = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                         lastSales.customerNo = [recDic objectForKey:@"customerno"];
                         lastSales.itemNo = [recDic objectForKey:@"itemno"];
                         lastSales.salesPrice = [recDic objectForKey:@"salesprice"];
                         lastSales.salesQty = [recDic objectForKey:@"salesqty"];
                         lastSales.salesUnit = [recDic objectForKey:@"salesunit"];
                         lastSales.postingDate = [recDic objectForKey:@"postingdate"];
                     }
                 }
                 else if ([table.name isEqualToString:@"NoSeries"]){
                     if ([recDic objectForKey:@"seriesno"] != nil){
                         NoSeries *noSeries = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                         noSeries.seriesCode = [recDic objectForKey:@"seriescode"];
                         noSeries.seriesNo = [recDic objectForKey:@"seriesno"];
                         noSeries.userNoSeriesCode = [recDic objectForKey:@"usernoseriescode"];
                     }
                 }
                 else if ([table.name isEqualToString:@"SalesHeader"]){
                     if ([recDic objectForKey:@"requesteddeliverydate"] != nil){
                         SalesHeader *salesHeader = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                         salesHeader.documentNo = [recDic objectForKey:@"docuemntno"];
                         salesHeader.comment = [recDic objectForKey:@"note"];
                         salesHeader.salesPersonCode = [recDic objectForKey:@"salespersoncode"];
                         salesHeader.sellToNo = [recDic objectForKey:@"selltono"];
                         salesHeader.requestedDeliveryDate = [recDic objectForKey:@"requesteddeliverydate"];
                         salesHeader.orderDate = [recDic objectForKey:@"orderdate"];
                     }
                 }
                 else if ([table.name isEqualToString:@"SalesLine"]){
                     if ([recDic objectForKey:@"lineno"] != nil){
                         SalesLine *salesLine = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                         salesLine.documentNo = [recDic objectForKey:@"documentno"];
                         salesLine.itemName = [recDic objectForKey:@"itemname"];
                         salesLine.itemNo = [recDic objectForKey:@"itemno"];
                         salesLine.itemUnit = [recDic objectForKey:@"itemunit"];
                         salesLine.lineAmount = [recDic objectForKey:@"lineamount"];
                         salesLine.itemUnitPrice = [recDic objectForKey:@"itemunitprice"];
                         salesLine.lineNo = [recDic objectForKey:@"lineno"];
                         salesLine.orderDate = [recDic objectForKey:@"orderdate"];
                         salesLine.quantity = [recDic objectForKey:@"quantity"];
                     }
                 }
                 else if ([table.name isEqualToString:@"SalesPrice"]){
                     if ([recDic objectForKey:@"minqty"] != nil){
                         SalesPrice *salesPrice = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                         salesPrice.itemNo = [recDic objectForKey:@"itemno"];
                         salesPrice.itemUnit = [recDic objectForKey:@"itemunit"];
                         salesPrice.minQty = [recDic objectForKey:@"minqty"];
                         salesPrice.unitPrice = [recDic objectForKey:@"unitprice"];
                     }
                 }
                 else if ([table.name isEqualToString:@"User"]){
                     if ([recDic objectForKey:@"usernoseriescode"] != nil){
                         User *user = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                         user.name = [recDic objectForKey:@"name"];
                         user.password = [recDic objectForKey:@"password"];
                         user.salesPersonCode = [recDic objectForKey:@"salespersoncode"];
                         user.userNoSeriesCode = [recDic objectForKey:@"usernoseriescode"];
                     }
                 }
             }
             [cdh saveContext];
         }];
     }];
}

- (void)updateCoreDataTable : (MSTable*) table {
    
    /*
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    //add predicate statement to get only neccessary changes by date!!!
    //NSPredicate * predicate = [NSPredicate predicateWithFormat:@"name != %@",@""];
    [table readWithPredicate:nil completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.records = [results mutableCopy];
         self.results = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:table predicate:nil];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.results){
                 if ([[recDic objectForKey:@"changetype"] isEqualToString:@"Item"]){
                     NSPredicate *filter = [NSPredicate predicateWithFormat:@"itemno == %@",[recDic objectForKey:@"primarykey"]];
                     [self.itemTable readWithPredicate:filter completion:^(NSArray *results, NSInteger totalCount, NSError *error)
                      {
                          [self logErrorIfNotNil:error];
                          self.records = [results mutableCopy];
                          self.results = [[NSMutableArray alloc] init];
                          MSQuery *query = [[MSQuery alloc] initWithTable:self.itemTable predicate:filter];
                          [self loadDataRecursiveForQuery:query withCompletion:^{
                              for (NSDictionary *recDic in self.results){
                                  [gf updateItem : recDic];
                                  
                                  NSPredicate *filter2 = [NSPredicate predicateWithFormat:@"itemno == %@",[recDic objectForKey:@"itemno"]];
                                  [self.itemUnitsTable readWithPredicate:filter2 completion:^(NSArray *results, NSInteger totalCount, NSError *error)
                                   {
                                       [self logErrorIfNotNil:error];
                                       self.records = [results mutableCopy];
                                       self.results = [[NSMutableArray alloc] init];
                                       MSQuery *query = [[MSQuery alloc] initWithTable:self.itemUnitsTable predicate:filter2];
                                       [self loadDataRecursiveForQuery:query withCompletion:^{
                                           for (NSDictionary *recDic in self.results){
                                               [gf updateItemUnits : recDic];
                                           }
                                       }];
                                       
                                   }];
                              }
                          }];
                          
                      }];
                 }
             }
         }];
     }];
    [gf populateSearchDescriptions:@"Item"];
     */
}

-(void)checkAzure{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    SystemTableFunctions *sysTable = [(AppDelegate*)[[UIApplication sharedApplication] delegate] sysTableF];
    [sysTable checkIfAzureBusy:@"" withCompletion:^{
        if (gf.isServerBusy == NO){
            [self doNewInstallation];
        }
        else{
            gf.isSyncCompleted = YES;
            if ([gf.errorMessage isEqualToString:@""]){
                gf.errorMessage = @"Server is being updated. Please try again later...!";
            }
        }
    }];
    
}

- (void)doNewInstallation{
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.errorMessage = @"";
    @try {
        
        
        CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
        if ([cdh.activeUser.name isEqualToString:@"orca"]){
            UserFunctions *userF = [(AppDelegate *)[[UIApplication sharedApplication] delegate] userF];
            NoSeriesFunctions *noSerF = [(AppDelegate *)[[UIApplication sharedApplication] delegate] noSerF];
            [userF emptyCoreDataTable];
            [self emptyCoreDataTable:@"NoSeries" :self.passedNoseries];
            [userF insertCoreDataTable:self.userTable];
            [noSerF insertCoreDataTable:self.noSeriesTable];
        }
        else{
            cdh.activeUser.isReadyToUse = [NSNumber numberWithBool:NO];
            //DELETE
            [self emptyCoreDataTable:@"BalanceHistory" :self.passedBalanceHistory];
            [self emptyCoreDataTable:@"Categories" :self.passedCategories];
            [self emptyCoreDataTable:@"Customer" :self.passedCustomer];
            //[self emptyCoreDataTable:@"User" :self.passedUser];
            [self emptyCoreDataTable:@"Item" :self.passedItem];
            [self emptyCoreDataTable:@"ItemUnits" :self.passedItemUnits];
            [self emptyCoreDataTable:@"LastSales" :self.passedLastSales];
            [self emptyCoreDataTable:@"NoSeries" :self.passedNoseries];
            [self emptyCoreDataTable:@"SalesHeader" :self.passedSalesHeader];
            [self emptyCoreDataTable:@"SalesLine" :self.passedSalesLine];
            [self emptyCoreDataTable:@"SalesPrice" :self.passedSalesPrice];
            //DELETE END
            
            
            
            ItemFunctions *itemF = [(AppDelegate *)[[UIApplication sharedApplication] delegate] itemF];
            CustomerFunctions *customerF = [(AppDelegate *)[[UIApplication sharedApplication] delegate] customerF];
            NoSeriesFunctions *noSerF = [(AppDelegate *)[[UIApplication sharedApplication] delegate] noSerF];
            BalanceHistoryFunctions *balanceF = [(AppDelegate *)[[UIApplication sharedApplication] delegate] balanceF];
            CatgoryFunctions *catF = [(AppDelegate *)[[UIApplication sharedApplication] delegate] catF];
            LastSalesFunctions *lastSaleF = [(AppDelegate *)[[UIApplication sharedApplication] delegate] lastSaleF];
            SalesPriceFunctions *salesPF = [(AppDelegate *)[[UIApplication sharedApplication] delegate] salesPF];
            ItemUnitsFunctions *itemUnitsF = [(AppDelegate *)[[UIApplication sharedApplication] delegate] itemUnitsF];
            SalesHeaderFunctions *salesHF = [(AppDelegate *)[[UIApplication sharedApplication] delegate] salesHF];
            SalesLineFunctions *salesLF = [(AppDelegate *)[[UIApplication sharedApplication] delegate] salesLF];
            
            //UserFunctions *userF = [(AppDelegate *)[[UIApplication sharedApplication] delegate] userF];
            //[userF insertCoreDataTable:self.userTable]; salespersoncode TEST needs to be removed!
            
            [salesHF insertCoreDataTable:self.salesHeaderTable];
            [salesLF insertCoreDataTable:self.salesLineTable];
            [lastSaleF insertCoreDataTable:self.lastSalesTable];
            [noSerF insertCoreDataTable:self.noSeriesTable];
            [customerF insertCoreDataTable:self.customerTable];
            [itemUnitsF insertCoreDataTable:self.itemUnitsTable];
            [itemF insertCoreDataTable:self.itemTable];
            [salesPF insertCoreDataTable:self.salesPriceTable];
            [balanceF insertCoreDataTable:self.balanceHistoryTable];
            [catF insertCoreDataTable:self.categoriesTable];
        }
    } @catch (NSException *ex) {
        gf.errorMessage = [NSString stringWithFormat:@"%@\n%@",gf.errorMessage,ex];
    }
}

- (void)updateCoreDataTables{
    [self updateCoreDataTable:self.changeLogTable];
    
}

- (void)logErrorIfNotNil:(NSError *) error
{
    if (error)
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.errorMessage = error.localizedDescription;
        NSLog(@"ERROR %@", error);
    }
}

- (void)busy:(BOOL)busy
{
    // assumes always executes on UI thread
    if (busy)
    {
        if (self.busyCount == 0 && self.busyUpdate != nil)
        {
            self.busyUpdate(YES);
        }
        self.busyCount++;
    }
    else
    {
        if (self.busyCount == 1 && self.busyUpdate != nil)
        {
            self.busyUpdate(FALSE);
        }
        self.busyCount--;
    }
}
#pragma mark * MSFilter methods


- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response
{
    // A wrapped response block that decrements the busy counter
    MSFilterResponseBlock wrappedResponse = ^(NSHTTPURLResponse *innerResponse, NSData *data, NSError *error)
    {
        [self busy:NO];
        response(innerResponse, data, error);
    };
    
    // Increment the busy counter before sending the request
    [self busy:YES];
    next(request, wrappedResponse);
}

@end
