//
//  SalesMenuForWhsViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 04/06/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesMenuForWhsViewController : UIViewController
- (IBAction)ordersInDevicePressed:(id)sender;
- (IBAction)parkedOrdersPressed:(id)sender;
- (IBAction)sentOrdersPressed:(id)sender;

@end
