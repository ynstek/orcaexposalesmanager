//
//  SalesOrderListTableViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 29/05/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "SalesOrderListTableViewController.h"
#import "AppDelegate.h"

@interface SalesOrderListTableViewController ()

@end

@implementation SalesOrderListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //to create a space between bar item buttons
    //START
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 40.0f;
    //END
    
    self.refreshButton = [[UIBarButtonItem alloc] initWithTitle:@"Refresh" style:UIBarButtonItemStyleDone target:self action:@selector(refreshOrders)];
    self.backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(backButtonPressed)];
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:self.backButton,fixedItem,self.refreshButton, nil];
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    [self refreshView];
}
-(void)backButtonPressed{
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:YES];
}

-(void)refreshView{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesHeader"];
    
    request.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"documentNo" ascending:YES],nil];
    NSPredicate *filter;
    if ([gf.selectedOrderList isEqualToString:@"ordersInDevice"]){
        self.navigationItem.title = @"Orders In Device";
        filter = [NSPredicate predicateWithFormat:@"markToDelete == %@ and markToSend == %@",[NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO]];
    }
    else if ([gf.selectedOrderList isEqualToString:@"parkedOrders"]){
        self.navigationItem.title = @"Parked Orders in NAV";
        filter = [NSPredicate predicateWithFormat:@"markToDelete == %@ and markToSend == %@",[NSNumber numberWithBool:NO],[NSNumber numberWithBool:YES]];
    }
    else{
        self.navigationItem.title = @"Sent Orders";
        filter = [NSPredicate predicateWithFormat:@"markToDelete == %@ and markToSend == %@",[NSNumber numberWithBool:YES],[NSNumber numberWithBool:NO]];
    }
    [request setPredicate:filter];
    
    gf.salesOrderList = [[cdh.context executeFetchRequest:request error:nil] mutableCopy];
    [self.tableView reloadData];
    
    gf.selectedOrder = nil;
}
-(void)refreshOrders{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    [gf refreshOrders];
    [self refreshView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    // Return the number of rows in the section.
    return gf.salesOrderList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"orderCell" forIndexPath:indexPath];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    SalesHeader *salesHeader = [gf.salesOrderList objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [gf getCustomerName:salesHeader.sellToNo];
    cell.detailTextLabel.text = salesHeader.documentNo;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    if ([gf.selectedOrderList isEqualToString:@"ordersInDevice"]){
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            SalesHeader *salesHeader = [gf.salesOrderList objectAtIndex:indexPath.row];
            [gf deleteOrder:salesHeader];
            [gf.salesOrderList removeObjectAtIndex:indexPath.row];
            [self refreshView];
        }
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You can not delete orders in NAV!" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
    }
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"CatalogView"])
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.selectedOrder = [gf.salesOrderList objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        gf.selectedCustomer = [gf getCustomer:gf.selectedOrder.sellToNo];
    }
    else if ([segue.identifier isEqualToString:@"addNewOrder"]){
        NSLog(@"you can not add new order!");
    }
    else {
        NSLog(@"Undefined Segue Attempted!");
    }
}
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    if ([gf.selectedOrderList isEqualToString:@"sentOrders"]){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"You can not modify sent orders!" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
        return NO;
    }
    return YES;
}
@end
