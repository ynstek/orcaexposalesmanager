//
//  SalesLineViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 09/03/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SalesLineViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>

//General
@property (strong, nonatomic) NSString *orderNo;
@property (strong, nonatomic) NSString *prevPrice;
@property BOOL isFiltered;
@property (strong, nonatomic) IBOutlet UISearchBar *itemSearchBar;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
//General END

- (IBAction)itemUnitPriceTextFieldEditingDidEnd:(id)sender;
- (IBAction)itemUnitPriceTextFieldEditingDidBegin:(id)sender;


- (IBAction)increaseButtonPressed:(id)sender;
- (IBAction)decreaseButtonPressed:(id)sender;
@end
