//
//  GridViewViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 12/02/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Captuvo.h"

@interface GridViewViewController : UIViewController<CaptuvoEventsProtocol,UISearchBarDelegate, UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIActionSheetDelegate,UIPopoverControllerDelegate,UIPrintInteractionControllerDelegate,UIGestureRecognizerDelegate>{
}

//General

@property BOOL isFiltered;
@property (strong, nonatomic) NSString *prevQuantity,*prevPrice,*barcodeUnit,*decoderData;
@property NSIndexPath *lastIndexPath;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *noteButton,*linesButton,*printButton;
@property (strong, nonatomic) IBOutlet UIButton *calculatorButton;

- (IBAction)calculatorButtonPressed:(id)sender;

- (IBAction)testButtonPressed:(id)sender;
- (IBAction)itemCardButtonPressed:(id)sender;
@property (strong, nonatomic) UITextField *activeTextField;
//General END


//Search Bar
@property (strong, nonatomic) IBOutlet UISearchBar *searchFilter1;
@property (strong, nonatomic) IBOutlet UISearchBar *searchFilter2;
//@property (strong, nonatomic) IBOutlet UITextField *barcodeSearchTextField;
@property (strong, nonatomic) IBOutlet UISwitch *autoAddSwitch;
@property (strong, nonatomic) IBOutlet UISwitch *searchIdOnlySwitch;
@property (strong, nonatomic) IBOutlet UIButton *menuOptionsButton;

@property (strong,nonatomic) NSMutableArray *filteredSearchResults;

//Search Bar END

//Categories
@property (strong,nonatomic) UIPopoverController *popover;
//Categories END

//Actions
- (IBAction)increaseButtonPressed:(id)sender;
- (IBAction)decreaseButtonPressed:(id)sender;
- (IBAction)unitSelectionChanged:(id)sender;
- (IBAction)addLastSale:(id)sender;
- (IBAction)addFreeButtonPressed:(id)sender;
- (IBAction)qtyTextFieldEditingDidBegin:(id)sender;
- (IBAction)qtyTextFieldEditingDidEnd:(id)sender;
- (IBAction)unitPriceTextFieldEditingDidBegin:(id)sender;
- (IBAction)unitPriceTextFieldEditingDidEnd:(id)sender;
- (IBAction)autoAddSwitchChanged:(id)sender;
- (IBAction)searchIdSwitchChanged:(id)sender;

//Actions END


//Selected Item Section

@property (strong, nonatomic) UICollectionViewCell *selectedCell;

//Selected Item Section END

@end
