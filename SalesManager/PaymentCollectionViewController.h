//
//  PaymentCollectionViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 28/01/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentCollectionViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIPopoverControllerDelegate>{
    BOOL blocking;
}

@property (strong, nonatomic) NSString *datePickerCaller;
@property (strong, nonatomic) IBOutlet UITextField *fromDateTextField;
@property (strong, nonatomic) IBOutlet UITextField *toDateTextField;
- (IBAction)showButtonPressed:(id)sender;
- (IBAction)printAndDeleteButtonPressed:(id)sender;
@property (strong,nonatomic) UIPopoverController *popover;
@property (strong, nonatomic) IBOutlet UILabel *cashTotalAmountLabel;
@property (strong, nonatomic) IBOutlet UILabel *chequeTotalAmountLabel;
@property (strong, nonatomic) IBOutlet UILabel *totalAmountLabel;
- (IBAction)fromDatePickerImagePressed:(id)sender;

- (IBAction)toDatePickerImagePressed:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *paymentTableView;

@end
