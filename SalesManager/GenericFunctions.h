//
//  GenericFunctions.h
//  SalesManager
//
//  Created by Cihan TASKIN on 14/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Customer.h"
#import "SalesHeader.h"
#import "SalesLine.h"
#import "NoSeries.h"
#import "LastSales.h"
#import "SalesPrice.h"
#import "ItemUnits.h"
#import "Categories.h"
#import "Customer.h"
#import "Item.h"
#import "BalanceHistory.h"
#import "SelectedItemDetails.h"
#import <UIKit/UIKit.h>

@interface GenericFunctions : NSObject

@property NSString *internalIPAddress;
-(NSDate *)dateForEndOfDay:(NSDate *)datDate;
-(void)populateSearchDescriptions : (NSString*)tableName;

@property (nonatomic, strong) NSOperationQueue *operationQueue;

//Warehouse START
@property (strong,nonatomic) NSMutableArray *salesOrderList,*customerList;
@property (strong, nonatomic) NSString *selectedOrderList;
-(void)getUOMList;

-(void)loadSalesHeaderRecords;
-(void)loadCustomerRecords;
-(void)loadItemRecords;
-(void)loadUOMRecords;
-(void)loadSalesPriceRecords;
-(void)sendNAVOrder:(NSNumber*)parked;
-(void)refreshOrders;

//Warehouse END




@property (strong,nonatomic) NSMutableArray *items,*customers,*itemUnits,*searchCustomerResults,*searchItemResults,*barcodes,*lastSaleItems,*unSoldItems,*uoms;
@property (strong, nonatomic) Customer *selectedCustomer;
@property (strong, nonatomic) SelectedItemDetails *selectedItemDetails;
@property (strong, nonatomic) SalesHeader *selectedOrder;
@property (strong, nonatomic) BalanceHistory *selectedPayment;
@property (strong, nonatomic) NSString *selectedDate;
@property (strong, nonatomic) NSString *errorMessage, *keyboardValue;
@property BOOL isUserEnabled,changesOnlyCompleted,isAutoAdded,isServerBusy,showOnlyNewItems,isSyncCompleted,isSearchItemsNeeded,filterViewIndicator;
@property float progress;
@property int itemArrayFilter; // 1: ALL ITEMS, 2: INACTIVE ITEMS, 3: LAST SALE ITEMS, 4: UNSOLD ITEMS

-(void)clearSelectedOrders;
-(void)refreshData;
-(void)populateItems : (NSString*)documentNo;
-(NSArray*)getFilteredCategories : (NSString*)categoryName : (NSString*)relatedValue;
-(void)downloadAllImages;
-(void)setPreviousSelectedItem;
-(SalesHeader*)getOrderByDocNo : (NSString*) docNo;
-(SalesLine*)getSalesLineByUnit:(NSString*)docNo : (NSString*)itemNo : (NSString*)unit;
-(SalesLine*)getSalesLine:(NSString*)docNo : (NSString*)lineNo : (BOOL)createNewIfNeeded;
-(BOOL)checkIfLineExistsInOrder : (NSString*) docNo;
-(NSNumber *)getQtyIfItemExistsInOrder : (NSString*)itemNo : (NSString*)unit;
-(NSDate*)getTodayDate;
-(NSNumber*)getCurrentLineNo:(NSString*)docNo : (NSString*)itemNo : (NSString*) itemUnit;
-(NoSeries*)getNewOrderNo;
-(NSString*)getLastUsedNo;
-(NSString*)getBestPrice : (NSString*)itemNo : (NSString*)itemUnit : (NSString*)qty;
-(LastSales*)getLastSales:(NSString*)customerNo : (NSString*)itemNo;
-(Customer*)getCustomer:(NSString*)customerNo;
-(NSString*)getCustomerName:(NSString*)customerNo;
-(Item*)getItem:(NSString*)itemNo;
-(NSString*)getLastInvoiceNo : (BOOL)setCurrentOrder;
-(NSArray*)getItemUnits : (NSString*) itemNo;
-(NSString*)setSelectedItemByBarcode : (NSString*)barcodeNo;
-(NSString*)setSelectedItemByBarcodeNEW : (NSString*)barcodeNo;
-(NSString*)getCustomerBalanceOnDevice : (Customer*)customer;
-(NSString*)getOrderTotalAmount:(NSString*)orderNo;
-(NSString*)getOrderTotalDiscountAmount:(NSString*)orderNo;
-(NSString*)getOrderTotalAmountInclVAT:(NSString*)orderNo;
-(NSString*)getOrderTotalVATAmount:(NSString*)orderNo;
-(void)setCategorySelection : (NSString*)categoryName : (NSString*)categoryValue : (BOOL)selection;
-(void)clearCategorySelections : (NSString*)categoryName;
-(BOOL)validateNumTextField : (NSString*)text;
-(void)deleteBalanceHistory:(BalanceHistory*) balanceHistory;
-(void)deleteOrder:(SalesHeader*)salesHeader;
-(void)deleteSalesLine : (SalesLine*)salesLine;
-(BalanceHistory*)createNewPayment : (NSString*)paymentType : (NSString*)dueDateText : (NSString*)paymentAmountText : (NSString*)chequeNo;
-(BalanceHistory*)getPayment : (NSString*)entryNo;
-(NSString*)getListPrice : (NSString*)itemNo : (NSString*)itemUnit;
-(SalesLine*)getFreeSalesLine: (NSString*)lineNo : (BOOL)createNewIfNeeded;
-(SalesLine*)getFreeSalesLineByUnit : (NSString*)itemNo : (NSString*)unit;
-(void)saveCurrentChanges;

- (void)getAllImages;


@end
