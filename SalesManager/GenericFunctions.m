//
//  GenericFunctions.m
//  SalesManager
//
//  Created by Cihan TASKIN on 14/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "GenericFunctions.h"
#import "AppDelegate.h"
#import "JSONHelper.h"

@interface GenericFunctions()
@end

@implementation GenericFunctions
#define debug 1

//Warehouse START

-(void)refreshOrders{
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    [gf loadSalesHeaderRecords];
}

-(void)refreshData{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    [gf loadCustomerRecords];
    [gf loadItemRecords];
    [self refreshOrders];
    [gf loadItemUOMRecords];
    [gf loadSalesPriceRecords];
    [gf loadBarcodeRecords];
    [gf loadUOMRecords];
    
    [gf saveCurrentChanges];
}

- (void)emptyCoreDataTable:(NSString*)tableName : (NSManagedObject*) table{

    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    
    if ([tableName isEqualToString:@"SalesHeader"]){
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:tableName];
        NSPredicate *filter = [NSPredicate predicateWithFormat:@"markToDelete == %@ or markToSend == %@",[NSNumber numberWithBool:YES],[NSNumber numberWithBool:YES]];
        [request setPredicate:filter];
        NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
        for(table in fetchedObjects){
            SalesHeader *salesH = (SalesHeader*)table;
            NSFetchRequest *request2 = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
            NSPredicate *filter2 = [NSPredicate predicateWithFormat:@"documentNo == %@",salesH.documentNo];
            [request2 setPredicate:filter2];
            NSArray *fetchedObjects2 = [cdh.context executeFetchRequest:request2 error:nil];
            for(SalesLine * salesL in fetchedObjects2){
                [cdh.context deleteObject:salesL];
            }
            
            [cdh.context deleteObject:table];
        }
    }
    else{
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:tableName];
        NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
        for(table in fetchedObjects){
            [cdh.context deleteObject:table];
        }
    }
}

-(void)sendNAVOrder:(NSNumber*)parked
{
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSString* JSONData = [NSString stringWithFormat:@"{\"documentNo\":\"%@\",\"sellToCustomerNo\":\"%@\",\"device\":\"%@\",\"parked\":\"%@\",\"userName\":\"%@\",\"salesLine\":[",self.selectedOrder.documentNo,self.selectedOrder.sellToNo, @"NO",parked,cdh.activeUser.name];
    
    int counter = 1;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@",self.selectedOrder.documentNo];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        for (SalesLine *salesLine in fetchedObjects){
            Item *item = [self getItem:salesLine.itemNo];
            if (counter == [fetchedObjects count]){
                JSONData = [NSString stringWithFormat:@"%@{\"documentNo\" : \"%@\",\"lineNo\" : \"%@\",\"itemNo\" : \"%@\",\"quantity\" : \"%@\",\"unitOfMeasureCode\":\"%@\",\"unitPrice\":\"%@\",\"kgQty\":\"%@\"}",JSONData,salesLine.documentNo,salesLine.lineNo,salesLine.itemNo,salesLine.quantity,salesLine.itemUnit,salesLine.itemUnitPrice,item.kgInBarcode];
                
            }
            else{
                JSONData = [NSString stringWithFormat:@"%@{\"documentNo\" : \"%@\",\"lineNo\" : \"%@\",\"itemNo\" : \"%@\",\"quantity\" : \"%@\",\"unitOfMeasureCode\":\"%@\",\"unitPrice\":\"%@\",\"kgQty\":\"%@\"},",JSONData,salesLine.documentNo,salesLine.lineNo,salesLine.itemNo,salesLine.quantity,salesLine.itemUnit,salesLine.itemUnitPrice,item.kgInBarcode];
            }
            counter = counter + 1;
        }
    }
    
    JSONData = [NSString stringWithFormat:@"%@]}",JSONData];
    
    NSString* WebServiceURL = [NSString stringWithFormat:@"%@Service1.svc/updateSalesOrder",self.internalIPAddress];
    
    // Get the JSON string from our web serivce
    NAVResult * result = [JSONHelper postJSONDataToURL:WebServiceURL JSONdata:JSONData];
    
    [JSONHelper showError:@"" :result.Exception];
    if (result.WasSuccessful == 1){
        if ([[NSNumber numberWithBool:YES] isEqualToNumber:parked]){
            self.selectedOrder.markToDelete = [NSNumber numberWithBool:NO];
            self.selectedOrder.markToSend = [NSNumber numberWithBool:YES];
        }
        else{
            self.selectedOrder.markToDelete = [NSNumber numberWithBool:YES];
            self.selectedOrder.markToSend = [NSNumber numberWithBool:NO];
        }
        [self updateLastUsedNo];
    }
    else{
        NSString *result = [self deleteSalesOrder:self.selectedOrder.documentNo];
        if (![result isEqualToString:@"OK"]){
            [JSONHelper showError:@"Error on deleting NAV Order" :result];
        }
    }
    
}

-(BOOL)updateLastUsedNo{
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];

    NSString* WebServiceURL = [NSString stringWithFormat:@"%@Service1.svc/updateLastNoSeries/%@",cdh.activeUser.name,[self getLastUsedNo]];
    
    NSDictionary *dic = [JSONHelper syncJSONDataFromURL:WebServiceURL];
    
    NSArray *results = [dic objectForKey:@"deleteSalesOrderResult"];
    
    NSString *result;
    for(NSDictionary * test in results){
        result = test;
    }
    return result;
}

-(NSString*)deleteSalesOrder : (NSString*)docNo
{
    NSString* WebServiceURL = [NSString stringWithFormat:@"%@Service1.svc/deleteSalesOrder/%@",self.internalIPAddress,self.selectedOrder.documentNo];
    
    NSDictionary *dic = [JSONHelper syncJSONDataFromURL:WebServiceURL];
    
    NSArray *results = [dic objectForKey:@"deleteSalesOrderResult"];
    
    NSString *result;
    for(NSDictionary * test in results){
        result = test;
    }
    return result;
}

-(void)loadSalesHeaderRecords
{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    //orderList = nil;
    NSString* WebServiceURL = [NSString stringWithFormat:@"%@Service1.svc/getSalesOrderList/%@",self.internalIPAddress,cdh.activeUser.name];
    // Get the JSON string from our web serivce
    NSDictionary * dictionary = [JSONHelper syncJSONDataFromURL:WebServiceURL];
    
    // Get a list of results in the Dictionary
    NSArray *results = [dictionary objectForKey:@"getSalesOrderListResult"];
    
    SalesHeader *salesH;
    [self emptyCoreDataTable:@"SalesHeader" :salesH];
    //SalesLine *salesL;
    //[self emptyCoreDataTable:@"SalesLine" :salesL]; check if you are deleting orders in device before uncommenting
    
    self.salesOrderList = [NSMutableArray array];
    for (NSDictionary *salesHeaderDic in results)
    {
        SalesHeader * newSalesHeader = [NSEntityDescription insertNewObjectForEntityForName:@"SalesHeader" inManagedObjectContext:cdh.context];
        newSalesHeader.documentNo = [salesHeaderDic objectForKey:@"noField"];
        newSalesHeader.sellToNo = [salesHeaderDic objectForKey:@"sell_to_Customer_NoField"];
        
        NSDateFormatter * formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"MM-dd-yy"];
        newSalesHeader.orderDate = [formatter dateFromString:[salesHeaderDic objectForKey:@"sOrderDateField"]];
        if ([[salesHeaderDic objectForKey:@"warehouse_Device_In_UseField"] isEqualToNumber:[NSNumber numberWithBool:YES]]){
            newSalesHeader.markToDelete = [NSNumber numberWithBool:NO];
            newSalesHeader.markToSend = [NSNumber numberWithBool:YES];
        }
        else{
            newSalesHeader.markToDelete = [NSNumber numberWithBool:YES];
            newSalesHeader.markToSend = [NSNumber numberWithBool:NO];
        }
        
        [self.salesOrderList addObject:newSalesHeader];
        
        for (NSDictionary * salesLineDic in [salesHeaderDic objectForKey:@"salesLinesField"]){
            SalesLine * newSalesLine = [NSEntityDescription insertNewObjectForEntityForName:@"SalesLine" inManagedObjectContext:cdh.context];
            newSalesLine.documentNo = [salesLineDic objectForKey:@"document_NoField"];
            newSalesLine.itemNo =[salesLineDic objectForKey:@"noField"];
            newSalesLine.itemName =[salesLineDic objectForKey:@"descriptionField"];
            newSalesLine.itemUnit =[salesLineDic objectForKey:@"unit_of_Measure_CodeField"];
            newSalesLine.itemUnitPrice =[salesLineDic objectForKey:@"unit_PriceField"];
            newSalesLine.quantity =[salesLineDic objectForKey:@"quantityField"];
            newSalesLine.lineNo =[salesLineDic objectForKey:@"line_NoField"];
            newSalesLine.orderDate =[salesHeaderDic objectForKey:@"document_DateField"];
            newSalesLine.lineAmount = [salesLineDic objectForKey:@"line_AmountField"];
            newSalesLine.freeItem = [NSNumber numberWithBool:NO];// line discount a gere gelmesi gerekli!!!
        }
        
        
    }
    //[cdh saveContext];
}
-(void)loadCustomerRecords
{
    self.customerList = nil;
    
    NSString* WebServiceURL = [NSString stringWithFormat:@"%@Service1.svc/getCustomerList",self.internalIPAddress];
    
    
    // Get the JSON string from our web serivce
    NSDictionary * dictionary = [JSONHelper syncJSONDataFromURL:WebServiceURL];
    
    
    // Get a list of results in the Dictionary
    NSArray *results = [dictionary objectForKey:@"getCustomerListResult"];
    
    self.customerList = [NSMutableArray array];
    Customer *customer;
    [self emptyCoreDataTable:@"Customer" :customer];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    for (NSDictionary * customerDic in results)
    {
        
        Customer * newCustomer = [NSEntityDescription insertNewObjectForEntityForName:@"Customer" inManagedObjectContext:cdh.context];
        
        newCustomer.customerNo = [customerDic objectForKey:@"noField"];
        newCustomer.name = [customerDic objectForKey:@"nameField"];
        newCustomer.priceGroup = [customerDic objectForKey:@"customer_Price_GroupField"];
        newCustomer.balance = [customerDic objectForKey:@"balanceField"];
        newCustomer.postCode = [customerDic objectForKey:@"post_CodeField"];
        [self.customerList addObject:newCustomer];
    }
    //[cdh saveContext];
}

-(void)loadItemRecords
{
    self.items = nil;
    
    NSString* WebServiceURL = [NSString stringWithFormat:@"%@Service1.svc/getItemList",self.internalIPAddress];
    
    // Get the JSON string from our web serivce
    NSDictionary * dictionary = [JSONHelper syncJSONDataFromURL:WebServiceURL];
    
    // Get a list of results in the Dictionary
    NSArray *results = [dictionary objectForKey:@"getItemListResult"];
    
    self.items = [NSMutableArray array];
    Item *item;
    [self emptyCoreDataTable:@"Item" :item];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    for (NSDictionary * itemDic in results)
    {
        Item * newItem = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:cdh.context];

        newItem.itemNo = [itemDic objectForKey:@"noField"];
        newItem.salesUnit = [itemDic objectForKey:@"sales_Unit_of_MeasureField"];
        newItem.name = [itemDic objectForKey:@"descriptionField"];
        newItem.unitSize = [itemDic objectForKey:@"unit_SizeField"];
        newItem.packSize = [itemDic objectForKey:@"pack_SizeField"];
        newItem.vat = [itemDic objectForKey:@"vatPercentageField"];
        newItem.kgInBarcode = [itemDic objectForKey:@"kG_Qty_in_BarcodeField"];
        newItem.unitPrice = [itemDic objectForKey:@"unit_PriceField"];

        [self.items addObject:newItem];
    }
}


-(void)loadItemUOMRecords
{
    NSString* WebServiceURL = [NSString stringWithFormat:@"%@Service1.svc/getItemUOMList",self.internalIPAddress];
    
    
    // Get the JSON string from our web serivce
    NSDictionary * dictionary = [JSONHelper syncJSONDataFromURL:WebServiceURL];
    
    
    // Get a list of results in the Dictionary
    NSArray *results = [dictionary objectForKey:@"getItemUOMListResult"];
    
    
    ItemUnits *itemUOM;
    [self emptyCoreDataTable:@"ItemUnits" :itemUOM];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    for (NSDictionary * itemUnitsDic in results)
    {
        ItemUnits * newItemUOM = [NSEntityDescription insertNewObjectForEntityForName:@"ItemUnits" inManagedObjectContext:cdh.context];
        newItemUOM.unit = [itemUnitsDic objectForKey:@"codeField"];
        newItemUOM.itemNo = [itemUnitsDic objectForKey:@"item_NoField"];
        newItemUOM.qtyPerUnit = [itemUnitsDic objectForKey:@"qty_per_Unit_of_MeasureField"];
    }
    //[cdh saveContext];
}

-(void)loadSalesPriceRecords
{
    NSString* WebServiceURL = [NSString stringWithFormat:@"%@Service1.svc/getSalesPriceList",self.internalIPAddress];
    
    
    // Get the JSON string from our web serivce
    NSDictionary * dictionary = [JSONHelper syncJSONDataFromURL:WebServiceURL];
    
    
    // Get a list of results in the Dictionary
    NSArray *results = [dictionary objectForKey:@"getSalesPriceListResult"];
    
    
    SalesPrice *salesP;
    [self emptyCoreDataTable:@"SalesPrice" :salesP];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    for (NSDictionary * salesPriceDic in results)
    {
        SalesPrice * newSalesPrice = [NSEntityDescription insertNewObjectForEntityForName:@"SalesPrice" inManagedObjectContext:cdh.context];
        newSalesPrice.itemUnit = [salesPriceDic objectForKey:@"unit_of_Measure_CodeField"];
        newSalesPrice.itemNo = [salesPriceDic objectForKey:@"item_NoField"];
        newSalesPrice.minQty = [salesPriceDic objectForKey:@"minimum_QuantityField"];
        newSalesPrice.salesCode = [salesPriceDic objectForKey:@"sales_CodeField"];
        newSalesPrice.unitPrice = [salesPriceDic objectForKey:@"unit_PriceField"];
    }
    //[cdh saveContext];
}
-(void)loadBarcodeRecords
{
    NSString* WebServiceURL = [NSString stringWithFormat:@"%@Service1.svc/getBarcodeList",self.internalIPAddress];
    
    
    // Get the JSON string from our web serivce
    NSDictionary * dictionary = [JSONHelper syncJSONDataFromURL:WebServiceURL];
    
    
    // Get a list of results in the Dictionary
    NSArray *results = [dictionary objectForKey:@"getBarcodeListResult"];
    
    
    Barcode *barcode;
    [self emptyCoreDataTable:@"Barcode" :barcode];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    for (NSDictionary * barcodeDic in results)
    {
        Barcode * newBarcode = [NSEntityDescription insertNewObjectForEntityForName:@"Barcode" inManagedObjectContext:cdh.context];
        newBarcode.unit = [barcodeDic objectForKey:@"unit_Of_MeasureField"];
        newBarcode.itemNo = [barcodeDic objectForKey:@"item_NoField"];
        newBarcode.barcode = [barcodeDic objectForKey:@"barcodeField"];
    }
}

-(void)loadUOMRecords
{
    NSString* WebServiceURL = [NSString stringWithFormat:@"%@Service1.svc/getUOMList",self.internalIPAddress];
    
    
    // Get the JSON string from our web serivce
    NSDictionary * dictionary = [JSONHelper syncJSONDataFromURL:WebServiceURL];
    
    
    // Get a list of results in the Dictionary
    NSArray *results = [dictionary objectForKey:@"getUOMListResult"];
    
    
    UnitsOfMeasure *uom;
    [self emptyCoreDataTable:@"UnitsOfMeasure" :uom];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    for (NSDictionary * uomDic in results)
    {
        UnitsOfMeasure * newUOM = [NSEntityDescription insertNewObjectForEntityForName:@"UnitsOfMeasure" inManagedObjectContext:cdh.context];
        newUOM.code = [uomDic objectForKey:@"codeField"];
        newUOM.name = [uomDic objectForKey:@"descriptionField"];
    }
}

-(void)getUOMList{
    
    [self updateUomList];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"UnitsOfMeasure"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"qtyOnOrder > 0"];
    [request setPredicate:filter];
    
    NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
    self.uoms = [[NSMutableArray alloc] init];
    if([fetchedObjects count]> 0){
        for (UnitsOfMeasure *uom in fetchedObjects){
            [self.uoms addObject:uom];
        }
    }
}

-(void)updateUomList {
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"UnitsOfMeasure"];
    //NSPredicate *filter = [NSPredicate predicateWithFormat:@"Code == %@",unit];
    //[request setPredicate:filter];
    NSDecimalNumber *unitQty;
    NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
    if([fetchedObjects count]> 0){
        for (UnitsOfMeasure *uom in fetchedObjects){
            unitQty = [NSDecimalNumber decimalNumberWithString:@"0"];
            NSFetchRequest *request2 = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
            NSPredicate *filter2 = [NSPredicate predicateWithFormat:@"documentNo == %@ and itemUnit == %@",self.selectedOrder.documentNo,uom.code];
            [request2 setPredicate:filter2];
        
            NSArray *fetchedObjects2 = [cdh.context executeFetchRequest:request2 error:nil];
            if([fetchedObjects2 count]> 0){
                for (SalesLine *salesL in fetchedObjects2){
                    unitQty = [unitQty decimalNumberByAdding:salesL.quantity];
                }
            }
            uom.qtyOnOrder = unitQty;
        }
    }
    [cdh saveContext];
}



//Warehouse END


-(NSDate *)dateForEndOfDay:(NSDate *)datDate
{
    
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSTimeZone *londonTime = [NSTimeZone timeZoneWithName:@"Europe/London"];
    
    
    NSDateComponents *dateComp = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:datDate];
    
    NSDateComponents *dateComps = [[NSDateComponents alloc] init];
    [dateComps setCalendar:gregorian];
    [dateComps setYear:[dateComp year]];
    [dateComps setMonth:[dateComp month]];
    [dateComps setDay:[dateComp day]];
    [dateComps setTimeZone:londonTime];
    [dateComps setHour:23];
    [dateComps setMinute:59];
    [dateComps setSecond:59];
    
    return [dateComps date];
}

-(BOOL)checkifItemLastInvoiceItem : (NSString*)itemNo{
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSPredicate *filter;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
    filter = [NSPredicate predicateWithFormat:@"documentNo == %@ and itemNo == %@",[self getLastInvoiceNo : NO],itemNo];
    [request setPredicate:filter];
    NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
    if([fetchedObjects count]> 0){
        return YES;
    }
    return NO;
}

-(BOOL)checkifItemLastSaleItem : (NSString*)itemNo{
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSPredicate *filter;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"LastSales"];
    filter = [NSPredicate predicateWithFormat:@"customerNo == %@ and itemNo == %@",self.selectedCustomer.customerNo, itemNo];
    [request setPredicate:filter];
    NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
    if([fetchedObjects count]> 0){
        return YES;
    }
    
    return NO;
}

-(NSArray*)getFilteredCategories : (NSString*)categoryName : (NSString*)relatedValue{
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Categories"];
    NSPredicate *filter;
    if ([relatedValue isEqualToString:@""]){
        filter = [NSPredicate predicateWithFormat:@"isFiltered == %@ and categoryName == %@",[NSNumber numberWithBool:YES],categoryName];
    }
    else{
        filter = [NSPredicate predicateWithFormat:@"isFiltered == %@ and categoryName == %@ and relatedValueCode == %@",[NSNumber numberWithBool:YES],categoryName,relatedValue];
    }
    [request setPredicate:filter];
    return [cdh.context executeFetchRequest:request error:nil];
}

-(BOOL)checkItemIfExistInItems : (NSMutableArray*)arr : (NSString*)itemNo{
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(itemNo == %@)", itemNo];
    NSArray *filtered = [arr filteredArrayUsingPredicate:pred];
    if ([filtered count] == 0 ){
        return NO;
    }
    else{
        return YES;
    }
}

-(void)checkCategories{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    NSArray *filteredCountries = [self getFilteredCategories:@"Country" :@""];
    NSArray *filteredItemCategories = [self getFilteredCategories:@"ItemCategory":@""];
    NSArray *filteredProductGroups = [self getFilteredCategories:@"ProductGroup":@""];
    NSArray *filtered;
    NSMutableArray*filteredItems = [[NSMutableArray alloc] init];
    if ([filteredCountries count] > 0){
        for (Categories *country in filteredCountries) {
            if ([filteredItemCategories count] > 0){
                for (Categories *itemCategory in filteredItemCategories) {
                    filteredProductGroups = [self getFilteredCategories:@"ProductGroup":itemCategory.categoryValueCode];
                    if ([filteredProductGroups count] > 0){
                        for(Categories * productGroup in filteredProductGroups){
                            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(countryName == %@ and itemCategoryCode == %@ and productGroupCode == %@)",country.categoryValue, itemCategory.categoryValueCode,productGroup.categoryValueCode];
                            filtered = [gf.items filteredArrayUsingPredicate:pred];
                            [filteredItems addObjectsFromArray:filtered];
                        }
                    }
                    else{
                        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(countryName == %@ and itemCategoryCode == %@)",country.categoryValue, itemCategory.categoryValueCode];
                        filtered = [gf.items filteredArrayUsingPredicate:pred];
                        [filteredItems addObjectsFromArray:filtered];
                    }
                }
            }
            else {
                NSPredicate *pred = [NSPredicate predicateWithFormat:@"(countryName == %@)", country.categoryValue];
                filtered = [gf.items filteredArrayUsingPredicate:pred];
                [filteredItems addObjectsFromArray:filtered];
            }
        }
    }
    else{
        if ([filteredItemCategories count] > 0){
            for (Categories *itemCategory in filteredItemCategories) {
                NSArray *filteredProductGroups = [self getFilteredCategories:@"ProductGroup":itemCategory.categoryValueCode];
                if ([filteredProductGroups count] > 0){
                    for(Categories * productGroup in filteredProductGroups){
                        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(itemCategoryCode == %@ and productGroupCode == %@)", itemCategory.categoryValueCode,productGroup.categoryValueCode];
                        filtered = [gf.items filteredArrayUsingPredicate:pred];
                        [filteredItems addObjectsFromArray:filtered];
                    }
                }
                else{
                    NSPredicate *pred = [NSPredicate predicateWithFormat:@"(itemCategoryCode == %@)",itemCategory.categoryValueCode];
                    filtered = [gf.items filteredArrayUsingPredicate:pred];
                    [filteredItems addObjectsFromArray:filtered];
                }
            }
        }
    }
    NSMutableArray *copyItems = [gf.items mutableCopy];

    if (([filteredCountries count] > 0) || ([filteredItemCategories count] > 0) || ([filteredProductGroups count] > 0)){
        if ([filteredItems count] > 0){
            for(Item *item in copyItems){
                if (![self checkItemIfExistInItems:filteredItems : item.itemNo])
                    [gf.items removeObject:item];
            }
        }
        else{
            gf.items = [[NSMutableArray alloc] init];
        }
    }
}

-(void)populateSearchDescriptions : (NSString*)tableName{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    NSFetchRequest *request;
    NSPredicate *filter;
    NSArray *fetchedObjects;
    
    if ([tableName isEqualToString:@"Item"]){
        request = [NSFetchRequest fetchRequestWithEntityName:@"Item"];
        if (gf.showOnlyNewItems){
            filter = [NSPredicate predicateWithFormat:@"discounted == %@",[NSNumber numberWithBool:gf.showOnlyNewItems]];
            [request setPredicate:filter];
        }
        request.sortDescriptors =
        [NSArray arrayWithObjects: [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES], nil];
        fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
        if([fetchedObjects count]> 0){
            gf.items = [[NSMutableArray alloc] init];
            for (Item *item in fetchedObjects){
                if (item != nil){
                    if (gf.itemArrayFilter == 1){
                        [gf.items addObject:item];
                    }
                    else if (gf.itemArrayFilter == 2){
                        if ([self checkifItemLastSaleItem:item.itemNo] == YES){
                            [gf.items addObject:item];
                        }
                    }
                    else if (gf.itemArrayFilter == 3){
                        if ([self checkifItemLastInvoiceItem:item.itemNo] == YES){
                            [gf.items addObject:item];
                        }
                    }
                    else if (gf.itemArrayFilter == 4){
                        if ([self checkifItemLastSaleItem:item.itemNo] == NO){
                            [gf.items addObject:item];
                        }
                    }
                    else{
                        [gf.items addObject:item];
                    }
                }
            }
        }
        
        [self checkCategories];
        
        
        request = [NSFetchRequest fetchRequestWithEntityName:@"ItemUnits"];
        request.sortDescriptors =
        [NSArray arrayWithObjects: [NSSortDescriptor sortDescriptorWithKey:@"itemNo" ascending:YES], nil];
        [request setFetchBatchSize:50];
        fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
        if([fetchedObjects count]> 0){
            gf.itemUnits = [[NSMutableArray alloc] init];
            for (ItemUnits *uom in fetchedObjects){
                [gf.itemUnits addObject:uom];
            }
        }
        
    }
    else{
        request = [NSFetchRequest fetchRequestWithEntityName:@"Customer"];
        
        filter = [NSPredicate predicateWithFormat:@"salesPersonCode == %@",cdh.activeUser.salesPersonCode];
        
        [request setPredicate:filter];
        
        request.sortDescriptors =
        [NSArray arrayWithObjects:
         [NSSortDescriptor sortDescriptorWithKey:@"name"
                                       ascending:YES], nil];
        [request setFetchBatchSize:50];
        
        fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
        
        if([fetchedObjects count]> 0){
            gf.customers = [[NSMutableArray alloc] init];
            for (Customer *customer in fetchedObjects){
                [gf.customers addObject:customer];
            }
        }
    }
}

-(SalesHeader*)getOrderByDocNo : (NSString*) docNo{
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesHeader"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@",docNo];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        return fetchedObjects[0];
    }
    else{
        return nil;
    }
}

-(SalesLine*)getSalesLineByUnit:(NSString*)docNo : (NSString*)itemNo : (NSString*)unit{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@ and itemNo == %@ and itemUnit == %@ and freeItem == %@",docNo,itemNo,unit,[NSNumber numberWithBool:NO]];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        return fetchedObjects[0];
    }
    else{
        return nil;
    }
}

-(SalesLine*)getSalesLine:(NSString*)docNo : (NSString*)lineNo : (BOOL)createNewIfNeeded{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    [cdh saveContext];
    if (![lineNo isEqualToString:@"0"]){
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
        NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@ and lineNo == %@ and freeItem == 0",docNo,lineNo];
        [request setPredicate:filter];
        NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
        if ([fetchedObjects count] > 0){
            return fetchedObjects[0];
        }
        else{
            //line doesn't exist
            if (createNewIfNeeded){
                return [self createNewSalesLine : NO];
            }
            else{
                return nil;
            }
        }
    }
    else{
        //line doesn't exist
        if (createNewIfNeeded){
            return [self createNewSalesLine : NO];
        }
        else{
            return nil;
        }
    }
}

-(NSNumber*)getNewLineNo{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@",self.selectedOrder.documentNo];
    [request setPredicate:filter];
    request.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"lineNo" ascending:NO],nil];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        SalesLine *lastLine = (SalesLine*)fetchedObjects[0];
        return [NSNumber numberWithInt:[lastLine.lineNo intValue] + 10000];
    }
    else{
        return [NSNumber numberWithInt:10000];//It is a new line
    }
}

-(SalesLine*)createNewSalesLine : (BOOL)isFreeItem{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    SalesLine *newSalesLine = [NSEntityDescription insertNewObjectForEntityForName:@"SalesLine" inManagedObjectContext:cdh.context];
    newSalesLine.documentNo = self.selectedOrder.documentNo;
    newSalesLine.orderDate = self.selectedOrder.orderDate;
    newSalesLine.lineNo = [self getNewLineNo];
    newSalesLine.freeItem = [NSNumber numberWithBool:isFreeItem];
    [cdh saveContext];
    return newSalesLine;
}

-(SalesLine*)getFreeSalesLine : (NSString*)lineNo : (BOOL)createNewIfNeeded{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@ and lineNo == %@ and freeItem == %@",self.selectedOrder.documentNo,lineNo,[NSNumber numberWithBool:YES]];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        return fetchedObjects[0];
    }
    else{
        //line doesn't exist
        if (createNewIfNeeded){
            return [self createNewSalesLine : YES];
        }
        else{
            return nil;
        }
    }
}

-(NSString*)setSelectedItemByBarcodeNEW : (NSString*)barcodeNo{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    
    NSFetchRequest *request2 = [NSFetchRequest fetchRequestWithEntityName:@"Barcode"];
    NSPredicate *filter2 = [NSPredicate predicateWithFormat:@"barcode == %@",barcodeNo];
    [request2 setPredicate:filter2];
    NSArray *fetchedObjects2 =[cdh.context executeFetchRequest:request2 error:nil];
    if ([fetchedObjects2 count] > 0){
        
        Barcode *barcode = fetchedObjects2[0];
        
        Item *item = [self getItem:barcode.itemNo];
        
        if (item == nil){
            item = [self getItem:[barcodeNo substringWithRange:NSMakeRange(0, [barcodeNo length] - 6)]];
        }
        
        if (item == nil)
            return @"NO";
        else{
            if (self.selectedItemDetails == nil){
                self.selectedItemDetails = [[SelectedItemDetails alloc] init];
            }
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                 NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* path = [documentsDirectory stringByAppendingPathComponent:
                              [NSString stringWithString:[NSString stringWithFormat:@"%@.jpeg",item.itemNo]] ];
            
            self.selectedItemDetails.itemImagePath = path;
            
            NSLog(item.name);
            NSLog(item.itemNo);
            self.selectedItemDetails.itemName = item.name;
            self.selectedItemDetails.itemNo = item.itemNo;
            self.selectedItemDetails.itemPackSize = item.packSize;
            self.selectedItemDetails.itemUnitSize = item.unitSize;
            self.selectedItemDetails.itemStockQty = item.stockQty.stringValue;
            self.selectedItemDetails.itemSalesUnit = item.salesUnit;//itemUOM.unit;
            self.selectedItemDetails.itemUnitPrice = [self getListPrice:item.itemNo :self.selectedItemDetails.itemSalesUnit];
            self.selectedItemDetails.itemBrandName = item.brandName;
            self.selectedItemDetails.itemCountryName = item.countryName;
            self.selectedItemDetails.itemSearchDescription = item.searchDescription;
            self.selectedItemDetails.itemVat = item.vat.stringValue;
            
            LastSales *lastSale = [self getLastSales:self.selectedOrder.sellToNo :item.itemNo];
            if (lastSale == nil){
                self.selectedItemDetails.itemLastSaleDate = @"";
                self.selectedItemDetails.itemLastSaleQty = @"0";
                self.selectedItemDetails.itemLastSaleUnit = @"0";
                self.selectedItemDetails.itemLastSalePrice = @"0.00";
            }
            else{
                NSDateFormatter * formatter = [NSDateFormatter new];
                [formatter setDateFormat:@"dd/MM/yyyy"];
                NSString * dateString = [formatter stringFromDate: lastSale.postingDate];
                
                self.selectedItemDetails.itemLastSaleDate = dateString;
                self.selectedItemDetails.itemLastSaleQty = lastSale.salesQty.stringValue;
                self.selectedItemDetails.itemLastSaleUnit = lastSale.salesUnit;
                self.selectedItemDetails.itemLastSalePrice = [NSString stringWithFormat:@"%@",lastSale.salesPrice.stringValue];
            }
            return @"YES";
        }
    }
    return @"NO";
}

-(NSString*)setSelectedItemByBarcode : (NSString*)barcodeNo{
    // this function is still used by Sales Role
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request2 = [NSFetchRequest fetchRequestWithEntityName:@"ItemUnits"];
    NSPredicate *filter2 = [NSPredicate predicateWithFormat:@"barcode == %@",barcodeNo];
    [request2 setPredicate:filter2];
    NSArray *fetchedObjects2 =[cdh.context executeFetchRequest:request2 error:nil];
    if ([fetchedObjects2 count] > 0){
        ItemUnits *itemUOM = fetchedObjects2[0];
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Item"];
        NSPredicate *filter = [NSPredicate predicateWithFormat:@"itemNo == %@",itemUOM.itemNo];
        [request setPredicate:filter];
        NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
        if ([fetchedObjects count] > 0){
            Item *item = fetchedObjects[0];
            if (self.selectedItemDetails == nil){
                self.selectedItemDetails = [[SelectedItemDetails alloc] init];
            }
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                                 NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* path = [documentsDirectory stringByAppendingPathComponent:
                              [NSString stringWithString:[NSString stringWithFormat:@"%@.jpeg",item.itemNo]] ];
            
            self.selectedItemDetails.itemImagePath = path;
            NSLog(item.name);
            NSLog(item.itemNo);
            self.selectedItemDetails.itemName = item.name;
            self.selectedItemDetails.itemNo = item.itemNo;
            self.selectedItemDetails.itemPackSize = item.packSize;
            self.selectedItemDetails.itemUnitSize = item.unitSize;
            self.selectedItemDetails.itemStockQty = item.stockQty.stringValue;
            self.selectedItemDetails.itemSalesUnit = item.salesUnit;//itemUOM.unit;
            self.selectedItemDetails.itemUnitPrice = [self getListPrice:item.itemNo :self.selectedItemDetails.itemSalesUnit];
            self.selectedItemDetails.itemBrandName = item.brandName;
            self.selectedItemDetails.itemCountryName = item.countryName;
            self.selectedItemDetails.itemSearchDescription = item.searchDescription;
            self.selectedItemDetails.itemVat = item.vat.stringValue;
            
            LastSales *lastSale = [self getLastSales:self.selectedOrder.sellToNo :item.itemNo];
            if (lastSale == nil){
                self.selectedItemDetails.itemLastSaleDate = @"";
                self.selectedItemDetails.itemLastSaleQty = @"0";
                self.selectedItemDetails.itemLastSaleUnit = @"0";
                self.selectedItemDetails.itemLastSalePrice = @"0.00";
            }
            else{
                NSDateFormatter * formatter = [NSDateFormatter new];
                [formatter setDateFormat:@"dd/MM/yyyy"];
                NSString * dateString = [formatter stringFromDate: lastSale.postingDate];
                
                self.selectedItemDetails.itemLastSaleDate = dateString;
                self.selectedItemDetails.itemLastSaleQty = lastSale.salesQty.stringValue;
                self.selectedItemDetails.itemLastSaleUnit = lastSale.salesUnit;
                self.selectedItemDetails.itemLastSalePrice = [NSString stringWithFormat:@"%@",lastSale.salesPrice.stringValue];
            }
        }
        return @"YES";
    }
    return @"NO";
}

-(SalesLine*)getFreeSalesLineByUnit: (NSString*)itemNo : (NSString*)unit{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@ and itemNo == %@ and itemUnit == %@ and freeItem == %@",self.selectedOrder.documentNo,itemNo,unit,[NSNumber numberWithBool:YES]];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        return fetchedObjects[0];
    }
    else{
        return nil;
    }
}

-(NSNumber*)getCurrentLineNo:(NSString*)docNo : (NSString*)itemNo : (NSString*) itemUnit{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@ and itemNo == %@ and itemUnit == %@ and freeItem == %@",docNo,itemNo,itemUnit,[NSNumber numberWithBool:NO]];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        SalesLine *salesLine = (SalesLine*)fetchedObjects[0];
        return salesLine.lineNo;
    }
    else{
        return [NSNumber numberWithInt:0];
    }
}

-(NSString*)getCustomerBalanceOnDevice : (Customer*)customer {
    
    /* we may use that in the future to show Customer balance on device
     
    NSDecimalNumber *ordersTotalAmountDecimalInDecimalNumber = customer.balance;
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request2 = [NSFetchRequest fetchRequestWithEntityName:@"SalesHeader"];
    NSPredicate *filter2 = [NSPredicate predicateWithFormat:@"sellToNo == %@",customer.customerNo];
    [request2 setPredicate:filter2];
    NSArray *fetchedObjects2 =[cdh.context executeFetchRequest:request2 error:nil];
    if ([fetchedObjects2 count] > 0){
        for (SalesHeader *salesHeader in fetchedObjects2){
            ordersTotalAmountDecimalInDecimalNumber = [ordersTotalAmountDecimalInDecimalNumber decimalNumberByAdding:[NSDecimalNumber decimalNumberWithString:[self getOrderTotalAmount:salesHeader.documentNo]]];
        }
    }
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"BalanceHistory"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"customerNo == %@ && paymentOnDevice == %@",customer.customerNo,[NSNumber numberWithBool:YES]];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        for (BalanceHistory *balance in fetchedObjects){
            ordersTotalAmountDecimalInDecimalNumber = [ordersTotalAmountDecimalInDecimalNumber decimalNumberByAdding:balance.remainingAmount];
        }
    }
        
     
    return ordersTotalAmountDecimalInDecimalNumber.stringValue;
     
     */
    
    return @"";
}

-(BOOL)checkIfLineExistsInOrder : (NSString*) docNo{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@",docNo];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        return YES;
    }
    else{
        return NO;
    }

}

-(SalesHeader*)checkIfOrderExists : (NSString*)docNo{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesHeader"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@",docNo];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        return fetchedObjects[0];
    }

    return nil;
}

-(NSString*)getOrderTotalAmount:(NSString*)orderNo{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@",orderNo];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    NSDecimalNumber *orderTotalAmountDecimalInDecimalNumber = [NSDecimalNumber numberWithInt:0];
    if ([fetchedObjects count] > 0){
        for (SalesLine *salesLine in fetchedObjects){
            orderTotalAmountDecimalInDecimalNumber = [orderTotalAmountDecimalInDecimalNumber decimalNumberByAdding:salesLine.lineAmount];
        }
    }
    
    return [NSString stringWithFormat:@"%.2f", [orderTotalAmountDecimalInDecimalNumber doubleValue]];
}
-(NSString*)getOrderTotalDiscountAmount:(NSString*)orderNo{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@",orderNo];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    NSDecimalNumber *orderTotalAmountDecimalInDecimalNumber = [NSDecimalNumber numberWithInt:0];
    if ([fetchedObjects count] > 0){
        for (SalesLine *salesLine in fetchedObjects){
            NSLog(salesLine.discountAmount.stringValue);
            orderTotalAmountDecimalInDecimalNumber = [orderTotalAmountDecimalInDecimalNumber decimalNumberByAdding:salesLine.discountAmount];
        }
    }
    
    return [NSString stringWithFormat:@"%.2f", [orderTotalAmountDecimalInDecimalNumber doubleValue]];
}
-(NSString*)getOrderTotalAmountInclVAT:(NSString*)orderNo{
    NSDecimalNumberHandler *roundUp = [NSDecimalNumberHandler
                                       decimalNumberHandlerWithRoundingMode:NSRoundUp
                                       scale:2
                                       raiseOnExactness:NO
                                       raiseOnOverflow:NO
                                       raiseOnUnderflow:NO
                                       raiseOnDivideByZero:YES];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@",orderNo];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    NSDecimalNumber *orderTotalAmountDecimalInDecimalNumber = [NSDecimalNumber numberWithInt:0];
    if ([fetchedObjects count] > 0){
        for (SalesLine *salesLine in fetchedObjects){
            orderTotalAmountDecimalInDecimalNumber = [orderTotalAmountDecimalInDecimalNumber decimalNumberByAdding:salesLine.lineAmount];
            
            NSFetchRequest *request2 = [NSFetchRequest fetchRequestWithEntityName:@"Item"];
            NSPredicate *filter2 = [NSPredicate predicateWithFormat:@"itemNo == %@",salesLine.itemNo];
            [request2 setPredicate:filter2];
            NSArray *fetchedObjects2 =[cdh.context executeFetchRequest:request2 error:nil];
            
            if ([fetchedObjects2 count] > 0){
                for (Item *item in fetchedObjects2){
                    //line amount incl. vat
                    float percentageAmount = (([item.vat floatValue] * [salesLine.lineAmount floatValue]) / 100.0f);
                    NSDecimal percInDecimal = [[NSNumber numberWithFloat:percentageAmount] decimalValue];
                    NSDecimalNumber *percInDecNum = [NSDecimalNumber decimalNumberWithDecimal:percInDecimal];
                    orderTotalAmountDecimalInDecimalNumber = [orderTotalAmountDecimalInDecimalNumber decimalNumberByAdding:percInDecNum withBehavior:roundUp];
                }
            }
        }
    }
    
    return [NSString stringWithFormat:@"%.2f", [orderTotalAmountDecimalInDecimalNumber doubleValue]];
}

-(NSString*)getOrderTotalVATAmount:(NSString*)orderNo{
    NSDecimalNumberHandler *roundUp = [NSDecimalNumberHandler
                                       decimalNumberHandlerWithRoundingMode:NSRoundUp
                                       scale:2
                                       raiseOnExactness:NO
                                       raiseOnOverflow:NO
                                       raiseOnUnderflow:NO
                                       raiseOnDivideByZero:YES];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    NSDecimalNumber *orderTotalVATAmount = [NSDecimalNumber numberWithInt:0];
    NSDecimalNumber *orderTotalAmount = [NSDecimalNumber decimalNumberWithString:[gf getOrderTotalAmount:orderNo]];
    NSDecimalNumber *orderTotalAmountInclVAT = [NSDecimalNumber decimalNumberWithString:[gf getOrderTotalAmountInclVAT:orderNo]];
    
    orderTotalVATAmount = [orderTotalAmountInclVAT decimalNumberBySubtracting:orderTotalAmount withBehavior:roundUp];
    
    return [NSString stringWithFormat:@"%.2f", [orderTotalVATAmount doubleValue]];
}
-(NSDate*)getTodayDate{
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = 0;
    
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    return [theCalendar dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
}

-(NoSeries*)getNewOrderNo{
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    //get new number from No Series
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NoSeries"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"userNoSeriesCode == %@ && seriesCode == %@",cdh.activeUser.userNoSeriesCode,@"SI"];
    [request setPredicate:filter];
    NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
    NoSeries *noSeries = fetchedObjects[0];
    
    
    
    NSString *str = [noSeries.seriesNo stringValue];
    float seriesNoInFloat = [str floatValue];
    seriesNoInFloat +=1;
    
    while ([self checkIfOrderExists:[NSString stringWithFormat:@"%@-SI%@",cdh.activeUser.userNoSeriesCode,[NSNumber numberWithFloat:seriesNoInFloat]]] != nil){
        seriesNoInFloat +=1;
    }
    
    noSeries.seriesNo = [NSNumber numberWithFloat:seriesNoInFloat];
    return noSeries;
}
-(NSString*)getLastUsedNo{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    //get new number from No Series
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NoSeries"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"userNoSeriesCode == %@ && seriesCode == %@",cdh.activeUser.userNoSeriesCode,@"SI"];
    [request setPredicate:filter];
    NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
    NoSeries *noSeries = fetchedObjects[0];
    return [noSeries.seriesNo stringValue];
}


-(NSString*)getLastInvoiceNo : (BOOL)setCurrentOrder{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesHeader"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"lastInvoice == %@ && sellToNo == %@",[NSNumber numberWithBool:YES],self.selectedCustomer.customerNo];
    [request setPredicate:filter];
    NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        SalesHeader *salesH = fetchedObjects[0];
        if (setCurrentOrder == YES){
            gf.selectedOrder = salesH;
        }
        return salesH.documentNo;
    }
    else{
        return nil;
    }
}

-(LastSales*)getLastSales:(NSString*)customerNo : (NSString*)itemNo{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"LastSales"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"customerNo == %@ && itemNo == %@",customerNo,itemNo];
    [request setPredicate:filter];
    NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        return (LastSales*)fetchedObjects[0];
    }
    else{
        return nil;
    }
}

-(NSString*)getListPrice : (NSString*)itemNo : (NSString*)itemUnit{
    Item *item = [self getItem:itemNo];
    ItemUnits *uom = [self getItemUnit:itemNo :itemUnit];
    NSDecimal qtyInDecimal = [[NSNumber numberWithFloat:[uom.qtyPerUnit floatValue]] decimalValue];
    NSDecimalNumber * qtyInDecimalNumber = [NSDecimalNumber decimalNumberWithDecimal:qtyInDecimal];
    return [item.unitPrice decimalNumberByMultiplyingBy:qtyInDecimalNumber].stringValue;
}

-(NSNumber *)getQtyIfItemExistsInOrder : (NSString*)itemNo : (NSString*)unit{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@ and itemNo == %@ and itemUnit != %@",self.selectedOrder.documentNo,itemNo,unit];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        SalesLine *salesLine = fetchedObjects[0];
        return salesLine.quantity;
    }
    return [NSNumber numberWithInt:0];
}

-(NSString*)getBestPrice : (NSString*)itemNo : (NSString*)itemUnit : (NSString*)qty{
    
    if ([qty isEqualToString:@"0"]){
        return [self getListPrice:itemNo :itemUnit];
    }
    else{
        SalesLine *salesL = [self getSalesLineByUnit:self.selectedOrder.documentNo :itemNo :itemUnit];
        if ([salesL.manualDiscount isEqualToNumber:[NSNumber numberWithBool:YES]]){
            salesL.autoDiscount = [NSNumber numberWithBool:NO];
            return salesL.itemUnitPrice.stringValue;
        }
        else{
            CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesPrice"];
            NSPredicate *filter = [NSPredicate predicateWithFormat:@"itemNo == %@ && itemUnit == %@ && minQty <= %@",itemNo,itemUnit,[NSNumber numberWithFloat:[qty floatValue]]];
            [request setPredicate:filter];
            request.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"unitPrice" ascending:YES],nil];
            NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
            if ([fetchedObjects count] > 0){
                for (SalesPrice *salesP in fetchedObjects){
                    if ([salesP.salesCode isEqualToString:self.selectedCustomer.priceGroup] || [salesP.salesCode isEqualToString:self.selectedCustomer.customerNo]){
                        salesL.autoDiscount = [NSNumber numberWithBool:YES];
                        salesL.manualDiscount = [NSNumber numberWithBool:NO];
                        return salesP.unitPrice.stringValue;
                    }
                }
            }
            return [self getListPrice:itemNo :itemUnit];
        }
    }    
}

-(ItemUnits*)getItemUnit : (NSString*)itemNo : (NSString*)itemUnit{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"ItemUnits"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"itemNo == %@ and unit == %@",itemNo,itemUnit];
    [request setPredicate:filter];
    NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        return fetchedObjects[0];
    }
    else{
        return nil;
    }
}

-(Item*)getItem:(NSString*)itemNo{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Item"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"itemNo == %@",itemNo];
    [request setPredicate:filter];
    NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        return fetchedObjects[0];
    }
    else{
        return nil;
    }
}

-(NSArray*)getItemUnits : (NSString*) itemNo{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"ItemUnits"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"itemNo == %@",itemNo];
    [request setPredicate:filter];
    
    request.sortDescriptors =
    [NSArray arrayWithObjects: [NSSortDescriptor sortDescriptorWithKey:@"qtyPerUnit" ascending:YES], nil];
    
    NSString *lastUnit = nil;
    NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
    NSMutableArray *itemUnitsArray = [[NSMutableArray alloc] init];
    for (ItemUnits *uom in fetchedObjects){
        if (lastUnit == nil){
            lastUnit = uom.unit;
            [itemUnitsArray addObject:uom];
        }
        else if (lastUnit != uom.unit){
            [itemUnitsArray addObject:uom];
        }
    }
    return itemUnitsArray;
}


-(Customer*)getCustomer:(NSString*)customerNo{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Customer"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"customerNo == %@",customerNo];
    [request setPredicate:filter];
    NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        return (Customer*)fetchedObjects[0];
    }
    else{
        return nil;
    }
}
-(NSString*)getCustomerName:(NSString*)customerNo{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Customer"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"customerNo == %@",customerNo];
    [request setPredicate:filter];
    NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        Customer *customer = fetchedObjects[0];
        return customer.name;
    }
    else{
        return nil;
    }
}

-(void)deleteBalanceHistory:(BalanceHistory*) balanceHistory{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    [cdh.context deleteObject:balanceHistory];
    [cdh saveContext];
}

-(void)deleteOrder:(SalesHeader*)salesHeader{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesLine"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"documentNo == %@",salesHeader.documentNo];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        for (SalesLine *salesLine in fetchedObjects){
            [self deleteSalesLine:salesLine];
        }
    }
    [cdh.context deleteObject:salesHeader];
    
    [cdh saveContext];
}

-(void)manageErrors : (NSError*)error{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.errorMessage = error.localizedDescription;
}


-(NSNumber*)getNewPaymentEntryNo {
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"BalanceHistory"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"customerNo == %@",gf.selectedCustomer.customerNo];
    [request setPredicate:filter];
    
    request.sortDescriptors =
    [NSArray arrayWithObjects: [NSSortDescriptor sortDescriptorWithKey:@"entryNo" ascending:NO], nil];
    
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        BalanceHistory *payment = fetchedObjects[0];
        return [NSNumber numberWithInt:payment.entryNo.intValue + 1];
    }
    else{
        return [NSNumber numberWithInt:1];
    }
}

-(BalanceHistory*)getPayment : (NSString*)entryNo{
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    BalanceHistory *payment;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"BalanceHistory"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"customerNo == %@ and entryNo == %@",gf.selectedCustomer.customerNo,entryNo];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    payment = fetchedObjects[0];
    return payment;
}

-(BalanceHistory*)createNewPayment : (NSString*)paymentType : (NSString*)dueDateText : (NSString*)paymentAmountText : (NSString*)chequeNo{
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    BalanceHistory *payment = [NSEntityDescription insertNewObjectForEntityForName:@"BalanceHistory" inManagedObjectContext:cdh.context];
    payment.documentType = @"Payment";
    payment.paymentType = paymentType;
    payment.documentDate = [NSDate date];
    payment.entryNo = [self getNewPaymentEntryNo];
    NSDateFormatter * formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd/MM/yy"];
    payment.dueDate = [formatter dateFromString:dueDateText];
    payment.documentAmount = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",paymentAmountText]];
    payment.remainingAmount = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"-%@",paymentAmountText]];
    payment.customerNo = gf.selectedCustomer.customerNo;
    payment.chequeNo = chequeNo;
    payment.paymentOnDevice = [NSNumber numberWithBool:YES];
    payment.documentNo = @"Payment On Device";
    payment.salesPersonCode = cdh.activeUser.salesPersonCode;
    payment.printed = [NSNumber numberWithBool:NO];
    [cdh saveContext];
    
    gf.selectedPayment = payment;
    return payment;
}

-(BOOL)validateNumTextField : (NSString*)text {
    
    NSMutableCharacterSet *digitsAndDots = [NSMutableCharacterSet decimalDigitCharacterSet];
    [digitsAndDots addCharactersInString:@"."];
    
    NSCharacterSet* notDigitsAndDots = [digitsAndDots invertedSet];
    if (![text isEqualToString:@""] && [text rangeOfCharacterFromSet:notDigitsAndDots].location == NSNotFound)
    {
        // newString consists only of the digits 0 through 9
        return YES;
    }
    return NO;
    
    /*
    BOOL valid;
    NSCharacterSet *alphaNums = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:text];
    valid = [alphaNums isSupersetOfSet:inStringSet];
    if (!valid){
        return NO;
    }
    return YES;
    */
}

-(void)deleteSalesLine : (SalesLine*)salesLine{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    [cdh.context deleteObject:salesLine];
}

-(void)setCategorySelection : (NSString*)categoryName : (NSString*)categoryValue : (BOOL)selection{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Categories"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"categoryName == %@ and categoryValue== %@",categoryName,categoryValue];
    [request setPredicate:filter];
    NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        Categories *category = fetchedObjects[0];
        category.isFiltered = [NSNumber numberWithBool:selection];
    }
}

-(void)clearSelectedOrders{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SalesHeader"];
    NSPredicate *filter;
    filter = [NSPredicate predicateWithFormat:@"markToSend == %@",[NSNumber numberWithBool:YES]];
    [request setPredicate:filter];
    NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        for (SalesHeader *SH in fetchedObjects){
            SH.markToSend = [NSNumber numberWithBool:NO];
        }
    }
    [cdh saveContext];
}

-(void)clearCategorySelections : (NSString*)categoryName {
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Categories"];
    NSPredicate *filter;
    if (![categoryName isEqualToString: @""])
        filter = [NSPredicate predicateWithFormat:@"isFiltered == %@ and categoryName == %@",[NSNumber numberWithBool:YES],categoryName];
    else
        filter = [NSPredicate predicateWithFormat:@"isFiltered == %@",[NSNumber numberWithBool:YES]];
    
    [request setPredicate:filter];
    NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
    if ([fetchedObjects count] > 0){
        for (Categories *cat in fetchedObjects){
            cat.isFiltered = [NSNumber numberWithBool:NO];
        }
    }
    [cdh saveContext];
    
    
    //GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    //gf.showOnlyNewItems = NO;
    //[self populateSearchDescriptions:@"Item"];
}

-(void)saveCurrentChanges{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    [cdh saveContext];
}

-(void)downloadImageAtURL:(NSString *)itemNo withCompletion:(CompletionBlock)completion{
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.expofoods.co.uk/shop/image/data/Expo/Products/%@.jpg",itemNo]];
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *jpegFilePath = [NSString stringWithFormat:@"%@/%@.jpeg",docDir,itemNo];
    
    NSFileManager *man = [NSFileManager defaultManager];
    NSDictionary *attrs = [man attributesOfItemAtPath: jpegFilePath error: NULL];
    UInt32 result = [attrs fileSize];
    
    //if (result == 0){
    
        NSURLRequest *test = [NSURLRequest requestWithURL:imageURL cachePolicy:NSURLCacheStorageAllowed timeoutInterval:600];
        
    [NSURLConnection sendAsynchronousRequest:test queue:self.operationQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        //This is your completion handler
            NSLog([NSString stringWithFormat:@"Downloading image for %@",itemNo]);
            
            //NSLog(@"%f,%f",image.size.width,image.size.height);
            
            // Let's save the file into Document folder.
            // You can also change this to your desktop for testing. (e.g. /Users/kiichi/Desktop/)
            // NSString *deskTopDir = @"/Users/kiichi/Desktop";
        
            
            // If you go to the folder below, you will find those pictures
            //NSLog(@"%@",docDir);
            
            /*
             NSLog(@"saving png");
             NSString *pngFilePath = [NSString stringWithFormat:@"%@/test.png",docDir];
             NSData *data1 = [NSData dataWithData:UIImagePNGRepresentation(image)];
             [data1 writeToFile:pngFilePath atomically:YES];
             */
            
            NSLog(@"saving jpeg");
            UIImage* image = [[UIImage alloc] initWithData:data];
        
        
        
            NSData *data2 = [NSData dataWithData:UIImageJPEGRepresentation(image, 1.0f)];//1.0f = 100% quality
            [data2 writeToFile:jpegFilePath atomically:YES];
        
        if (error){
            NSLog([NSString stringWithFormat:@"ERROR : %@",error.localizedDescription]);
            completion();
        }
        else{
            NSLog(@"saving image done");
            completion();
        }
        
    }];
    //}
}
- (void)getAllImages
{
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *jpegFilePath;
    NSFileManager *man = [NSFileManager defaultManager];
    NSDictionary *attrs;
    UInt32 result;
    
    self.operationQueue = [[NSOperationQueue alloc] init];
    self.operationQueue.maxConcurrentOperationCount = 1;
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Item"];
    request.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES],nil];
    NSArray *fetchedObjects = [cdh.context executeFetchRequest:request error:nil];
    int counter = 0;
    if ([fetchedObjects count] > 0){
        for (Item *item in fetchedObjects){
            
            //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
            //               ^{
            // Get an image from the URL below
            
            jpegFilePath = [NSString stringWithFormat:@"%@/%@.jpeg",docDir,item.itemNo];
            attrs = [man attributesOfItemAtPath: jpegFilePath error: NULL];
            result = [attrs fileSize];
            //if (result == 0){
                [self downloadImageAtURL:item.itemNo withCompletion:^{
                }];
            //}
            
            
            //               });
        }
        
    }
    //Any code placed outside of the block will likely
    // be executed before the block finishes.
}


@end
