//
//  LoginViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 14/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "User.h"

@implementation LoginViewController

#define debug 1

#pragma mark - INTERACTION

- (void)hideKeyboardWhenBackgroundIsTapped {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    UITapGestureRecognizer *tgr =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(hideKeyboard)];
    [tgr setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:tgr];
}
- (void)hideKeyboard {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [self.view endEditing:YES];
}

#pragma mark - DELEGATE: UITextField
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    if (textField == self.usernameTextField) {
        
        if ([self.usernameTextField.text isEqualToString:@"Username"]) {
            self.usernameTextField.text = @"";
        }
    }
    else
        if (textField == self.passwordTextField) {
            
            if ([self.passwordTextField.text isEqualToString:@"Password"]) {
                self.passwordTextField.text = @"";
            }
        }
    _activeField = textField;
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    _activeField = nil;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self hideKeyboardWhenBackgroundIsTapped];
    
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"ipadmini_loginbg.jpg"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    self.versionLabel.text = [NSString stringWithFormat:@"Version %@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - DATA
- (void)ensureUserCredentialIsRight {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"name == %@",self.usernameTextField.text];
    
    [request setPredicate:filter];
    
    NSArray *fetchedObjects =
    [cdh.context executeFetchRequest:request error:nil];
    
    if ([fetchedObjects count] > 0){
        for (User *user in fetchedObjects)
        {
            if ([user.name isEqualToString:self.usernameTextField.text] &&
                [user.password isEqualToString:self.passwordTextField.text]){
                
                
                cdh.activeUser = user;
                //check if currentuser
                if ([user.currentUser isEqualToNumber:[NSNumber numberWithBool:NO]]){
                    
                }
                
                
                GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
                
                [gf populateSearchDescriptions : @"Item"];
                [gf populateSearchDescriptions : @"Customer"];
                
                if ([user.password isEqualToString:@"firsttime"]){
                    [self performSegueWithIdentifier:@"ChangePassword" sender:self];
                }
                else{
                    if ([user.role isEqualToString:@"Warehouse"]){
                        gf.internalIPAddress = @"http://192.168.100.9/iPADManagerForWarehouse/";
                        //[gf refreshData];
                        [self performSegueWithIdentifier:@"LoginSuccessForWarehouse" sender:self];
                    }
                    else{
                        [self performSegueWithIdentifier:@"LoginSuccess" sender:self];
                    }
                }
                break;
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"FAILED TO LOGIN" message:@"Please check your login details and try again" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
                break;
            }
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"FAILED TO LOGIN" message:@"Please check your login details and try again" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    }
}

- (IBAction)loginButtonPressed:(id)sender {
    [self hideKeyboard];
    [self.navigationController popViewControllerAnimated:YES];
    [self ensureUserCredentialIsRight];
}
@end
