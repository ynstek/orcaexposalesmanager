//
//  CustomerForWarehouseTableViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 01/06/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "CoreDataTableViewController.h"

@interface CustomerForWarehouseTableViewController : CoreDataTableViewController<UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property BOOL isFiltered;

@end
