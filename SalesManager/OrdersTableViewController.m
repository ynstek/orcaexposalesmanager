//
//  OrdersTableViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 10/01/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "OrdersTableViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import <unistd.h>

@interface OrdersTableViewController() <MBProgressHUDDelegate> {
    MBProgressHUD *HUD;
    long long expectedLength;
    long long currentLength;
}

@end

@implementation OrdersTableViewController
#define debug 1

- (void)configureFetch {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request =
    [NSFetchRequest fetchRequestWithEntityName:@"SalesHeader"];
    
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"salesPersonCode == %@ && markToDelete == %@ && lastInvoice == %@",cdh.activeUser.salesPersonCode,[NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO]];
    [request setPredicate:filter];
    
    request.sortDescriptors =
    [NSArray arrayWithObjects:
     [NSSortDescriptor sortDescriptorWithKey:@"documentNo"
                                   ascending:YES],
     nil];
    [request setFetchBatchSize:50];
    self.frc =
    [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                        managedObjectContext:cdh.context
                                          sectionNameKeyPath:nil
                                                   cacheName:nil];
    self.frc.delegate = self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureFetch];
    [self performFetch];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(performFetch) name:@"SomethingChanged" object:nil];
    
    //to create a space between bar item buttons
    //START
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = 40.0f;
    //END
    
    
    //eger sadece order da create buttonu gormek istersen bunu kaldir didselect itemdakini ac
    self.doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Send" style:UIBarButtonItemStyleDone target:self action:@selector(sendOrders)];
    self.doneButton2 = [[UIBarButtonItem alloc] initWithTitle:@"Select/Deselect All" style:UIBarButtonItemStyleDone target:self action:@selector(selectAllOrders)];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:self.doneButton,fixedItem, self.doneButton2, nil];
    
    
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.progress = 0.0f;
    
    self.salesHF = [SalesHeaderFunctions defaultService];
    [self followBusyStatus];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sendOrders{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirmation"
                                                    message:@"Are you sure to send order(s)?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    switch(buttonIndex) {
        case 0: //"No" pressed
            //do something?
            break;
        case 1: //"Yes" pressed
            gf.errorMessage = @"";
            HUD = nil;
            [self.salesHF SendHeaders:NO];
            break;
    }
}
- (void)followBusyStatus{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    self.salesHF.busyUpdate = ^(BOOL busy)
    {
        if (busy)
        {
            gf.progress = 0.0f;
            if (HUD == nil){
                HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
                [self.navigationController.view addSubview:HUD];
                HUD.dimBackground = YES;
                HUD.delegate = self;
                HUD.labelText = @"Sending Order(s)";
                HUD.detailsLabelText = @"Please Wait";
                [HUD showWhileExecuting:@selector(myProgressTask) onTarget:self withObject:nil animated:YES];
            }
        } else
        {
        }
    };
}
- (void)myProgressTask {
    // This just increases the progress indicator in a loop
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    while (gf.progress < 1.0f) {
    }
    if ([gf.errorMessage isEqualToString:@""]){
        gf.errorMessage = @"Order(s) are sent sucessfully!";
    }
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Sending Orders" message:gf.errorMessage delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
}

- (void)selectAllOrders{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request2 =
    [NSFetchRequest fetchRequestWithEntityName:@"SalesHeader"];
    
    NSPredicate *filter2 = [NSPredicate predicateWithFormat:@"salesPersonCode == %@ && markToDelete == %@",cdh.activeUser.salesPersonCode,[NSNumber numberWithBool:NO]];
    [request2 setPredicate:filter2];
    
    NSArray *fetchedObjects2 =[cdh.context executeFetchRequest:request2 error:nil];
    
    
    
    NSFetchRequest *request =
    [NSFetchRequest fetchRequestWithEntityName:@"SalesHeader"];
    
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"salesPersonCode == %@ && markToDelete == %@ && markToSend == %@",cdh.activeUser.salesPersonCode,[NSNumber numberWithBool:NO],[NSNumber numberWithBool:YES]];
    [request setPredicate:filter];
    
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    
    if ([fetchedObjects count] == [fetchedObjects2 count]){
        for (SalesHeader *salesH in fetchedObjects2){
            salesH.markToSend = [NSNumber numberWithBool:NO];
        }
    }
    else{
        for (SalesHeader *salesH in fetchedObjects2){
            salesH.markToSend = [NSNumber numberWithBool:YES];
        }
    }
    [cdh saveContext];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSArray*)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    static NSString *cellIdentifier = @"Order Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    SalesHeader *salesHeader = [self.frc objectAtIndexPath:indexPath];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *localDateString = [dateFormatter stringFromDate:salesHeader.orderDate];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@ - %@",salesHeader.documentNo,[gf getCustomerName:salesHeader.sellToNo],localDateString];
    
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"£ %@",[gf getOrderTotalAmount:salesHeader.documentNo]];
    
    
    if ([salesHeader.markToSend isEqualToNumber:[NSNumber numberWithBool:YES]]){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    SalesHeader *salesHeader = [self.frc objectAtIndexPath:indexPath];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark){
        salesHeader.markToSend = [NSNumber numberWithBool:NO];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else{
        salesHeader.markToSend = [NSNumber numberWithBool:YES];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    [cdh saveContext];
}

/*kullanicam!!!
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"OrderView"])
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.selectedOrder = [self.frc objectAtIndexPath:[self.tableView indexPathForSelectedRow]];
    }
    else {
        NSLog(@"Undefined Segue Attempted!");
    }
}
*/

@end
