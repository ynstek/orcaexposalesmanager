//
//  SalesLineFunctions.h
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WindowsAzureMobileServices/WindowsAzureMobileServices.h>
#import "SalesLine.h"

#pragma mark * Block Definitions

typedef void (^CompletionBlock) ();
typedef void (^CompletionWithIndexBlock) (NSUInteger index);
typedef void (^BusyUpdateBlock) (BOOL busy);


@interface SalesLineFunctions : NSObject

@property (nonatomic, strong)   NSArray *salesLRecords;
@property (nonatomic, copy)     BusyUpdateBlock busyUpdate;
@property (nonatomic, strong)   MSClient *client;
@property (nonatomic, strong) NSMutableArray *salesLResults;

-(void)insertCoreDataTable : (MSTable*) table;
-(void)SendLines : (NSString*)docNo completion:(CompletionWithIndexBlock)completion;

+ (SalesLineFunctions *)defaultService;
- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response;

@end
