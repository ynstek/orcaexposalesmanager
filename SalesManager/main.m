//
//  main.m
//  SalesManager
//
//  Created by Cihan TASKIN on 12/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
