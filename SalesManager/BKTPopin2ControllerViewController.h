//
// The MIT License (MIT)
//
// Copyright (c) 2013 Backelite
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <UIKit/UIKit.h>
#import "BKTPopinControllerViewController.h"
@interface BKTPopin2ControllerViewController : BKTPopinControllerViewController

@property (strong, nonatomic) IBOutlet UITextField *qtyTextField;
@property (strong, nonatomic) IBOutlet UITextField *unitPriceTextField;
@property (strong, nonatomic) IBOutlet UISegmentedControl *unitSelectionSegment;
@property (strong, nonatomic) IBOutlet UILabel *freeQtyLabel;
@property (strong, nonatomic) IBOutlet UIView *syncErrorView;
@property (strong, nonatomic) IBOutlet UILabel *syncErrorLabel;

@property (strong, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *itemImage;
@property (strong, nonatomic) IBOutlet UILabel *itemUnitLabel;
@property (strong, nonatomic) IBOutlet UILabel *itemListPriceLabel;
@property (strong, nonatomic) IBOutlet UILabel *itemPackAndUnitSize;
@property (strong, nonatomic) IBOutlet UILabel *itemStockQty;
@property (strong, nonatomic) IBOutlet UILabel *lastSaleDateLabel;
@property (strong, nonatomic) IBOutlet UILabel *lastSaleQty;
@property (strong, nonatomic) IBOutlet UILabel *lastSaleUnit;
@property (strong, nonatomic) IBOutlet UILabel *lastSalePrice;


- (IBAction)increaseButtonPressed:(id)sender;
- (IBAction)decreaseButtonPressed:(id)sender;
- (IBAction)qtyTextFieldEditingDidEnd:(id)sender;
- (IBAction)qtyTextFieldEditingDidBegin:(id)sender;
- (IBAction)unitPriceTextFieldEditingDidEnd:(id)sender;
- (IBAction)unitPriceTextFieldEditingDidBegin:(id)sender;
- (IBAction)unitSelectionChanged:(id)sender;
- (IBAction)addFreeButtonPressed:(id)sender;
- (IBAction)addButtonPressed:(id)sender;

@end
