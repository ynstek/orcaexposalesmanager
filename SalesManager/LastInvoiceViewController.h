//
//  LastInvoiceViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 05/03/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LastInvoiceViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UISearchBar *itemSearchBar;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property BOOL isFiltered;
@end
