//
//  KeyboardViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 03/03/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeyboardViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *keyboardValueLabel;
- (IBAction)dotButtonPressed:(id)sender;

- (IBAction)clearButtonPressed:(id)sender;
- (IBAction)numberZeroButtonPressed:(id)sender;
- (IBAction)numberOneButtonPressed:(id)sender;
- (IBAction)numberTwoButtonPressed:(id)sender;
- (IBAction)numberThreeButtonPressed:(id)sender;
- (IBAction)numberFourButtonPressed:(id)sender;
- (IBAction)numberFiveButtonPressed:(id)sender;
- (IBAction)numberSixButtonPressed:(id)sender;
- (IBAction)numberSevenButtonPressed:(id)sender;
- (IBAction)numberEightButtonPressed:(id)sender;
- (IBAction)numberNineButtonPressed:(id)sender;

@end
