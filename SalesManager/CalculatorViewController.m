//
//  CalculatorViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 28/05/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "CalculatorViewController.h"

@interface CalculatorViewController ()

@end

@implementation CalculatorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.keyboardValueLabel.text = @"0";
    self.firstNum = @"";
    self.secondNum = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setKeyboardText : (NSString*)buttonValue{
    
    if ([buttonValue isEqualToString:@""]){
        self.keyboardValueLabel.text = @"0";
    }
    else if (self.newNum){
        self.keyboardValueLabel.text = buttonValue;
        self.newNum = NO;
    }
    else if ([self.keyboardValueLabel.text isEqualToString:@"0"]){
        self.keyboardValueLabel.text = buttonValue;
    }
    else{
        if ([buttonValue isEqualToString:@"."]){
            if ([self.keyboardValueLabel.text rangeOfString:@"."].location == NSNotFound) {
                self.keyboardValueLabel.text = [self.keyboardValueLabel.text stringByAppendingString:buttonValue];
            }
        }
        else{
            self.keyboardValueLabel.text = [self.keyboardValueLabel.text stringByAppendingString:buttonValue];
        }
    }
}

-(void)setNums{
}

-(void)calculateOperation{
    //self.nextOperation = opr;
    NSDecimalNumber *result;
    if ([self.firstNum isEqualToString:@""]){
        self.firstNum = self.keyboardValueLabel.text;
    }
    else{
        NSLog(self.keyboardValueLabel.text);
        if (([self.keyboardValueLabel.text isEqualToString:@"0"]) || ([self.keyboardValueLabel.text isEqualToString: @"(null)"])){
            self.firstNum = @"";
            self.secondNum = @"";
            result = [NSDecimalNumber numberWithInt:0];
        }
        else{
            self.secondNum = self.keyboardValueLabel.text;
        
            NSDecimalNumber *firstDecimal = [NSDecimalNumber decimalNumberWithString:self.firstNum];
            NSDecimalNumber *secondDecimal = [NSDecimalNumber decimalNumberWithString:self.secondNum];
        
            if ([self.nextOperation isEqualToString:@"/"]){
                result = [firstDecimal decimalNumberByDividingBy:secondDecimal];
            }
            else if ([self.nextOperation isEqualToString:@"X"]){
                result = [firstDecimal decimalNumberByMultiplyingBy:secondDecimal];
            }
            else if ([self.nextOperation isEqualToString:@"-"]){
                result = [firstDecimal decimalNumberBySubtracting:secondDecimal];
            }
            else if ([self.nextOperation isEqualToString:@"+"]){
                result = [firstDecimal decimalNumberByAdding:secondDecimal];
            }
            else if ([self.nextOperation isEqualToString:@"="]){
            }
        }
        self.keyboardValueLabel.text = [NSString stringWithFormat:@"%@",result];
        self.firstNum = self.keyboardValueLabel.text;
        self.secondNum = @"";
    }
    self.newNum = YES;
}

- (IBAction)dotButtonPressed:(id)sender {
    [self setKeyboardText:@"."];
}

- (IBAction)clearButtonPressed:(id)sender {
    self.firstNum = @"";
    self.secondNum = @"";
    self.nextOperation = @"";
    [self setKeyboardText:@""];
}

- (IBAction)numberZeroButtonPressed:(id)sender {
    [self setKeyboardText:@"0"];
}

- (IBAction)numberOneButtonPressed:(id)sender {
    [self setKeyboardText:@"1"];
}

- (IBAction)numberTwoButtonPressed:(id)sender {
    [self setKeyboardText:@"2"];
}

- (IBAction)numberThreeButtonPressed:(id)sender {
    [self setKeyboardText:@"3"];
}

- (IBAction)numberFourButtonPressed:(id)sender {
    [self setKeyboardText:@"4"];
}

- (IBAction)numberFiveButtonPressed:(id)sender {
    [self setKeyboardText:@"5"];
}

- (IBAction)numberSixButtonPressed:(id)sender {
    [self setKeyboardText:@"6"];
}

- (IBAction)numberSevenButtonPressed:(id)sender {
    [self setKeyboardText:@"7"];
}

- (IBAction)numberEightButtonPressed:(id)sender {
    [self setKeyboardText:@"8"];
}

- (IBAction)numberNineButtonPressed:(id)sender {
    [self setKeyboardText:@"9"];
}

- (IBAction)divideButtonPressed:(id)sender {
    [self calculateOperation];
    self.nextOperation = @"/";
}

- (IBAction)multiplyButtonPressed:(id)sender {
    [self calculateOperation];
    self.nextOperation = @"X";
}

- (IBAction)subtractButtonPressed:(id)sender {
    [self calculateOperation];
    self.nextOperation = @"-";
}

- (IBAction)plusButtonPressed:(id)sender {
    [self calculateOperation];
    self.nextOperation = @"+";
}

- (IBAction)equalsButtonPressed:(id)sender {
    [self calculateOperation];
    self.firstNum = @"";
    self.secondNum = @"";
    self.nextOperation = @"=";
}

@end
