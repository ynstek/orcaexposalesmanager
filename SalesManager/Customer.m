//
//  Customer.m
//  SalesManager
//
//  Created by Cihan TASKIN on 03/02/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "Customer.h"


@implementation Customer

@dynamic address;
@dynamic address2;
@dynamic balance;
@dynamic block;
@dynamic city;
@dynamic customerNo;
@dynamic name;
@dynamic phone;
@dynamic postCode;
@dynamic priceGroup;
@dynamic salesPersonCode;
@dynamic unclearedFunds;

@end
