//
//  PaymentCollectionViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 28/01/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "PaymentCollectionViewController.h"
#import "AppDelegate.h"
#import "MiniPrinterFunctions.h"
#import "DatePickerViewController.h"

@interface PaymentCollectionViewController()
@property NSMutableArray *paymentArray;
@end

@implementation PaymentCollectionViewController
NSDate *fromDate,*toDate;
- (void)viewDidLoad {
    [super viewDidLoad];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    NSDateFormatter * formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd/MM/yy"];
    NSString * dateString = [formatter stringFromDate: [gf getTodayDate]];
    
    self.fromDateTextField.text = dateString;
    self.toDateTextField.text = dateString;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissDatePickerPopover) name:@"dismissDatePickerPopover" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.paymentArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    UITableViewCell *cell;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *localDateString;
        static NSString *cellIdentifier = @"balanceCell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        BalanceHistory *balanceHistory = [self.paymentArray objectAtIndex:indexPath.row];
        localDateString = [dateFormatter stringFromDate:balanceHistory.documentDate];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Amount :%@",balanceHistory.documentAmount];
        cell.textLabel.text = [NSString stringWithFormat:@"%@ | %@ %@",localDateString,balanceHistory.paymentType,[gf getCustomerName:balanceHistory.customerNo]];
    return cell;
}


- (IBAction)showButtonPressed:(id)sender {
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSDecimalNumber *totalAmount = [NSDecimalNumber numberWithInt:0], *totalCashAmount = [NSDecimalNumber numberWithInt:0], *totalChequeAmount = [NSDecimalNumber numberWithInt:0];

    
    
    NSDateFormatter * formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd-MM-yy"];
    
    fromDate = [formatter dateFromString:self.fromDateTextField.text];
    toDate = [gf dateForEndOfDay:[formatter dateFromString:self.toDateTextField.text]];

    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"BalanceHistory"];
    
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"salesPersonCode == %@ && documentDate >= %@ && documentDate <= %@ && paymentOnDevice == %@",cdh.activeUser.salesPersonCode, fromDate,toDate,[NSNumber numberWithBool:YES]];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    self.paymentArray = [fetchedObjects mutableCopy];
    
    if ([fetchedObjects count] > 0){
        for (BalanceHistory *balance in fetchedObjects){
            totalAmount = [totalAmount decimalNumberByAdding:balance.documentAmount];
            if ([balance.paymentType isEqualToString:@"Cash"]){
                totalCashAmount = [totalCashAmount decimalNumberByAdding:balance.documentAmount];
            }
            else{
                totalChequeAmount = [totalChequeAmount decimalNumberByAdding:balance.documentAmount];
            }
        }
    }
    self.totalAmountLabel.text = [NSString stringWithFormat:@"£ %@",totalAmount.stringValue];
    self.cashTotalAmountLabel.text = [NSString stringWithFormat:@"£ %@",totalCashAmount.stringValue];
    self.chequeTotalAmountLabel.text = [NSString stringWithFormat:@"£ %@",totalChequeAmount.stringValue];
    [self.paymentTableView reloadData];
}

- (IBAction)printAndDeleteButtonPressed:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirmation"
                                                    message:@"Do you want to print Collection Report?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex) {
        case 0: //"No" pressed
            break;
        case 1: //"Yes" pressed
                if (blocking) {
                    return;
                }
                blocking = YES;
                NSString *portName = [AppDelegate getPortName];
                NSString *portSettings = [AppDelegate getPortSettings];
            [MiniPrinterFunctions printCollectionReportWithPortname:portName portSettings:portSettings widthInch:0:fromDate:toDate];
                blocking = NO;
            break;
    }
}

- (IBAction)fromDatePickerImagePressed:(id)sender {
    self.datePickerCaller = @"fromDate";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];
    DatePickerViewController *datePickerView =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"DatePickerViewController"];
    
    datePickerView.parentName = @"PaymentCollection";
    datePickerView.titleText = @"From Date";
    datePickerView.isHistoryEnabled = YES;
    self.popover = [[UIPopoverController alloc]initWithContentViewController:datePickerView];
    self.popover.delegate = self;
    self.popover.popoverContentSize = CGSizeMake(150, 220);
    [self.popover presentPopoverFromRect:self.fromDateTextField.bounds inView:self.fromDateTextField permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (IBAction)toDatePickerImagePressed:(id)sender {
    self.datePickerCaller = @"toDate";
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];
    DatePickerViewController *datePickerView =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"DatePickerViewController"];
    
    datePickerView.parentName = @"PaymentCollection";
    datePickerView.titleText = @"To Date";
    datePickerView.isHistoryEnabled = YES;
    
    self.popover = [[UIPopoverController alloc]initWithContentViewController:datePickerView];
    self.popover.delegate = self;
    self.popover.popoverContentSize = CGSizeMake(150, 220);
    [self.popover presentPopoverFromRect:self.fromDateTextField.bounds inView:self.fromDateTextField permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)dismissDatePickerPopover {
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    if ([self.datePickerCaller isEqualToString:@"fromDate"]){
        self.fromDateTextField.text = gf.selectedDate;
    }
    else{
        self.toDateTextField.text = gf.selectedDate;
    }
}
@end
