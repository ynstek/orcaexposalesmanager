//
//  AppDelegate.h
//  SalesManager
//
//  Created by Cihan TASKIN on 12/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GenericFunctions.h"
#import "SyncFunctions.h"
#import "CoreDataHelper.h"
#import <WindowsAzureMobileServices/WindowsAzureMobileServices.h>
#import "ItemFunctions.h"
#import "CustomerFunctions.h"
#import "UserFunctions.h"
#import "NoSeriesFunctions.h"
#import "BalanceHistoryFunctions.h"
#import "CatgoryFunctions.h"
#import "LastSalesFunctions.h"
#import "ItemUnitsFunctions.h"
#import "SalesHeaderFunctions.h"
#import "SalesLineFunctions.h"
#import "SalesPriceFunctions.h"
#import "SystemTableFunctions.h"
#import "Barcode.h"
#import "UnitsOfMeasure.h"
#import "Captuvo.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate,CaptuvoEventsProtocol>
@property (strong, nonatomic) MSClient *client;


@property (strong, nonatomic) UIWindow *window;

- (NSURL *)applicationDocumentsDirectory;

@property (strong, nonatomic) UIViewController *viewController1;

@property (nonatomic,strong, readonly) CoreDataHelper * coreDataHelper;
- (CoreDataHelper*)cdh;

@property (nonatomic,strong, readonly) GenericFunctions * genericFunctions;
- (GenericFunctions*)gf;

@property (nonatomic,strong, readonly) ItemFunctions * itemFunctions;
- (ItemFunctions*)itemF;
@property (nonatomic,strong, readonly) UserFunctions * userFunctions;
- (UserFunctions*)userF;
@property (nonatomic,strong, readonly) SystemTableFunctions * systemTableFunctions;
- (SystemTableFunctions*)sysTableF;
@property (nonatomic,strong, readonly) NoSeriesFunctions * noSeriesFunctions;
- (NoSeriesFunctions*)noSerF;
@property (nonatomic,strong, readonly) BalanceHistoryFunctions * balanceHistoryFunctions;
- (BalanceHistoryFunctions*)balanceF;
@property (nonatomic,strong, readonly) CatgoryFunctions * catgoryFunctions;
- (CatgoryFunctions*)catF;
@property (nonatomic,strong, readonly) LastSalesFunctions * lastSalesFunctions;
- (LastSalesFunctions*)lastSaleF;
@property (nonatomic,strong, readonly) ItemUnitsFunctions * itemUnitsFunctions;
- (ItemUnitsFunctions*)itemUnitsF;
@property (nonatomic,strong, readonly) SalesHeaderFunctions * salesHeaderFunctions;
- (SalesHeaderFunctions*)salesHF;
@property (nonatomic,strong, readonly) SalesLineFunctions * salesLineFunctions ;
- (SalesLineFunctions*)salesLF;
@property (nonatomic,strong, readonly) SalesPriceFunctions * salesPriceFunctions;
- (SalesPriceFunctions*)salesPF;
@property (nonatomic,strong, readonly) CustomerFunctions * customerFunctions;
- (CustomerFunctions*)customerF;

-(void)loading;


@property (nonatomic, copy) void(^backgroundTransferCompletionHandler)();


//Printing

+ (NSString *)getPortName;
+ (NSString*)getPortSettings;


@end

