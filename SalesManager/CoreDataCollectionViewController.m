//
//  CoreDataCollectionViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 15/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "CoreDataCollectionViewController.h"

@implementation CoreDataCollectionViewController
#define debug 1


-(void)loadView{
    
    
    self.view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 238, 36)];

}

#pragma mark - FETCHING
- (void)performFetch {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    if (self.frc) {
        [self.frc.managedObjectContext performBlockAndWait:^{
            
            NSError *error = nil;
            if (![self.frc performFetch:&error]) {
                
                NSLog(@"Failed to perform fetch: %@", error);
            }
            [self.collectionView reloadData];
        }];
    } else {
        NSLog(@"Failed to fetch, the fetched results controller is nil.");
    }
}

#pragma mark - DATASOURCE: UICollectionView
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    return [[self.frc.sections objectAtIndex:section] numberOfObjects];
}



@end
