//
//  PaymentViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 04/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "PaymentViewController.h"
#import "AppDelegate.h"
#import "DatePickerViewController.h"
#import "MiniPrinterFunctions.h"

@interface PaymentViewController ()

@end

@implementation PaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dismissDatePickerPopover) name:@"dismissDatePickerPopover" object:nil];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    NSDateFormatter * formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd/MM/yy"];
    if (gf.selectedPayment == nil){
        self.dueDateTextField.text = [formatter stringFromDate: [NSDate date]];
        self.paymentDateTextField.text = [formatter stringFromDate: [NSDate date]];
        self.paymentTypeSelection.selectedSegmentIndex = 0;
        [self showControls: [NSNumber numberWithInt:self.paymentTypeSelection.selectedSegmentIndex]];
    }
    else{
        self.dueDateTextField.text = [formatter stringFromDate: gf.selectedPayment.dueDate];
        self.paymentDateTextField.text = [formatter stringFromDate: gf.selectedPayment.documentDate];
        self.paymentAmountTextField.text = gf.selectedPayment.documentAmount.stringValue;
        if ([gf.selectedPayment.paymentType isEqualToString:@"Cash"]){
            self.paymentTypeSelection.selectedSegmentIndex = 0;
        }
        else{
            self.paymentTypeSelection.selectedSegmentIndex = 1;
        }
        self.chequeNoTextField.text = gf.selectedPayment.chequeNo;
        [self showControls: [NSNumber numberWithInt:self.paymentTypeSelection.selectedSegmentIndex]];
    }
    
    gf.selectedDate = self.dueDateTextField.text;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"dismissPaymentPopover" object:self];
}

- (IBAction)saveButtonPressed:(id)sender {
    [self savePayment];
    [self dismissViewControllerAnimated:YES completion:Nil];
}

- (IBAction)datePickerImagePressed:(id)sender {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle:nil];
    DatePickerViewController *datePickerView =
    [mainStoryboard instantiateViewControllerWithIdentifier:@"DatePickerViewController"];
    
    datePickerView.parentName = @"Payment";
    datePickerView.titleText = @"Due Date";
    self.popover = [[UIPopoverController alloc]initWithContentViewController:datePickerView];
    self.popover.delegate = self;
    self.popover.popoverContentSize = CGSizeMake(150, 220);
    [self.popover presentPopoverFromRect:self.dueDateTextField.bounds inView:self.dueDateTextField permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

- (void)dismissDatePickerPopover {
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    self.dueDateTextField.text = gf.selectedDate;
}
-(void)savePayment {
    NSString *selectedPaymentType = [self.paymentTypeSelection titleForSegmentAtIndex:self.paymentTypeSelection.selectedSegmentIndex];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    if (gf.selectedPayment == nil){
        [gf createNewPayment:selectedPaymentType :self.dueDateTextField.text :self.paymentAmountTextField.text :self.chequeNoTextField.text];
    }
    else{
        CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
        gf.selectedPayment.documentType = @"Payment";
        gf.selectedPayment.paymentType = selectedPaymentType;
        gf.selectedPayment.documentDate = [NSDate date];
        NSDateFormatter * formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"dd/MM/yy"];
        gf.selectedPayment.dueDate = [formatter dateFromString:self.dueDateTextField.text];
        gf.selectedPayment.documentAmount = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",self.paymentAmountTextField.text]];
        gf.selectedPayment.remainingAmount = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"-%@",self.paymentAmountTextField.text]];
        gf.selectedPayment.customerNo = gf.selectedCustomer.customerNo;
        gf.selectedPayment.paymentOnDevice = [NSNumber numberWithBool:YES];
        gf.selectedPayment.salesPersonCode = cdh.activeUser.salesPersonCode;
        [cdh saveContext];
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirmation"
                                                    message:@"Do you want to print?"
                                                   delegate:self
                                          cancelButtonTitle:@"No"
                                          otherButtonTitles:@"Yes", nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex) {
        case 0: //"No" pressed
            //do something?
            break;
        case 1: //"Yes" pressed
            [self printPayment];
            break;
    }
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.selectedPayment = nil;
}

-(void)printPayment{
    if (blocking) {
        return;
    }
    blocking = YES;
    NSString *portName = [AppDelegate getPortName];
    NSString *portSettings = [AppDelegate getPortSettings];
    
    [MiniPrinterFunctions PrintSavedPaymentWithPortName:portName portSettings:portSettings widthInch:0];
    blocking = NO;
}
- (IBAction)paymentTypeSelectionChanged:(id)sender {
    [self showControls: [NSNumber numberWithInt:self.paymentTypeSelection.selectedSegmentIndex]];
}

-(void)showControls : (NSNumber*)selectedIndex{
    if (selectedIndex == [NSNumber numberWithInt:0]){
        self.dueDateLabel.hidden = YES;
        self.dueDateTextField.hidden = YES;
        self.chequeNoLabel.hidden = YES;
        self.chequeNoTextField.hidden = YES;
    }
    else{
        self.dueDateLabel.hidden = NO;
        self.dueDateTextField.hidden = NO;
        self.chequeNoLabel.hidden = NO;
        self.chequeNoTextField.hidden = NO;
    }
}
@end
