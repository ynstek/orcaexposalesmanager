//
// The MIT License (MIT)
//
// Copyright (c) 2013 Backelite
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "BKTPopin2ControllerViewController.h"
#import "AppDelegate.h"
#import "UIViewController+MaryPopin.h"

@interface BKTPopin2ControllerViewController ()
@property (strong, nonatomic) NSString *prevQuantity,*prevPrice;
@property BOOL isNewSelectedItem,isConfirmationMessage;
@property float changedQty;
@property SalesLine *freeLine;

@end

@implementation BKTPopin2ControllerViewController

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"dismissItemCardWithBarcode" object:self];
}
-(void)viewWillAppear:(BOOL)animated{
    
    @try {
        [self.syncErrorView setHidden:YES];
        self.isNewSelectedItem = YES;
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        SalesLine *salesLine = [gf getSalesLineByUnit:gf.selectedOrder.documentNo :gf.selectedItemDetails.itemNo :gf.selectedItemDetails.itemSalesUnit];
        
        self.itemNameLabel.text = gf.selectedItemDetails.itemName;
        self.itemImage.image=[UIImage imageWithContentsOfFile:gf.selectedItemDetails.itemImagePath];
        self.itemUnitLabel.text = gf.selectedItemDetails.itemSalesUnit;
        self.itemListPriceLabel.text = gf.selectedItemDetails.itemUnitPrice;
        self.itemPackAndUnitSize.text = [NSString stringWithFormat:@"%@ X %@",gf.selectedItemDetails.itemPackSize,gf.selectedItemDetails.itemUnitSize];
        self.itemStockQty.text = gf.selectedItemDetails.itemStockQty;
        self.lastSaleDateLabel.text = gf.selectedItemDetails.itemLastSaleDate;
        self.lastSalePrice.text = gf.selectedItemDetails.itemLastSalePrice;
        self.lastSaleQty.text = gf.selectedItemDetails.itemLastSaleQty;
        self.lastSaleUnit.text = gf.selectedItemDetails.itemLastSaleUnit;
        
        while(self.unitSelectionSegment.numberOfSegments > 0) {
            [self.unitSelectionSegment removeSegmentAtIndex:0 animated:NO];
        }
        
        NSArray *itemUnits = [gf getItemUnits:gf.selectedItemDetails.itemNo];
        int counter = 0;
        for (ItemUnits *uom in itemUnits){
            [self.unitSelectionSegment insertSegmentWithTitle:uom.unit
                                                      atIndex:counter
                                                     animated:false];
            if ([uom.unit isEqualToString:gf.selectedItemDetails.itemSalesUnit]){
                self.unitSelectionSegment.selectedSegmentIndex = counter;
                //NSLog(uom.unit);
            }
            counter += 1;
        }
        
        NSString *selectedUnit = [self.unitSelectionSegment titleForSegmentAtIndex:self.unitSelectionSegment.selectedSegmentIndex];
        
        [self setUnitColor:self.unitSelectionSegment : selectedUnit];
        
        if (salesLine != nil){
            self.qtyTextField.text = salesLine.quantity.stringValue;
            self.unitPriceTextField.text = salesLine.itemUnitPrice.stringValue;
        }
        else{
            self.qtyTextField.text = @"0";
            self.unitPriceTextField.text = [gf getBestPrice:gf.selectedItemDetails.itemNo :selectedUnit : @"0"];
        }
        
        if (gf.isAutoAdded == YES){
            self.changedQty = [self.qtyTextField.text floatValue];
            self.changedQty +=1;
            
            [self showConfirmationMessageIfNeeded];
        }
        [self setFreeLineQuantity:selectedUnit];
    } @catch (NSException *ex) {
        [self.syncErrorView setHidden:NO];
    }
}

-(void)showErrorMessage : (NSString*) errMsg{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Item Card Screen Error" message:[NSString stringWithFormat:@"Error : %@",errMsg] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){}];
    [alert addAction:okAction];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)viewDidLoad{
}


-(void)setUnitColor : (UISegmentedControl*) unitSelection : (NSString*)selectedUnit{
    if ([selectedUnit isEqualToString:@"BOX"]){
        unitSelection.tintColor = [UIColor blueColor];
    }
    else if ([selectedUnit isEqualToString:@"EACH"]) {
        unitSelection.tintColor = [UIColor redColor];
    }
    else{
        unitSelection.tintColor = [UIColor blackColor];
    }
}


-(void)setFreeLineQuantity : (NSString*)unit{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    self.freeLine = [gf getFreeSalesLineByUnit :gf.selectedItemDetails.itemNo : unit];
    
    if (self.freeLine == nil){
        self.freeQtyLabel.text = @"Add Free";
    }
    else{
        self.freeQtyLabel.text = [NSString stringWithFormat:@"Free (%@)",self.freeLine.quantity.stringValue];
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (self.isConfirmationMessage == YES){
        switch(buttonIndex) {
            case 0: //"No" pressed
                break;
            case 1: //"Yes" pressed
                [self updateSalesLine:[[NSNumber numberWithFloat:self.changedQty] stringValue]:nil];
                break;
        }
        self.isConfirmationMessage = NO;
    }
}

- (IBAction)qtyTextFieldEditingDidBegin:(id)sender {
    self.prevQuantity = self.qtyTextField.text;
    self.qtyTextField.text = @"";
}

- (IBAction)qtyTextFieldEditingDidEnd:(id)sender {
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    if ([gf validateNumTextField:self.qtyTextField.text] == YES){
        if (![self.prevQuantity isEqualToString:self.qtyTextField.text]){
            self.changedQty = self.qtyTextField.text.floatValue;
            [self showConfirmationMessageIfNeeded];
            //[self updateSalesLine:self.qtyTextField.text :nil];
        }
    }
    else{
        self.qtyTextField.text = self.prevQuantity;
    }
}
- (IBAction)increaseButtonPressed:(id)sender {
    
    self.changedQty = [self.qtyTextField.text floatValue];
    self.changedQty +=1;
    
    [self showConfirmationMessageIfNeeded];
}

- (IBAction)decreaseButtonPressed:(id)sender {
    self.changedQty = [self.qtyTextField.text floatValue];
    self.changedQty -=1;
    [self showConfirmationMessageIfNeeded];
}
-(void)showConfirmationMessageIfNeeded{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    NSString *selectedUnit = [self.unitSelectionSegment titleForSegmentAtIndex:self.unitSelectionSegment.selectedSegmentIndex];
    
    
    NSNumber *otherQty = [gf getQtyIfItemExistsInOrder:gf.selectedItemDetails.itemNo :selectedUnit];
    if ((self.isNewSelectedItem == YES) && (![otherQty isEqualToNumber:[NSNumber numberWithInt:0]])){
        self.isConfirmationMessage = YES;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Item is already added with a different unit. Qty = %@",otherQty]
                                                        message:@"Do you want to change Quantity?"
                                                       delegate:self
                                              cancelButtonTitle:@"No"
                                              otherButtonTitles:@"Yes", nil];
        [alert show];
        
    }
    else{
        [self updateSalesLine:[[NSNumber numberWithFloat:self.changedQty] stringValue]:nil];
    }
}

- (IBAction)unitPriceTextFieldEditingDidBegin:(id)sender {
    self.prevPrice = self.unitPriceTextField.text;
    self.unitPriceTextField.text = @"";
}

- (IBAction)unitPriceTextFieldEditingDidEnd:(id)sender {
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    if ([gf validateNumTextField:self.unitPriceTextField.text] == YES){
        if (![self.prevPrice isEqualToString:self.unitPriceTextField.text]){
            [self updateSalesLine:nil :self.unitPriceTextField.text];
        }
    }
    else{
        self.unitPriceTextField.text = self.prevPrice;
    }
}

-(void)changeUnit : (BOOL)setLastUnit{
    
    
    if (setLastUnit == YES){
        int counter = 0;
        while (self.unitSelectionSegment.numberOfSegments > counter){
            if ([self.lastSaleUnit.text isEqualToString:[self.unitSelectionSegment titleForSegmentAtIndex:counter]]){
                self.unitSelectionSegment.selectedSegmentIndex = counter;
                break;
            }
            counter += 1;
        }
    }
    
    NSString *selectedUnit = [self.unitSelectionSegment titleForSegmentAtIndex:self.unitSelectionSegment.selectedSegmentIndex];
    
    [self setUnitColor:self.unitSelectionSegment : selectedUnit];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    SalesLine *currentSalesLine = [gf getSalesLineByUnit:gf.selectedOrder.documentNo :gf.selectedItemDetails.itemNo : selectedUnit];
    if (currentSalesLine == nil){
        self.qtyTextField.text = @"0";
        self.unitPriceTextField.text = [gf getBestPrice:gf.selectedItemDetails.itemNo :selectedUnit : @"0"];
    }
    else{
        self.qtyTextField.text = currentSalesLine.quantity.stringValue;
        self.unitPriceTextField.text = currentSalesLine.itemUnitPrice.stringValue;
    }
    
    self.isNewSelectedItem = YES;
    
    [self setFreeLineQuantity:selectedUnit];
}

- (IBAction)unitSelectionChanged:(id)sender {
    [self changeUnit:NO];
}

- (IBAction)addFreeButtonPressed:(id)sender {
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    NSString *selectedUnit = [self.unitSelectionSegment titleForSegmentAtIndex:self.unitSelectionSegment.selectedSegmentIndex];
    SalesLine *currentSalesLine;
    @try {
        currentSalesLine = [gf getFreeSalesLineByUnit :gf.selectedItemDetails.itemNo : selectedUnit];
        if (currentSalesLine == nil){
            currentSalesLine = [gf getFreeSalesLine:@"0" :YES];
        }
        
        currentSalesLine.itemNo = gf.selectedItemDetails.itemNo;
        currentSalesLine.itemName = self.itemNameLabel.text;
        currentSalesLine.itemUnit = selectedUnit;
        currentSalesLine.itemUnitPrice = [NSDecimalNumber decimalNumberWithString:@"0"];
        
        float test = currentSalesLine.quantity.floatValue;
        test +=1;
        
        currentSalesLine.manualDiscount = [NSNumber numberWithBool:YES];
        currentSalesLine.discountAmount = [NSDecimalNumber decimalNumberWithString:self.itemListPriceLabel.text];
        currentSalesLine.quantity = [NSNumber numberWithFloat:test];
        currentSalesLine.orderDate = gf.selectedOrder.orderDate;
        currentSalesLine.lineAmount = [NSNumber numberWithInt:0];
        
        [gf saveCurrentChanges];
        [self setFreeLineQuantity:selectedUnit];
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Free Item Added" message:[NSString stringWithFormat:@"Free item quantity is %@",currentSalesLine.quantity.stringValue] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
    } @catch (NSException *ex) {
        [gf deleteSalesLine:currentSalesLine];
        [self showErrorMessage:[NSString stringWithFormat:@"%@",ex]];
    }
}

- (IBAction)addButtonPressed:(id)sender {
     if (![self.lastSalePrice.text isEqualToString:@"0.00"]){
         [self changeUnit:YES];
         
         self.unitPriceTextField.text = self.lastSalePrice.text;
     
         NSString *selectedUnit = [self.unitSelectionSegment titleForSegmentAtIndex:self.unitSelectionSegment.selectedSegmentIndex];
     
         GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
         SalesLine *currentSalesLine = [gf getSalesLineByUnit:gf.selectedOrder.documentNo :gf.selectedItemDetails.itemNo : selectedUnit];
         if (currentSalesLine == nil){
             self.qtyTextField.text = @"0";
         }
         else{
             self.qtyTextField.text = currentSalesLine.quantity.stringValue;
         }
         self.unitPriceTextField.text = self.lastSalePrice.text;
         float test = [self.qtyTextField.text floatValue];
         test +=1;
         
         [self updateSalesLine:nil:self.lastSalePrice.text];
         [self updateSalesLine:[[NSNumber numberWithFloat:test] stringValue]:nil];
         
         //UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Last Sale Added" message:[NSString stringWithFormat:@"%@ %@ added for %@.",self.lastSaleQty.text,self.lastSaleUnit.text,self.lastSalePrice.text] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
         //[alert show];
     }
     else{
         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No Last Sales Info" message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
         [alert show];
     }
}

-(void)updateSalesLine : (NSString*)newQty : (NSString*)newPrice{
    NSString *selectedUnit = [self.unitSelectionSegment titleForSegmentAtIndex:self.unitSelectionSegment.selectedSegmentIndex];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    SalesLine *currentSalesLine;
    @try {
        
        currentSalesLine = [gf getSalesLineByUnit:gf.selectedOrder.documentNo :gf.selectedItemDetails.itemNo : selectedUnit];
        if (currentSalesLine == nil){
            currentSalesLine = [gf getSalesLine:gf.selectedOrder.documentNo :@"0" :YES];
        }
        
        
        if (newPrice == nil){
            
            self.qtyTextField.text = newQty;
            float test = [self.qtyTextField.text floatValue];
            if (test <= 0){
                [gf deleteSalesLine:currentSalesLine];
                self.qtyTextField.text = @"0";
                self.unitPriceTextField.text = [gf getBestPrice:gf.selectedItemDetails.itemNo :selectedUnit :self.qtyTextField.text];
            }
            else{
                if ([currentSalesLine.manualDiscount isEqualToNumber:[NSNumber numberWithBool:NO]]){
                    NSString *bestPrice = [gf getBestPrice:gf.selectedItemDetails.itemNo :selectedUnit : self.qtyTextField.text];
                    if (bestPrice != nil){
                        self.unitPriceTextField.text = bestPrice;
                        currentSalesLine.autoDiscount = [NSNumber numberWithBool:YES];
                    }
                }
                
                
                currentSalesLine.itemNo = gf.selectedItemDetails.itemNo;
                currentSalesLine.itemName = self.itemNameLabel.text;
                currentSalesLine.itemUnit = selectedUnit;
                currentSalesLine.itemUnitPrice = [NSDecimalNumber decimalNumberWithString:self.unitPriceTextField.text];
                currentSalesLine.quantity = [NSNumber numberWithFloat:self.qtyTextField.text.floatValue];
                currentSalesLine.orderDate = gf.selectedOrder.orderDate;
                
                NSDecimal unitPriceTextInDecimal = [[NSNumber numberWithFloat:[self.unitPriceTextField.text floatValue]] decimalValue];
                NSDecimalNumber * unitPriceDecimalInDecimalNumber = [NSDecimalNumber decimalNumberWithDecimal:unitPriceTextInDecimal];
                
                NSDecimal qtyTextInDecimal = [[NSNumber numberWithFloat:[self.qtyTextField.text floatValue]] decimalValue];
                NSDecimalNumber * qtyDecimalInDecimalNumber = [NSDecimalNumber decimalNumberWithDecimal:qtyTextInDecimal];
                
                currentSalesLine.lineAmount = [unitPriceDecimalInDecimalNumber decimalNumberByMultiplyingBy:qtyDecimalInDecimalNumber];
            }
        }
        else{
            self.unitPriceTextField.text = newPrice;
            currentSalesLine.itemNo = gf.selectedItemDetails.itemNo;
            currentSalesLine.itemName = self.itemNameLabel.text;
            currentSalesLine.itemUnit = selectedUnit;
            if (![currentSalesLine.quantity isEqualToNumber:[NSNumber numberWithInt:0]]){
                currentSalesLine.quantity = [NSDecimalNumber decimalNumberWithString:self.qtyTextField.text];
            }
            else{
                self.qtyTextField.text = [NSNumber numberWithFloat:1.0f].stringValue;
                currentSalesLine.quantity = [NSDecimalNumber decimalNumberWithString:@"1"];
            }
            currentSalesLine.itemUnitPrice = [NSDecimalNumber decimalNumberWithString:newPrice];
            currentSalesLine.orderDate = gf.selectedOrder.orderDate;
            currentSalesLine.manualDiscount = [NSNumber numberWithBool:YES];
            currentSalesLine.autoDiscount = [NSNumber numberWithBool:NO];
            
            NSDecimal unitPriceTextInDecimal = [[NSNumber numberWithFloat:[self.unitPriceTextField.text floatValue]] decimalValue];
            NSDecimalNumber * unitPriceDecimalInDecimalNumber = [NSDecimalNumber decimalNumberWithDecimal:unitPriceTextInDecimal];
            
            NSDecimal qtyTextInDecimal = [[NSNumber numberWithFloat:[self.qtyTextField.text floatValue]] decimalValue];
            NSDecimalNumber * qtyDecimalInDecimalNumber = [NSDecimalNumber decimalNumberWithDecimal:qtyTextInDecimal];
            
            currentSalesLine.lineAmount = [unitPriceDecimalInDecimalNumber decimalNumberByMultiplyingBy:qtyDecimalInDecimalNumber];
        }
        
        self.qtyTextField.text = currentSalesLine.quantity.stringValue;
        
        [gf saveCurrentChanges];
    } @catch (NSException *ex) {
        [gf deleteSalesLine:currentSalesLine];
        [self showErrorMessage:[NSString stringWithFormat:@"%@.This sales line will be deleted!",ex]];
    }
}

@end
