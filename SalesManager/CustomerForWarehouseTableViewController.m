//
//  CustomerForWarehouseTableViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 01/06/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "CustomerForWarehouseTableViewController.h"
#import "AppDelegate.h"

@interface CustomerForWarehouseTableViewController ()

@end

@implementation CustomerForWarehouseTableViewController

#define debug 1

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated{
    
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(200, 0, 300, 44)];
    self.searchBar.showsCancelButton = YES;
    self.searchBar.translucent = NO;
    self.searchBar.delegate = self;
    [self.navigationController.navigationBar addSubview:self.searchBar];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Customer"];
    
    request.sortDescriptors = [NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"customerNo" ascending:YES],nil];
    gf.customerList = [[cdh.context executeFetchRequest:request error:nil] mutableCopy];
    [self.tableView reloadData];
    
    gf.selectedCustomer = nil;
}
-(void)viewWillDisappear:(BOOL)animated{
    [self.searchBar removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    return 1;
}

- (NSArray*)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    static NSString *cellIdentifier = @"Customer Cell";
    UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                    forIndexPath:indexPath];
    
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    Customer *customer;
    
    if (self.isFiltered == NO){
        customer = (Customer*)[gf.customerList objectAtIndex:indexPath.row];
    }
    else{
        customer = (Customer*)[gf.searchCustomerResults objectAtIndex:indexPath.row];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@",customer.name,customer.postCode];
    cell.detailTextLabel.text = customer.customerNo;
    
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    if (self.isFiltered == NO)
    {
        return [gf.customerList count];
    }
    else{
        return [gf.searchCustomerResults count];
    }
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    if (searchText.length == 0)
    {
        self.isFiltered = NO;
    }
    else{
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        self.isFiltered = YES;
        gf.searchCustomerResults = [[NSMutableArray alloc] init];
        
        for (Customer *customer in gf.customerList){
            NSRange itemNameRange = [customer.name rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (itemNameRange.location != NSNotFound){
                [gf.searchCustomerResults addObject:customer];
            }
        }
    }
    [self.tableView reloadData];
}

-(void)createNewOrder{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    gf.isAutoAdded = NO;
    
    SalesHeader *newSalesHeader = [NSEntityDescription insertNewObjectForEntityForName:@"SalesHeader" inManagedObjectContext:cdh.context];
    
    NSError *error = nil;
    if (![cdh.context
          obtainPermanentIDsForObjects:[NSArray arrayWithObject:newSalesHeader]
          error:&error]) {
        NSLog(@"Couldn't obtain a permanent ID for object %@", error);
    }
    
    NoSeries *noSeries = gf.getNewOrderNo;
    newSalesHeader.documentNo = [NSMutableString stringWithFormat:@"%@-%@%@",noSeries.userNoSeriesCode,noSeries.seriesCode,noSeries.seriesNo];
    newSalesHeader.sellToNo = gf.selectedCustomer.customerNo;
    newSalesHeader.salesPersonCode = cdh.activeUser.salesPersonCode;
    newSalesHeader.orderDate = gf.getTodayDate;
    newSalesHeader.requestedDeliveryDate = newSalesHeader.orderDate;
    
    gf.selectedOrder = newSalesHeader;
    
    [cdh saveContext];
    
    /*
     CatalogViewController *catalog = [self.storyboard instantiateViewControllerWithIdentifier:@"CatalogViewController"];
     [self.navigationController pushViewController:catalog animated:YES];
     */
    //GridViewViewController *catalog = [self.storyboard instantiateViewControllerWithIdentifier:@"GridViewViewController"];
    //[self.navigationController pushViewController:catalog animated:YES];
    
    
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"CatalogViewNew"])
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        Customer *customer;
        if (self.isFiltered == NO){
            customer = [gf.customerList objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        }
        else{
            customer = [gf.searchCustomerResults objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        }
        gf.selectedCustomer = customer;
        [self createNewOrder];
        self.isFiltered = NO;
    }
    else {
        NSLog(@"Undefined Segue Attempted!");
    }
}
@end
