//
//  MainMenuForWhsViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 04/06/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "MainMenuForWhsViewController.h"
#import "AppDelegate.h"

@interface MainMenuForWhsViewController ()

@end

@implementation MainMenuForWhsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    gf.itemArrayFilter = 1;
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"warehouseMainMenu.jpg"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    
    UserFunctions *userF = [(AppDelegate *)[[UIApplication sharedApplication] delegate] userF];
    [userF setActiveUser];
    self.navigationItem.hidesBackButton = YES;
    
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    self.activeUserName.text = cdh.activeUser.name;
    
    self.versionLabel.text = [NSString stringWithFormat:@"Version %@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)syncButtonPressed:(id)sender {
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    [gf refreshData];
}
@end
