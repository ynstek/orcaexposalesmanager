//
//  MainMenuForWhsViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 04/06/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainMenuForWhsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *activeUserName;
@property (strong, nonatomic) IBOutlet UILabel *versionLabel;
- (IBAction)syncButtonPressed:(id)sender;

@end
