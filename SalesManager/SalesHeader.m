//
//  SalesHeader.m
//  SalesManager
//
//  Created by Cihan TASKIN on 23/01/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import "SalesHeader.h"


@implementation SalesHeader

@dynamic comment;
@dynamic documentNo;
@dynamic markToDelete;
@dynamic orderDate;
@dynamic requestedDeliveryDate;
@dynamic salesPersonCode;
@dynamic sellToNo;
@dynamic markToSend;
@dynamic lastInvoice;

@end
