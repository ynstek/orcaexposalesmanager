//
//  CatgoryFunctions.m
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "CatgoryFunctions.h"
#import "AppDelegate.h"

@interface CatgoryFunctions() <MSFilter>

@property (nonatomic, strong) MSTable *categoryTable;
@property (nonatomic) NSInteger busyCount;

@end
@implementation CatgoryFunctions
@synthesize categoryRecords;

- (void)emptyCoreDataTable:(NSString*)tableName : (NSManagedObject*) table{
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:tableName];
    
    NSArray *fetchedObjects =
    [cdh.context executeFetchRequest:request error:nil];
    
    for(table in fetchedObjects){
        [cdh.context deleteObject:table];
    }
}

- (void)loadDataRecursiveForQuery:(MSQuery *)query withCompletion:(CompletionBlock)completion
{
    query.includeTotalCount = YES;
    query.fetchLimit = 1000; //note: you can adjust this to whatever amount is optimum
    query.fetchOffset = self.categoryResults.count;
    
    [query readWithCompletion:^(NSArray *items,NSInteger totalCount,NSError *error){
        if(!error) {
            //add the items to our local copy
            [self.categoryResults addObjectsFromArray:items];
            
            if (totalCount > [self.categoryResults count]) {
                [self loadDataRecursiveForQuery:query withCompletion:completion];
            } else {
                completion();
            }
        }
        else{
            [self logErrorIfNotNil:error];
            completion();
        }
    }];
}

-(void)updateCategory : (NSDictionary*) recDic{
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Categories"];
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"categoryName == %@ and categoryValue == %@",[recDic objectForKey:@"categoryname"],[recDic objectForKey:@"categoryvalue"]];
    [request setPredicate:filter];
    NSArray *fetchedObjects =[cdh.context executeFetchRequest:request error:nil];
    Categories *category;
    if ([fetchedObjects count] == 0){
        category = [NSEntityDescription insertNewObjectForEntityForName:@"Categories" inManagedObjectContext:cdh.context];
    }
    else{
        category = fetchedObjects[0];
    }
    
    category.categoryName = [recDic objectForKey:@"categoryname"];
    category.categoryValue = [recDic objectForKey:@"categoryvalue"];
}

-(void)updateCoreDataTable : (NSString*)categoryName : (NSString*) categoryValue{
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    //GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"categoryname == %@ and categoryvalue == %@",categoryName,categoryValue];

    [self.categoryTable readWithPredicate:predicate completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.categoryRecords = [results mutableCopy];
         self.categoryResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:self.categoryTable predicate:predicate];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.categoryResults){
                 [self updateCategory:recDic];
             }
             [cdh saveContext];
         }];
     }];
    
}

-(void)insertCoreDataTable : (MSTable*) table{
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    cdh.activeUser.isReadyToUse = [NSNumber numberWithBool:NO];
    //NSPredicate * predicate = [NSPredicate predicateWithFormat:@"name != %@",@""];
    [table readWithPredicate:nil completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.categoryRecords = [results mutableCopy];
         self.categoryResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:table predicate:nil];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.categoryResults){
                 if ([table.name isEqualToString:@"Categories"]){
                     if ([recDic objectForKey:@"categoryvaluecode"] != nil){
                         Categories *categories = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                         categories.categoryName = [recDic objectForKey:@"categoryname"];
                         categories.categoryValue = [recDic objectForKey:@"categoryvalue"];
                         categories.relatedValueCode = [recDic objectForKey:@"relatedvaluecode"];
                         categories.categoryValueCode = [recDic objectForKey:@"categoryvaluecode"];
                     }
                 }
             }
             [cdh saveContext];
             NSLog(@"Categories saved!");
         }];
     }];
}

#pragma mark * MSFilter methods

- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response
{
    // A wrapped response block that decrements the busy counter
    MSFilterResponseBlock wrappedResponse = ^(NSHTTPURLResponse *innerResponse, NSData *data, NSError *error)
    {
        [self busy:NO];
        response(innerResponse, data, error);
    };
    
    // Increment the busy counter before sending the request
    [self busy:YES];
    next(request, wrappedResponse);
}


+ (CatgoryFunctions *)defaultService
{
    static CatgoryFunctions* service;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[CatgoryFunctions alloc] init];
    });
    return service;
}

-(CatgoryFunctions *)init
{
    self = [super init];
    
    if (self)
    {
        // Initialize the Mobile Service client with your URL and key
        MSClient *client = [MSClient clientWithApplicationURLString:@"https://expoipadapp.azure-mobile.net/"
                                                     applicationKey:@"MgayYYCxnOsHFtZJhTbtGPmFeeTdDU98"];
        
        // Add a Mobile Service filter to enable the busy indicator
        self.client = [client clientWithFilter:self];
        self.categoryTable = [_client tableWithName:@"Categories"];
        self.categoryRecords = [[NSMutableArray alloc] init];
        self.busyCount = 0;
    }
    return self;
}

- (void)busy:(BOOL)busy
{
    // assumes always executes on UI thread
    if (busy)
    {
        if (self.busyCount == 0 && self.busyUpdate != nil)
        {
            self.busyUpdate(YES);
        }
        self.busyCount++;
    }
    else
    {
        if (self.busyCount == 1 && self.busyUpdate != nil)
        {
            self.busyUpdate(FALSE);
        }
        self.busyCount--;
    }
}

- (void)logErrorIfNotNil:(NSError *) error
{
    if (error)
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.errorMessage = error.localizedDescription;
        gf.progress = 1.0f;
        NSLog(@"ERROR %@", error);
    }
}

@end
