//
//  CustomersTableViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 14/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "CustomersTableViewController.h"
#import "AppDelegate.h"
#import "Customer.h"

@implementation CustomersTableViewController
#define debug 1

#pragma mark - DATA
- (void)configureFetch {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request =
    [NSFetchRequest fetchRequestWithEntityName:@"Customer"];
    
    NSPredicate *filter = [NSPredicate predicateWithFormat:@"salesPersonCode == %@",cdh.activeUser.salesPersonCode];
    [request setPredicate:filter];
    
    request.sortDescriptors =
    [NSArray arrayWithObjects:
     [NSSortDescriptor sortDescriptorWithKey:@"name"
                                   ascending:YES],
     nil];
    [request setFetchBatchSize:50];
    self.frc =
    [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                        managedObjectContext:cdh.context
                                          sectionNameKeyPath:nil
                                                   cacheName:nil];
    self.frc.delegate = self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    gf.itemArrayFilter = 1;
    [gf populateSearchDescriptions : @"Item"];
    [gf populateSearchDescriptions : @"Customer"];
    
    [self configureFetch];
    [self performFetch];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(performFetch)
                                                 name:@"SomethingChanged"
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(200, 0, 300, 44)];
    self.searchBar.showsCancelButton = YES;
    self.searchBar.translucent = NO;
    self.searchBar.delegate = self;
    [self.navigationController.navigationBar addSubview:self.searchBar];
    [self.tableView reloadData];
}
-(void)viewWillDisappear:(BOOL)animated{
    [self.searchBar removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    return 1;
}

- (NSArray*)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    static NSString *cellIdentifier = @"Customer Cell";
    UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:cellIdentifier
                                    forIndexPath:indexPath];
    
    
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    
    Customer *customer;
    
    if (self.isFiltered == NO){
        customer = [self.frc objectAtIndexPath:indexPath];
    }
    else{
        customer = (Customer*)[gf.searchCustomerResults objectAtIndex:indexPath.row];
    }
    
    cell.textLabel.text = customer.name;
    cell.detailTextLabel.text = customer.customerNo;

    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    if (self.isFiltered == NO)
    {
        return [gf.customers count];
    }
    else{
        return [gf.searchCustomerResults count];
    }
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Customer Screen"])
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        
        Customer *customer;
        
        if (self.isFiltered == NO){
            customer = [gf.customers objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        }
        else{
            customer = [gf.searchCustomerResults objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        }
        
        gf.selectedCustomer = customer;
        self.isFiltered = NO;
    }
    else {
        NSLog(@"Undefined Segue Attempted!");
    }
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    if (searchText.length == 0)
    {
        self.isFiltered = NO;
    }
    else{
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        self.isFiltered = YES;
        gf.searchCustomerResults = [[NSMutableArray alloc] init];
        
        for (Customer *customer in gf.customers){
            NSRange itemNameRange = [customer.name rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (itemNameRange.location != NSNotFound){
                [gf.searchCustomerResults addObject:customer];
            }
        }
    }
    [self.tableView reloadData];
}
@end
