//
//  DatePickerViewController.m
//  SalesManager
//
//  Created by Cihan TASKIN on 29/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "DatePickerViewController.h"
#import "AppDelegate.h"

@implementation DatePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    if (!self.isHistoryEnabled){
        self.datePicker.minimumDate = [gf getTodayDate];
    }
    if (gf.selectedOrder.requestedDeliveryDate != nil){
        self.datePicker.date = gf.selectedOrder.requestedDeliveryDate;
    }
    //self.titleLabel.text = self.titleText;
}

-(void)viewDidDisappear:(BOOL)animated{
    GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
    if ([self.parentName isEqualToString:@"Catalog"]){
        SalesHeader *salesHeader = [gf getOrderByDocNo:gf.selectedOrder.documentNo];
        salesHeader.requestedDeliveryDate = [self.datePicker date];
        gf.selectedOrder.requestedDeliveryDate = [self.datePicker date];
        [gf saveCurrentChanges];
    }
    else{
        NSDateFormatter * formatter = [NSDateFormatter new];
        [formatter setDateFormat:@"dd/MM/yy"];
        gf.selectedDate = [formatter stringFromDate: [self.datePicker date]];
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:@"dismissDatePickerPopover" object:self];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
