//
//  NoSeries.m
//  SalesManager
//
//  Created by Cihan TASKIN on 24/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "NoSeries.h"


@implementation NoSeries

@dynamic seriesCode;
@dynamic seriesNo;
@dynamic userNoSeriesCode;

@end
