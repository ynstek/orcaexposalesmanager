//
//  SelectedItemDetails.h
//  SalesManager
//
//  Created by Cihan TASKIN on 14/02/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SelectedItemDetails : NSObject

@property (strong, nonatomic) NSString * itemName;
@property (strong, nonatomic) NSString * itemNo;
@property (strong, nonatomic) NSString * itemUnitSize;
@property (strong, nonatomic) NSString * itemPackSize;
@property (strong, nonatomic) NSString * itemStockQty;
@property (strong, nonatomic) NSString * itemUnitPrice;
@property (strong, nonatomic) NSString * itemSalesUnit;
@property (strong, nonatomic) NSString * itemBrandName;
@property (strong, nonatomic) NSString * itemCountryName;
@property (strong, nonatomic) NSString * itemSearchDescription;
@property (strong, nonatomic) NSString * itemVat;
@property (strong, nonatomic) NSString * itemLastSaleDate;
@property (strong, nonatomic) NSString * itemLastSaleQty;
@property (strong, nonatomic) NSString * itemLastSaleUnit;
@property (strong, nonatomic) NSString * itemLastSalePrice;
@property (strong, nonatomic) NSString * itemImagePath;
@end
