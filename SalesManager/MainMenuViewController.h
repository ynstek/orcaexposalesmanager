//
//  MainMenuViewController.h
//  SalesManager
//
//  Created by Cihan TASKIN on 14/11/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SyncFunctions.h"

@interface MainMenuViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *activeUserName;
@property (strong, nonatomic) IBOutlet UILabel *versionLabel;

@end
