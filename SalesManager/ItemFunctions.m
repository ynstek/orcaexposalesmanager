//
//  ItemFunctions.m
//  SalesManager
//
//  Created by Cihan TASKIN on 28/12/2014.
//  Copyright (c) 2014 Orca Business Solutions. All rights reserved.
//

#import "ItemFunctions.h"
#import "AppDelegate.h"

@interface ItemFunctions() <MSFilter>

@property (nonatomic, strong) MSTable *itemTable;
@property (nonatomic) NSInteger busyCount;

@end

@implementation ItemFunctions
@synthesize itemRecords;

- (void)emptyCoreDataTable:(NSString*)tableName : (NSManagedObject*) table{
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:tableName];
    
    NSArray *fetchedObjects =
    [cdh.context executeFetchRequest:request error:nil];
    
    for(table in fetchedObjects){
        [cdh.context deleteObject:table];
    }
}

- (void)loadDataRecursiveForQuery:(MSQuery *)query withCompletion:(CompletionBlock)completion
{
    query.includeTotalCount = YES;
    query.fetchLimit = 1000; //note: you can adjust this to whatever amount is optimum
    query.fetchOffset = self.itemResults.count;
    
    [query readWithCompletion:^(NSArray *items,NSInteger totalCount,NSError *error){
        if(!error) {
            //add the items to our local copy
            [self.itemResults addObjectsFromArray:items];
            
            if (totalCount > [self.itemResults count]) {
                [self loadDataRecursiveForQuery:query withCompletion:completion];
            } else {
                completion();
            }
        }
        else{
            [self logErrorIfNotNil:error];
            completion();
        }
    }];
}

-(void)insertCoreDataTable : (MSTable*) table{
    
    CoreDataHelper *cdh =
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    
    cdh.activeUser.isReadyToUse = [NSNumber numberWithBool:NO];
    //NSPredicate * predicate = [NSPredicate predicateWithFormat:@"itemno != %@",@""];
    [table readWithPredicate:nil completion:^(NSArray *results, NSInteger totalCount, NSError *error)
     {
         [self logErrorIfNotNil:error];
         self.itemRecords = [results mutableCopy];
         self.itemResults = [[NSMutableArray alloc] init];
         MSQuery *query = [[MSQuery alloc] initWithTable:table predicate:nil];
         [self loadDataRecursiveForQuery:query withCompletion:^{
             for (NSDictionary *recDic in self.itemResults){
                 if ([table.name isEqualToString:@"Item"]){
                     Item *item = [NSEntityDescription insertNewObjectForEntityForName:table.name inManagedObjectContext:cdh.context];
                     item.brandName = [recDic objectForKey:@"brandname"];
                     item.countryName = [recDic objectForKey:@"countryname"];
                     item.discounted = [recDic objectForKey:@"discounted"];
                     item.itemNo = [recDic objectForKey:@"itemno"];
                     item.name = [recDic objectForKey:@"name"];
                     item.packSize = [recDic objectForKey:@"packsize"];
                     item.salesUnit = [recDic objectForKey:@"salesunit"];
                     item.searchDescription = [recDic objectForKey:@"searchdescription"];
                     item.stockQty = [recDic objectForKey:@"stockqty"];
                     item.unitPrice = [recDic objectForKey:@"unitprice"];
                     item.unitSize = [recDic objectForKey:@"unitsize"];
                     item.vat = [recDic objectForKey:@"vat"];
                     item.itemCategoryCode = [recDic objectForKey:@"itemcategory"];
                     item.productGroupCode = [recDic objectForKey:@"productgroup"];
                         //ItemUnitsFunctions *itemUnitsF = [ItemUnitsFunctions new];
                         //[itemUnitsF insertCoreDataTable:self.itemUnitsTable:item.itemNo];
                 }
             }
             [cdh saveContext];
             NSLog(@"Items saved!");
         }];
     }];
}

#pragma mark * MSFilter methods

- (void)handleRequest:(NSURLRequest *)request
                 next:(MSFilterNextBlock)next
             response:(MSFilterResponseBlock)response
{
    // A wrapped response block that decrements the busy counter
    MSFilterResponseBlock wrappedResponse = ^(NSHTTPURLResponse *innerResponse, NSData *data, NSError *error)
    {
        [self busy:NO];
        response(innerResponse, data, error);
    };
    
    // Increment the busy counter before sending the request
    [self busy:YES];
    
    next(request, wrappedResponse);
}

+ (SyncFunctions *)defaultService
{
    static SyncFunctions* service;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[SyncFunctions alloc] init];
    });
    
    return service;
}

-(ItemFunctions *)init
{
    self = [super init];
    
    if (self)
    {
        // Initialize the Mobile Service client with your URL and key
        MSClient *client = [MSClient clientWithApplicationURLString:@"https://expoipadapp.azure-mobile.net/"
                                                     applicationKey:@"MgayYYCxnOsHFtZJhTbtGPmFeeTdDU98"];
        
        // Add a Mobile Service filter to enable the busy indicator
        self.client = [client clientWithFilter:self];
        self.itemTable = [_client tableWithName:@"Item"];
        self.itemRecords = [[NSMutableArray alloc] init];
        self.busyCount = 0;
    }
    return self;
}

- (void)busy:(BOOL)busy
{
    // assumes always executes on UI thread
    if (busy)
    {
        if (self.busyCount == 0 && self.busyUpdate != nil)
        {
            self.busyUpdate(YES);
        }
        self.busyCount++;
    }
    else
    {
        if (self.busyCount == 1 && self.busyUpdate != nil)
        {
            self.busyUpdate(FALSE);
        }
        self.busyCount--;
    }
}

- (void)logErrorIfNotNil:(NSError *) error
{
    if (error)
    {
        GenericFunctions *gf = [(AppDelegate *)[[UIApplication sharedApplication] delegate] gf];
        gf.errorMessage = error.localizedDescription;
        gf.progress = 1.0f;
        NSLog(@"ERROR %@", error);
        
    }
}

@end
