//
//  MiniPrinterFunctions.h
//  IOS_SDK
//
//  Created by Tzvi on 8/2/11.
//  Copyright 2011 - 2013 STAR MICRONICS CO., LTD. All rights reserved.
//
#import <Foundation/Foundation.h>
//#import "PrinterFunctions.h"
#import "CommonEnum.h"
#import "StarIO/SMPort.h"
#import "CoreDataHelper.h"



typedef enum {
    BarcodeWidth_125 = 0,
    BarcodeWidth_250 = 1,
    BarcodeWidth_375 = 2,
    BarcodeWidth_500 = 3,
    BarcodeWidth_625 = 4,
    BarcodeWidth_750 = 5,
    BarcodeWidth_875 = 6,
    BarcodeWidth_1_0 = 7
} BarcodeWidth;

typedef enum {
    BarcodeType_code39 = 0,
    BarcodeType_code93 = 1,
    BarcodeType_ITF = 2,
    BarcodeType_code128 = 3
}BarcodeType;

@interface MiniPrinterFunctions : NSObject {
    SMPort *starPort;
}
+ (void)PrintText:(NSString*)portName
     PortSettings:(NSString*)portSettings
        Underline:(bool)underline
       Emphasized:(bool)emphasized
      Upsideddown:(bool)upsideddown
      InvertColor:(bool)invertColor
  HeightExpansion:(unsigned char)heightExpansion
   WidthExpansion:(unsigned char)widthExpansion
       LeftMargin:(int)leftMargin
        Alignment:(Alignment)alignment
      TextToPrint:(unsigned char*)textToPrint
  TextToPrintSize:(unsigned int)textToPrintSize;

+ (void)printOrderListWithPortname:(NSString *)portName portSettings:(NSString *)portSettings widthInch:(int)printableWidth : (NSDate*)fromDateString : (NSDate*)toDateString;
+ (void)PrintSampleReceiptWithPortname:(NSString *)portName portSettings:(NSString *)portSettings widthInch:(int)printableWidth : (BOOL)showPrice;
+ (void)PrintDailySummaryReceiptWithPortname:(NSString *)portName portSettings:(NSString *)portSettings widthInch:(int)printableWidth;
+ (void)PrintSavedPaymentWithPortName:(NSString *)portName portSettings:(NSString *)portSettings widthInch:(int)printableWidth;
//+ (NSData *)create2InchReceipt;
+ (void)printCollectionReportWithPortname:(NSString *)portName portSettings:(NSString *)portSettings widthInch:(int)printableWidth : (NSDate*)fromDateString : (NSDate*)toDateString;
+ (void)printMiniStatementWithPortName:(NSString *)portName portSettings:(NSString *)portSettings widthInch:(int)printableWidth;
@end
