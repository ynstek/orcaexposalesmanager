//
//  Item.h
//  SalesManager
//
//  Created by Cihan TASKIN on 29/05/2015.
//  Copyright (c) 2015 Orca Business Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Item : NSManagedObject

@property (nonatomic, retain) NSString * brandName;
@property (nonatomic, retain) NSString * countryName;
@property (nonatomic, retain) NSNumber * discounted;
@property (nonatomic, retain) NSString * itemCategoryCode;
@property (nonatomic, retain) NSString * itemNo;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * packSize;
@property (nonatomic, retain) NSData * photoData;
@property (nonatomic, retain) NSString * productGroupCode;
@property (nonatomic, retain) NSNumber * quantity;
@property (nonatomic, retain) NSString * salesUnit;
@property (nonatomic, retain) NSString * searchDescription;
@property (nonatomic, retain) NSNumber * stockQty;
@property (nonatomic, retain) NSDecimalNumber * unitPrice;
@property (nonatomic, retain) NSString * unitSize;
@property (nonatomic, retain) NSNumber * vat;
@property (nonatomic, retain) NSNumber * kgInBarcode;

@end
